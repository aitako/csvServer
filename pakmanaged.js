var global = Function("return this;")();
/*jshint strict:true node:true es5:true onevar:true laxcomma:true laxbreak:true eqeqeq:true immed:true latedef:true*/
(function () {
  "use strict";

  var oldRequire = require
    , modules = {}
    ;

  function newRequire(modulename) {
    var err
      , mod
      , metamod
      ;

    try {
      mod = oldRequire(modulename);
    } catch(e) {
      err = e;
    }

    if (mod) {
      return mod;
    }

    metamod = modules[modulename];
    
    if (metamod) {
      mod = metamod();
      return mod;
    }

    // make it possible to require 'process', etc
    mod = global[modulename];

    if (mod) {
      return mod;
    }

    console.error(modulename);
    throw err;
  }

  function provide(modulename, factory) {
    var modReal
      ;

    function metamod() {
      if (modReal) {
        return modReal;
      }

      if (!factory.__pakmanager_factory__) {
        modReal = factory;
        return factory;
      }

      if (factory.__factoryIsResolving) {
        console.error('Your circular dependencies are too powerful!');
        return factory.__moduleExports;
      }

      factory.__factoryIsResolving = true;
      factory.__moduleExports = {};
      modReal = factory(factory.__moduleExports);
      factory.__factoryIsResolving = false;

      return modReal;
    }

    modules[modulename] = metamod;
    // somewhat of a dirty hack since I don't have a plug for loading the "main" module otherwise
    modules['pakmanager.main'] = metamod;
  }

  require = newRequire;
  global.require = newRequire;
  global.provide = provide;
}());

// pakmanager:core-util-is
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // NOTE: These type checking functions intentionally don't use `instanceof`
    // because it is fragile and can be easily faked with `Object.create()`.
    function isArray(ar) {
      return Array.isArray(ar);
    }
    exports.isArray = isArray;
    
    function isBoolean(arg) {
      return typeof arg === 'boolean';
    }
    exports.isBoolean = isBoolean;
    
    function isNull(arg) {
      return arg === null;
    }
    exports.isNull = isNull;
    
    function isNullOrUndefined(arg) {
      return arg == null;
    }
    exports.isNullOrUndefined = isNullOrUndefined;
    
    function isNumber(arg) {
      return typeof arg === 'number';
    }
    exports.isNumber = isNumber;
    
    function isString(arg) {
      return typeof arg === 'string';
    }
    exports.isString = isString;
    
    function isSymbol(arg) {
      return typeof arg === 'symbol';
    }
    exports.isSymbol = isSymbol;
    
    function isUndefined(arg) {
      return arg === void 0;
    }
    exports.isUndefined = isUndefined;
    
    function isRegExp(re) {
      return isObject(re) && objectToString(re) === '[object RegExp]';
    }
    exports.isRegExp = isRegExp;
    
    function isObject(arg) {
      return typeof arg === 'object' && arg !== null;
    }
    exports.isObject = isObject;
    
    function isDate(d) {
      return isObject(d) && objectToString(d) === '[object Date]';
    }
    exports.isDate = isDate;
    
    function isError(e) {
      return isObject(e) &&
          (objectToString(e) === '[object Error]' || e instanceof Error);
    }
    exports.isError = isError;
    
    function isFunction(arg) {
      return typeof arg === 'function';
    }
    exports.isFunction = isFunction;
    
    function isPrimitive(arg) {
      return arg === null ||
             typeof arg === 'boolean' ||
             typeof arg === 'number' ||
             typeof arg === 'string' ||
             typeof arg === 'symbol' ||  // ES6 symbol
             typeof arg === 'undefined';
    }
    exports.isPrimitive = isPrimitive;
    
    function isBuffer(arg) {
      return Buffer.isBuffer(arg);
    }
    exports.isBuffer = isBuffer;
    
    function objectToString(o) {
      return Object.prototype.toString.call(o);
    }
  provide("core-util-is", module.exports);
}(global));

// pakmanager:isarray
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = Array.isArray || function (arr) {
      return Object.prototype.toString.call(arr) == '[object Array]';
    };
    
  provide("isarray", module.exports);
}(global));

// pakmanager:inherits
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = require('util').inherits
    
  provide("inherits", module.exports);
}(global));

// pakmanager:debug
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var tty = require('tty');
    
    /**
     * Expose `debug()` as the module.
     */
    
    module.exports = debug;
    
    /**
     * Enabled debuggers.
     */
    
    var names = []
      , skips = [];
    
    (process.env.DEBUG || '')
      .split(/[\s,]+/)
      .forEach(function(name){
        name = name.replace('*', '.*?');
        if (name[0] === '-') {
          skips.push(new RegExp('^' + name.substr(1) + '$'));
        } else {
          names.push(new RegExp('^' + name + '$'));
        }
      });
    
    /**
     * Colors.
     */
    
    var colors = [6, 2, 3, 4, 5, 1];
    
    /**
     * Previous debug() call.
     */
    
    var prev = {};
    
    /**
     * Previously assigned color.
     */
    
    var prevColor = 0;
    
    /**
     * Is stdout a TTY? Colored output is disabled when `true`.
     */
    
    var isatty = tty.isatty(2);
    
    /**
     * Select a color.
     *
     * @return {Number}
     * @api private
     */
    
    function color() {
      return colors[prevColor++ % colors.length];
    }
    
    /**
     * Humanize the given `ms`.
     *
     * @param {Number} m
     * @return {String}
     * @api private
     */
    
    function humanize(ms) {
      var sec = 1000
        , min = 60 * 1000
        , hour = 60 * min;
    
      if (ms >= hour) return (ms / hour).toFixed(1) + 'h';
      if (ms >= min) return (ms / min).toFixed(1) + 'm';
      if (ms >= sec) return (ms / sec | 0) + 's';
      return ms + 'ms';
    }
    
    /**
     * Create a debugger with the given `name`.
     *
     * @param {String} name
     * @return {Type}
     * @api public
     */
    
    function debug(name) {
      function disabled(){}
      disabled.enabled = false;
    
      var match = skips.some(function(re){
        return re.test(name);
      });
    
      if (match) return disabled;
    
      match = names.some(function(re){
        return re.test(name);
      });
    
      if (!match) return disabled;
      var c = color();
    
      function colored(fmt) {
        fmt = coerce(fmt);
    
        var curr = new Date;
        var ms = curr - (prev[name] || curr);
        prev[name] = curr;
    
        fmt = '  \u001b[9' + c + 'm' + name + ' '
          + '\u001b[3' + c + 'm\u001b[90m'
          + fmt + '\u001b[3' + c + 'm'
          + ' +' + humanize(ms) + '\u001b[0m';
    
        console.error.apply(this, arguments);
      }
    
      function plain(fmt) {
        fmt = coerce(fmt);
    
        fmt = new Date().toUTCString()
          + ' ' + name + ' ' + fmt;
        console.error.apply(this, arguments);
      }
    
      colored.enabled = plain.enabled = true;
    
      return isatty || process.env.DEBUG_COLORS
        ? colored
        : plain;
    }
    
    /**
     * Coerce `val`.
     */
    
    function coerce(val) {
      if (val instanceof Error) return val.stack || val.message;
      return val;
    }
    
  provide("debug", module.exports);
}(global));

// pakmanager:mime
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var path = require('path');
    var fs = require('fs');
    
    function Mime() {
      // Map of extension -> mime type
      this.types = Object.create(null);
    
      // Map of mime type -> extension
      this.extensions = Object.create(null);
    }
    
    /**
     * Define mimetype -> extension mappings.  Each key is a mime-type that maps
     * to an array of extensions associated with the type.  The first extension is
     * used as the default extension for the type.
     *
     * e.g. mime.define({'audio/ogg', ['oga', 'ogg', 'spx']});
     *
     * @param map (Object) type definitions
     */
    Mime.prototype.define = function (map) {
      for (var type in map) {
        var exts = map[type];
    
        for (var i = 0; i < exts.length; i++) {
          if (process.env.DEBUG_MIME && this.types[exts]) {
            console.warn(this._loading.replace(/.*\//, ''), 'changes "' + exts[i] + '" extension type from ' +
              this.types[exts] + ' to ' + type);
          }
    
          this.types[exts[i]] = type;
        }
    
        // Default extension is the first one we encounter
        if (!this.extensions[type]) {
          this.extensions[type] = exts[0];
        }
      }
    };
    
    /**
     * Load an Apache2-style ".types" file
     *
     * This may be called multiple times (it's expected).  Where files declare
     * overlapping types/extensions, the last file wins.
     *
     * @param file (String) path of file to load.
     */
    Mime.prototype.load = function(file) {
    
      this._loading = file;
      // Read file and split into lines
      var map = {},
          content = fs.readFileSync(file, 'ascii'),
          lines = content.split(/[\r\n]+/);
    
      lines.forEach(function(line) {
        // Clean up whitespace/comments, and split into fields
        var fields = line.replace(/\s*#.*|^\s*|\s*$/g, '').split(/\s+/);
        map[fields.shift()] = fields;
      });
    
      this.define(map);
    
      this._loading = null;
    };
    
    /**
     * Lookup a mime type based on extension
     */
    Mime.prototype.lookup = function(path, fallback) {
      var ext = path.replace(/.*[\.\/\\]/, '').toLowerCase();
    
      return this.types[ext] || fallback || this.default_type;
    };
    
    /**
     * Return file extension associated with a mime type
     */
    Mime.prototype.extension = function(mimeType) {
      var type = mimeType.match(/^\s*([^;\s]*)(?:;|\s|$)/)[1].toLowerCase();
      return this.extensions[type];
    };
    
    // Default instance
    var mime = new Mime();
    
    // Load local copy of
    // http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
    mime.load(path.join(__dirname, 'types/mime.types'));
    
    // Load additional types from node.js community
    mime.load(path.join(__dirname, 'types/node.types'));
    
    // Default type
    mime.default_type = mime.lookup('bin');
    
    //
    // Additional API specific to the default instance
    //
    
    mime.Mime = Mime;
    
    /**
     * Lookup a charset based on mime type.
     */
    mime.charsets = {
      lookup: function(mimeType, fallback) {
        // Assume text types are utf8
        return (/^text\//).test(mimeType) ? 'UTF-8' : fallback;
      }
    };
    
    module.exports = mime;
    
  provide("mime", module.exports);
}(global));

// pakmanager:fresh
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Expose `fresh()`.
     */
    
    module.exports = fresh;
    
    /**
     * Check freshness of `req` and `res` headers.
     *
     * When the cache is "fresh" __true__ is returned,
     * otherwise __false__ is returned to indicate that
     * the cache is now stale.
     *
     * @param {Object} req
     * @param {Object} res
     * @return {Boolean}
     * @api public
     */
    
    function fresh(req, res) {
      // defaults
      var etagMatches = true;
      var notModified = true;
    
      // fields
      var modifiedSince = req['if-modified-since'];
      var noneMatch = req['if-none-match'];
      var lastModified = res['last-modified'];
      var etag = res['etag'];
      var cc = req['cache-control'];
    
      // unconditional request
      if (!modifiedSince && !noneMatch) return false;
    
      // check for no-cache cache request directive
      if (cc && cc.indexOf('no-cache') !== -1) return false;  
    
      // parse if-none-match
      if (noneMatch) noneMatch = noneMatch.split(/ *, */);
    
      // if-none-match
      if (noneMatch) etagMatches = ~noneMatch.indexOf(etag) || '*' == noneMatch[0];
    
      // if-modified-since
      if (modifiedSince) {
        modifiedSince = new Date(modifiedSince);
        lastModified = new Date(lastModified);
        notModified = lastModified <= modifiedSince;
      }
    
      return !! (etagMatches && notModified);
    }
  provide("fresh", module.exports);
}(global));

// pakmanager:range-parser
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Parse "Range" header `str` relative to the given file `size`.
     *
     * @param {Number} size
     * @param {String} str
     * @return {Array}
     * @api public
     */
    
    module.exports = function(size, str){
      var valid = true;
      var i = str.indexOf('=');
    
      if (-1 == i) return -2;
    
      var arr = str.slice(i + 1).split(',').map(function(range){
        var range = range.split('-')
          , start = parseInt(range[0], 10)
          , end = parseInt(range[1], 10);
    
        // -nnn
        if (isNaN(start)) {
          start = size - end;
          end = size - 1;
        // nnn-
        } else if (isNaN(end)) {
          end = size - 1;
        }
    
        // limit last-byte-pos to current length
        if (end > size - 1) end = size - 1;
    
        // invalid
        if (isNaN(start)
          || isNaN(end)
          || start > end
          || start < 0) valid = false;
    
        return {
          start: start,
          end: end
        };
      });
    
      arr.type = str.slice(0, i);
    
      return valid ? arr : -1;
    };
  provide("range-parser", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_readable
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    module.exports = Readable;
    
    /*<replacement>*/
    var isArray = require('isarray');
    /*</replacement>*/
    
    
    /*<replacement>*/
    var Buffer = require('buffer').Buffer;
    /*</replacement>*/
    
    Readable.ReadableState = ReadableState;
    
    var EE = require('events').EventEmitter;
    
    /*<replacement>*/
    if (!EE.listenerCount) EE.listenerCount = function(emitter, type) {
      return emitter.listeners(type).length;
    };
    /*</replacement>*/
    
    var Stream = require('stream');
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    var StringDecoder;
    
    util.inherits(Readable, Stream);
    
    function ReadableState(options, stream) {
      options = options || {};
    
      // the point at which it stops calling _read() to fill the buffer
      // Note: 0 is a valid value, means "don't call _read preemptively ever"
      var hwm = options.highWaterMark;
      this.highWaterMark = (hwm || hwm === 0) ? hwm : 16 * 1024;
    
      // cast to ints.
      this.highWaterMark = ~~this.highWaterMark;
    
      this.buffer = [];
      this.length = 0;
      this.pipes = null;
      this.pipesCount = 0;
      this.flowing = false;
      this.ended = false;
      this.endEmitted = false;
      this.reading = false;
    
      // In streams that never have any data, and do push(null) right away,
      // the consumer can miss the 'end' event if they do some I/O before
      // consuming the stream.  So, we don't emit('end') until some reading
      // happens.
      this.calledRead = false;
    
      // a flag to be able to tell if the onwrite cb is called immediately,
      // or on a later tick.  We set this to true at first, becuase any
      // actions that shouldn't happen until "later" should generally also
      // not happen before the first write call.
      this.sync = true;
    
      // whenever we return null, then we set a flag to say
      // that we're awaiting a 'readable' event emission.
      this.needReadable = false;
      this.emittedReadable = false;
      this.readableListening = false;
    
    
      // object stream flag. Used to make read(n) ignore n and to
      // make all the buffer merging and length checks go away
      this.objectMode = !!options.objectMode;
    
      // Crypto is kind of old and crusty.  Historically, its default string
      // encoding is 'binary' so we have to make this configurable.
      // Everything else in the universe uses 'utf8', though.
      this.defaultEncoding = options.defaultEncoding || 'utf8';
    
      // when piping, we only care about 'readable' events that happen
      // after read()ing all the bytes and not getting any pushback.
      this.ranOut = false;
    
      // the number of writers that are awaiting a drain event in .pipe()s
      this.awaitDrain = 0;
    
      // if true, a maybeReadMore has been scheduled
      this.readingMore = false;
    
      this.decoder = null;
      this.encoding = null;
      if (options.encoding) {
        if (!StringDecoder)
          StringDecoder = require('string_decoder/').StringDecoder;
        this.decoder = new StringDecoder(options.encoding);
        this.encoding = options.encoding;
      }
    }
    
    function Readable(options) {
      if (!(this instanceof Readable))
        return new Readable(options);
    
      this._readableState = new ReadableState(options, this);
    
      // legacy
      this.readable = true;
    
      Stream.call(this);
    }
    
    // Manually shove something into the read() buffer.
    // This returns true if the highWaterMark has not been hit yet,
    // similar to how Writable.write() returns true if you should
    // write() some more.
    Readable.prototype.push = function(chunk, encoding) {
      var state = this._readableState;
    
      if (typeof chunk === 'string' && !state.objectMode) {
        encoding = encoding || state.defaultEncoding;
        if (encoding !== state.encoding) {
          chunk = new Buffer(chunk, encoding);
          encoding = '';
        }
      }
    
      return readableAddChunk(this, state, chunk, encoding, false);
    };
    
    // Unshift should *always* be something directly out of read()
    Readable.prototype.unshift = function(chunk) {
      var state = this._readableState;
      return readableAddChunk(this, state, chunk, '', true);
    };
    
    function readableAddChunk(stream, state, chunk, encoding, addToFront) {
      var er = chunkInvalid(state, chunk);
      if (er) {
        stream.emit('error', er);
      } else if (chunk === null || chunk === undefined) {
        state.reading = false;
        if (!state.ended)
          onEofChunk(stream, state);
      } else if (state.objectMode || chunk && chunk.length > 0) {
        if (state.ended && !addToFront) {
          var e = new Error('stream.push() after EOF');
          stream.emit('error', e);
        } else if (state.endEmitted && addToFront) {
          var e = new Error('stream.unshift() after end event');
          stream.emit('error', e);
        } else {
          if (state.decoder && !addToFront && !encoding)
            chunk = state.decoder.write(chunk);
    
          // update the buffer info.
          state.length += state.objectMode ? 1 : chunk.length;
          if (addToFront) {
            state.buffer.unshift(chunk);
          } else {
            state.reading = false;
            state.buffer.push(chunk);
          }
    
          if (state.needReadable)
            emitReadable(stream);
    
          maybeReadMore(stream, state);
        }
      } else if (!addToFront) {
        state.reading = false;
      }
    
      return needMoreData(state);
    }
    
    
    
    // if it's past the high water mark, we can push in some more.
    // Also, if we have no data yet, we can stand some
    // more bytes.  This is to work around cases where hwm=0,
    // such as the repl.  Also, if the push() triggered a
    // readable event, and the user called read(largeNumber) such that
    // needReadable was set, then we ought to push more, so that another
    // 'readable' event will be triggered.
    function needMoreData(state) {
      return !state.ended &&
             (state.needReadable ||
              state.length < state.highWaterMark ||
              state.length === 0);
    }
    
    // backwards compatibility.
    Readable.prototype.setEncoding = function(enc) {
      if (!StringDecoder)
        StringDecoder = require('string_decoder/').StringDecoder;
      this._readableState.decoder = new StringDecoder(enc);
      this._readableState.encoding = enc;
    };
    
    // Don't raise the hwm > 128MB
    var MAX_HWM = 0x800000;
    function roundUpToNextPowerOf2(n) {
      if (n >= MAX_HWM) {
        n = MAX_HWM;
      } else {
        // Get the next highest power of 2
        n--;
        for (var p = 1; p < 32; p <<= 1) n |= n >> p;
        n++;
      }
      return n;
    }
    
    function howMuchToRead(n, state) {
      if (state.length === 0 && state.ended)
        return 0;
    
      if (state.objectMode)
        return n === 0 ? 0 : 1;
    
      if (isNaN(n) || n === null) {
        // only flow one buffer at a time
        if (state.flowing && state.buffer.length)
          return state.buffer[0].length;
        else
          return state.length;
      }
    
      if (n <= 0)
        return 0;
    
      // If we're asking for more than the target buffer level,
      // then raise the water mark.  Bump up to the next highest
      // power of 2, to prevent increasing it excessively in tiny
      // amounts.
      if (n > state.highWaterMark)
        state.highWaterMark = roundUpToNextPowerOf2(n);
    
      // don't have that much.  return null, unless we've ended.
      if (n > state.length) {
        if (!state.ended) {
          state.needReadable = true;
          return 0;
        } else
          return state.length;
      }
    
      return n;
    }
    
    // you can override either this method, or the async _read(n) below.
    Readable.prototype.read = function(n) {
      var state = this._readableState;
      state.calledRead = true;
      var nOrig = n;
    
      if (typeof n !== 'number' || n > 0)
        state.emittedReadable = false;
    
      // if we're doing read(0) to trigger a readable event, but we
      // already have a bunch of data in the buffer, then just trigger
      // the 'readable' event and move on.
      if (n === 0 &&
          state.needReadable &&
          (state.length >= state.highWaterMark || state.ended)) {
        emitReadable(this);
        return null;
      }
    
      n = howMuchToRead(n, state);
    
      // if we've ended, and we're now clear, then finish it up.
      if (n === 0 && state.ended) {
        if (state.length === 0)
          endReadable(this);
        return null;
      }
    
      // All the actual chunk generation logic needs to be
      // *below* the call to _read.  The reason is that in certain
      // synthetic stream cases, such as passthrough streams, _read
      // may be a completely synchronous operation which may change
      // the state of the read buffer, providing enough data when
      // before there was *not* enough.
      //
      // So, the steps are:
      // 1. Figure out what the state of things will be after we do
      // a read from the buffer.
      //
      // 2. If that resulting state will trigger a _read, then call _read.
      // Note that this may be asynchronous, or synchronous.  Yes, it is
      // deeply ugly to write APIs this way, but that still doesn't mean
      // that the Readable class should behave improperly, as streams are
      // designed to be sync/async agnostic.
      // Take note if the _read call is sync or async (ie, if the read call
      // has returned yet), so that we know whether or not it's safe to emit
      // 'readable' etc.
      //
      // 3. Actually pull the requested chunks out of the buffer and return.
    
      // if we need a readable event, then we need to do some reading.
      var doRead = state.needReadable;
    
      // if we currently have less than the highWaterMark, then also read some
      if (state.length - n <= state.highWaterMark)
        doRead = true;
    
      // however, if we've ended, then there's no point, and if we're already
      // reading, then it's unnecessary.
      if (state.ended || state.reading)
        doRead = false;
    
      if (doRead) {
        state.reading = true;
        state.sync = true;
        // if the length is currently zero, then we *need* a readable event.
        if (state.length === 0)
          state.needReadable = true;
        // call internal read method
        this._read(state.highWaterMark);
        state.sync = false;
      }
    
      // If _read called its callback synchronously, then `reading`
      // will be false, and we need to re-evaluate how much data we
      // can return to the user.
      if (doRead && !state.reading)
        n = howMuchToRead(nOrig, state);
    
      var ret;
      if (n > 0)
        ret = fromList(n, state);
      else
        ret = null;
    
      if (ret === null) {
        state.needReadable = true;
        n = 0;
      }
    
      state.length -= n;
    
      // If we have nothing in the buffer, then we want to know
      // as soon as we *do* get something into the buffer.
      if (state.length === 0 && !state.ended)
        state.needReadable = true;
    
      // If we happened to read() exactly the remaining amount in the
      // buffer, and the EOF has been seen at this point, then make sure
      // that we emit 'end' on the very next tick.
      if (state.ended && !state.endEmitted && state.length === 0)
        endReadable(this);
    
      return ret;
    };
    
    function chunkInvalid(state, chunk) {
      var er = null;
      if (!Buffer.isBuffer(chunk) &&
          'string' !== typeof chunk &&
          chunk !== null &&
          chunk !== undefined &&
          !state.objectMode &&
          !er) {
        er = new TypeError('Invalid non-string/buffer chunk');
      }
      return er;
    }
    
    
    function onEofChunk(stream, state) {
      if (state.decoder && !state.ended) {
        var chunk = state.decoder.end();
        if (chunk && chunk.length) {
          state.buffer.push(chunk);
          state.length += state.objectMode ? 1 : chunk.length;
        }
      }
      state.ended = true;
    
      // if we've ended and we have some data left, then emit
      // 'readable' now to make sure it gets picked up.
      if (state.length > 0)
        emitReadable(stream);
      else
        endReadable(stream);
    }
    
    // Don't emit readable right away in sync mode, because this can trigger
    // another read() call => stack overflow.  This way, it might trigger
    // a nextTick recursion warning, but that's not so bad.
    function emitReadable(stream) {
      var state = stream._readableState;
      state.needReadable = false;
      if (state.emittedReadable)
        return;
    
      state.emittedReadable = true;
      if (state.sync)
        process.nextTick(function() {
          emitReadable_(stream);
        });
      else
        emitReadable_(stream);
    }
    
    function emitReadable_(stream) {
      stream.emit('readable');
    }
    
    
    // at this point, the user has presumably seen the 'readable' event,
    // and called read() to consume some data.  that may have triggered
    // in turn another _read(n) call, in which case reading = true if
    // it's in progress.
    // However, if we're not ended, or reading, and the length < hwm,
    // then go ahead and try to read some more preemptively.
    function maybeReadMore(stream, state) {
      if (!state.readingMore) {
        state.readingMore = true;
        process.nextTick(function() {
          maybeReadMore_(stream, state);
        });
      }
    }
    
    function maybeReadMore_(stream, state) {
      var len = state.length;
      while (!state.reading && !state.flowing && !state.ended &&
             state.length < state.highWaterMark) {
        stream.read(0);
        if (len === state.length)
          // didn't get any data, stop spinning.
          break;
        else
          len = state.length;
      }
      state.readingMore = false;
    }
    
    // abstract method.  to be overridden in specific implementation classes.
    // call cb(er, data) where data is <= n in length.
    // for virtual (non-string, non-buffer) streams, "length" is somewhat
    // arbitrary, and perhaps not very meaningful.
    Readable.prototype._read = function(n) {
      this.emit('error', new Error('not implemented'));
    };
    
    Readable.prototype.pipe = function(dest, pipeOpts) {
      var src = this;
      var state = this._readableState;
    
      switch (state.pipesCount) {
        case 0:
          state.pipes = dest;
          break;
        case 1:
          state.pipes = [state.pipes, dest];
          break;
        default:
          state.pipes.push(dest);
          break;
      }
      state.pipesCount += 1;
    
      var doEnd = (!pipeOpts || pipeOpts.end !== false) &&
                  dest !== process.stdout &&
                  dest !== process.stderr;
    
      var endFn = doEnd ? onend : cleanup;
      if (state.endEmitted)
        process.nextTick(endFn);
      else
        src.once('end', endFn);
    
      dest.on('unpipe', onunpipe);
      function onunpipe(readable) {
        if (readable !== src) return;
        cleanup();
      }
    
      function onend() {
        dest.end();
      }
    
      // when the dest drains, it reduces the awaitDrain counter
      // on the source.  This would be more elegant with a .once()
      // handler in flow(), but adding and removing repeatedly is
      // too slow.
      var ondrain = pipeOnDrain(src);
      dest.on('drain', ondrain);
    
      function cleanup() {
        // cleanup event handlers once the pipe is broken
        dest.removeListener('close', onclose);
        dest.removeListener('finish', onfinish);
        dest.removeListener('drain', ondrain);
        dest.removeListener('error', onerror);
        dest.removeListener('unpipe', onunpipe);
        src.removeListener('end', onend);
        src.removeListener('end', cleanup);
    
        // if the reader is waiting for a drain event from this
        // specific writer, then it would cause it to never start
        // flowing again.
        // So, if this is awaiting a drain, then we just call it now.
        // If we don't know, then assume that we are waiting for one.
        if (!dest._writableState || dest._writableState.needDrain)
          ondrain();
      }
    
      // if the dest has an error, then stop piping into it.
      // however, don't suppress the throwing behavior for this.
      function onerror(er) {
        unpipe();
        dest.removeListener('error', onerror);
        if (EE.listenerCount(dest, 'error') === 0)
          dest.emit('error', er);
      }
      // This is a brutally ugly hack to make sure that our error handler
      // is attached before any userland ones.  NEVER DO THIS.
      if (!dest._events || !dest._events.error)
        dest.on('error', onerror);
      else if (isArray(dest._events.error))
        dest._events.error.unshift(onerror);
      else
        dest._events.error = [onerror, dest._events.error];
    
    
    
      // Both close and finish should trigger unpipe, but only once.
      function onclose() {
        dest.removeListener('finish', onfinish);
        unpipe();
      }
      dest.once('close', onclose);
      function onfinish() {
        dest.removeListener('close', onclose);
        unpipe();
      }
      dest.once('finish', onfinish);
    
      function unpipe() {
        src.unpipe(dest);
      }
    
      // tell the dest that it's being piped to
      dest.emit('pipe', src);
    
      // start the flow if it hasn't been started already.
      if (!state.flowing) {
        // the handler that waits for readable events after all
        // the data gets sucked out in flow.
        // This would be easier to follow with a .once() handler
        // in flow(), but that is too slow.
        this.on('readable', pipeOnReadable);
    
        state.flowing = true;
        process.nextTick(function() {
          flow(src);
        });
      }
    
      return dest;
    };
    
    function pipeOnDrain(src) {
      return function() {
        var dest = this;
        var state = src._readableState;
        state.awaitDrain--;
        if (state.awaitDrain === 0)
          flow(src);
      };
    }
    
    function flow(src) {
      var state = src._readableState;
      var chunk;
      state.awaitDrain = 0;
    
      function write(dest, i, list) {
        var written = dest.write(chunk);
        if (false === written) {
          state.awaitDrain++;
        }
      }
    
      while (state.pipesCount && null !== (chunk = src.read())) {
    
        if (state.pipesCount === 1)
          write(state.pipes, 0, null);
        else
          forEach(state.pipes, write);
    
        src.emit('data', chunk);
    
        // if anyone needs a drain, then we have to wait for that.
        if (state.awaitDrain > 0)
          return;
      }
    
      // if every destination was unpiped, either before entering this
      // function, or in the while loop, then stop flowing.
      //
      // NB: This is a pretty rare edge case.
      if (state.pipesCount === 0) {
        state.flowing = false;
    
        // if there were data event listeners added, then switch to old mode.
        if (EE.listenerCount(src, 'data') > 0)
          emitDataEvents(src);
        return;
      }
    
      // at this point, no one needed a drain, so we just ran out of data
      // on the next readable event, start it over again.
      state.ranOut = true;
    }
    
    function pipeOnReadable() {
      if (this._readableState.ranOut) {
        this._readableState.ranOut = false;
        flow(this);
      }
    }
    
    
    Readable.prototype.unpipe = function(dest) {
      var state = this._readableState;
    
      // if we're not piping anywhere, then do nothing.
      if (state.pipesCount === 0)
        return this;
    
      // just one destination.  most common case.
      if (state.pipesCount === 1) {
        // passed in one, but it's not the right one.
        if (dest && dest !== state.pipes)
          return this;
    
        if (!dest)
          dest = state.pipes;
    
        // got a match.
        state.pipes = null;
        state.pipesCount = 0;
        this.removeListener('readable', pipeOnReadable);
        state.flowing = false;
        if (dest)
          dest.emit('unpipe', this);
        return this;
      }
    
      // slow case. multiple pipe destinations.
    
      if (!dest) {
        // remove all.
        var dests = state.pipes;
        var len = state.pipesCount;
        state.pipes = null;
        state.pipesCount = 0;
        this.removeListener('readable', pipeOnReadable);
        state.flowing = false;
    
        for (var i = 0; i < len; i++)
          dests[i].emit('unpipe', this);
        return this;
      }
    
      // try to find the right one.
      var i = indexOf(state.pipes, dest);
      if (i === -1)
        return this;
    
      state.pipes.splice(i, 1);
      state.pipesCount -= 1;
      if (state.pipesCount === 1)
        state.pipes = state.pipes[0];
    
      dest.emit('unpipe', this);
    
      return this;
    };
    
    // set up data events if they are asked for
    // Ensure readable listeners eventually get something
    Readable.prototype.on = function(ev, fn) {
      var res = Stream.prototype.on.call(this, ev, fn);
    
      if (ev === 'data' && !this._readableState.flowing)
        emitDataEvents(this);
    
      if (ev === 'readable' && this.readable) {
        var state = this._readableState;
        if (!state.readableListening) {
          state.readableListening = true;
          state.emittedReadable = false;
          state.needReadable = true;
          if (!state.reading) {
            this.read(0);
          } else if (state.length) {
            emitReadable(this, state);
          }
        }
      }
    
      return res;
    };
    Readable.prototype.addListener = Readable.prototype.on;
    
    // pause() and resume() are remnants of the legacy readable stream API
    // If the user uses them, then switch into old mode.
    Readable.prototype.resume = function() {
      emitDataEvents(this);
      this.read(0);
      this.emit('resume');
    };
    
    Readable.prototype.pause = function() {
      emitDataEvents(this, true);
      this.emit('pause');
    };
    
    function emitDataEvents(stream, startPaused) {
      var state = stream._readableState;
    
      if (state.flowing) {
        // https://github.com/isaacs/readable-stream/issues/16
        throw new Error('Cannot switch to old mode now.');
      }
    
      var paused = startPaused || false;
      var readable = false;
    
      // convert to an old-style stream.
      stream.readable = true;
      stream.pipe = Stream.prototype.pipe;
      stream.on = stream.addListener = Stream.prototype.on;
    
      stream.on('readable', function() {
        readable = true;
    
        var c;
        while (!paused && (null !== (c = stream.read())))
          stream.emit('data', c);
    
        if (c === null) {
          readable = false;
          stream._readableState.needReadable = true;
        }
      });
    
      stream.pause = function() {
        paused = true;
        this.emit('pause');
      };
    
      stream.resume = function() {
        paused = false;
        if (readable)
          process.nextTick(function() {
            stream.emit('readable');
          });
        else
          this.read(0);
        this.emit('resume');
      };
    
      // now make it start, just in case it hadn't already.
      stream.emit('readable');
    }
    
    // wrap an old-style stream as the async data source.
    // This is *not* part of the readable stream interface.
    // It is an ugly unfortunate mess of history.
    Readable.prototype.wrap = function(stream) {
      var state = this._readableState;
      var paused = false;
    
      var self = this;
      stream.on('end', function() {
        if (state.decoder && !state.ended) {
          var chunk = state.decoder.end();
          if (chunk && chunk.length)
            self.push(chunk);
        }
    
        self.push(null);
      });
    
      stream.on('data', function(chunk) {
        if (state.decoder)
          chunk = state.decoder.write(chunk);
        if (!chunk || !state.objectMode && !chunk.length)
          return;
    
        var ret = self.push(chunk);
        if (!ret) {
          paused = true;
          stream.pause();
        }
      });
    
      // proxy all the other methods.
      // important when wrapping filters and duplexes.
      for (var i in stream) {
        if (typeof stream[i] === 'function' &&
            typeof this[i] === 'undefined') {
          this[i] = function(method) { return function() {
            return stream[method].apply(stream, arguments);
          }}(i);
        }
      }
    
      // proxy certain important events.
      var events = ['error', 'close', 'destroy', 'pause', 'resume'];
      forEach(events, function(ev) {
        stream.on(ev, self.emit.bind(self, ev));
      });
    
      // when we try to consume some more bytes, simply unpause the
      // underlying stream.
      self._read = function(n) {
        if (paused) {
          paused = false;
          stream.resume();
        }
      };
    
      return self;
    };
    
    
    
    // exposed for testing purposes only.
    Readable._fromList = fromList;
    
    // Pluck off n bytes from an array of buffers.
    // Length is the combined lengths of all the buffers in the list.
    function fromList(n, state) {
      var list = state.buffer;
      var length = state.length;
      var stringMode = !!state.decoder;
      var objectMode = !!state.objectMode;
      var ret;
    
      // nothing in the list, definitely empty.
      if (list.length === 0)
        return null;
    
      if (length === 0)
        ret = null;
      else if (objectMode)
        ret = list.shift();
      else if (!n || n >= length) {
        // read it all, truncate the array.
        if (stringMode)
          ret = list.join('');
        else
          ret = Buffer.concat(list, length);
        list.length = 0;
      } else {
        // read just some of it.
        if (n < list[0].length) {
          // just take a part of the first list item.
          // slice is the same for buffers and strings.
          var buf = list[0];
          ret = buf.slice(0, n);
          list[0] = buf.slice(n);
        } else if (n === list[0].length) {
          // first list is a perfect match
          ret = list.shift();
        } else {
          // complex case.
          // we have enough to cover it, but it spans past the first buffer.
          if (stringMode)
            ret = '';
          else
            ret = new Buffer(n);
    
          var c = 0;
          for (var i = 0, l = list.length; i < l && c < n; i++) {
            var buf = list[0];
            var cpy = Math.min(n - c, buf.length);
    
            if (stringMode)
              ret += buf.slice(0, cpy);
            else
              buf.copy(ret, c, 0, cpy);
    
            if (cpy < buf.length)
              list[0] = buf.slice(cpy);
            else
              list.shift();
    
            c += cpy;
          }
        }
      }
    
      return ret;
    }
    
    function endReadable(stream) {
      var state = stream._readableState;
    
      // If we get here before consuming all the bytes, then that is a
      // bug in node.  Should never happen.
      if (state.length > 0)
        throw new Error('endReadable called on non-empty stream');
    
      if (!state.endEmitted && state.calledRead) {
        state.ended = true;
        process.nextTick(function() {
          // Check that we didn't get one last unshift.
          if (!state.endEmitted && state.length === 0) {
            state.endEmitted = true;
            stream.readable = false;
            stream.emit('end');
          }
        });
      }
    }
    
    function forEach (xs, f) {
      for (var i = 0, l = xs.length; i < l; i++) {
        f(xs[i], i);
      }
    }
    
    function indexOf (xs, x) {
      for (var i = 0, l = xs.length; i < l; i++) {
        if (xs[i] === x) return i;
      }
      return -1;
    }
    
  provide("readable-stream/lib/_stream_readable", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_writable
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // A bit simpler than readable streams.
    // Implement an async ._write(chunk, cb), and it'll handle all
    // the drain event emission and buffering.
    
    module.exports = Writable;
    
    /*<replacement>*/
    var Buffer = require('buffer').Buffer;
    /*</replacement>*/
    
    Writable.WritableState = WritableState;
    
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    
    var Stream = require('stream');
    
    util.inherits(Writable, Stream);
    
    function WriteReq(chunk, encoding, cb) {
      this.chunk = chunk;
      this.encoding = encoding;
      this.callback = cb;
    }
    
    function WritableState(options, stream) {
      options = options || {};
    
      // the point at which write() starts returning false
      // Note: 0 is a valid value, means that we always return false if
      // the entire buffer is not flushed immediately on write()
      var hwm = options.highWaterMark;
      this.highWaterMark = (hwm || hwm === 0) ? hwm : 16 * 1024;
    
      // object stream flag to indicate whether or not this stream
      // contains buffers or objects.
      this.objectMode = !!options.objectMode;
    
      // cast to ints.
      this.highWaterMark = ~~this.highWaterMark;
    
      this.needDrain = false;
      // at the start of calling end()
      this.ending = false;
      // when end() has been called, and returned
      this.ended = false;
      // when 'finish' is emitted
      this.finished = false;
    
      // should we decode strings into buffers before passing to _write?
      // this is here so that some node-core streams can optimize string
      // handling at a lower level.
      var noDecode = options.decodeStrings === false;
      this.decodeStrings = !noDecode;
    
      // Crypto is kind of old and crusty.  Historically, its default string
      // encoding is 'binary' so we have to make this configurable.
      // Everything else in the universe uses 'utf8', though.
      this.defaultEncoding = options.defaultEncoding || 'utf8';
    
      // not an actual buffer we keep track of, but a measurement
      // of how much we're waiting to get pushed to some underlying
      // socket or file.
      this.length = 0;
    
      // a flag to see when we're in the middle of a write.
      this.writing = false;
    
      // a flag to be able to tell if the onwrite cb is called immediately,
      // or on a later tick.  We set this to true at first, becuase any
      // actions that shouldn't happen until "later" should generally also
      // not happen before the first write call.
      this.sync = true;
    
      // a flag to know if we're processing previously buffered items, which
      // may call the _write() callback in the same tick, so that we don't
      // end up in an overlapped onwrite situation.
      this.bufferProcessing = false;
    
      // the callback that's passed to _write(chunk,cb)
      this.onwrite = function(er) {
        onwrite(stream, er);
      };
    
      // the callback that the user supplies to write(chunk,encoding,cb)
      this.writecb = null;
    
      // the amount that is being written when _write is called.
      this.writelen = 0;
    
      this.buffer = [];
    
      // True if the error was already emitted and should not be thrown again
      this.errorEmitted = false;
    }
    
    function Writable(options) {
      var Duplex =  require('readable-stream/lib/_stream_duplex');
    
      // Writable ctor is applied to Duplexes, though they're not
      // instanceof Writable, they're instanceof Readable.
      if (!(this instanceof Writable) && !(this instanceof Duplex))
        return new Writable(options);
    
      this._writableState = new WritableState(options, this);
    
      // legacy.
      this.writable = true;
    
      Stream.call(this);
    }
    
    // Otherwise people can pipe Writable streams, which is just wrong.
    Writable.prototype.pipe = function() {
      this.emit('error', new Error('Cannot pipe. Not readable.'));
    };
    
    
    function writeAfterEnd(stream, state, cb) {
      var er = new Error('write after end');
      // TODO: defer error events consistently everywhere, not just the cb
      stream.emit('error', er);
      process.nextTick(function() {
        cb(er);
      });
    }
    
    // If we get something that is not a buffer, string, null, or undefined,
    // and we're not in objectMode, then that's an error.
    // Otherwise stream chunks are all considered to be of length=1, and the
    // watermarks determine how many objects to keep in the buffer, rather than
    // how many bytes or characters.
    function validChunk(stream, state, chunk, cb) {
      var valid = true;
      if (!Buffer.isBuffer(chunk) &&
          'string' !== typeof chunk &&
          chunk !== null &&
          chunk !== undefined &&
          !state.objectMode) {
        var er = new TypeError('Invalid non-string/buffer chunk');
        stream.emit('error', er);
        process.nextTick(function() {
          cb(er);
        });
        valid = false;
      }
      return valid;
    }
    
    Writable.prototype.write = function(chunk, encoding, cb) {
      var state = this._writableState;
      var ret = false;
    
      if (typeof encoding === 'function') {
        cb = encoding;
        encoding = null;
      }
    
      if (Buffer.isBuffer(chunk))
        encoding = 'buffer';
      else if (!encoding)
        encoding = state.defaultEncoding;
    
      if (typeof cb !== 'function')
        cb = function() {};
    
      if (state.ended)
        writeAfterEnd(this, state, cb);
      else if (validChunk(this, state, chunk, cb))
        ret = writeOrBuffer(this, state, chunk, encoding, cb);
    
      return ret;
    };
    
    function decodeChunk(state, chunk, encoding) {
      if (!state.objectMode &&
          state.decodeStrings !== false &&
          typeof chunk === 'string') {
        chunk = new Buffer(chunk, encoding);
      }
      return chunk;
    }
    
    // if we're already writing something, then just put this
    // in the queue, and wait our turn.  Otherwise, call _write
    // If we return false, then we need a drain event, so set that flag.
    function writeOrBuffer(stream, state, chunk, encoding, cb) {
      chunk = decodeChunk(state, chunk, encoding);
      if (Buffer.isBuffer(chunk))
        encoding = 'buffer';
      var len = state.objectMode ? 1 : chunk.length;
    
      state.length += len;
    
      var ret = state.length < state.highWaterMark;
      // we must ensure that previous needDrain will not be reset to false.
      if (!ret)
        state.needDrain = true;
    
      if (state.writing)
        state.buffer.push(new WriteReq(chunk, encoding, cb));
      else
        doWrite(stream, state, len, chunk, encoding, cb);
    
      return ret;
    }
    
    function doWrite(stream, state, len, chunk, encoding, cb) {
      state.writelen = len;
      state.writecb = cb;
      state.writing = true;
      state.sync = true;
      stream._write(chunk, encoding, state.onwrite);
      state.sync = false;
    }
    
    function onwriteError(stream, state, sync, er, cb) {
      if (sync)
        process.nextTick(function() {
          cb(er);
        });
      else
        cb(er);
    
      stream._writableState.errorEmitted = true;
      stream.emit('error', er);
    }
    
    function onwriteStateUpdate(state) {
      state.writing = false;
      state.writecb = null;
      state.length -= state.writelen;
      state.writelen = 0;
    }
    
    function onwrite(stream, er) {
      var state = stream._writableState;
      var sync = state.sync;
      var cb = state.writecb;
    
      onwriteStateUpdate(state);
    
      if (er)
        onwriteError(stream, state, sync, er, cb);
      else {
        // Check if we're actually ready to finish, but don't emit yet
        var finished = needFinish(stream, state);
    
        if (!finished && !state.bufferProcessing && state.buffer.length)
          clearBuffer(stream, state);
    
        if (sync) {
          process.nextTick(function() {
            afterWrite(stream, state, finished, cb);
          });
        } else {
          afterWrite(stream, state, finished, cb);
        }
      }
    }
    
    function afterWrite(stream, state, finished, cb) {
      if (!finished)
        onwriteDrain(stream, state);
      cb();
      if (finished)
        finishMaybe(stream, state);
    }
    
    // Must force callback to be called on nextTick, so that we don't
    // emit 'drain' before the write() consumer gets the 'false' return
    // value, and has a chance to attach a 'drain' listener.
    function onwriteDrain(stream, state) {
      if (state.length === 0 && state.needDrain) {
        state.needDrain = false;
        stream.emit('drain');
      }
    }
    
    
    // if there's something in the buffer waiting, then process it
    function clearBuffer(stream, state) {
      state.bufferProcessing = true;
    
      for (var c = 0; c < state.buffer.length; c++) {
        var entry = state.buffer[c];
        var chunk = entry.chunk;
        var encoding = entry.encoding;
        var cb = entry.callback;
        var len = state.objectMode ? 1 : chunk.length;
    
        doWrite(stream, state, len, chunk, encoding, cb);
    
        // if we didn't call the onwrite immediately, then
        // it means that we need to wait until it does.
        // also, that means that the chunk and cb are currently
        // being processed, so move the buffer counter past them.
        if (state.writing) {
          c++;
          break;
        }
      }
    
      state.bufferProcessing = false;
      if (c < state.buffer.length)
        state.buffer = state.buffer.slice(c);
      else
        state.buffer.length = 0;
    }
    
    Writable.prototype._write = function(chunk, encoding, cb) {
      cb(new Error('not implemented'));
    };
    
    Writable.prototype.end = function(chunk, encoding, cb) {
      var state = this._writableState;
    
      if (typeof chunk === 'function') {
        cb = chunk;
        chunk = null;
        encoding = null;
      } else if (typeof encoding === 'function') {
        cb = encoding;
        encoding = null;
      }
    
      if (typeof chunk !== 'undefined' && chunk !== null)
        this.write(chunk, encoding);
    
      // ignore unnecessary end() calls.
      if (!state.ending && !state.finished)
        endWritable(this, state, cb);
    };
    
    
    function needFinish(stream, state) {
      return (state.ending &&
              state.length === 0 &&
              !state.finished &&
              !state.writing);
    }
    
    function finishMaybe(stream, state) {
      var need = needFinish(stream, state);
      if (need) {
        state.finished = true;
        stream.emit('finish');
      }
      return need;
    }
    
    function endWritable(stream, state, cb) {
      state.ending = true;
      finishMaybe(stream, state);
      if (cb) {
        if (state.finished)
          process.nextTick(cb);
        else
          stream.once('finish', cb);
      }
      state.ended = true;
    }
    
  provide("readable-stream/lib/_stream_writable", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_duplex
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // a duplex stream is just a stream that is both readable and writable.
    // Since JS doesn't have multiple prototypal inheritance, this class
    // prototypally inherits from Readable, and then parasitically from
    // Writable.
    
    module.exports = Duplex;
    
    /*<replacement>*/
    var objectKeys = Object.keys || function (obj) {
      var keys = [];
      for (var key in obj) keys.push(key);
      return keys;
    }
    /*</replacement>*/
    
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    var Readable =  require('readable-stream/lib/_stream_readable');
    var Writable =  require('readable-stream/lib/_stream_writable');
    
    util.inherits(Duplex, Readable);
    
    forEach(objectKeys(Writable.prototype), function(method) {
      if (!Duplex.prototype[method])
        Duplex.prototype[method] = Writable.prototype[method];
    });
    
    function Duplex(options) {
      if (!(this instanceof Duplex))
        return new Duplex(options);
    
      Readable.call(this, options);
      Writable.call(this, options);
    
      if (options && options.readable === false)
        this.readable = false;
    
      if (options && options.writable === false)
        this.writable = false;
    
      this.allowHalfOpen = true;
      if (options && options.allowHalfOpen === false)
        this.allowHalfOpen = false;
    
      this.once('end', onend);
    }
    
    // the no-half-open enforcer
    function onend() {
      // if we allow half-open state, or if the writable side ended,
      // then we're ok.
      if (this.allowHalfOpen || this._writableState.ended)
        return;
    
      // no more data can be written.
      // But allow more writes to happen in this tick.
      process.nextTick(this.end.bind(this));
    }
    
    function forEach (xs, f) {
      for (var i = 0, l = xs.length; i < l; i++) {
        f(xs[i], i);
      }
    }
    
  provide("readable-stream/lib/_stream_duplex", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_transform
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    
    // a transform stream is a readable/writable stream where you do
    // something with the data.  Sometimes it's called a "filter",
    // but that's not a great name for it, since that implies a thing where
    // some bits pass through, and others are simply ignored.  (That would
    // be a valid example of a transform, of course.)
    //
    // While the output is causally related to the input, it's not a
    // necessarily symmetric or synchronous transformation.  For example,
    // a zlib stream might take multiple plain-text writes(), and then
    // emit a single compressed chunk some time in the future.
    //
    // Here's how this works:
    //
    // The Transform stream has all the aspects of the readable and writable
    // stream classes.  When you write(chunk), that calls _write(chunk,cb)
    // internally, and returns false if there's a lot of pending writes
    // buffered up.  When you call read(), that calls _read(n) until
    // there's enough pending readable data buffered up.
    //
    // In a transform stream, the written data is placed in a buffer.  When
    // _read(n) is called, it transforms the queued up data, calling the
    // buffered _write cb's as it consumes chunks.  If consuming a single
    // written chunk would result in multiple output chunks, then the first
    // outputted bit calls the readcb, and subsequent chunks just go into
    // the read buffer, and will cause it to emit 'readable' if necessary.
    //
    // This way, back-pressure is actually determined by the reading side,
    // since _read has to be called to start processing a new chunk.  However,
    // a pathological inflate type of transform can cause excessive buffering
    // here.  For example, imagine a stream where every byte of input is
    // interpreted as an integer from 0-255, and then results in that many
    // bytes of output.  Writing the 4 bytes {ff,ff,ff,ff} would result in
    // 1kb of data being output.  In this case, you could write a very small
    // amount of input, and end up with a very large amount of output.  In
    // such a pathological inflating mechanism, there'd be no way to tell
    // the system to stop doing the transform.  A single 4MB write could
    // cause the system to run out of memory.
    //
    // However, even in such a pathological case, only a single written chunk
    // would be consumed, and then the rest would wait (un-transformed) until
    // the results of the previous transformed chunk were consumed.
    
    module.exports = Transform;
    
    var Duplex =  require('readable-stream/lib/_stream_duplex');
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    util.inherits(Transform, Duplex);
    
    
    function TransformState(options, stream) {
      this.afterTransform = function(er, data) {
        return afterTransform(stream, er, data);
      };
    
      this.needTransform = false;
      this.transforming = false;
      this.writecb = null;
      this.writechunk = null;
    }
    
    function afterTransform(stream, er, data) {
      var ts = stream._transformState;
      ts.transforming = false;
    
      var cb = ts.writecb;
    
      if (!cb)
        return stream.emit('error', new Error('no writecb in Transform class'));
    
      ts.writechunk = null;
      ts.writecb = null;
    
      if (data !== null && data !== undefined)
        stream.push(data);
    
      if (cb)
        cb(er);
    
      var rs = stream._readableState;
      rs.reading = false;
      if (rs.needReadable || rs.length < rs.highWaterMark) {
        stream._read(rs.highWaterMark);
      }
    }
    
    
    function Transform(options) {
      if (!(this instanceof Transform))
        return new Transform(options);
    
      Duplex.call(this, options);
    
      var ts = this._transformState = new TransformState(options, this);
    
      // when the writable side finishes, then flush out anything remaining.
      var stream = this;
    
      // start out asking for a readable event once data is transformed.
      this._readableState.needReadable = true;
    
      // we have implemented the _read method, and done the other things
      // that Readable wants before the first _read call, so unset the
      // sync guard flag.
      this._readableState.sync = false;
    
      this.once('finish', function() {
        if ('function' === typeof this._flush)
          this._flush(function(er) {
            done(stream, er);
          });
        else
          done(stream);
      });
    }
    
    Transform.prototype.push = function(chunk, encoding) {
      this._transformState.needTransform = false;
      return Duplex.prototype.push.call(this, chunk, encoding);
    };
    
    // This is the part where you do stuff!
    // override this function in implementation classes.
    // 'chunk' is an input chunk.
    //
    // Call `push(newChunk)` to pass along transformed output
    // to the readable side.  You may call 'push' zero or more times.
    //
    // Call `cb(err)` when you are done with this chunk.  If you pass
    // an error, then that'll put the hurt on the whole operation.  If you
    // never call cb(), then you'll never get another chunk.
    Transform.prototype._transform = function(chunk, encoding, cb) {
      throw new Error('not implemented');
    };
    
    Transform.prototype._write = function(chunk, encoding, cb) {
      var ts = this._transformState;
      ts.writecb = cb;
      ts.writechunk = chunk;
      ts.writeencoding = encoding;
      if (!ts.transforming) {
        var rs = this._readableState;
        if (ts.needTransform ||
            rs.needReadable ||
            rs.length < rs.highWaterMark)
          this._read(rs.highWaterMark);
      }
    };
    
    // Doesn't matter what the args are here.
    // _transform does all the work.
    // That we got here means that the readable side wants more data.
    Transform.prototype._read = function(n) {
      var ts = this._transformState;
    
      if (ts.writechunk !== null && ts.writecb && !ts.transforming) {
        ts.transforming = true;
        this._transform(ts.writechunk, ts.writeencoding, ts.afterTransform);
      } else {
        // mark that we need a transform, so that any data that comes in
        // will get processed, now that we've asked for it.
        ts.needTransform = true;
      }
    };
    
    
    function done(stream, er) {
      if (er)
        return stream.emit('error', er);
    
      // if there's nothing in the write buffer, then that means
      // that nothing more will ever be provided
      var ws = stream._writableState;
      var rs = stream._readableState;
      var ts = stream._transformState;
    
      if (ws.length)
        throw new Error('calling transform done when ws.length != 0');
    
      if (ts.transforming)
        throw new Error('calling transform done when still transforming');
    
      return stream.push(null);
    }
    
  provide("readable-stream/lib/_stream_transform", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_readable.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    module.exports = Readable;
    
    /*<replacement>*/
    var isArray = require('isarray');
    /*</replacement>*/
    
    
    /*<replacement>*/
    var Buffer = require('buffer').Buffer;
    /*</replacement>*/
    
    Readable.ReadableState = ReadableState;
    
    var EE = require('events').EventEmitter;
    
    /*<replacement>*/
    if (!EE.listenerCount) EE.listenerCount = function(emitter, type) {
      return emitter.listeners(type).length;
    };
    /*</replacement>*/
    
    var Stream = require('stream');
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    var StringDecoder;
    
    util.inherits(Readable, Stream);
    
    function ReadableState(options, stream) {
      options = options || {};
    
      // the point at which it stops calling _read() to fill the buffer
      // Note: 0 is a valid value, means "don't call _read preemptively ever"
      var hwm = options.highWaterMark;
      this.highWaterMark = (hwm || hwm === 0) ? hwm : 16 * 1024;
    
      // cast to ints.
      this.highWaterMark = ~~this.highWaterMark;
    
      this.buffer = [];
      this.length = 0;
      this.pipes = null;
      this.pipesCount = 0;
      this.flowing = false;
      this.ended = false;
      this.endEmitted = false;
      this.reading = false;
    
      // In streams that never have any data, and do push(null) right away,
      // the consumer can miss the 'end' event if they do some I/O before
      // consuming the stream.  So, we don't emit('end') until some reading
      // happens.
      this.calledRead = false;
    
      // a flag to be able to tell if the onwrite cb is called immediately,
      // or on a later tick.  We set this to true at first, becuase any
      // actions that shouldn't happen until "later" should generally also
      // not happen before the first write call.
      this.sync = true;
    
      // whenever we return null, then we set a flag to say
      // that we're awaiting a 'readable' event emission.
      this.needReadable = false;
      this.emittedReadable = false;
      this.readableListening = false;
    
    
      // object stream flag. Used to make read(n) ignore n and to
      // make all the buffer merging and length checks go away
      this.objectMode = !!options.objectMode;
    
      // Crypto is kind of old and crusty.  Historically, its default string
      // encoding is 'binary' so we have to make this configurable.
      // Everything else in the universe uses 'utf8', though.
      this.defaultEncoding = options.defaultEncoding || 'utf8';
    
      // when piping, we only care about 'readable' events that happen
      // after read()ing all the bytes and not getting any pushback.
      this.ranOut = false;
    
      // the number of writers that are awaiting a drain event in .pipe()s
      this.awaitDrain = 0;
    
      // if true, a maybeReadMore has been scheduled
      this.readingMore = false;
    
      this.decoder = null;
      this.encoding = null;
      if (options.encoding) {
        if (!StringDecoder)
          StringDecoder = require('string_decoder/').StringDecoder;
        this.decoder = new StringDecoder(options.encoding);
        this.encoding = options.encoding;
      }
    }
    
    function Readable(options) {
      if (!(this instanceof Readable))
        return new Readable(options);
    
      this._readableState = new ReadableState(options, this);
    
      // legacy
      this.readable = true;
    
      Stream.call(this);
    }
    
    // Manually shove something into the read() buffer.
    // This returns true if the highWaterMark has not been hit yet,
    // similar to how Writable.write() returns true if you should
    // write() some more.
    Readable.prototype.push = function(chunk, encoding) {
      var state = this._readableState;
    
      if (typeof chunk === 'string' && !state.objectMode) {
        encoding = encoding || state.defaultEncoding;
        if (encoding !== state.encoding) {
          chunk = new Buffer(chunk, encoding);
          encoding = '';
        }
      }
    
      return readableAddChunk(this, state, chunk, encoding, false);
    };
    
    // Unshift should *always* be something directly out of read()
    Readable.prototype.unshift = function(chunk) {
      var state = this._readableState;
      return readableAddChunk(this, state, chunk, '', true);
    };
    
    function readableAddChunk(stream, state, chunk, encoding, addToFront) {
      var er = chunkInvalid(state, chunk);
      if (er) {
        stream.emit('error', er);
      } else if (chunk === null || chunk === undefined) {
        state.reading = false;
        if (!state.ended)
          onEofChunk(stream, state);
      } else if (state.objectMode || chunk && chunk.length > 0) {
        if (state.ended && !addToFront) {
          var e = new Error('stream.push() after EOF');
          stream.emit('error', e);
        } else if (state.endEmitted && addToFront) {
          var e = new Error('stream.unshift() after end event');
          stream.emit('error', e);
        } else {
          if (state.decoder && !addToFront && !encoding)
            chunk = state.decoder.write(chunk);
    
          // update the buffer info.
          state.length += state.objectMode ? 1 : chunk.length;
          if (addToFront) {
            state.buffer.unshift(chunk);
          } else {
            state.reading = false;
            state.buffer.push(chunk);
          }
    
          if (state.needReadable)
            emitReadable(stream);
    
          maybeReadMore(stream, state);
        }
      } else if (!addToFront) {
        state.reading = false;
      }
    
      return needMoreData(state);
    }
    
    
    
    // if it's past the high water mark, we can push in some more.
    // Also, if we have no data yet, we can stand some
    // more bytes.  This is to work around cases where hwm=0,
    // such as the repl.  Also, if the push() triggered a
    // readable event, and the user called read(largeNumber) such that
    // needReadable was set, then we ought to push more, so that another
    // 'readable' event will be triggered.
    function needMoreData(state) {
      return !state.ended &&
             (state.needReadable ||
              state.length < state.highWaterMark ||
              state.length === 0);
    }
    
    // backwards compatibility.
    Readable.prototype.setEncoding = function(enc) {
      if (!StringDecoder)
        StringDecoder = require('string_decoder/').StringDecoder;
      this._readableState.decoder = new StringDecoder(enc);
      this._readableState.encoding = enc;
    };
    
    // Don't raise the hwm > 128MB
    var MAX_HWM = 0x800000;
    function roundUpToNextPowerOf2(n) {
      if (n >= MAX_HWM) {
        n = MAX_HWM;
      } else {
        // Get the next highest power of 2
        n--;
        for (var p = 1; p < 32; p <<= 1) n |= n >> p;
        n++;
      }
      return n;
    }
    
    function howMuchToRead(n, state) {
      if (state.length === 0 && state.ended)
        return 0;
    
      if (state.objectMode)
        return n === 0 ? 0 : 1;
    
      if (isNaN(n) || n === null) {
        // only flow one buffer at a time
        if (state.flowing && state.buffer.length)
          return state.buffer[0].length;
        else
          return state.length;
      }
    
      if (n <= 0)
        return 0;
    
      // If we're asking for more than the target buffer level,
      // then raise the water mark.  Bump up to the next highest
      // power of 2, to prevent increasing it excessively in tiny
      // amounts.
      if (n > state.highWaterMark)
        state.highWaterMark = roundUpToNextPowerOf2(n);
    
      // don't have that much.  return null, unless we've ended.
      if (n > state.length) {
        if (!state.ended) {
          state.needReadable = true;
          return 0;
        } else
          return state.length;
      }
    
      return n;
    }
    
    // you can override either this method, or the async _read(n) below.
    Readable.prototype.read = function(n) {
      var state = this._readableState;
      state.calledRead = true;
      var nOrig = n;
    
      if (typeof n !== 'number' || n > 0)
        state.emittedReadable = false;
    
      // if we're doing read(0) to trigger a readable event, but we
      // already have a bunch of data in the buffer, then just trigger
      // the 'readable' event and move on.
      if (n === 0 &&
          state.needReadable &&
          (state.length >= state.highWaterMark || state.ended)) {
        emitReadable(this);
        return null;
      }
    
      n = howMuchToRead(n, state);
    
      // if we've ended, and we're now clear, then finish it up.
      if (n === 0 && state.ended) {
        if (state.length === 0)
          endReadable(this);
        return null;
      }
    
      // All the actual chunk generation logic needs to be
      // *below* the call to _read.  The reason is that in certain
      // synthetic stream cases, such as passthrough streams, _read
      // may be a completely synchronous operation which may change
      // the state of the read buffer, providing enough data when
      // before there was *not* enough.
      //
      // So, the steps are:
      // 1. Figure out what the state of things will be after we do
      // a read from the buffer.
      //
      // 2. If that resulting state will trigger a _read, then call _read.
      // Note that this may be asynchronous, or synchronous.  Yes, it is
      // deeply ugly to write APIs this way, but that still doesn't mean
      // that the Readable class should behave improperly, as streams are
      // designed to be sync/async agnostic.
      // Take note if the _read call is sync or async (ie, if the read call
      // has returned yet), so that we know whether or not it's safe to emit
      // 'readable' etc.
      //
      // 3. Actually pull the requested chunks out of the buffer and return.
    
      // if we need a readable event, then we need to do some reading.
      var doRead = state.needReadable;
    
      // if we currently have less than the highWaterMark, then also read some
      if (state.length - n <= state.highWaterMark)
        doRead = true;
    
      // however, if we've ended, then there's no point, and if we're already
      // reading, then it's unnecessary.
      if (state.ended || state.reading)
        doRead = false;
    
      if (doRead) {
        state.reading = true;
        state.sync = true;
        // if the length is currently zero, then we *need* a readable event.
        if (state.length === 0)
          state.needReadable = true;
        // call internal read method
        this._read(state.highWaterMark);
        state.sync = false;
      }
    
      // If _read called its callback synchronously, then `reading`
      // will be false, and we need to re-evaluate how much data we
      // can return to the user.
      if (doRead && !state.reading)
        n = howMuchToRead(nOrig, state);
    
      var ret;
      if (n > 0)
        ret = fromList(n, state);
      else
        ret = null;
    
      if (ret === null) {
        state.needReadable = true;
        n = 0;
      }
    
      state.length -= n;
    
      // If we have nothing in the buffer, then we want to know
      // as soon as we *do* get something into the buffer.
      if (state.length === 0 && !state.ended)
        state.needReadable = true;
    
      // If we happened to read() exactly the remaining amount in the
      // buffer, and the EOF has been seen at this point, then make sure
      // that we emit 'end' on the very next tick.
      if (state.ended && !state.endEmitted && state.length === 0)
        endReadable(this);
    
      return ret;
    };
    
    function chunkInvalid(state, chunk) {
      var er = null;
      if (!Buffer.isBuffer(chunk) &&
          'string' !== typeof chunk &&
          chunk !== null &&
          chunk !== undefined &&
          !state.objectMode &&
          !er) {
        er = new TypeError('Invalid non-string/buffer chunk');
      }
      return er;
    }
    
    
    function onEofChunk(stream, state) {
      if (state.decoder && !state.ended) {
        var chunk = state.decoder.end();
        if (chunk && chunk.length) {
          state.buffer.push(chunk);
          state.length += state.objectMode ? 1 : chunk.length;
        }
      }
      state.ended = true;
    
      // if we've ended and we have some data left, then emit
      // 'readable' now to make sure it gets picked up.
      if (state.length > 0)
        emitReadable(stream);
      else
        endReadable(stream);
    }
    
    // Don't emit readable right away in sync mode, because this can trigger
    // another read() call => stack overflow.  This way, it might trigger
    // a nextTick recursion warning, but that's not so bad.
    function emitReadable(stream) {
      var state = stream._readableState;
      state.needReadable = false;
      if (state.emittedReadable)
        return;
    
      state.emittedReadable = true;
      if (state.sync)
        process.nextTick(function() {
          emitReadable_(stream);
        });
      else
        emitReadable_(stream);
    }
    
    function emitReadable_(stream) {
      stream.emit('readable');
    }
    
    
    // at this point, the user has presumably seen the 'readable' event,
    // and called read() to consume some data.  that may have triggered
    // in turn another _read(n) call, in which case reading = true if
    // it's in progress.
    // However, if we're not ended, or reading, and the length < hwm,
    // then go ahead and try to read some more preemptively.
    function maybeReadMore(stream, state) {
      if (!state.readingMore) {
        state.readingMore = true;
        process.nextTick(function() {
          maybeReadMore_(stream, state);
        });
      }
    }
    
    function maybeReadMore_(stream, state) {
      var len = state.length;
      while (!state.reading && !state.flowing && !state.ended &&
             state.length < state.highWaterMark) {
        stream.read(0);
        if (len === state.length)
          // didn't get any data, stop spinning.
          break;
        else
          len = state.length;
      }
      state.readingMore = false;
    }
    
    // abstract method.  to be overridden in specific implementation classes.
    // call cb(er, data) where data is <= n in length.
    // for virtual (non-string, non-buffer) streams, "length" is somewhat
    // arbitrary, and perhaps not very meaningful.
    Readable.prototype._read = function(n) {
      this.emit('error', new Error('not implemented'));
    };
    
    Readable.prototype.pipe = function(dest, pipeOpts) {
      var src = this;
      var state = this._readableState;
    
      switch (state.pipesCount) {
        case 0:
          state.pipes = dest;
          break;
        case 1:
          state.pipes = [state.pipes, dest];
          break;
        default:
          state.pipes.push(dest);
          break;
      }
      state.pipesCount += 1;
    
      var doEnd = (!pipeOpts || pipeOpts.end !== false) &&
                  dest !== process.stdout &&
                  dest !== process.stderr;
    
      var endFn = doEnd ? onend : cleanup;
      if (state.endEmitted)
        process.nextTick(endFn);
      else
        src.once('end', endFn);
    
      dest.on('unpipe', onunpipe);
      function onunpipe(readable) {
        if (readable !== src) return;
        cleanup();
      }
    
      function onend() {
        dest.end();
      }
    
      // when the dest drains, it reduces the awaitDrain counter
      // on the source.  This would be more elegant with a .once()
      // handler in flow(), but adding and removing repeatedly is
      // too slow.
      var ondrain = pipeOnDrain(src);
      dest.on('drain', ondrain);
    
      function cleanup() {
        // cleanup event handlers once the pipe is broken
        dest.removeListener('close', onclose);
        dest.removeListener('finish', onfinish);
        dest.removeListener('drain', ondrain);
        dest.removeListener('error', onerror);
        dest.removeListener('unpipe', onunpipe);
        src.removeListener('end', onend);
        src.removeListener('end', cleanup);
    
        // if the reader is waiting for a drain event from this
        // specific writer, then it would cause it to never start
        // flowing again.
        // So, if this is awaiting a drain, then we just call it now.
        // If we don't know, then assume that we are waiting for one.
        if (!dest._writableState || dest._writableState.needDrain)
          ondrain();
      }
    
      // if the dest has an error, then stop piping into it.
      // however, don't suppress the throwing behavior for this.
      function onerror(er) {
        unpipe();
        dest.removeListener('error', onerror);
        if (EE.listenerCount(dest, 'error') === 0)
          dest.emit('error', er);
      }
      // This is a brutally ugly hack to make sure that our error handler
      // is attached before any userland ones.  NEVER DO THIS.
      if (!dest._events || !dest._events.error)
        dest.on('error', onerror);
      else if (isArray(dest._events.error))
        dest._events.error.unshift(onerror);
      else
        dest._events.error = [onerror, dest._events.error];
    
    
    
      // Both close and finish should trigger unpipe, but only once.
      function onclose() {
        dest.removeListener('finish', onfinish);
        unpipe();
      }
      dest.once('close', onclose);
      function onfinish() {
        dest.removeListener('close', onclose);
        unpipe();
      }
      dest.once('finish', onfinish);
    
      function unpipe() {
        src.unpipe(dest);
      }
    
      // tell the dest that it's being piped to
      dest.emit('pipe', src);
    
      // start the flow if it hasn't been started already.
      if (!state.flowing) {
        // the handler that waits for readable events after all
        // the data gets sucked out in flow.
        // This would be easier to follow with a .once() handler
        // in flow(), but that is too slow.
        this.on('readable', pipeOnReadable);
    
        state.flowing = true;
        process.nextTick(function() {
          flow(src);
        });
      }
    
      return dest;
    };
    
    function pipeOnDrain(src) {
      return function() {
        var dest = this;
        var state = src._readableState;
        state.awaitDrain--;
        if (state.awaitDrain === 0)
          flow(src);
      };
    }
    
    function flow(src) {
      var state = src._readableState;
      var chunk;
      state.awaitDrain = 0;
    
      function write(dest, i, list) {
        var written = dest.write(chunk);
        if (false === written) {
          state.awaitDrain++;
        }
      }
    
      while (state.pipesCount && null !== (chunk = src.read())) {
    
        if (state.pipesCount === 1)
          write(state.pipes, 0, null);
        else
          forEach(state.pipes, write);
    
        src.emit('data', chunk);
    
        // if anyone needs a drain, then we have to wait for that.
        if (state.awaitDrain > 0)
          return;
      }
    
      // if every destination was unpiped, either before entering this
      // function, or in the while loop, then stop flowing.
      //
      // NB: This is a pretty rare edge case.
      if (state.pipesCount === 0) {
        state.flowing = false;
    
        // if there were data event listeners added, then switch to old mode.
        if (EE.listenerCount(src, 'data') > 0)
          emitDataEvents(src);
        return;
      }
    
      // at this point, no one needed a drain, so we just ran out of data
      // on the next readable event, start it over again.
      state.ranOut = true;
    }
    
    function pipeOnReadable() {
      if (this._readableState.ranOut) {
        this._readableState.ranOut = false;
        flow(this);
      }
    }
    
    
    Readable.prototype.unpipe = function(dest) {
      var state = this._readableState;
    
      // if we're not piping anywhere, then do nothing.
      if (state.pipesCount === 0)
        return this;
    
      // just one destination.  most common case.
      if (state.pipesCount === 1) {
        // passed in one, but it's not the right one.
        if (dest && dest !== state.pipes)
          return this;
    
        if (!dest)
          dest = state.pipes;
    
        // got a match.
        state.pipes = null;
        state.pipesCount = 0;
        this.removeListener('readable', pipeOnReadable);
        state.flowing = false;
        if (dest)
          dest.emit('unpipe', this);
        return this;
      }
    
      // slow case. multiple pipe destinations.
    
      if (!dest) {
        // remove all.
        var dests = state.pipes;
        var len = state.pipesCount;
        state.pipes = null;
        state.pipesCount = 0;
        this.removeListener('readable', pipeOnReadable);
        state.flowing = false;
    
        for (var i = 0; i < len; i++)
          dests[i].emit('unpipe', this);
        return this;
      }
    
      // try to find the right one.
      var i = indexOf(state.pipes, dest);
      if (i === -1)
        return this;
    
      state.pipes.splice(i, 1);
      state.pipesCount -= 1;
      if (state.pipesCount === 1)
        state.pipes = state.pipes[0];
    
      dest.emit('unpipe', this);
    
      return this;
    };
    
    // set up data events if they are asked for
    // Ensure readable listeners eventually get something
    Readable.prototype.on = function(ev, fn) {
      var res = Stream.prototype.on.call(this, ev, fn);
    
      if (ev === 'data' && !this._readableState.flowing)
        emitDataEvents(this);
    
      if (ev === 'readable' && this.readable) {
        var state = this._readableState;
        if (!state.readableListening) {
          state.readableListening = true;
          state.emittedReadable = false;
          state.needReadable = true;
          if (!state.reading) {
            this.read(0);
          } else if (state.length) {
            emitReadable(this, state);
          }
        }
      }
    
      return res;
    };
    Readable.prototype.addListener = Readable.prototype.on;
    
    // pause() and resume() are remnants of the legacy readable stream API
    // If the user uses them, then switch into old mode.
    Readable.prototype.resume = function() {
      emitDataEvents(this);
      this.read(0);
      this.emit('resume');
    };
    
    Readable.prototype.pause = function() {
      emitDataEvents(this, true);
      this.emit('pause');
    };
    
    function emitDataEvents(stream, startPaused) {
      var state = stream._readableState;
    
      if (state.flowing) {
        // https://github.com/isaacs/readable-stream/issues/16
        throw new Error('Cannot switch to old mode now.');
      }
    
      var paused = startPaused || false;
      var readable = false;
    
      // convert to an old-style stream.
      stream.readable = true;
      stream.pipe = Stream.prototype.pipe;
      stream.on = stream.addListener = Stream.prototype.on;
    
      stream.on('readable', function() {
        readable = true;
    
        var c;
        while (!paused && (null !== (c = stream.read())))
          stream.emit('data', c);
    
        if (c === null) {
          readable = false;
          stream._readableState.needReadable = true;
        }
      });
    
      stream.pause = function() {
        paused = true;
        this.emit('pause');
      };
    
      stream.resume = function() {
        paused = false;
        if (readable)
          process.nextTick(function() {
            stream.emit('readable');
          });
        else
          this.read(0);
        this.emit('resume');
      };
    
      // now make it start, just in case it hadn't already.
      stream.emit('readable');
    }
    
    // wrap an old-style stream as the async data source.
    // This is *not* part of the readable stream interface.
    // It is an ugly unfortunate mess of history.
    Readable.prototype.wrap = function(stream) {
      var state = this._readableState;
      var paused = false;
    
      var self = this;
      stream.on('end', function() {
        if (state.decoder && !state.ended) {
          var chunk = state.decoder.end();
          if (chunk && chunk.length)
            self.push(chunk);
        }
    
        self.push(null);
      });
    
      stream.on('data', function(chunk) {
        if (state.decoder)
          chunk = state.decoder.write(chunk);
        if (!chunk || !state.objectMode && !chunk.length)
          return;
    
        var ret = self.push(chunk);
        if (!ret) {
          paused = true;
          stream.pause();
        }
      });
    
      // proxy all the other methods.
      // important when wrapping filters and duplexes.
      for (var i in stream) {
        if (typeof stream[i] === 'function' &&
            typeof this[i] === 'undefined') {
          this[i] = function(method) { return function() {
            return stream[method].apply(stream, arguments);
          }}(i);
        }
      }
    
      // proxy certain important events.
      var events = ['error', 'close', 'destroy', 'pause', 'resume'];
      forEach(events, function(ev) {
        stream.on(ev, self.emit.bind(self, ev));
      });
    
      // when we try to consume some more bytes, simply unpause the
      // underlying stream.
      self._read = function(n) {
        if (paused) {
          paused = false;
          stream.resume();
        }
      };
    
      return self;
    };
    
    
    
    // exposed for testing purposes only.
    Readable._fromList = fromList;
    
    // Pluck off n bytes from an array of buffers.
    // Length is the combined lengths of all the buffers in the list.
    function fromList(n, state) {
      var list = state.buffer;
      var length = state.length;
      var stringMode = !!state.decoder;
      var objectMode = !!state.objectMode;
      var ret;
    
      // nothing in the list, definitely empty.
      if (list.length === 0)
        return null;
    
      if (length === 0)
        ret = null;
      else if (objectMode)
        ret = list.shift();
      else if (!n || n >= length) {
        // read it all, truncate the array.
        if (stringMode)
          ret = list.join('');
        else
          ret = Buffer.concat(list, length);
        list.length = 0;
      } else {
        // read just some of it.
        if (n < list[0].length) {
          // just take a part of the first list item.
          // slice is the same for buffers and strings.
          var buf = list[0];
          ret = buf.slice(0, n);
          list[0] = buf.slice(n);
        } else if (n === list[0].length) {
          // first list is a perfect match
          ret = list.shift();
        } else {
          // complex case.
          // we have enough to cover it, but it spans past the first buffer.
          if (stringMode)
            ret = '';
          else
            ret = new Buffer(n);
    
          var c = 0;
          for (var i = 0, l = list.length; i < l && c < n; i++) {
            var buf = list[0];
            var cpy = Math.min(n - c, buf.length);
    
            if (stringMode)
              ret += buf.slice(0, cpy);
            else
              buf.copy(ret, c, 0, cpy);
    
            if (cpy < buf.length)
              list[0] = buf.slice(cpy);
            else
              list.shift();
    
            c += cpy;
          }
        }
      }
    
      return ret;
    }
    
    function endReadable(stream) {
      var state = stream._readableState;
    
      // If we get here before consuming all the bytes, then that is a
      // bug in node.  Should never happen.
      if (state.length > 0)
        throw new Error('endReadable called on non-empty stream');
    
      if (!state.endEmitted && state.calledRead) {
        state.ended = true;
        process.nextTick(function() {
          // Check that we didn't get one last unshift.
          if (!state.endEmitted && state.length === 0) {
            state.endEmitted = true;
            stream.readable = false;
            stream.emit('end');
          }
        });
      }
    }
    
    function forEach (xs, f) {
      for (var i = 0, l = xs.length; i < l; i++) {
        f(xs[i], i);
      }
    }
    
    function indexOf (xs, x) {
      for (var i = 0, l = xs.length; i < l; i++) {
        if (xs[i] === x) return i;
      }
      return -1;
    }
    
  provide("readable-stream/lib/_stream_readable.js", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_writable.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // A bit simpler than readable streams.
    // Implement an async ._write(chunk, cb), and it'll handle all
    // the drain event emission and buffering.
    
    module.exports = Writable;
    
    /*<replacement>*/
    var Buffer = require('buffer').Buffer;
    /*</replacement>*/
    
    Writable.WritableState = WritableState;
    
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    
    var Stream = require('stream');
    
    util.inherits(Writable, Stream);
    
    function WriteReq(chunk, encoding, cb) {
      this.chunk = chunk;
      this.encoding = encoding;
      this.callback = cb;
    }
    
    function WritableState(options, stream) {
      options = options || {};
    
      // the point at which write() starts returning false
      // Note: 0 is a valid value, means that we always return false if
      // the entire buffer is not flushed immediately on write()
      var hwm = options.highWaterMark;
      this.highWaterMark = (hwm || hwm === 0) ? hwm : 16 * 1024;
    
      // object stream flag to indicate whether or not this stream
      // contains buffers or objects.
      this.objectMode = !!options.objectMode;
    
      // cast to ints.
      this.highWaterMark = ~~this.highWaterMark;
    
      this.needDrain = false;
      // at the start of calling end()
      this.ending = false;
      // when end() has been called, and returned
      this.ended = false;
      // when 'finish' is emitted
      this.finished = false;
    
      // should we decode strings into buffers before passing to _write?
      // this is here so that some node-core streams can optimize string
      // handling at a lower level.
      var noDecode = options.decodeStrings === false;
      this.decodeStrings = !noDecode;
    
      // Crypto is kind of old and crusty.  Historically, its default string
      // encoding is 'binary' so we have to make this configurable.
      // Everything else in the universe uses 'utf8', though.
      this.defaultEncoding = options.defaultEncoding || 'utf8';
    
      // not an actual buffer we keep track of, but a measurement
      // of how much we're waiting to get pushed to some underlying
      // socket or file.
      this.length = 0;
    
      // a flag to see when we're in the middle of a write.
      this.writing = false;
    
      // a flag to be able to tell if the onwrite cb is called immediately,
      // or on a later tick.  We set this to true at first, becuase any
      // actions that shouldn't happen until "later" should generally also
      // not happen before the first write call.
      this.sync = true;
    
      // a flag to know if we're processing previously buffered items, which
      // may call the _write() callback in the same tick, so that we don't
      // end up in an overlapped onwrite situation.
      this.bufferProcessing = false;
    
      // the callback that's passed to _write(chunk,cb)
      this.onwrite = function(er) {
        onwrite(stream, er);
      };
    
      // the callback that the user supplies to write(chunk,encoding,cb)
      this.writecb = null;
    
      // the amount that is being written when _write is called.
      this.writelen = 0;
    
      this.buffer = [];
    
      // True if the error was already emitted and should not be thrown again
      this.errorEmitted = false;
    }
    
    function Writable(options) {
      var Duplex =  require('readable-stream/lib/_stream_duplex');
    
      // Writable ctor is applied to Duplexes, though they're not
      // instanceof Writable, they're instanceof Readable.
      if (!(this instanceof Writable) && !(this instanceof Duplex))
        return new Writable(options);
    
      this._writableState = new WritableState(options, this);
    
      // legacy.
      this.writable = true;
    
      Stream.call(this);
    }
    
    // Otherwise people can pipe Writable streams, which is just wrong.
    Writable.prototype.pipe = function() {
      this.emit('error', new Error('Cannot pipe. Not readable.'));
    };
    
    
    function writeAfterEnd(stream, state, cb) {
      var er = new Error('write after end');
      // TODO: defer error events consistently everywhere, not just the cb
      stream.emit('error', er);
      process.nextTick(function() {
        cb(er);
      });
    }
    
    // If we get something that is not a buffer, string, null, or undefined,
    // and we're not in objectMode, then that's an error.
    // Otherwise stream chunks are all considered to be of length=1, and the
    // watermarks determine how many objects to keep in the buffer, rather than
    // how many bytes or characters.
    function validChunk(stream, state, chunk, cb) {
      var valid = true;
      if (!Buffer.isBuffer(chunk) &&
          'string' !== typeof chunk &&
          chunk !== null &&
          chunk !== undefined &&
          !state.objectMode) {
        var er = new TypeError('Invalid non-string/buffer chunk');
        stream.emit('error', er);
        process.nextTick(function() {
          cb(er);
        });
        valid = false;
      }
      return valid;
    }
    
    Writable.prototype.write = function(chunk, encoding, cb) {
      var state = this._writableState;
      var ret = false;
    
      if (typeof encoding === 'function') {
        cb = encoding;
        encoding = null;
      }
    
      if (Buffer.isBuffer(chunk))
        encoding = 'buffer';
      else if (!encoding)
        encoding = state.defaultEncoding;
    
      if (typeof cb !== 'function')
        cb = function() {};
    
      if (state.ended)
        writeAfterEnd(this, state, cb);
      else if (validChunk(this, state, chunk, cb))
        ret = writeOrBuffer(this, state, chunk, encoding, cb);
    
      return ret;
    };
    
    function decodeChunk(state, chunk, encoding) {
      if (!state.objectMode &&
          state.decodeStrings !== false &&
          typeof chunk === 'string') {
        chunk = new Buffer(chunk, encoding);
      }
      return chunk;
    }
    
    // if we're already writing something, then just put this
    // in the queue, and wait our turn.  Otherwise, call _write
    // If we return false, then we need a drain event, so set that flag.
    function writeOrBuffer(stream, state, chunk, encoding, cb) {
      chunk = decodeChunk(state, chunk, encoding);
      if (Buffer.isBuffer(chunk))
        encoding = 'buffer';
      var len = state.objectMode ? 1 : chunk.length;
    
      state.length += len;
    
      var ret = state.length < state.highWaterMark;
      // we must ensure that previous needDrain will not be reset to false.
      if (!ret)
        state.needDrain = true;
    
      if (state.writing)
        state.buffer.push(new WriteReq(chunk, encoding, cb));
      else
        doWrite(stream, state, len, chunk, encoding, cb);
    
      return ret;
    }
    
    function doWrite(stream, state, len, chunk, encoding, cb) {
      state.writelen = len;
      state.writecb = cb;
      state.writing = true;
      state.sync = true;
      stream._write(chunk, encoding, state.onwrite);
      state.sync = false;
    }
    
    function onwriteError(stream, state, sync, er, cb) {
      if (sync)
        process.nextTick(function() {
          cb(er);
        });
      else
        cb(er);
    
      stream._writableState.errorEmitted = true;
      stream.emit('error', er);
    }
    
    function onwriteStateUpdate(state) {
      state.writing = false;
      state.writecb = null;
      state.length -= state.writelen;
      state.writelen = 0;
    }
    
    function onwrite(stream, er) {
      var state = stream._writableState;
      var sync = state.sync;
      var cb = state.writecb;
    
      onwriteStateUpdate(state);
    
      if (er)
        onwriteError(stream, state, sync, er, cb);
      else {
        // Check if we're actually ready to finish, but don't emit yet
        var finished = needFinish(stream, state);
    
        if (!finished && !state.bufferProcessing && state.buffer.length)
          clearBuffer(stream, state);
    
        if (sync) {
          process.nextTick(function() {
            afterWrite(stream, state, finished, cb);
          });
        } else {
          afterWrite(stream, state, finished, cb);
        }
      }
    }
    
    function afterWrite(stream, state, finished, cb) {
      if (!finished)
        onwriteDrain(stream, state);
      cb();
      if (finished)
        finishMaybe(stream, state);
    }
    
    // Must force callback to be called on nextTick, so that we don't
    // emit 'drain' before the write() consumer gets the 'false' return
    // value, and has a chance to attach a 'drain' listener.
    function onwriteDrain(stream, state) {
      if (state.length === 0 && state.needDrain) {
        state.needDrain = false;
        stream.emit('drain');
      }
    }
    
    
    // if there's something in the buffer waiting, then process it
    function clearBuffer(stream, state) {
      state.bufferProcessing = true;
    
      for (var c = 0; c < state.buffer.length; c++) {
        var entry = state.buffer[c];
        var chunk = entry.chunk;
        var encoding = entry.encoding;
        var cb = entry.callback;
        var len = state.objectMode ? 1 : chunk.length;
    
        doWrite(stream, state, len, chunk, encoding, cb);
    
        // if we didn't call the onwrite immediately, then
        // it means that we need to wait until it does.
        // also, that means that the chunk and cb are currently
        // being processed, so move the buffer counter past them.
        if (state.writing) {
          c++;
          break;
        }
      }
    
      state.bufferProcessing = false;
      if (c < state.buffer.length)
        state.buffer = state.buffer.slice(c);
      else
        state.buffer.length = 0;
    }
    
    Writable.prototype._write = function(chunk, encoding, cb) {
      cb(new Error('not implemented'));
    };
    
    Writable.prototype.end = function(chunk, encoding, cb) {
      var state = this._writableState;
    
      if (typeof chunk === 'function') {
        cb = chunk;
        chunk = null;
        encoding = null;
      } else if (typeof encoding === 'function') {
        cb = encoding;
        encoding = null;
      }
    
      if (typeof chunk !== 'undefined' && chunk !== null)
        this.write(chunk, encoding);
    
      // ignore unnecessary end() calls.
      if (!state.ending && !state.finished)
        endWritable(this, state, cb);
    };
    
    
    function needFinish(stream, state) {
      return (state.ending &&
              state.length === 0 &&
              !state.finished &&
              !state.writing);
    }
    
    function finishMaybe(stream, state) {
      var need = needFinish(stream, state);
      if (need) {
        state.finished = true;
        stream.emit('finish');
      }
      return need;
    }
    
    function endWritable(stream, state, cb) {
      state.ending = true;
      finishMaybe(stream, state);
      if (cb) {
        if (state.finished)
          process.nextTick(cb);
        else
          stream.once('finish', cb);
      }
      state.ended = true;
    }
    
  provide("readable-stream/lib/_stream_writable.js", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_duplex.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // a duplex stream is just a stream that is both readable and writable.
    // Since JS doesn't have multiple prototypal inheritance, this class
    // prototypally inherits from Readable, and then parasitically from
    // Writable.
    
    module.exports = Duplex;
    
    /*<replacement>*/
    var objectKeys = Object.keys || function (obj) {
      var keys = [];
      for (var key in obj) keys.push(key);
      return keys;
    }
    /*</replacement>*/
    
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    var Readable =  require('readable-stream/lib/_stream_readable');
    var Writable =  require('readable-stream/lib/_stream_writable');
    
    util.inherits(Duplex, Readable);
    
    forEach(objectKeys(Writable.prototype), function(method) {
      if (!Duplex.prototype[method])
        Duplex.prototype[method] = Writable.prototype[method];
    });
    
    function Duplex(options) {
      if (!(this instanceof Duplex))
        return new Duplex(options);
    
      Readable.call(this, options);
      Writable.call(this, options);
    
      if (options && options.readable === false)
        this.readable = false;
    
      if (options && options.writable === false)
        this.writable = false;
    
      this.allowHalfOpen = true;
      if (options && options.allowHalfOpen === false)
        this.allowHalfOpen = false;
    
      this.once('end', onend);
    }
    
    // the no-half-open enforcer
    function onend() {
      // if we allow half-open state, or if the writable side ended,
      // then we're ok.
      if (this.allowHalfOpen || this._writableState.ended)
        return;
    
      // no more data can be written.
      // But allow more writes to happen in this tick.
      process.nextTick(this.end.bind(this));
    }
    
    function forEach (xs, f) {
      for (var i = 0, l = xs.length; i < l; i++) {
        f(xs[i], i);
      }
    }
    
  provide("readable-stream/lib/_stream_duplex.js", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_transform.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    
    // a transform stream is a readable/writable stream where you do
    // something with the data.  Sometimes it's called a "filter",
    // but that's not a great name for it, since that implies a thing where
    // some bits pass through, and others are simply ignored.  (That would
    // be a valid example of a transform, of course.)
    //
    // While the output is causally related to the input, it's not a
    // necessarily symmetric or synchronous transformation.  For example,
    // a zlib stream might take multiple plain-text writes(), and then
    // emit a single compressed chunk some time in the future.
    //
    // Here's how this works:
    //
    // The Transform stream has all the aspects of the readable and writable
    // stream classes.  When you write(chunk), that calls _write(chunk,cb)
    // internally, and returns false if there's a lot of pending writes
    // buffered up.  When you call read(), that calls _read(n) until
    // there's enough pending readable data buffered up.
    //
    // In a transform stream, the written data is placed in a buffer.  When
    // _read(n) is called, it transforms the queued up data, calling the
    // buffered _write cb's as it consumes chunks.  If consuming a single
    // written chunk would result in multiple output chunks, then the first
    // outputted bit calls the readcb, and subsequent chunks just go into
    // the read buffer, and will cause it to emit 'readable' if necessary.
    //
    // This way, back-pressure is actually determined by the reading side,
    // since _read has to be called to start processing a new chunk.  However,
    // a pathological inflate type of transform can cause excessive buffering
    // here.  For example, imagine a stream where every byte of input is
    // interpreted as an integer from 0-255, and then results in that many
    // bytes of output.  Writing the 4 bytes {ff,ff,ff,ff} would result in
    // 1kb of data being output.  In this case, you could write a very small
    // amount of input, and end up with a very large amount of output.  In
    // such a pathological inflating mechanism, there'd be no way to tell
    // the system to stop doing the transform.  A single 4MB write could
    // cause the system to run out of memory.
    //
    // However, even in such a pathological case, only a single written chunk
    // would be consumed, and then the rest would wait (un-transformed) until
    // the results of the previous transformed chunk were consumed.
    
    module.exports = Transform;
    
    var Duplex =  require('readable-stream/lib/_stream_duplex');
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    util.inherits(Transform, Duplex);
    
    
    function TransformState(options, stream) {
      this.afterTransform = function(er, data) {
        return afterTransform(stream, er, data);
      };
    
      this.needTransform = false;
      this.transforming = false;
      this.writecb = null;
      this.writechunk = null;
    }
    
    function afterTransform(stream, er, data) {
      var ts = stream._transformState;
      ts.transforming = false;
    
      var cb = ts.writecb;
    
      if (!cb)
        return stream.emit('error', new Error('no writecb in Transform class'));
    
      ts.writechunk = null;
      ts.writecb = null;
    
      if (data !== null && data !== undefined)
        stream.push(data);
    
      if (cb)
        cb(er);
    
      var rs = stream._readableState;
      rs.reading = false;
      if (rs.needReadable || rs.length < rs.highWaterMark) {
        stream._read(rs.highWaterMark);
      }
    }
    
    
    function Transform(options) {
      if (!(this instanceof Transform))
        return new Transform(options);
    
      Duplex.call(this, options);
    
      var ts = this._transformState = new TransformState(options, this);
    
      // when the writable side finishes, then flush out anything remaining.
      var stream = this;
    
      // start out asking for a readable event once data is transformed.
      this._readableState.needReadable = true;
    
      // we have implemented the _read method, and done the other things
      // that Readable wants before the first _read call, so unset the
      // sync guard flag.
      this._readableState.sync = false;
    
      this.once('finish', function() {
        if ('function' === typeof this._flush)
          this._flush(function(er) {
            done(stream, er);
          });
        else
          done(stream);
      });
    }
    
    Transform.prototype.push = function(chunk, encoding) {
      this._transformState.needTransform = false;
      return Duplex.prototype.push.call(this, chunk, encoding);
    };
    
    // This is the part where you do stuff!
    // override this function in implementation classes.
    // 'chunk' is an input chunk.
    //
    // Call `push(newChunk)` to pass along transformed output
    // to the readable side.  You may call 'push' zero or more times.
    //
    // Call `cb(err)` when you are done with this chunk.  If you pass
    // an error, then that'll put the hurt on the whole operation.  If you
    // never call cb(), then you'll never get another chunk.
    Transform.prototype._transform = function(chunk, encoding, cb) {
      throw new Error('not implemented');
    };
    
    Transform.prototype._write = function(chunk, encoding, cb) {
      var ts = this._transformState;
      ts.writecb = cb;
      ts.writechunk = chunk;
      ts.writeencoding = encoding;
      if (!ts.transforming) {
        var rs = this._readableState;
        if (ts.needTransform ||
            rs.needReadable ||
            rs.length < rs.highWaterMark)
          this._read(rs.highWaterMark);
      }
    };
    
    // Doesn't matter what the args are here.
    // _transform does all the work.
    // That we got here means that the readable side wants more data.
    Transform.prototype._read = function(n) {
      var ts = this._transformState;
    
      if (ts.writechunk !== null && ts.writecb && !ts.transforming) {
        ts.transforming = true;
        this._transform(ts.writechunk, ts.writeencoding, ts.afterTransform);
      } else {
        // mark that we need a transform, so that any data that comes in
        // will get processed, now that we've asked for it.
        ts.needTransform = true;
      }
    };
    
    
    function done(stream, er) {
      if (er)
        return stream.emit('error', er);
    
      // if there's nothing in the write buffer, then that means
      // that nothing more will ever be provided
      var ws = stream._writableState;
      var rs = stream._readableState;
      var ts = stream._transformState;
    
      if (ws.length)
        throw new Error('calling transform done when ws.length != 0');
    
      if (ts.transforming)
        throw new Error('calling transform done when still transforming');
    
      return stream.push(null);
    }
    
  provide("readable-stream/lib/_stream_transform.js", module.exports);
}(global));

// pakmanager:readable-stream/lib/_stream_passthrough.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Copyright Joyent, Inc. and other Node contributors.
    //
    // Permission is hereby granted, free of charge, to any person obtaining a
    // copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to permit
    // persons to whom the Software is furnished to do so, subject to the
    // following conditions:
    //
    // The above copyright notice and this permission notice shall be included
    // in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    // OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
    // NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    // DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    // OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
    // USE OR OTHER DEALINGS IN THE SOFTWARE.
    
    // a passthrough stream.
    // basically just the most minimal sort of Transform stream.
    // Every written chunk gets output as-is.
    
    module.exports = PassThrough;
    
    var Transform =  require('readable-stream/lib/_stream_transform');
    
    /*<replacement>*/
    var util = require('core-util-is');
    util.inherits = require('inherits');
    /*</replacement>*/
    
    util.inherits(PassThrough, Transform);
    
    function PassThrough(options) {
      if (!(this instanceof PassThrough))
        return new PassThrough(options);
    
      Transform.call(this, options);
    }
    
    PassThrough.prototype._transform = function(chunk, encoding, cb) {
      cb(null, chunk);
    };
    
  provide("readable-stream/lib/_stream_passthrough.js", module.exports);
}(global));

// pakmanager:readable-stream
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  exports = module.exports =  require('readable-stream/lib/_stream_readable.js');
    exports.Readable = exports;
    exports.Writable =  require('readable-stream/lib/_stream_writable.js');
    exports.Duplex =  require('readable-stream/lib/_stream_duplex.js');
    exports.Transform =  require('readable-stream/lib/_stream_transform.js');
    exports.PassThrough =  require('readable-stream/lib/_stream_passthrough.js');
    
  provide("readable-stream", module.exports);
}(global));

// pakmanager:cookie
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /// Serialize the a name value pair into a cookie string suitable for
    /// http headers. An optional options object specified cookie parameters
    ///
    /// serialize('foo', 'bar', { httpOnly: true })
    ///   => "foo=bar; httpOnly"
    ///
    /// @param {String} name
    /// @param {String} val
    /// @param {Object} options
    /// @return {String}
    var serialize = function(name, val, opt){
        opt = opt || {};
        var enc = opt.encode || encode;
        var pairs = [name + '=' + enc(val)];
    
        if (null != opt.maxAge) {
            var maxAge = opt.maxAge - 0;
            if (isNaN(maxAge)) throw new Error('maxAge should be a Number');
            pairs.push('Max-Age=' + maxAge);
        }
    
        if (opt.domain) pairs.push('Domain=' + opt.domain);
        if (opt.path) pairs.push('Path=' + opt.path);
        if (opt.expires) pairs.push('Expires=' + opt.expires.toUTCString());
        if (opt.httpOnly) pairs.push('HttpOnly');
        if (opt.secure) pairs.push('Secure');
    
        return pairs.join('; ');
    };
    
    /// Parse the given cookie header string into an object
    /// The object has the various cookies as keys(names) => values
    /// @param {String} str
    /// @return {Object}
    var parse = function(str, opt) {
        opt = opt || {};
        var obj = {}
        var pairs = str.split(/; */);
        var dec = opt.decode || decode;
    
        pairs.forEach(function(pair) {
            var eq_idx = pair.indexOf('=')
    
            // skip things that don't look like key=value
            if (eq_idx < 0) {
                return;
            }
    
            var key = pair.substr(0, eq_idx).trim()
            var val = pair.substr(++eq_idx, pair.length).trim();
    
            // quoted values
            if ('"' == val[0]) {
                val = val.slice(1, -1);
            }
    
            // only assign once
            if (undefined == obj[key]) {
                try {
                    obj[key] = dec(val);
                } catch (e) {
                    obj[key] = val;
                }
            }
        });
    
        return obj;
    };
    
    var encode = encodeURIComponent;
    var decode = decodeURIComponent;
    
    module.exports.serialize = serialize;
    module.exports.parse = parse;
    
  provide("cookie", module.exports);
}(global));

// pakmanager:cookie-signature
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var crypto = require('crypto');
    
    /**
     * Sign the given `val` with `secret`.
     *
     * @param {String} val
     * @param {String} secret
     * @return {String}
     * @api private
     */
    
    exports.sign = function(val, secret){
      if ('string' != typeof val) throw new TypeError('cookie required');
      if ('string' != typeof secret) throw new TypeError('secret required');
      return val + '.' + crypto
        .createHmac('sha256', secret)
        .update(val)
        .digest('base64')
        .replace(/\=+$/, '');
    };
    
    /**
     * Unsign and decode the given `val` with `secret`,
     * returning `false` if the signature is invalid.
     *
     * @param {String} val
     * @param {String} secret
     * @return {String|Boolean}
     * @api private
     */
    
    exports.unsign = function(val, secret){
      if ('string' != typeof val) throw new TypeError('cookie required');
      if ('string' != typeof secret) throw new TypeError('secret required');
      var str = val.slice(0, val.lastIndexOf('.'))
        , mac = exports.sign(str, secret);
      
      return exports.sign(mac, secret) == exports.sign(val, secret) ? str : false;
    };
    
  provide("cookie-signature", module.exports);
}(global));

// pakmanager:bytes
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Parse byte `size` string.
     *
     * @param {String} size
     * @return {Number}
     * @api public
     */
    
    module.exports = function(size) {
      if ('number' == typeof size) return convert(size);
      var parts = size.match(/^(\d+(?:\.\d+)?) *(kb|mb|gb|tb)$/)
        , n = parseFloat(parts[1])
        , type = parts[2];
    
      var map = {
          kb: 1 << 10
        , mb: 1 << 20
        , gb: 1 << 30
        , tb: ((1 << 30) * 1024)
      };
    
      return map[type] * n;
    };
    
    /**
     * convert bytes into string.
     *
     * @param {Number} b - bytes to convert
     * @return {String}
     * @api public
     */
    
    function convert (b) {
      var tb = ((1 << 30) * 1024), gb = 1 << 30, mb = 1 << 20, kb = 1 << 10;
      if (b >= tb) return (Math.round(b / tb * 100) / 100) + 'tb';
      if (b >= gb) return (Math.round(b / gb * 100) / 100) + 'gb';
      if (b >= mb) return (Math.round(b / mb * 100) / 100) + 'mb';
      if (b >= kb) return (Math.round(b / kb * 100) / 100) + 'kb';
      return b + 'b';
    }
    
  provide("bytes", module.exports);
}(global));

// pakmanager:negotiator
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = Negotiator;
    Negotiator.Negotiator = Negotiator;
    
    function Negotiator(request) {
      if (!(this instanceof Negotiator)) return new Negotiator(request);
      this.request = request;
    }
    
    var set = { charset: 'accept-charset',
                encoding: 'accept-encoding',
                language: 'accept-language',
                mediaType: 'accept' };
    
    
    function capitalize(string){
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    Object.keys(set).forEach(function (k) {
      var header = set[k],
          method = require('./'+k+'.js'),
          singular = k,
          plural = k + 's';
    
      Negotiator.prototype[plural] = function (available) {
        return method(this.request.headers[header], available);
      };
    
      Negotiator.prototype[singular] = function(available) {
        var set = this[plural](available);
        if (set) return set[0];
      };
    
      // Keep preferred* methods for legacy compatibility
      Negotiator.prototype['preferred'+capitalize(plural)] = Negotiator.prototype[plural];
      Negotiator.prototype['preferred'+capitalize(singular)] = Negotiator.prototype[singular];
    })
    
  provide("negotiator", module.exports);
}(global));

// pakmanager:compressible
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = compressible
    
    compressible.specs =
    compressible.specifications = require('./specifications.json')
    
    compressible.regex =
    compressible.regexp = /json|text|xml/
    
    compressible.get = get
    
    function compressible(type) {
      if (!type || typeof type !== "string") return false
      var i = type.indexOf(';')
        , spec = compressible.specs[~i ? type.slice(0, i) : type]
      return spec ? spec.compressible : compressible.regex.test(type)
    }
    
    function get(type) {
      if (!type || typeof type !== "string") return {
        compressible: false,
        sources: [],
        notes: "Invalid type."
      }
      var i = type.indexOf(';')
        , spec = compressible.specs[~i ? type.slice(0, i) : type]
      return spec ? spec : {
        compressible: compressible.regex.test(type),
        sources: ["compressible.regex"],
        notes: "Automatically generated via regex."
      }
    }
  provide("compressible", module.exports);
}(global));

// pakmanager:uid2
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies
     */
    
    var crypto = require('crypto');
    
    /**
     * 62 characters in the ascii range that can be used in URLs without special
     * encoding.
     */
    var UIDCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    
    /**
     * Make a Buffer into a string ready for use in URLs
     *
     * @param {String}
     * @returns {String}
     * @api private
     */
    function tostr(bytes) {
      var chars, r, i;
    
      r = [];
      for (i = 0; i < bytes.length; i++) {
        r.push(UIDCHARS[bytes[i] % UIDCHARS.length]);
      }
    
      return r.join('');
    }
    
    /**
     * Generate an Unique Id
     *
     * @param {Number} length  The number of chars of the uid
     * @param {Number} cb (optional)  Callback for async uid generation
     * @api public
     */
    
    function uid(length, cb) {
    
      if (typeof cb === 'undefined') {
        return tostr(crypto.pseudoRandomBytes(length));
      } else {
        crypto.pseudoRandomBytes(length, function(err, bytes) {
           if (err) return cb(err);
           cb(null, tostr(bytes));
        })
      }
    }
    
    /**
     * Exports
     */
    
    module.exports = uid;
    
  provide("uid2", module.exports);
}(global));

// pakmanager:utils-merge
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Merge object b with object a.
     *
     *     var a = { foo: 'bar' }
     *       , b = { bar: 'baz' };
     *
     *     merge(a, b);
     *     // => { foo: 'bar', bar: 'baz' }
     *
     * @param {Object} a
     * @param {Object} b
     * @return {Object}
     * @api public
     */
    
    exports = module.exports = function(a, b){
      if (a && b) {
        for (var key in b) {
          a[key] = b[key];
        }
      }
      return a;
    };
    
  provide("utils-merge", module.exports);
}(global));

// pakmanager:buffer-crc32
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var Buffer = require('buffer').Buffer;
    
    var CRC_TABLE = [
      0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419,
      0x706af48f, 0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4,
      0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07,
      0x90bf1d91, 0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de,
      0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7, 0x136c9856,
      0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
      0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4,
      0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
      0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3,
      0x45df5c75, 0xdcd60dcf, 0xabd13d59, 0x26d930ac, 0x51de003a,
      0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599,
      0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
      0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190,
      0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f,
      0x9fbfe4a5, 0xe8b8d433, 0x7807c9a2, 0x0f00f934, 0x9609a88e,
      0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
      0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed,
      0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
      0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3,
      0xfbd44c65, 0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2,
      0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a,
      0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5,
      0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa, 0xbe0b1010,
      0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
      0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17,
      0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6,
      0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615,
      0x73dc1683, 0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8,
      0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1, 0xf00f9344,
      0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
      0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a,
      0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
      0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1,
      0xa6bc5767, 0x3fb506dd, 0x48b2364b, 0xd80d2bda, 0xaf0a1b4c,
      0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef,
      0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
      0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe,
      0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31,
      0x2cd99e8b, 0x5bdeae1d, 0x9b64c2b0, 0xec63f226, 0x756aa39c,
      0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
      0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b,
      0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
      0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1,
      0x18b74777, 0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c,
      0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45, 0xa00ae278,
      0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7,
      0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc, 0x40df0b66,
      0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
      0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605,
      0xcdd70693, 0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8,
      0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b,
      0x2d02ef8d
    ];
    
    function bufferizeInt(num) {
      var tmp = Buffer(4);
      tmp.writeInt32BE(num, 0);
      return tmp;
    }
    
    function _crc32(buf, previous) {
      if (!Buffer.isBuffer(buf)) {
        buf = Buffer(buf);
      }
      if (Buffer.isBuffer(previous)) {
        previous = previous.readUInt32BE(0);
      }
      var crc = ~~previous ^ -1;
      for (var n = 0; n < buf.length; n++) {
        crc = CRC_TABLE[(crc ^ buf[n]) & 0xff] ^ (crc >>> 8);
      }
      return (crc ^ -1);
    }
    
    function crc32() {
      return bufferizeInt(_crc32.apply(null, arguments));
    }
    crc32.signed = function () {
      return _crc32.apply(null, arguments);
    };
    crc32.unsigned = function () {
      return _crc32.apply(null, arguments) >>> 0;
    };
    
    module.exports = crc32;
    
  provide("buffer-crc32", module.exports);
}(global));

// pakmanager:methods
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    var http = require('http');
    
    if (http.METHODS) {
      module.exports = http.METHODS.map(function(method){
        return method.toLowerCase();
      });
    
      return;
    }
    
    module.exports = [
      'get',
      'post',
      'put',
      'head',
      'delete',
      'options',
      'trace',
      'copy',
      'lock',
      'mkcol',
      'move',
      'propfind',
      'proppatch',
      'unlock',
      'report',
      'mkactivity',
      'checkout',
      'merge',
      'm-search',
      'notify',
      'subscribe',
      'unsubscribe',
      'patch',
      'search'
    ];
    
  provide("methods", module.exports);
}(global));

// pakmanager:batch
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    try {
      var EventEmitter = require('events').EventEmitter;
    } catch (err) {
      var Emitter = require('emitter');
    }
    
    /**
     * Noop.
     */
    
    function noop(){}
    
    /**
     * Expose `Batch`.
     */
    
    module.exports = Batch;
    
    /**
     * Create a new Batch.
     */
    
    function Batch() {
      if (!(this instanceof Batch)) return new Batch;
      this.fns = [];
      this.concurrency(Infinity);
      this.throws(true);
      for (var i = 0, len = arguments.length; i < len; ++i) {
        this.push(arguments[i]);
      }
    }
    
    /**
     * Inherit from `EventEmitter.prototype`.
     */
    
    if (EventEmitter) {
      Batch.prototype.__proto__ = EventEmitter.prototype;
    } else {
      Emitter(Batch.prototype);
    }
    
    /**
     * Set concurrency to `n`.
     *
     * @param {Number} n
     * @return {Batch}
     * @api public
     */
    
    Batch.prototype.concurrency = function(n){
      this.n = n;
      return this;
    };
    
    /**
     * Queue a function.
     *
     * @param {Function} fn
     * @return {Batch}
     * @api public
     */
    
    Batch.prototype.push = function(fn){
      this.fns.push(fn);
      return this;
    };
    
    /**
     * Set wether Batch will or will not throw up.
     *
     * @param  {Boolean} throws
     * @return {Batch}
     * @api public
     */
    Batch.prototype.throws = function(throws) {
      this.e = !!throws;
      return this;
    };
    
    /**
     * Execute all queued functions in parallel,
     * executing `cb(err, results)`.
     *
     * @param {Function} cb
     * @return {Batch}
     * @api public
     */
    
    Batch.prototype.end = function(cb){
      var self = this
        , total = this.fns.length
        , pending = total
        , results = []
        , errors = []
        , cb = cb || noop
        , fns = this.fns
        , max = this.n
        , throws = this.e
        , index = 0
        , done;
    
      // empty
      if (!fns.length) return cb(null, results);
    
      // process
      function next() {
        var i = index++;
        var fn = fns[i];
        if (!fn) return;
        var start = new Date;
    
        try {
          fn(callback);
        } catch (err) {
          callback(err);
        }
    
        function callback(err, res){
          if (done) return;
          if (err && throws) return done = true, cb(err);
          var complete = total - pending + 1;
          var end = new Date;
    
          results[i] = res;
          errors[i] = err;
    
          self.emit('progress', {
            index: i,
            value: res,
            error: err,
            pending: pending,
            total: total,
            complete: complete,
            percent: complete / total * 100 | 0,
            start: start,
            end: end,
            duration: end - start
          });
    
          if (--pending) next()
          else if(!throws) cb(errors, results);
          else cb(null, results);
        }
      }
    
      // concurrency
      for (var i = 0; i < fns.length; i++) {
        if (i == max) break;
        next();
      }
    
      return this;
    };
    
  provide("batch", module.exports);
}(global));

// pakmanager:send/lib/utils
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Return an ETag in the form of `"<size>-<mtime>"`
     * from the given `stat`.
     *
     * @param {Object} stat
     * @return {String}
     * @api private
     */
    
    exports.etag = function(stat) {
      return '"' + stat.size + '-' + Number(stat.mtime) + '"';
    };
    
    /**
     * decodeURIComponent.
     *
     * Allows V8 to only deoptimize this fn instead of all
     * of send().
     *
     * @param {String} path
     * @api private
     */
    
    exports.decode = function(path){
      try {
        return decodeURIComponent(path);
      } catch (err) {
        return -1;
      }
    };
    
    /**
     * Escape the given string of `html`.
     *
     * @param {String} html
     * @return {String}
     * @api private
     */
    
    exports.escape = function(html){
      return String(html)
        .replace(/&(?!\w+;)/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  provide("send/lib/utils", module.exports);
}(global));

// pakmanager:send/lib/send
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var debug = require('debug')('send')
      , parseRange = require('range-parser')
      , Stream = require('stream')
      , mime = require('mime')
      , fresh = require('fresh')
      , path = require('path')
      , http = require('http')
      , fs = require('fs')
      , basename = path.basename
      , normalize = path.normalize
      , join = path.join
      , utils =  require('send/lib/utils');
    
    /**
     * Expose `send`.
     */
    
    exports = module.exports = send;
    
    /**
     * Expose mime module.
     */
    
    exports.mime = mime;
    
    /**
     * Return a `SendStream` for `req` and `path`.
     *
     * @param {Request} req
     * @param {String} path
     * @param {Object} options
     * @return {SendStream}
     * @api public
     */
    
    function send(req, path, options) {
      return new SendStream(req, path, options);
    }
    
    /**
     * Initialize a `SendStream` with the given `path`.
     *
     * Events:
     *
     *  - `error` an error occurred
     *  - `stream` file streaming has started
     *  - `end` streaming has completed
     *  - `directory` a directory was requested
     *
     * @param {Request} req
     * @param {String} path
     * @param {Object} options
     * @api private
     */
    
    function SendStream(req, path, options) {
      var self = this;
      this.req = req;
      this.path = path;
      this.options = options || {};
      this.maxage(0);
      this.hidden(false);
      this.index('index.html');
    }
    
    /**
     * Inherits from `Stream.prototype`.
     */
    
    SendStream.prototype.__proto__ = Stream.prototype;
    
    /**
     * Enable or disable "hidden" (dot) files.
     *
     * @param {Boolean} path
     * @return {SendStream}
     * @api public
     */
    
    SendStream.prototype.hidden = function(val){
      debug('hidden %s', val);
      this._hidden = val;
      return this;
    };
    
    /**
     * Set index `path`, set to a falsy
     * value to disable index support.
     *
     * @param {String|Boolean} path
     * @return {SendStream}
     * @api public
     */
    
    SendStream.prototype.index = function(path){
      debug('index %s', path);
      this._index = path;
      return this;
    };
    
    /**
     * Set root `path`.
     *
     * @param {String} path
     * @return {SendStream}
     * @api public
     */
    
    SendStream.prototype.root = 
    SendStream.prototype.from = function(path){
      this._root = normalize(path);
      return this;
    };
    
    /**
     * Set max-age to `ms`.
     *
     * @param {Number} ms
     * @return {SendStream}
     * @api public
     */
    
    SendStream.prototype.maxage = function(ms){
      if (Infinity == ms) ms = 60 * 60 * 24 * 365 * 1000;
      debug('max-age %d', ms);
      this._maxage = ms;
      return this;
    };
    
    /**
     * Emit error with `status`.
     *
     * @param {Number} status
     * @api private
     */
    
    SendStream.prototype.error = function(status, err){
      var res = this.res;
      var msg = http.STATUS_CODES[status];
      err = err || new Error(msg);
      err.status = status;
      if (this.listeners('error').length) return this.emit('error', err);
      res.statusCode = err.status;
      res.end(msg);
    };
    
    /**
     * Check if the pathname is potentially malicious.
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.isMalicious = function(){
      return !this._root && ~this.path.indexOf('..');
    };
    
    /**
     * Check if the pathname ends with "/".
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.hasTrailingSlash = function(){
      return '/' == this.path[this.path.length - 1];
    };
    
    /**
     * Check if the basename leads with ".".
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.hasLeadingDot = function(){
      return '.' == basename(this.path)[0];
    };
    
    /**
     * Check if this is a conditional GET request.
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.isConditionalGET = function(){
      return this.req.headers['if-none-match']
        || this.req.headers['if-modified-since'];
    };
    
    /**
     * Strip content-* header fields.
     *
     * @api private
     */
    
    SendStream.prototype.removeContentHeaderFields = function(){
      var res = this.res;
      Object.keys(res._headers).forEach(function(field){
        if (0 == field.indexOf('content')) {
          res.removeHeader(field);
        }
      });
    };
    
    /**
     * Respond with 304 not modified.
     *
     * @api private
     */
    
    SendStream.prototype.notModified = function(){
      var res = this.res;
      debug('not modified');
      this.removeContentHeaderFields();
      res.statusCode = 304;
      res.end();
    };
    
    /**
     * Check if the request is cacheable, aka
     * responded with 2xx or 304 (see RFC 2616 section 14.2{5,6}).
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.isCachable = function(){
      var res = this.res;
      return (res.statusCode >= 200 && res.statusCode < 300) || 304 == res.statusCode;
    };
    
    /**
     * Handle stat() error.
     *
     * @param {Error} err
     * @api private
     */
    
    SendStream.prototype.onStatError = function(err){
      var notfound = ['ENOENT', 'ENAMETOOLONG', 'ENOTDIR'];
      if (~notfound.indexOf(err.code)) return this.error(404, err);
      this.error(500, err);
    };
    
    /**
     * Check if the cache is fresh.
     *
     * @return {Boolean}
     * @api private
     */
    
    SendStream.prototype.isFresh = function(){
      return fresh(this.req.headers, this.res._headers);
    };
    
    /**
     * Redirect to `path`.
     *
     * @param {String} path
     * @api private
     */
    
    SendStream.prototype.redirect = function(path){
      if (this.listeners('directory').length) return this.emit('directory');
      var res = this.res;
      path += '/';
      res.statusCode = 301;
      res.setHeader('Location', path);
      res.end('Redirecting to ' + utils.escape(path));
    };
    
    /**
     * Pipe to `res.
     *
     * @param {Stream} res
     * @return {Stream} res
     * @api public
     */
    
    SendStream.prototype.pipe = function(res){
      var self = this
        , args = arguments
        , path = this.path
        , root = this._root;
    
      // references
      this.res = res;
    
      // invalid request uri
      path = utils.decode(path);
      if (-1 == path) return this.error(400);
    
      // null byte(s)
      if (~path.indexOf('\0')) return this.error(400);
    
      // join / normalize from optional root dir
      if (root) path = normalize(join(this._root, path));
    
      // ".." is malicious without "root"
      if (this.isMalicious()) return this.error(403);
    
      // malicious path
      if (root && 0 != path.indexOf(root)) return this.error(403);
    
      // hidden file support
      if (!this._hidden && this.hasLeadingDot()) return this.error(404);
    
      // index file support
      if (this._index && this.hasTrailingSlash()) path += this._index;
    
      debug('stat "%s"', path);
      fs.stat(path, function(err, stat){
        if (err) return self.onStatError(err);
        if (stat.isDirectory()) return self.redirect(self.path);
        self.emit('file', path, stat);
        self.send(path, stat);
      });
    
      return res;
    };
    
    /**
     * Transfer `path`.
     *
     * @param {String} path
     * @api public
     */
    
    SendStream.prototype.send = function(path, stat){
      var options = this.options;
      var len = stat.size;
      var res = this.res;
      var req = this.req;
      var ranges = req.headers.range;
      var offset = options.start || 0;
    
      // set header fields
      this.setHeader(stat);
    
      // set content-type
      this.type(path);
    
      // conditional GET support
      if (this.isConditionalGET()
        && this.isCachable()
        && this.isFresh()) {
        return this.notModified();
      }
    
      // adjust len to start/end options
      len = Math.max(0, len - offset);
      if (options.end !== undefined) {
        var bytes = options.end - offset + 1;
        if (len > bytes) len = bytes;
      }
    
      // Range support
      if (ranges) {
        ranges = parseRange(len, ranges);
    
        // unsatisfiable
        if (-1 == ranges) {
          res.setHeader('Content-Range', 'bytes */' + stat.size);
          return this.error(416);
        }
    
        // valid (syntactically invalid ranges are treated as a regular response)
        if (-2 != ranges) {
          options.start = offset + ranges[0].start;
          options.end = offset + ranges[0].end;
    
          // Content-Range
          res.statusCode = 206;
          res.setHeader('Content-Range', 'bytes '
            + ranges[0].start
            + '-'
            + ranges[0].end
            + '/'
            + len);
          len = options.end - options.start + 1;
        }
      }
    
      // content-length
      res.setHeader('Content-Length', len);
    
      // HEAD support
      if ('HEAD' == req.method) return res.end();
    
      this.stream(path, options);
    };
    
    /**
     * Stream `path` to the response.
     *
     * @param {String} path
     * @param {Object} options
     * @api private
     */
    
    SendStream.prototype.stream = function(path, options){
      // TODO: this is all lame, refactor meeee
      var self = this;
      var res = this.res;
      var req = this.req;
    
      // pipe
      var stream = fs.createReadStream(path, options);
      this.emit('stream', stream);
      stream.pipe(res);
    
      // socket closed, done with the fd
      req.on('close', stream.destroy.bind(stream));
    
      // error handling code-smell
      stream.on('error', function(err){
        // no hope in responding
        if (res._header) {
          console.error(err.stack);
          req.destroy();
          return;
        }
    
        // 500
        err.status = 500;
        self.emit('error', err);
      });
    
      // end
      stream.on('end', function(){
        self.emit('end');
      });
    };
    
    /**
     * Set content-type based on `path`
     * if it hasn't been explicitly set.
     *
     * @param {String} path
     * @api private
     */
    
    SendStream.prototype.type = function(path){
      var res = this.res;
      if (res.getHeader('Content-Type')) return;
      var type = mime.lookup(path);
      var charset = mime.charsets.lookup(type);
      debug('content-type %s', type);
      res.setHeader('Content-Type', type + (charset ? '; charset=' + charset : ''));
    };
    
    /**
     * Set reaponse header fields, most
     * fields may be pre-defined.
     *
     * @param {Object} stat
     * @api private
     */
    
    SendStream.prototype.setHeader = function(stat){
      var res = this.res;
      if (!res.getHeader('Accept-Ranges')) res.setHeader('Accept-Ranges', 'bytes');
      if (!res.getHeader('ETag')) res.setHeader('ETag', utils.etag(stat));
      if (!res.getHeader('Date')) res.setHeader('Date', new Date().toUTCString());
      if (!res.getHeader('Cache-Control')) res.setHeader('Cache-Control', 'public, max-age=' + (this._maxage / 1000));
      if (!res.getHeader('Last-Modified')) res.setHeader('Last-Modified', stat.mtime.toUTCString());
    };
    
  provide("send/lib/send", module.exports);
}(global));

// pakmanager:send
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    module.exports =  require('send/lib/send');
  provide("send", module.exports);
}(global));

// pakmanager:stream-counter
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = ByteCounter;
    
    var Writable = require('readable-stream').Writable;
    var util = require('util');
    
    util.inherits(ByteCounter, Writable);
    function ByteCounter(options) {
      Writable.call(this, options);
      this.bytes = 0;
    }
    
    ByteCounter.prototype._write = function(chunk, encoding, cb) {
      this.bytes += chunk.length;
      this.emit('progress');
      cb();
    };
    
  provide("stream-counter", module.exports);
}(global));

// pakmanager:basic-auth-connect
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var http = require('http');
    
    /*!
     * Connect - basicAuth
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Basic Auth:
     *
     * Status: Deprecated. No bug reports or pull requests are welcomed
     * for this middleware. However, this middleware will not be removed.
     * Instead, you should use [basic-auth](https://github.com/visionmedia/node-basic-auth).
     *
     * Enfore basic authentication by providing a `callback(user, pass)`,
     * which must return `true` in order to gain access. Alternatively an async
     * method is provided as well, invoking `callback(user, pass, callback)`. Populates
     * `req.user`. The final alternative is simply passing username / password
     * strings.
     *
     *  Simple username and password
     *
     *     connect(connect.basicAuth('username', 'password'));
     *
     *  Callback verification
     *
     *     connect()
     *       .use(connect.basicAuth(function(user, pass){
     *         return 'tj' == user && 'wahoo' == pass;
     *       }))
     *
     *  Async callback verification, accepting `fn(err, user)`.
     *
     *     connect()
     *       .use(connect.basicAuth(function(user, pass, fn){
     *         User.authenticate({ user: user, pass: pass }, fn);
     *       }))
     *
     * @param {Function|String} callback or username
     * @param {String} realm
     * @api public
     */
    
    module.exports = function basicAuth(callback, realm) {
      var username, password;
    
      // user / pass strings
      if ('string' == typeof callback) {
        username = callback;
        password = realm;
        if ('string' != typeof password) throw new Error('password argument required');
        realm = arguments[2];
        callback = function(user, pass){
          return user == username && pass == password;
        }
      }
    
      realm = realm || 'Authorization Required';
    
      return function(req, res, next) {
        var authorization = req.headers.authorization;
    
        if (req.user) return next();
        if (!authorization) return unauthorized(res, realm);
    
        var parts = authorization.split(' ');
    
        if (parts.length !== 2) return next(error(400));
    
        var scheme = parts[0]
          , credentials = new Buffer(parts[1], 'base64').toString()
          , index = credentials.indexOf(':');
    
        if ('Basic' != scheme || index < 0) return next(error(400));
    
        var user = credentials.slice(0, index)
          , pass = credentials.slice(index + 1);
    
        // async
        if (callback.length >= 3) {
          callback(user, pass, function(err, user){
            if (err || !user)  return unauthorized(res, realm);
            req.user = req.remoteUser = user;
            next();
          });
        // sync
        } else {
          if (callback(user, pass)) {
            req.user = req.remoteUser = user;
            next();
          } else {
            unauthorized(res, realm);
          }
        }
      }
    };
    
    /**
     * Respond with 401 "Unauthorized".
     *
     * @param {ServerResponse} res
     * @param {String} realm
     * @api private
     */
    
    function unauthorized(res, realm) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="' + realm + '"');
      res.end('Unauthorized');
    };
    
    /**
     * Generate an `Error` from the given status `code`
     * and optional `msg`.
     *
     * @param {Number} code
     * @param {String} msg
     * @return {Error}
     * @api private
     */
    
    function error(code, msg){
      var err = new Error(msg || http.STATUS_CODES[code]);
      err.status = code;
      return err;
    };
  provide("basic-auth-connect", module.exports);
}(global));

// pakmanager:cookie-parser/lib/parse
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var signature = require('cookie-signature');
    
    /**
     * Parse signed cookies, returning an object
     * containing the decoded key/value pairs,
     * while removing the signed key from `obj`.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */
    
    exports.signedCookies = function(obj, secret){
      var ret = {};
      Object.keys(obj).forEach(function(key){
        var val = obj[key];
        if (0 == val.indexOf('s:')) {
          val = signature.unsign(val.slice(2), secret);
          if (val) {
            ret[key] = val;
            delete obj[key];
          }
        }
      });
      return ret;
    };
    
    /**
     * Parse JSON cookies.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */
    
    exports.JSONCookies = function(obj){
      Object.keys(obj).forEach(function(key){
        var val = obj[key];
        var res = exports.JSONCookie(val);
        if (res) obj[key] = res;
      });
      return obj;
    };
    
    /**
     * Parse JSON cookie string
     *
     * @param {String} str
     * @return {Object} Parsed object or null if not json cookie
     * @api private
     */
    
    exports.JSONCookie = function(str) {
      if (0 == str.indexOf('j:')) {
        try {
          return JSON.parse(str.slice(2));
        } catch (err) {
          // no op
        }
      }
    };
    
  provide("cookie-parser/lib/parse", module.exports);
}(global));

// pakmanager:cookie-parser
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var cookie = require('cookie');
    var parse =  require('cookie-parser/lib/parse');
    
    /**
     * Parse _Cookie_ header and populate `req.cookies`
     * with an object keyed by the cookie names. Optionally
     * you may enabled signed cookie support by passing
     * a `secret` string, which assigns `req.secret` so
     * it may be used by other middleware.
     *
     * Examples:
     *
     *     connect()
     *       .use(connect.cookieParser('optional secret string'))
     *       .use(function(req, res, next){
     *         res.end(JSON.stringify(req.cookies));
     *       })
     *
     * @param {String} secret
     * @return {Function}
     * @api public
     */
    
    module.exports = function cookieParser(secret, opt){
      return function cookieParser(req, res, next) {
        if (req.cookies) return next();
        var cookies = req.headers.cookie;
    
        req.secret = secret;
        req.cookies = {};
        req.signedCookies = {};
    
        if (cookies) {
          try {
            req.cookies = cookie.parse(cookies, opt);
            if (secret) {
              req.signedCookies = parse.signedCookies(req.cookies, secret);
              req.signedCookies = parse.JSONCookies(req.signedCookies);
            }
            req.cookies = parse.JSONCookies(req.cookies);
          } catch (err) {
            err.status = 400;
            return next(err);
          }
        }
        next();
      };
    };
    
  provide("cookie-parser", module.exports);
}(global));

// pakmanager:compression
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - compress
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var zlib = require('zlib');
    var bytes = require('bytes');
    var Negotiator = require('negotiator');
    var compressible = require('compressible');
    
    /**
     * Supported content-encoding methods.
     */
    
    exports.methods = {
        gzip: zlib.createGzip
      , deflate: zlib.createDeflate
    };
    
    /**
     * Default filter function.
     */
    
    exports.filter = function(req, res){
      return compressible(res.getHeader('Content-Type'));
    };
    
    /**
     * Compress:
     *
     * Compress response data with gzip/deflate.
     *
     * Filter:
     *
     *  A `filter` callback function may be passed to
     *  replace the default logic of:
     *
     *     exports.filter = function(req, res){
     *       return compressible(res.getHeader('Content-Type'));
     *     };
     *
     * Threshold:
     *
     *  Only compress the response if the byte size is at or above a threshold.
     *  Always compress while streaming.
     *
     *   - `threshold` - string representation of size or bytes as an integer.
     *
     * Options:
     *
     *  All remaining options are passed to the gzip/deflate
     *  creation functions. Consult node's docs for additional details.
     *
     *   - `chunkSize` (default: 16*1024)
     *   - `windowBits`
     *   - `level`: 0-9 where 0 is no compression, and 9 is slow but best compression
     *   - `memLevel`: 1-9 low is slower but uses less memory, high is fast but uses more
     *   - `strategy`: compression strategy
     *
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    module.exports = function compress(options) {
      options = options || {};
      var filter = options.filter || exports.filter;
      var threshold;
    
      if (false === options.threshold || 0 === options.threshold) {
        threshold = 0
      } else if ('string' === typeof options.threshold) {
        threshold = bytes(options.threshold)
      } else {
        threshold = options.threshold || 1024
      }
    
      return function compress(req, res, next){
        var accept = req.headers['accept-encoding']
          , writeHead = res.writeHead
          , write = res.write
          , end = res.end
          , compress = true
          , stream;
    
        // see #724
        req.on('close', function(){
          res.write = res.end = function(){};
        });
    
        // flush is noop by default
        res.flush = noop;
    
        // proxy
    
        res.write = function(chunk, encoding){
          if (!this.headersSent) {
            // if content-length is set and is lower
            // than the threshold, don't compress
            var length = res.getHeader('content-length');
            if (!isNaN(length) && length < threshold) compress = false;
            this._implicitHeader();
          }
          return stream
            ? stream.write(new Buffer(chunk, encoding))
            : write.call(res, chunk, encoding);
        };
    
        res.end = function(chunk, encoding){
          if (chunk) {
            if (!this.headersSent && getSize(chunk) < threshold) compress = false;
            this.write(chunk, encoding);
          } else if (!this.headersSent) {
            // response size === 0
            compress = false;
          }
          return stream
            ? stream.end()
            : end.call(res);
        };
    
        res.writeHead = function(){
          // default request filter
          if (!filter(req, res)) return writeHead.apply(res, arguments);
    
          // vary
          var vary = res.getHeader('Vary');
          if (!vary) {
            res.setHeader('Vary', 'Accept-Encoding');
          } else if (!~vary.indexOf('Accept-Encoding')) {
            res.setHeader('Vary', vary + ', Accept-Encoding');
          }
    
          if (!compress) return writeHead.apply(res, arguments);
    
          var encoding = res.getHeader('Content-Encoding') || 'identity';
    
          // already encoded
          if ('identity' != encoding) return writeHead.apply(res, arguments);
    
          // SHOULD use identity
          if (!accept) return writeHead.apply(res, arguments);
    
          // head
          if ('HEAD' == req.method) return writeHead.apply(res, arguments);
    
          // compression method
          var method = new Negotiator(req).preferredEncoding(['gzip', 'deflate', 'identity']);
          // negotiation failed
          if (!method || method === 'identity') return writeHead.apply(res, arguments);
    
          // compression stream
          stream = exports.methods[method](options);
    
          // overwrite the flush method
          res.flush = function(){
            stream.flush();
          }
    
          // header fields
          res.setHeader('Content-Encoding', method);
          res.removeHeader('Content-Length');
    
          // compression
          stream.on('data', function(chunk){
            write.call(res, chunk);
          });
    
          stream.on('end', function(){
            end.call(res);
          });
    
          stream.on('drain', function() {
            res.emit('drain');
          });
    
          writeHead.apply(res, arguments);
        };
    
        next();
      };
    };
    
    function getSize(chunk) {
      return Buffer.isBuffer(chunk)
        ? chunk.length
        : Buffer.byteLength(chunk);
    }
    
    function noop(){}
    
  provide("compression", module.exports);
}(global));

// pakmanager:connect-timeout
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - timeout
     * Ported from https://github.com/LearnBoost/connect-timeout
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var debug = require('debug')('connect:timeout');
    
    /**
     * Timeout:
     *
     * Times out the request in `ms`, defaulting to `5000`. The
     * method `req.clearTimeout()` is added to revert this behaviour
     * programmatically within your application's middleware, routes, etc.
     *
     * The timeout error is passed to `next()` so that you may customize
     * the response behaviour. This error has the `.timeout` property as
     * well as `.status == 503`.
     *
     * @param {Number} ms
     * @return {Function}
     * @api public
     */
    
    module.exports = function timeout(ms) {
      ms = ms || 5000;
    
      return function(req, res, next) {
        var id = setTimeout(function(){
          req.emit('timeout', ms);
        }, ms);
    
        req.on('timeout', function(){
          if (res.headersSent) return debug('response started, cannot timeout');
          var err = new Error('Response timeout');
          err.timeout = ms;
          err.status = 503;
          next(err);
        });
    
        req.clearTimeout = function(){
          clearTimeout(id);
        };
    
        var writeHead = res.writeHead;
        res.writeHead = function(){
          clearTimeout(id);
          writeHead.apply(res, arguments);
        }
    
        next();
      };
    };
    
  provide("connect-timeout", module.exports);
}(global));

// pakmanager:csurf
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - csrf
     * Copyright(c) 2011 Sencha Inc.
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var uid = require('uid2');
    var crypto = require('crypto');
    
    /**
     * Anti CSRF:
     *
     * CSRF protection middleware.
     *
     * This middleware adds a `req.csrfToken()` function to make a token
     * which should be added to requests which mutate
     * state, within a hidden form field, query-string etc. This
     * token is validated against the visitor's session.
     *
     * The default `value` function checks `req.body` generated
     * by the `bodyParser()` middleware, `req.query` generated
     * by `query()`, and the "X-CSRF-Token" header field.
     *
     * This middleware requires session support, thus should be added
     * somewhere _below_ `session()` and `cookieParser()`.
     *
     * Options:
     *
     *    - `value` a function accepting the request, returning the token
     *
     * @param {Object} options
     * @api public
     */
    
    module.exports = function csrf(options) {
      options = options || {};
      var value = options.value || defaultValue;
    
      return function(req, res, next){
    
        // already have one
        var secret = req.session.csrfSecret;
        if (secret) return createToken(secret);
    
        // generate secret
        uid(24, function(err, secret){
          if (err) return next(err);
          req.session.csrfSecret = secret;
          createToken(secret);
        });
    
        // generate the token
        function createToken(secret) {
          var token;
    
          // lazy-load token
          req.csrfToken = function csrfToken() {
            return token || (token = saltedToken(secret));
          };
    
          // ignore these methods
          if ('GET' == req.method || 'HEAD' == req.method || 'OPTIONS' == req.method) return next();
    
          // determine user-submitted value
          var val = value(req);
    
          // check
          if (!checkToken(val, secret)) {
            var err = new Error('invalid csrf token');
            err.status = 403;
            next(err);
            return;
          }
    
          next();
        }
      }
    };
    
    /**
     * Default value function, checking the `req.body`
     * and `req.query` for the CSRF token.
     *
     * @param {IncomingMessage} req
     * @return {String}
     * @api private
     */
    
    function defaultValue(req) {
      return (req.body && req.body._csrf)
        || (req.query && req.query._csrf)
        || (req.headers['x-csrf-token'])
        || (req.headers['x-xsrf-token']);
    }
    
    /**
     * Return salted token.
     *
     * @param {String} secret
     * @return {String}
     * @api private
     */
    
    function saltedToken(secret) {
      return createToken(generateSalt(10), secret);
    }
    
    /**
     * Creates a CSRF token from a given salt and secret.
     *
     * @param {String} salt (should be 10 characters)
     * @param {String} secret
     * @return {String}
     * @api private
     */
    
    function createToken(salt, secret) {
      return salt + crypto
        .createHash('sha1')
        .update(salt + secret)
        .digest('base64');
    }
    
    /**
     * Checks if a given CSRF token matches the given secret.
     *
     * @param {String} token
     * @param {String} secret
     * @return {Boolean}
     * @api private
     */
    
    function checkToken(token, secret) {
      if ('string' != typeof token) return false;
      return token === createToken(token.slice(0, 10), secret);
    }
    
    /**
     * Generates a random salt, using a fast non-blocking PRNG (Math.random()).
     *
     * @param {Number} length
     * @return {String}
     * @api private
     */
    
    function generateSalt(length) {
      var i, r = [];
      for (i = 0; i < length; ++i) {
        r.push(SALTCHARS[Math.floor(Math.random() * SALTCHARS.length)]);
      }
      return r.join('');
    }
    
    var SALTCHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    
  provide("csurf", module.exports);
}(global));

// pakmanager:errorhandler
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - errorHandler
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var fs;
    try {
      fs = require('graceful-fs');
    } catch (_) {
      fs = require('fs');
    }
    
    // environment
    
    var env = process.env.NODE_ENV || 'development';
    
    /**
     * Error handler:
     *
     * Development error handler, providing stack traces
     * and error message responses for requests accepting text, html,
     * or json.
     *
     * Text:
     *
     *   By default, and when _text/plain_ is accepted a simple stack trace
     *   or error message will be returned.
     *
     * JSON:
     *
     *   When _application/json_ is accepted, connect will respond with
     *   an object in the form of `{ "error": error }`.
     *
     * HTML:
     *
     *   When accepted connect will output a nice html stack trace.
     *
     * @return {Function}
     * @api public
     */
    
    exports = module.exports = function errorHandler(){
      return function errorHandler(err, req, res, next){
        if (err.status) res.statusCode = err.status;
        if (res.statusCode < 400) res.statusCode = 500;
        if ('test' != env) console.error(err.stack);
        var accept = req.headers.accept || '';
        // html
        if (~accept.indexOf('html')) {
          fs.readFile(__dirname + '/public/style.css', 'utf8', function(e, style){
            fs.readFile(__dirname + '/public/error.html', 'utf8', function(e, html){
              var stack = (err.stack || '')
                .split('\n').slice(1)
                .map(function(v){ return '<li>' + v + '</li>'; }).join('');
                html = html
                  .replace('{style}', style)
                  .replace('{stack}', stack)
                  .replace('{title}', exports.title)
                  .replace('{statusCode}', res.statusCode)
                  .replace(/\{error\}/g, escapeHTML(err.toString().replace(/\n/g, '<br/>')));
                res.setHeader('Content-Type', 'text/html; charset=utf-8');
                res.end(html);
            });
          });
        // json
        } else if (~accept.indexOf('json')) {
          var error = { message: err.message, stack: err.stack };
          for (var prop in err) error[prop] = err[prop];
          var json = JSON.stringify({ error: error });
          res.setHeader('Content-Type', 'application/json');
          res.end(json);
        // plain text
        } else {
          res.setHeader('Content-Type', 'text/plain');
          res.end(err.stack);
        }
      };
    };
    
    /**
     * Template title, framework authors may override this value.
     */
    
    exports.title = 'Connect';
    
    
    /**
     * Escape the given string of `html`.
     *
     * @param {String} html
     * @return {String}
     * @api private
     */
    
    function escapeHTML(html){
      return String(html)
        .replace(/&(?!\w+;)/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
  provide("errorhandler", module.exports);
}(global));

// pakmanager:express-session/session/session
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - session - Session
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var merge = require('utils-merge')
    
    /**
     * Create a new `Session` with the given request and `data`.
     *
     * @param {IncomingRequest} req
     * @param {Object} data
     * @api private
     */
    
    var Session = module.exports = function Session(req, data) {
      Object.defineProperty(this, 'req', { value: req });
      Object.defineProperty(this, 'id', { value: req.sessionID });
      if ('object' == typeof data) merge(this, data);
    };
    
    /**
     * Update reset `.cookie.maxAge` to prevent
     * the cookie from expiring when the
     * session is still active.
     *
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.touch = function(){
      return this.resetMaxAge();
    };
    
    /**
     * Reset `.maxAge` to `.originalMaxAge`.
     *
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.resetMaxAge = function(){
      this.cookie.maxAge = this.cookie.originalMaxAge;
      return this;
    };
    
    /**
     * Save the session data with optional callback `fn(err)`.
     *
     * @param {Function} fn
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.save = function(fn){
      this.req.sessionStore.set(this.id, this, fn || function(){});
      return this;
    };
    
    /**
     * Re-loads the session data _without_ altering
     * the maxAge properties. Invokes the callback `fn(err)`,
     * after which time if no exception has occurred the
     * `req.session` property will be a new `Session` object,
     * although representing the same session.
     *
     * @param {Function} fn
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.reload = function(fn){
      var req = this.req
        , store = this.req.sessionStore;
      store.get(this.id, function(err, sess){
        if (err) return fn(err);
        if (!sess) return fn(new Error('failed to load session'));
        store.createSession(req, sess);
        fn();
      });
      return this;
    };
    
    /**
     * Destroy `this` session.
     *
     * @param {Function} fn
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.destroy = function(fn){
      delete this.req.session;
      this.req.sessionStore.destroy(this.id, fn);
      return this;
    };
    
    /**
     * Regenerate this request's session.
     *
     * @param {Function} fn
     * @return {Session} for chaining
     * @api public
     */
    
    Session.prototype.regenerate = function(fn){
      this.req.sessionStore.regenerate(this.req, fn);
      return this;
    };
    
  provide("express-session/session/session", module.exports);
}(global));

// pakmanager:express-session/session/cookie
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - session - Cookie
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var merge = require('utils-merge')
      , cookie = require('cookie');
    
    /**
     * Initialize a new `Cookie` with the given `options`.
     *
     * @param {IncomingMessage} req
     * @param {Object} options
     * @api private
     */
    
    var Cookie = module.exports = function Cookie(options) {
      this.path = '/';
      this.maxAge = null;
      this.httpOnly = true;
      if (options) merge(this, options);
      this.originalMaxAge = undefined == this.originalMaxAge
        ? this.maxAge
        : this.originalMaxAge;
    };
    
    /*!
     * Prototype.
     */
    
    Cookie.prototype = {
    
      /**
       * Set expires `date`.
       *
       * @param {Date} date
       * @api public
       */
    
      set expires(date) {
        this._expires = date;
        this.originalMaxAge = this.maxAge;
      },
    
      /**
       * Get expires `date`.
       *
       * @return {Date}
       * @api public
       */
    
      get expires() {
        return this._expires;
      },
    
      /**
       * Set expires via max-age in `ms`.
       *
       * @param {Number} ms
       * @api public
       */
    
      set maxAge(ms) {
        this.expires = 'number' == typeof ms
          ? new Date(Date.now() + ms)
          : ms;
      },
    
      /**
       * Get expires max-age in `ms`.
       *
       * @return {Number}
       * @api public
       */
    
      get maxAge() {
        return this.expires instanceof Date
          ? this.expires.valueOf() - Date.now()
          : this.expires;
      },
    
      /**
       * Return cookie data object.
       *
       * @return {Object}
       * @api private
       */
    
      get data() {
        return {
            originalMaxAge: this.originalMaxAge
          , expires: this._expires
          , secure: this.secure
          , httpOnly: this.httpOnly
          , domain: this.domain
          , path: this.path
        }
      },
    
      /**
       * Return a serialized cookie string.
       *
       * @return {String}
       * @api public
       */
    
      serialize: function(name, val){
        return cookie.serialize(name, val, this.data);
      },
    
      /**
       * Return JSON representation of this cookie.
       *
       * @return {Object}
       * @api private
       */
    
      toJSON: function(){
        return this.data;
      }
    };
    
  provide("express-session/session/cookie", module.exports);
}(global));

// pakmanager:express-session/session/store
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - session - Store
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var EventEmitter = require('events').EventEmitter
      , Session =  require('express-session/session/session')
      , Cookie =  require('express-session/session/cookie');
    
    /**
     * Initialize abstract `Store`.
     *
     * @api private
     */
    
    var Store = module.exports = function Store(options){};
    
    /**
     * Inherit from `EventEmitter.prototype`.
     */
    
    Store.prototype.__proto__ = EventEmitter.prototype;
    
    /**
     * Re-generate the given requests's session.
     *
     * @param {IncomingRequest} req
     * @return {Function} fn
     * @api public
     */
    
    Store.prototype.regenerate = function(req, fn){
      var self = this;
      this.destroy(req.sessionID, function(err){
        self.generate(req);
        fn(err);
      });
    };
    
    /**
     * Load a `Session` instance via the given `sid`
     * and invoke the callback `fn(err, sess)`.
     *
     * @param {String} sid
     * @param {Function} fn
     * @api public
     */
    
    Store.prototype.load = function(sid, fn){
      var self = this;
      this.get(sid, function(err, sess){
        if (err) return fn(err);
        if (!sess) return fn();
        var req = { sessionID: sid, sessionStore: self };
        sess = self.createSession(req, sess);
        fn(null, sess);
      });
    };
    
    /**
     * Create session from JSON `sess` data.
     *
     * @param {IncomingRequest} req
     * @param {Object} sess
     * @return {Session}
     * @api private
     */
    
    Store.prototype.createSession = function(req, sess){
      var expires = sess.cookie.expires
        , orig = sess.cookie.originalMaxAge;
      sess.cookie = new Cookie(sess.cookie);
      if ('string' == typeof expires) sess.cookie.expires = new Date(expires);
      sess.cookie.originalMaxAge = orig;
      req.session = new Session(req, sess);
      return req.session;
    };
    
  provide("express-session/session/store", module.exports);
}(global));

// pakmanager:express-session/session/memory
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - session - MemoryStore
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var Store =  require('express-session/session/store');
    
    /**
     * Initialize a new `MemoryStore`.
     *
     * @api public
     */
    
    var MemoryStore = module.exports = function MemoryStore() {
      this.sessions = {};
    };
    
    /**
     * Inherit from `Store.prototype`.
     */
    
    MemoryStore.prototype.__proto__ = Store.prototype;
    
    /**
     * Attempt to fetch session by the given `sid`.
     *
     * @param {String} sid
     * @param {Function} fn
     * @api public
     */
    
    MemoryStore.prototype.get = function(sid, fn){
      var self = this;
      setImmediate(function(){
        var expires
          , sess = self.sessions[sid];
        if (sess) {
          sess = JSON.parse(sess);
          expires = 'string' == typeof sess.cookie.expires
            ? new Date(sess.cookie.expires)
            : sess.cookie.expires;
          if (!expires || new Date < expires) {
            fn(null, sess);
          } else {
            self.destroy(sid, fn);
          }
        } else {
          fn();
        }
      });
    };
    
    /**
     * Commit the given `sess` object associated with the given `sid`.
     *
     * @param {String} sid
     * @param {Session} sess
     * @param {Function} fn
     * @api public
     */
    
    MemoryStore.prototype.set = function(sid, sess, fn){
      var self = this;
      setImmediate(function(){
        self.sessions[sid] = JSON.stringify(sess);
        fn && fn();
      });
    };
    
    /**
     * Destroy the session associated with the given `sid`.
     *
     * @param {String} sid
     * @api public
     */
    
    MemoryStore.prototype.destroy = function(sid, fn){
      var self = this;
      setImmediate(function(){
        delete self.sessions[sid];
        fn && fn();
      });
    };
    
    /**
     * Invoke the given callback `fn` with all active sessions.
     *
     * @param {Function} fn
     * @api public
     */
    
    MemoryStore.prototype.all = function(fn){
      var arr = []
        , keys = Object.keys(this.sessions);
      for (var i = 0, len = keys.length; i < len; ++i) {
        arr.push(this.sessions[keys[i]]);
      }
      fn(null, arr);
    };
    
    /**
     * Clear all sessions.
     *
     * @param {Function} fn
     * @api public
     */
    
    MemoryStore.prototype.clear = function(fn){
      this.sessions = {};
      fn && fn();
    };
    
    /**
     * Fetch number of sessions.
     *
     * @param {Function} fn
     * @api public
     */
    
    MemoryStore.prototype.length = function(fn){
      fn(null, Object.keys(this.sessions).length);
    };
    
  provide("express-session/session/memory", module.exports);
}(global));

// pakmanager:express-session
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - session
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var uid = require('uid2')
      , crc32 = require('buffer-crc32')
      , parse = require('url').parse
      , signature = require('cookie-signature')
      , debug = require('debug')('session')
    
    var Session =  require('express-session/session/session')
      , MemoryStore =  require('express-session/session/memory')
      , Cookie =  require('express-session/session/cookie')
      , Store =  require('express-session/session/store')
    
    // environment
    
    var env = process.env.NODE_ENV;
    
    /**
     * Expose the middleware.
     */
    
    exports = module.exports = session;
    
    /**
     * Expose constructors.
     */
    
    exports.Store = Store;
    exports.Cookie = Cookie;
    exports.Session = Session;
    exports.MemoryStore = MemoryStore;
    
    /**
     * Warning message for `MemoryStore` usage in production.
     */
    
    var warning = 'Warning: connect.session() MemoryStore is not\n'
      + 'designed for a production environment, as it will leak\n'
      + 'memory, and will not scale past a single process.';
    
    /**
     * Session:
     *
     *   Setup session store with the given `options`.
     *
     *   Session data is _not_ saved in the cookie itself, however
     *   cookies are used, so we must use the [cookieParser()](cookieParser.html)
     *   middleware _before_ `session()`.
     *
     * Examples:
     *
     *     connect()
     *       .use(connect.cookieParser())
     *       .use(connect.session({ secret: 'keyboard cat', key: 'sid', cookie: { secure: true }}))
     *
     * Options:
     *
     *   - `key` cookie name defaulting to `connect.sid`
     *   - `store` session store instance
     *   - `secret` session cookie is signed with this secret to prevent tampering
     *   - `cookie` session cookie settings, defaulting to `{ path: '/', httpOnly: true, maxAge: null }`
     *   - `proxy` trust the reverse proxy when setting secure cookies (via "x-forwarded-proto")
     *
     * Cookie option:
     *
     *  By default `cookie.maxAge` is `null`, meaning no "expires" parameter is set
     *  so the cookie becomes a browser-session cookie. When the user closes the
     *  browser the cookie (and session) will be removed.
     *
     * ## req.session
     *
     *  To store or access session data, simply use the request property `req.session`,
     *  which is (generally) serialized as JSON by the store, so nested objects
     *  are typically fine. For example below is a user-specific view counter:
     *
     *       connect()
     *         .use(connect.favicon())
     *         .use(connect.cookieParser())
     *         .use(connect.session({ secret: 'keyboard cat', cookie: { maxAge: 60000 }}))
     *         .use(function(req, res, next){
     *           var sess = req.session;
     *           if (sess.views) {
     *             res.setHeader('Content-Type', 'text/html');
     *             res.write('<p>views: ' + sess.views + '</p>');
     *             res.write('<p>expires in: ' + (sess.cookie.maxAge / 1000) + 's</p>');
     *             res.end();
     *             sess.views++;
     *           } else {
     *             sess.views = 1;
     *             res.end('welcome to the session demo. refresh!');
     *           }
     *         }
     *       )).listen(3000);
     *
     * ## Session#regenerate()
     *
     *  To regenerate the session simply invoke the method, once complete
     *  a new SID and `Session` instance will be initialized at `req.session`.
     *
     *      req.session.regenerate(function(err){
     *        // will have a new session here
     *      });
     *
     * ## Session#destroy()
     *
     *  Destroys the session, removing `req.session`, will be re-generated next request.
     *
     *      req.session.destroy(function(err){
     *        // cannot access session here
     *      });
     *
     * ## Session#reload()
     *
     *  Reloads the session data.
     *
     *      req.session.reload(function(err){
     *        // session updated
     *      });
     *
     * ## Session#save()
     *
     *  Save the session.
     *
     *      req.session.save(function(err){
     *        // session saved
     *      });
     *
     * ## Session#touch()
     *
     *   Updates the `.maxAge` property. Typically this is
     *   not necessary to call, as the session middleware does this for you.
     *
     * ## Session#cookie
     *
     *  Each session has a unique cookie object accompany it. This allows
     *  you to alter the session cookie per visitor. For example we can
     *  set `req.session.cookie.expires` to `false` to enable the cookie
     *  to remain for only the duration of the user-agent.
     *
     * ## Session#maxAge
     *
     *  Alternatively `req.session.cookie.maxAge` will return the time
     *  remaining in milliseconds, which we may also re-assign a new value
     *  to adjust the `.expires` property appropriately. The following
     *  are essentially equivalent
     *
     *     var hour = 3600000;
     *     req.session.cookie.expires = new Date(Date.now() + hour);
     *     req.session.cookie.maxAge = hour;
     *
     * For example when `maxAge` is set to `60000` (one minute), and 30 seconds
     * has elapsed it will return `30000` until the current request has completed,
     * at which time `req.session.touch()` is called to reset `req.session.maxAge`
     * to its original value.
     *
     *     req.session.cookie.maxAge;
     *     // => 30000
     *
     * Session Store Implementation:
     *
     * Every session store _must_ implement the following methods
     *
     *    - `.get(sid, callback)`
     *    - `.set(sid, session, callback)`
     *    - `.destroy(sid, callback)`
     *
     * Recommended methods include, but are not limited to:
     *
     *    - `.length(callback)`
     *    - `.clear(callback)`
     *
     * For an example implementation view the [connect-redis](http://github.com/visionmedia/connect-redis) repo.
     *
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    function session(options){
      var options = options || {}
        , key = options.key || 'connect.sid'
        , store = options.store || new MemoryStore
        , cookie = options.cookie || {}
        , trustProxy = options.proxy
        , storeReady = true
        , rollingSessions = options.rolling || false;
    
      // notify user that this store is not
      // meant for a production environment
      if ('production' == env && store instanceof MemoryStore) {
        console.warn(warning);
      }
    
      // generates the new session
      store.generate = function(req){
        req.sessionID = uid(24);
        req.session = new Session(req);
        req.session.cookie = new Cookie(cookie);
      };
    
      store.on('disconnect', function(){ storeReady = false; });
      store.on('connect', function(){ storeReady = true; });
    
      return function session(req, res, next) {
        // self-awareness
        if (req.session) return next();
    
        // Handle connection as if there is no session if
        // the store has temporarily disconnected etc
        if (!storeReady) return debug('store is disconnected'), next();
    
        // pathname mismatch
        var originalPath = parse(req.originalUrl).pathname;
        if (0 != originalPath.indexOf(cookie.path || '/')) return next();
    
        // backwards compatibility for signed cookies
        // req.secret is passed from the cookie parser middleware
        var secret = options.secret || req.secret;
    
        // ensure secret is available or bail
        if (!secret) throw new Error('`secret` option required for sessions');
    
        var originalHash
          , originalId;
    
        // expose store
        req.sessionStore = store;
    
        // grab the session cookie value and check the signature
        var rawCookie = req.cookies[key];
    
        // get signedCookies for backwards compat with signed cookies
        var unsignedCookie = req.signedCookies[key];
    
        if (!unsignedCookie && rawCookie) {
          unsignedCookie = (0 == rawCookie.indexOf('s:'))
            ? signature.unsign(rawCookie.slice(2), secret)
            : rawCookie;
        }
    
        // set-cookie
        var writeHead = res.writeHead;
        res.writeHead = function(){
          if (!req.session) {
            debug('no session');
            writeHead.apply(res, arguments);
            return;
          }
    
          var cookie = req.session.cookie
            , proto = (req.headers['x-forwarded-proto'] || '').split(',')[0].toLowerCase().trim()
            , tls = req.connection.encrypted || (trustProxy && 'https' == proto)
            , isNew = unsignedCookie != req.sessionID;
    
          // only send secure cookies via https
          if (cookie.secure && !tls) {
            debug('not secured');
            writeHead.apply(res, arguments);
            return;
          }
    
          // in case of rolling session, always reset the cookie
          if (!rollingSessions) {
    
            // browser-session length cookie
            if (null == cookie.expires) {
              if (!isNew) {
                debug('already set browser-session cookie');
                writeHead.apply(res, arguments);
                return
              }
            // compare hashes and ids
            } else if (originalHash == hash(req.session) && originalId == req.session.id) {
              debug('unmodified session');
              writeHead.apply(res, arguments);
              return
            }
    
          }
    
          var val = 's:' + signature.sign(req.sessionID, secret);
          val = cookie.serialize(key, val);
          debug('set-cookie %s', val);
          res.setHeader('Set-Cookie', val);
          writeHead.apply(res, arguments);
        };
    
        // proxy end() to commit the session
        var end = res.end;
        res.end = function(data, encoding){
          res.end = end;
          if (!req.session) return res.end(data, encoding);
          debug('saving');
          req.session.resetMaxAge();
          req.session.save(function(err){
            if (err) console.error(err.stack);
            debug('saved');
            res.end(data, encoding);
          });
        };
    
        // generate the session
        function generate() {
          store.generate(req);
        }
    
        // get the sessionID from the cookie
        req.sessionID = unsignedCookie;
    
        // generate a session if the browser doesn't send a sessionID
        if (!req.sessionID) {
          debug('no SID sent, generating session');
          generate();
          next();
          return;
        }
    
        // generate the session object
        debug('fetching %s', req.sessionID);
        store.get(req.sessionID, function(err, sess){
          // error handling
          if (err) {
            debug('error %j', err);
            if ('ENOENT' == err.code) {
              generate();
              next();
            } else {
              next(err);
            }
          // no session
          } else if (!sess) {
            debug('no session found');
            generate();
            next();
          // populate req.session
          } else {
            debug('session found');
            store.createSession(req, sess);
            originalId = req.sessionID;
            originalHash = hash(sess);
            next();
          }
        });
      };
    };
    
    /**
     * Hash the given `sess` object omitting changes
     * to `.cookie`.
     *
     * @param {Object} sess
     * @return {String}
     * @api private
     */
    
    function hash(sess) {
      return crc32.signed(JSON.stringify(sess, function(key, val){
        if ('cookie' != key) return val;
      }));
    }
    
  provide("express-session", module.exports);
}(global));

// pakmanager:method-override
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - methodOverride
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var methods = require('methods');
    
    /**
     * Method Override:
     *
     * Provides faux HTTP method support.
     *
     * Pass an optional `key` to use when checking for
     * a method override, otherwise defaults to _\_method_.
     * The original method is available via `req.originalMethod`.
     *
     * @param {String} key
     * @return {Function}
     * @api public
     */
    
    module.exports = function methodOverride(key){
      key = key || "_method";
      return function methodOverride(req, res, next) {
        var method;
        req.originalMethod = req.originalMethod || req.method;
    
        // req.body
        if (req.body && typeof req.body === 'object' && key in req.body) {
          method = req.body[key].toLowerCase();
          delete req.body[key];
        }
    
        // check X-HTTP-Method-Override
        if (req.headers['x-http-method-override']) {
          method = req.headers['x-http-method-override'].toLowerCase();
        }
    
        // replace
        if (supports(method)) req.method = method.toUpperCase();
    
        next();
      };
    };
    
    /**
     * Check if node supports `method`.
     */
    
    function supports(method) {
      return ~methods.indexOf(method);
    }
    
  provide("method-override", module.exports);
}(global));

// pakmanager:morgan
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - logger
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var bytes = require('bytes');
    
    /*!
     * Log buffer.
     */
    
    var buf = [];
    
    /*!
     * Default log buffer duration.
     */
    
    var defaultBufferDuration = 1000;
    
    /**
     * Logger:
     *
     * Log requests with the given `options` or a `format` string.
     *
     * Options:
     *
     *   - `format`  Format string, see below for tokens
     *   - `stream`  Output stream, defaults to _stdout_
     *   - `buffer`  Buffer duration, defaults to 1000ms when _true_
     *   - `immediate`  Write log line on request instead of response (for response times)
     *   - `skip`    Function to determine if logging is skipped, called as
     *               `skip(req, res)`, defaults to always false.
     *
     * Tokens:
     *
     *   - `:req[header]` ex: `:req[Accept]`
     *   - `:res[header]` ex: `:res[Content-Length]`
     *   - `:http-version`
     *   - `:response-time`
     *   - `:remote-addr`
     *   - `:date`
     *   - `:method`
     *   - `:url`
     *   - `:referrer`
     *   - `:user-agent`
     *   - `:status`
     *
     * Formats:
     *
     *   Pre-defined formats that ship with connect:
     *
     *    - `default` ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
     *    - `short` ':remote-addr - :method :url HTTP/:http-version :status :res[content-length] - :response-time ms'
     *    - `tiny`  ':method :url :status :res[content-length] - :response-time ms'
     *    - `dev` concise output colored by response status for development use
     *
     * Examples:
     *
     *      connect.logger() // default
     *      connect.logger('short')
     *      connect.logger('tiny')
     *      connect.logger({ immediate: true, format: 'dev' })
     *      connect.logger(':method :url - :referrer')
     *      connect.logger(':req[content-type] -> :res[content-type]')
     *      connect.logger(function(tokens, req, res){ return 'some format string' })
     *      connect.logger({ format: 'dev', skip: function(req, res){ return res.statusCode === 304; }})
     *
     * Defining Tokens:
     *
     *   To define a token, simply invoke `connect.logger.token()` with the
     *   name and a callback function. The value returned is then available
     *   as ":type" in this case.
     *
     *      connect.logger.token('type', function(req, res){ return req.headers['content-type']; })
     *
     * Defining Formats:
     *
     *   All default formats are defined this way, however it's public API as well:
     *
     *       connect.logger.format('name', 'string or function')
     *
     * @param {String|Function|Object} format or options
     * @return {Function}
     * @api public
     */
    
    exports = module.exports = function logger(options) {
      if ('object' == typeof options) {
        options = options || {};
      } else if (options) {
        options = { format: options };
      } else {
        options = {};
      }
    
      // output on request instead of response
      var immediate = options.immediate;
    
      // check if log entry should be skipped
      var skip = options.skip || function () { return false; };
    
      // format name
      var fmt = exports[options.format] || options.format || exports.default;
    
      // compile format
      if ('function' != typeof fmt) fmt = compile(fmt);
    
      // options
      var stream = options.stream || process.stdout
        , buffer = options.buffer;
    
      // buffering support
      if (buffer) {
        var realStream = stream
          , interval = 'number' == typeof buffer
            ? buffer
            : defaultBufferDuration;
    
        // flush interval
        setInterval(function(){
          if (buf.length) {
            realStream.write(buf.join(''));
            buf.length = 0;
          }
        }, interval);
    
        // swap the stream
        stream = {
          write: function(str){
            buf.push(str);
          }
        };
      }
    
      return function logger(req, res, next) {
        var sock = req.socket;
        req._startTime = new Date;
        req._remoteAddress = sock.socket ? sock.socket.remoteAddress : sock.remoteAddress;
    
        function logRequest(){
          res.removeListener('finish', logRequest);
          res.removeListener('close', logRequest);
          if (skip(req, res)) return;
          var line = fmt(exports, req, res);
          if (null == line) return;
          stream.write(line + '\n');
        };
    
        // immediate
        if (immediate) {
          logRequest();
        // proxy end to output logging
        } else {
          res.on('finish', logRequest);
          res.on('close', logRequest);
        }
    
    
        next();
      };
    };
    
    /**
     * Compile `fmt` into a function.
     *
     * @param {String} fmt
     * @return {Function}
     * @api private
     */
    
    function compile(fmt) {
      fmt = fmt.replace(/"/g, '\\"');
      var js = '  return "' + fmt.replace(/:([-\w]{2,})(?:\[([^\]]+)\])?/g, function(_, name, arg){
        return '"\n    + (tokens["' + name + '"](req, res, "' + arg + '") || "-") + "';
      }) + '";'
      return new Function('tokens, req, res', js);
    };
    
    /**
     * Define a token function with the given `name`,
     * and callback `fn(req, res)`.
     *
     * @param {String} name
     * @param {Function} fn
     * @return {Object} exports for chaining
     * @api public
     */
    
    exports.token = function(name, fn) {
      exports[name] = fn;
      return this;
    };
    
    /**
     * Define a `fmt` with the given `name`.
     *
     * @param {String} name
     * @param {String|Function} fmt
     * @return {Object} exports for chaining
     * @api public
     */
    
    exports.format = function(name, str){
      exports[name] = str;
      return this;
    };
    
    /**
     * Default format.
     */
    
    exports.format('default', ':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"');
    
    /**
     * Short format.
     */
    
    exports.format('short', ':remote-addr - :method :url HTTP/:http-version :status :res[content-length] - :response-time ms');
    
    /**
     * Tiny format.
     */
    
    exports.format('tiny', ':method :url :status :res[content-length] - :response-time ms');
    
    /**
     * dev (colored)
     */
    
    exports.format('dev', function(tokens, req, res){
      var status = res.statusCode
        , len = parseInt(res.getHeader('Content-Length'), 10)
        , color = 32;
    
      if (status >= 500) color = 31
      else if (status >= 400) color = 33
      else if (status >= 300) color = 36;
    
      len = isNaN(len)
        ? ''
        : len = ' - ' + bytes(len);
    
      return '\x1b[90m' + req.method
        + ' ' + (req.originalUrl || req.url) + ' '
        + '\x1b[' + color + 'm' + res.statusCode
        + ' \x1b[90m'
        + (new Date - req._startTime)
        + 'ms' + len
        + '\x1b[0m';
    });
    
    /**
     * request url
     */
    
    exports.token('url', function(req){
      return req.originalUrl || req.url;
    });
    
    /**
     * request method
     */
    
    exports.token('method', function(req){
      return req.method;
    });
    
    /**
     * response time in milliseconds
     */
    
    exports.token('response-time', function(req){
      return String(Date.now() - req._startTime);
    });
    
    /**
     * UTC date
     */
    
    exports.token('date', function(){
      return new Date().toUTCString();
    });
    
    /**
     * response status code
     */
    
    exports.token('status', function(req, res){
      return res.headersSent ? res.statusCode : null;
    });
    
    /**
     * normalized referrer
     */
    
    exports.token('referrer', function(req){
      return req.headers['referer'] || req.headers['referrer'];
    });
    
    /**
     * remote address
     */
    
    exports.token('remote-addr', function(req){
      if (req.ip) return req.ip;
      if (req._remoteAddress) return req._remoteAddress;
      var sock = req.socket;
      if (sock.socket) return sock.socket.remoteAddress;
      return sock.remoteAddress;
    });
    
    /**
     * HTTP version
     */
    
    exports.token('http-version', function(req){
      return req.httpVersionMajor + '.' + req.httpVersionMinor;
    });
    
    /**
     * UA string
     */
    
    exports.token('user-agent', function(req){
      return req.headers['user-agent'];
    });
    
    /**
     * request header
     */
    
    exports.token('req', function(req, res, field){
      return req.headers[field.toLowerCase()];
    });
    
    /**
     * response header
     */
    
    exports.token('res', function(req, res, field){
      return (res._headers || {})[field.toLowerCase()];
    });
    
    
  provide("morgan", module.exports);
}(global));

// pakmanager:qs
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Object#toString() ref for stringify().
     */
    
    var toString = Object.prototype.toString;
    
    /**
     * Object#hasOwnProperty ref
     */
    
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    
    /**
     * Array#indexOf shim.
     */
    
    var indexOf = typeof Array.prototype.indexOf === 'function'
      ? function(arr, el) { return arr.indexOf(el); }
      : function(arr, el) {
          for (var i = 0; i < arr.length; i++) {
            if (arr[i] === el) return i;
          }
          return -1;
        };
    
    /**
     * Array.isArray shim.
     */
    
    var isArray = Array.isArray || function(arr) {
      return toString.call(arr) == '[object Array]';
    };
    
    /**
     * Object.keys shim.
     */
    
    var objectKeys = Object.keys || function(obj) {
      var ret = [];
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          ret.push(key);
        }
      }
      return ret;
    };
    
    /**
     * Array#forEach shim.
     */
    
    var forEach = typeof Array.prototype.forEach === 'function'
      ? function(arr, fn) { return arr.forEach(fn); }
      : function(arr, fn) {
          for (var i = 0; i < arr.length; i++) fn(arr[i]);
        };
    
    /**
     * Array#reduce shim.
     */
    
    var reduce = function(arr, fn, initial) {
      if (typeof arr.reduce === 'function') return arr.reduce(fn, initial);
      var res = initial;
      for (var i = 0; i < arr.length; i++) res = fn(res, arr[i]);
      return res;
    };
    
    /**
     * Cache non-integer test regexp.
     */
    
    var isint = /^[0-9]+$/;
    
    function promote(parent, key) {
      if (parent[key].length == 0) return parent[key] = {}
      var t = {};
      for (var i in parent[key]) {
        if (hasOwnProperty.call(parent[key], i)) {
          t[i] = parent[key][i];
        }
      }
      parent[key] = t;
      return t;
    }
    
    function parse(parts, parent, key, val) {
      var part = parts.shift();
      
      // illegal
      if (Object.getOwnPropertyDescriptor(Object.prototype, key)) return;
      
      // end
      if (!part) {
        if (isArray(parent[key])) {
          parent[key].push(val);
        } else if ('object' == typeof parent[key]) {
          parent[key] = val;
        } else if ('undefined' == typeof parent[key]) {
          parent[key] = val;
        } else {
          parent[key] = [parent[key], val];
        }
        // array
      } else {
        var obj = parent[key] = parent[key] || [];
        if (']' == part) {
          if (isArray(obj)) {
            if ('' != val) obj.push(val);
          } else if ('object' == typeof obj) {
            obj[objectKeys(obj).length] = val;
          } else {
            obj = parent[key] = [parent[key], val];
          }
          // prop
        } else if (~indexOf(part, ']')) {
          part = part.substr(0, part.length - 1);
          if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
          parse(parts, obj, part, val);
          // key
        } else {
          if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
          parse(parts, obj, part, val);
        }
      }
    }
    
    /**
     * Merge parent key/val pair.
     */
    
    function merge(parent, key, val){
      if (~indexOf(key, ']')) {
        var parts = key.split('[')
          , len = parts.length
          , last = len - 1;
        parse(parts, parent, 'base', val);
        // optimize
      } else {
        if (!isint.test(key) && isArray(parent.base)) {
          var t = {};
          for (var k in parent.base) t[k] = parent.base[k];
          parent.base = t;
        }
        set(parent.base, key, val);
      }
    
      return parent;
    }
    
    /**
     * Compact sparse arrays.
     */
    
    function compact(obj) {
      if ('object' != typeof obj) return obj;
    
      if (isArray(obj)) {
        var ret = [];
    
        for (var i in obj) {
          if (hasOwnProperty.call(obj, i)) {
            ret.push(obj[i]);
          }
        }
    
        return ret;
      }
    
      for (var key in obj) {
        obj[key] = compact(obj[key]);
      }
    
      return obj;
    }
    
    /**
     * Parse the given obj.
     */
    
    function parseObject(obj){
      var ret = { base: {} };
    
      forEach(objectKeys(obj), function(name){
        merge(ret, name, obj[name]);
      });
    
      return compact(ret.base);
    }
    
    /**
     * Parse the given str.
     */
    
    function parseString(str){
      var ret = reduce(String(str).split('&'), function(ret, pair){
        var eql = indexOf(pair, '=')
          , brace = lastBraceInKey(pair)
          , key = pair.substr(0, brace || eql)
          , val = pair.substr(brace || eql, pair.length)
          , val = val.substr(indexOf(val, '=') + 1, val.length);
    
        // ?foo
        if ('' == key) key = pair, val = '';
        if ('' == key) return ret;
    
        return merge(ret, decode(key), decode(val));
      }, { base: {} }).base;
    
      return compact(ret);
    }
    
    /**
     * Parse the given query `str` or `obj`, returning an object.
     *
     * @param {String} str | {Object} obj
     * @return {Object}
     * @api public
     */
    
    exports.parse = function(str){
      if (null == str || '' == str) return {};
      return 'object' == typeof str
        ? parseObject(str)
        : parseString(str);
    };
    
    /**
     * Turn the given `obj` into a query string
     *
     * @param {Object} obj
     * @return {String}
     * @api public
     */
    
    var stringify = exports.stringify = function(obj, prefix) {
      if (isArray(obj)) {
        return stringifyArray(obj, prefix);
      } else if ('[object Object]' == toString.call(obj)) {
        return stringifyObject(obj, prefix);
      } else if ('string' == typeof obj) {
        return stringifyString(obj, prefix);
      } else {
        return prefix + '=' + encodeURIComponent(String(obj));
      }
    };
    
    /**
     * Stringify the given `str`.
     *
     * @param {String} str
     * @param {String} prefix
     * @return {String}
     * @api private
     */
    
    function stringifyString(str, prefix) {
      if (!prefix) throw new TypeError('stringify expects an object');
      return prefix + '=' + encodeURIComponent(str);
    }
    
    /**
     * Stringify the given `arr`.
     *
     * @param {Array} arr
     * @param {String} prefix
     * @return {String}
     * @api private
     */
    
    function stringifyArray(arr, prefix) {
      var ret = [];
      if (!prefix) throw new TypeError('stringify expects an object');
      for (var i = 0; i < arr.length; i++) {
        ret.push(stringify(arr[i], prefix + '[' + i + ']'));
      }
      return ret.join('&');
    }
    
    /**
     * Stringify the given `obj`.
     *
     * @param {Object} obj
     * @param {String} prefix
     * @return {String}
     * @api private
     */
    
    function stringifyObject(obj, prefix) {
      var ret = []
        , keys = objectKeys(obj)
        , key;
    
      for (var i = 0, len = keys.length; i < len; ++i) {
        key = keys[i];
        if ('' == key) continue;
        if (null == obj[key]) {
          ret.push(encodeURIComponent(key) + '=');
        } else {
          ret.push(stringify(obj[key], prefix
            ? prefix + '[' + encodeURIComponent(key) + ']'
            : encodeURIComponent(key)));
        }
      }
    
      return ret.join('&');
    }
    
    /**
     * Set `obj`'s `key` to `val` respecting
     * the weird and wonderful syntax of a qs,
     * where "foo=bar&foo=baz" becomes an array.
     *
     * @param {Object} obj
     * @param {String} key
     * @param {String} val
     * @api private
     */
    
    function set(obj, key, val) {
      var v = obj[key];
      if (Object.getOwnPropertyDescriptor(Object.prototype, key)) return;
      if (undefined === v) {
        obj[key] = val;
      } else if (isArray(v)) {
        v.push(val);
      } else {
        obj[key] = [v, val];
      }
    }
    
    /**
     * Locate last brace in `str` within the key.
     *
     * @param {String} str
     * @return {Number}
     * @api private
     */
    
    function lastBraceInKey(str) {
      var len = str.length
        , brace
        , c;
      for (var i = 0; i < len; ++i) {
        c = str[i];
        if (']' == c) brace = false;
        if ('[' == c) brace = true;
        if ('=' == c && !brace) return i;
      }
    }
    
    /**
     * Decode `str`.
     *
     * @param {String} str
     * @return {String}
     * @api private
     */
    
    function decode(str) {
      try {
        return decodeURIComponent(str.replace(/\+/g, ' '));
      } catch (err) {
        return str;
      }
    }
    
  provide("qs", module.exports);
}(global));

// pakmanager:response-time
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - responseTime
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Reponse time:
     *
     * Adds the `X-Response-Time` header displaying the response
     * duration in milliseconds.
     *
     * @return {Function}
     * @api public
     */
    
    module.exports = function responseTime(){
      return function(req, res, next){
        next = next || noop;
        if (res._responseTime) return next();
        var writeHead = res.writeHead;
        var start = Date.now();
        res._responseTime = true;
        res.writeHead = function(){
          var duration = Date.now() - start;
          res.setHeader('X-Response-Time', duration + 'ms');
          writeHead.apply(res, arguments);
        };
        next();
      };
    };
    
    function noop() {};
  provide("response-time", module.exports);
}(global));

// pakmanager:serve-index
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - directory
     * Copyright(c) 2011 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * Copyright(c) 2014 Douglas Christopher Wilson
     * MIT Licensed
     */
    
    // TODO: arrow key navigation
    // TODO: make icons extensible
    
    /**
     * Module dependencies.
     */
    
    var http = require('http')
      , fs = require('fs')
      , parse = require('url').parse
      , path = require('path')
      , normalize = path.normalize
      , sep = path.sep
      , extname = path.extname
      , join = path.join;
    var Batch = require('batch');
    var Negotiator = require('negotiator');
    
    /*!
     * Icon cache.
     */
    
    var cache = {};
    
    /*!
     * Default template.
     */
    
    var defaultTemplate = join(__dirname, 'public', 'directory.html');
    
    /*!
     * Stylesheet.
     */
    
    var stylesheet = join(__dirname, 'public', 'style.css');
    
    /**
     * Media types and the map for content negotiation.
     */
    
    var mediaTypes = [
      'text/html',
      'text/plain',
      'application/json'
    ];
    
    var mediaType = {
      'text/html': 'html',
      'text/plain': 'plain',
      'application/json': 'json'
    };
    
    /**
     * Directory:
     *
     * Serve directory listings with the given `root` path.
     *
     * Options:
     *
     *  - `hidden` display hidden (dot) files. Defaults to false.
     *  - `icons`  display icons. Defaults to false.
     *  - `filter` Apply this filter function to files. Defaults to false.
     *  - `template` Optional path to html template. Defaults to a built-in template.
     *    The following tokens are replaced:
     *      - `{directory}` with the name of the directory.
     *      - `{files}` with the HTML of an unordered list of file links.
     *      - `{linked-path}` with the HTML of a link to the directory.
     *      - `{style}` with the built-in CSS and embedded images.
     *
     * @param {String} root
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    exports = module.exports = function directory(root, options){
      options = options || {};
    
      // root required
      if (!root) throw new Error('directory() root path required');
      var hidden = options.hidden
        , icons = options.icons
        , view = options.view || 'tiles'
        , filter = options.filter
        , root = normalize(root + sep)
        , template = options.template || defaultTemplate;
    
      return function directory(req, res, next) {
        if ('GET' != req.method && 'HEAD' != req.method) return next();
    
        var url = parse(req.url)
          , dir = decodeURIComponent(url.pathname)
          , path = normalize(join(root, dir))
          , originalUrl = parse(req.originalUrl)
          , originalDir = decodeURIComponent(originalUrl.pathname)
          , showUp = path != root;
    
        // null byte(s), bad request
        if (~path.indexOf('\0')) return next(createError(400));
    
        // malicious path, forbidden
        if (0 != path.indexOf(root)) return next(createError(403));
    
        // check if we have a directory
        fs.stat(path, function(err, stat){
          if (err) return 'ENOENT' == err.code
            ? next()
            : next(err);
    
          if (!stat.isDirectory()) return next();
    
          // fetch files
          fs.readdir(path, function(err, files){
            if (err) return next(err);
            if (!hidden) files = removeHidden(files);
            if (filter) files = files.filter(filter);
            files.sort();
    
            // content-negotiation
            var type = new Negotiator(req).preferredMediaType(mediaTypes);
    
            // not acceptable
            if (!type) return next(createError(406));
            exports[mediaType[type]](req, res, files, next, originalDir, showUp, icons, path, view, template);
          });
        });
      };
    };
    
    /**
     * Respond with text/html.
     */
    
    exports.html = function(req, res, files, next, dir, showUp, icons, path, view, template){
      fs.readFile(template, 'utf8', function(err, str){
        if (err) return next(err);
        fs.readFile(stylesheet, 'utf8', function(err, style){
          if (err) return next(err);
          stat(path, files, function(err, stats){
            if (err) return next(err);
            files = files.map(function(file, i){ return { name: file, stat: stats[i] }; });
            files.sort(fileSort);
            if (showUp) files.unshift({ name: '..' });
            str = str
              .replace('{style}', style.concat(iconStyle(files, icons)))
              .replace('{files}', html(files, dir, icons, view))
              .replace('{directory}', dir)
              .replace('{linked-path}', htmlPath(dir));
            res.setHeader('Content-Type', 'text/html');
            res.setHeader('Content-Length', str.length);
            res.end(str);
          });
        });
      });
    };
    
    /**
     * Respond with application/json.
     */
    
    exports.json = function(req, res, files){
      files = JSON.stringify(files);
      res.setHeader('Content-Type', 'application/json');
      res.setHeader('Content-Length', files.length);
      res.end(files);
    };
    
    /**
     * Respond with text/plain.
     */
    
    exports.plain = function(req, res, files){
      files = files.join('\n') + '\n';
      res.setHeader('Content-Type', 'text/plain');
      res.setHeader('Content-Length', files.length);
      res.end(files);
    };
    
    /**
     * Generate an `Error` from the given status `code`
     * and optional `msg`.
     *
     * @param {Number} code
     * @param {String} msg
     * @return {Error}
     * @api private
     */
    
    function createError(code, msg) {
      var err = new Error(msg || http.STATUS_CODES[code]);
      err.status = code;
      return err;
    };
    
    /**
     * Sort function for with directories first.
     */
    
    function fileSort(a, b) {
      return Number(b.stat && b.stat.isDirectory()) - Number(a.stat && a.stat.isDirectory()) ||
        String(a.name).toLocaleLowerCase().localeCompare(String(b.name).toLocaleLowerCase());
    }
    
    /**
     * Map html `dir`, returning a linked path.
     */
    
    function htmlPath(dir) {
      var curr = [];
      return dir.split('/').map(function(part){
        curr.push(encodeURIComponent(part));
        return part ? '<a href="' + curr.join('/') + '">' + part + '</a>' : '';
      }).join(' / ');
    }
    
    /**
     * Load icon images, return css string.
     */
    
    function iconStyle (files, useIcons) {
      if (!useIcons) return '';
      var className;
      var i;
      var icon;
      var list = [];
      var rules = {};
      var selector;
      var selectors = {};
      var style = '';
    
      for (i = 0; i < files.length; i++) {
        var file = files[i];
    
        var isDir = '..' == file.name || (file.stat && file.stat.isDirectory());
        icon = isDir ? icons.folder : icons[extname(file.name)] || icons.default;
    
        var ext = extname(file.name);
        className = 'icon-' + (isDir ? 'directory' : (icons[ext] ? ext.substring(1) : 'default'));
        selector = '#files .' + className + ' .name';
    
        if (!rules[icon]) {
          rules[icon] = 'background-image: url(data:image/png;base64,' + load(icon) + ');'
          selectors[icon] = [];
          list.push(icon);
        }
    
        if (!~selectors[icon].indexOf(selector)) {
          selectors[icon].push(selector);
        }
      }
    
      for (i = 0; i < list.length; i++) {
        icon = list[i];
        style += selectors[icon].join(',\n') + ' {\n  ' + rules[icon] + '\n}\n';
      }
    
      return style;
    }
    
    /**
     * Map html `files`, returning an html unordered list.
     */
    
    function html(files, dir, useIcons, view) {
      	return '<ul id="files" class="view-'+view+'">'
        + (view == 'details' ? (
          '<li class="header">'
          + '<span class="name">Name</span>'
          + '<span class="size">Size</span>'
          + '<span class="date">Modified</span>'
          + '</li>') : '')
        + files.map(function(file){
        var isDir = '..' == file.name || (file.stat && file.stat.isDirectory())
          , classes = []
          , path = dir.split('/').map(function (c) { return encodeURIComponent(c); });
    
        if (useIcons) {
          var ext = extname(file.name);
          ext = isDir ? '.directory' : (icons[ext] ? ext : '.default');
          classes.push('icon');
          classes.push('icon-' + ext.substring(1));
        }
    
        path.push(encodeURIComponent(file.name));
    
        var date = file.name == '..' ? ''
          : file.stat.mtime.toDateString()+' '+file.stat.mtime.toLocaleTimeString();
        var size = isDir ? '' : file.stat.size;
    
        return '<li><a href="'
          + normalizeSlashes(normalize(path.join('/')))
          + '" class="'
          + classes.join(' ') + '"'
          + ' title="' + file.name + '">'
          + '<span class="name">'+file.name+'</span>'
          + '<span class="size">'+size+'</span>'
          + '<span class="date">'+date+'</span>'
          + '</a></li>';
    
      }).join('\n') + '</ul>';
    }
    
    /**
     * Load and cache the given `icon`.
     *
     * @param {String} icon
     * @return {String}
     * @api private
     */
    
    function load(icon) {
      if (cache[icon]) return cache[icon];
      return cache[icon] = fs.readFileSync(__dirname + '/public/icons/' + icon, 'base64');
    }
    
    /**
     * Normalizes the path separator from system separator
     * to URL separator, aka `/`.
     *
     * @param {String} path
     * @return {String}
     * @api private
     */
    
    function normalizeSlashes(path) {
      return path.split(sep).join('/');
    };
    
    /**
     * Filter "hidden" `files`, aka files
     * beginning with a `.`.
     *
     * @param {Array} files
     * @return {Array}
     * @api private
     */
    
    function removeHidden(files) {
      return files.filter(function(file){
        return '.' != file[0];
      });
    }
    
    /**
     * Stat all files and return array of stat
     * in same order.
     */
    
    function stat(dir, files, cb) {
      var batch = new Batch();
    
      batch.concurrency(10);
    
      files.forEach(function(file){
        batch.push(function(done){
          fs.stat(join(dir, file), done);
        });
      });
    
      batch.end(cb);
    }
    
    /**
     * Icon map.
     */
    
    var icons = {
        '.js': 'page_white_code_red.png'
      , '.json': 'page_white_code.png'
      , '.c': 'page_white_c.png'
      , '.h': 'page_white_h.png'
      , '.cc': 'page_white_cplusplus.png'
      , '.php': 'page_white_php.png'
      , '.rb': 'page_white_ruby.png'
      , '.erb': 'page_white_ruby.png'
      , '.cpp': 'page_white_cplusplus.png'
      , '.as': 'page_white_actionscript.png'
      , '.cfm': 'page_white_coldfusion.png'
      , '.cs': 'page_white_csharp.png'
      , '.java': 'page_white_cup.png'
      , '.jsp': 'page_white_cup.png'
      , '.dll': 'page_white_gear.png'
      , '.ini': 'page_white_gear.png'
      , '.asp': 'page_white_code.png'
      , '.aspx': 'page_white_code.png'
      , '.clj': 'page_white_code.png'
      , '.css': 'page_white_code.png'
      , '.sass': 'page_white_code.png'
      , '.scss': 'page_white_code.png'
      , '.less': 'page_white_code.png'
      , '.htm': 'page_white_code.png'
      , '.html': 'page_white_code.png'
      , '.xhtml': 'page_white_code.png'
      , '.lua': 'page_white_code.png'
      , '.m': 'page_white_code.png'
      , '.pl': 'page_white_code.png'
      , '.py': 'page_white_code.png'
      , '.vb': 'page_white_code.png'
      , '.vbs': 'page_white_code.png'
      , '.xml': 'page_white_code.png'
      , '.yaws': 'page_white_code.png'
      , '.map': 'map.png'
    
      , '.app': 'application_xp.png'
      , '.exe': 'application_xp.png'
      , '.bat': 'application_xp_terminal.png'
      , '.cgi': 'application_xp_terminal.png'
      , '.sh': 'application_xp_terminal.png'
    
      , '.avi': 'film.png'
      , '.flv': 'film.png'
      , '.mv4': 'film.png'
      , '.mov': 'film.png'
      , '.mp4': 'film.png'
      , '.mpeg': 'film.png'
      , '.mpg': 'film.png'
      , '.ogv': 'film.png'
      , '.rm': 'film.png'
      , '.webm': 'film.png'
      , '.wmv': 'film.png'
      , '.fnt': 'font.png'
      , '.otf': 'font.png'
      , '.ttf': 'font.png'
      , '.woff': 'font.png'
      , '.bmp': 'image.png'
      , '.gif': 'image.png'
      , '.ico': 'image.png'
      , '.jpeg': 'image.png'
      , '.jpg': 'image.png'
      , '.png': 'image.png'
      , '.psd': 'page_white_picture.png'
      , '.xcf': 'page_white_picture.png'
      , '.pdf': 'page_white_acrobat.png'
      , '.swf': 'page_white_flash.png'
      , '.ai': 'page_white_vector.png'
      , '.eps': 'page_white_vector.png'
      , '.ps': 'page_white_vector.png'
      , '.svg': 'page_white_vector.png'
    
      , '.ods': 'page_white_excel.png'
      , '.xls': 'page_white_excel.png'
      , '.xlsx': 'page_white_excel.png'
      , '.odp': 'page_white_powerpoint.png'
      , '.ppt': 'page_white_powerpoint.png'
      , '.pptx': 'page_white_powerpoint.png'
      , '.md': 'page_white_text.png'
      , '.srt': 'page_white_text.png'
      , '.txt': 'page_white_text.png'
      , '.doc': 'page_white_word.png'
      , '.docx': 'page_white_word.png'
      , '.odt': 'page_white_word.png'
      , '.rtf': 'page_white_word.png'
    
      , '.dmg': 'drive.png'
      , '.iso': 'cd.png'
      , '.7z': 'box.png'
      , '.apk': 'box.png'
      , '.bz2': 'box.png'
      , '.cab': 'box.png'
      , '.deb': 'box.png'
      , '.gz': 'box.png'
      , '.jar': 'box.png'
      , '.lz': 'box.png'
      , '.lzma': 'box.png'
      , '.msi': 'box.png'
      , '.pkg': 'box.png'
      , '.rar': 'box.png'
      , '.rpm': 'box.png'
      , '.tar': 'box.png'
      , '.tbz2': 'box.png'
      , '.tgz': 'box.png'
      , '.tlz': 'box.png'
      , '.xz': 'box.png'
      , '.zip': 'box.png'
    
      , '.accdb': 'page_white_database.png'
      , '.db': 'page_white_database.png'
      , '.dbf': 'page_white_database.png'
      , '.mdb': 'page_white_database.png'
      , '.pdb': 'page_white_database.png'
      , '.sql': 'page_white_database.png'
    
      , '.gam': 'controller.png'
      , '.rom': 'controller.png'
      , '.sav': 'controller.png'
    
      , 'folder': 'folder.png'
      , 'default': 'page_white.png'
    };
    
  provide("serve-index", module.exports);
}(global));

// pakmanager:serve-static
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - static
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * Copyright(c) 2014 Douglas Christopher Wilson
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var send = require('send');
    var url = require('url');
    
    /**
     * Static:
     *
     *   Static file server with the given `root` path.
     *
     * Examples:
     *
     *     var oneDay = 86400000;
     *     var serveStatic =  require('serve-static');
     *
     *     connect()
     *       .use(serveStatic(__dirname + '/public'))
     *
     *     connect()
     *       .use(serveStatic(__dirname + '/public', { maxAge: oneDay }))
     *
     * Options:
     *
     *    - `maxAge`     Browser cache maxAge in milliseconds. defaults to 0
     *    - `hidden`     Allow transfer of hidden files. defaults to false
     *    - `redirect`   Redirect to trailing "/" when the pathname is a dir. defaults to true
     *    - `index`      Default file name, defaults to 'index.html'
     *
     * @param {String} root
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    exports = module.exports = function(root, options){
      options = options || {};
    
      // root required
      if (!root) throw new TypeError('root path required');
    
      // default redirect
      var redirect = false !== options.redirect;
    
      return function staticMiddleware(req, res, next) {
        if ('GET' != req.method && 'HEAD' != req.method) return next();
        var originalUrl = url.parse(req.originalUrl || req.url);
        var path = parse(req).pathname;
    
        if (path == '/' && originalUrl.pathname[originalUrl.pathname.length - 1] != '/') {
          return directory();
        }
    
        function directory() {
          if (!redirect) return next();
          var target;
          originalUrl.pathname += '/';
          target = url.format(originalUrl);
          res.statusCode = 303;
          res.setHeader('Location', target);
          res.end('Redirecting to ' + escape(target));
        }
    
        function error(err) {
          if (404 == err.status) return next();
          next(err);
        }
    
        send(req, path)
          .maxage(options.maxAge || 0)
          .root(root)
          .index(options.index || 'index.html')
          .hidden(options.hidden)
          .on('error', error)
          .on('directory', directory)
          .pipe(res);
      };
    };
    
    /**
     * Expose mime module.
     *
     * If you wish to extend the mime table use this
     * reference to the "mime" module in the npm registry.
     */
    
    exports.mime = send.mime;
    
    /**
     * Escape the given string of `html`.
     *
     * @param {String} html
     * @return {String}
     * @api private
     */
    
    function escape(html) {
      return String(html)
        .replace(/&(?!\w+;)/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
    
    /**
     * Parse the `req` url.
     *
     * @param {ServerRequest} req
     * @return {Object}
     * @api private
     */
    
    function parse(req) {
      var parsed = url.parse(req.url);
    
      if (parsed.auth && !parsed.protocol && ~parsed.href.indexOf('//')) {
        // This parses pathnames, and a strange pathname like //r@e should work
        parsed = url.parse(req.url.replace(/@/g, '%40'));
      }
    
      return parsed;
    };
    
  provide("serve-static", module.exports);
}(global));

// pakmanager:static-favicon
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - favicon
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var fs = require('fs');
    var crypto = require('crypto');
    
    /**
     * Favicon:
     *
     * By default serves the connect favicon, or the favicon
     * located by the given `path`.
     *
     * Options:
     *
     *   - `maxAge`  cache-control max-age directive, defaulting to 1 day
     *
     * Examples:
     *
     *   Serve default favicon:
     *
     *     connect()
     *       .use(connect.favicon())
     *
     *   Serve favicon before logging for brevity:
     *
     *     connect()
     *       .use(connect.favicon())
     *       .use(connect.logger('dev'))
     *
     *   Serve custom favicon:
     *
     *     connect()
     *       .use(connect.favicon('public/favicon.ico'))
     *
     * @param {String} path
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    module.exports = function favicon(path, options){
      var options = options || {}
        , path = path || __dirname + '/favicon.ico'
        , maxAge = options.maxAge || 86400000
        , icon; // favicon cache
    
      return function favicon(req, res, next){
        if ('/favicon.ico' == req.url) {
          if (icon) {
            res.writeHead(200, icon.headers);
            res.end(icon.body);
          } else {
            fs.readFile(path, function(err, buf){
              if (err) return next(err);
              icon = {
                headers: {
                    'Content-Type': 'image/x-icon'
                  , 'Content-Length': buf.length
                  , 'ETag': '"' + md5(buf) + '"'
                  , 'Cache-Control': 'public, max-age=' + (maxAge / 1000)
                },
                body: buf
              };
              res.writeHead(200, icon.headers);
              res.end(icon.body);
            });
          }
        } else {
          next();
        }
      };
    };
    
    function md5(str, encoding){
      return crypto
        .createHash('md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex');
    };
    
  provide("static-favicon", module.exports);
}(global));

// pakmanager:vhost
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - vhost
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Vhost:
     *
     *   Setup vhost for the given `hostname` and `server`.
     *
     *     connect()
     *       .use(connect.vhost('foo.com', fooApp))
     *       .use(connect.vhost('bar.com', barApp))
     *       .use(connect.vhost('*.com', mainApp))
     *
     *  The `server` may be a Connect server or
     *  a regular Node `http.Server`.
     *
     * @param {String} hostname
     * @param {Server} server
     * @return {Function}
     * @api public
     */
    
    module.exports = function vhost(hostname, server){
      if (!hostname) throw new Error('vhost hostname required');
      if (!server) throw new Error('vhost server required');
      var regexp = new RegExp('^' + hostname.replace(/[^*\w]/g, '\\$&').replace(/[*]/g, '(?:.*?)')  + '$', 'i');
      if (server.onvhost) server.onvhost(hostname);
      return function vhost(req, res, next){
        if (!req.headers.host) return next();
        var host = req.headers.host.split(':')[0];
        if (!regexp.test(host)) return next();
        if ('function' == typeof server) return server(req, res, next);
        server.emit('request', req, res);
      };
    };
    
  provide("vhost", module.exports);
}(global));

// pakmanager:pause
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    module.exports = function(obj){
      var onData
        , onEnd
        , events = [];
    
      // buffer data
      obj.on('data', onData = function(data, encoding){
        events.push(['data', data, encoding]);
      });
    
      // buffer end
      obj.on('end', onEnd = function(data, encoding){
        events.push(['end', data, encoding]);
      });
    
      return {
        end: function(){
          obj.removeListener('data', onData);
          obj.removeListener('end', onEnd);
        },
        resume: function(){
          this.end();
          for (var i = 0, len = events.length; i < len; ++i) {
            obj.emit.apply(obj, events[i]);
          }
        }
      };
    };
  provide("pause", module.exports);
}(global));

// pakmanager:raw-body
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var StringDecoder = require('string_decoder').StringDecoder
    var bytes = require('bytes')
    
    module.exports = function (stream, options, done) {
      if (typeof options === 'function') {
        done = options
        options = {}
      } else if (!options) {
        options = {}
      }
    
      // convert the limit to an integer
      var limit = null
      if (typeof options.limit === 'number')
        limit = options.limit
      if (typeof options.limit === 'string')
        limit = bytes(options.limit)
    
      // convert the expected length to an integer
      var length = null
      if (options.length != null && !isNaN(options.length))
        length = parseInt(options.length, 10)
    
      // check the length and limit options.
      // note: we intentionally leave the stream paused,
      // so users should handle the stream themselves.
      if (limit !== null && length !== null && length > limit) {
        if (typeof stream.pause === 'function')
          stream.pause()
    
        process.nextTick(function () {
          var err = makeError('request entity too large', 'entity.too.large')
          err.status = err.statusCode = 413
          err.length = err.expected = length
          err.limit = limit
          done(err)
        })
        return defer
      }
    
      var state = stream._readableState
      // streams2+: assert the stream encoding is buffer.
      if (state && state.encoding != null) {
        if (typeof stream.pause === 'function')
          stream.pause()
    
        process.nextTick(function () {
          var err = makeError('stream encoding should not be set',
            'stream.encoding.set')
          // developer error
          err.status = err.statusCode = 500
          done(err)
        })
        return defer
      }
    
      var received = 0
      // note: we delegate any invalid encodings to the constructor
      var decoder = options.encoding
        ? new StringDecoder(options.encoding === true ? 'utf8' : options.encoding)
        : null
      var buffer = decoder
        ? ''
        : []
    
      stream.on('data', onData)
      stream.once('end', onEnd)
      stream.once('error', onEnd)
      stream.once('close', cleanup)
    
      return defer
    
      // yieldable support
      function defer(fn) {
        done = fn
      }
    
      function onData(chunk) {
        received += chunk.length
        decoder
          ? buffer += decoder.write(chunk)
          : buffer.push(chunk)
    
        if (limit !== null && received > limit) {
          if (typeof stream.pause === 'function')
            stream.pause()
          var err = makeError('request entity too large', 'entity.too.large')
          err.status = err.statusCode = 413
          err.received = received
          err.limit = limit
          done(err)
          cleanup()
        }
      }
    
      function onEnd(err) {
        if (err) {
          if (typeof stream.pause === 'function')
            stream.pause()
          done(err)
        } else if (length !== null && received !== length) {
          err = makeError('request size did not match content length',
            'request.size.invalid')
          err.status = err.statusCode = 400
          err.received = received
          err.length = err.expected = length
          done(err)
        } else {
          done(null, decoder
            ? buffer + endStringDecoder(decoder)
            : Buffer.concat(buffer)
          )
        }
    
        cleanup()
      }
    
      function cleanup() {
        received = buffer = null
    
        stream.removeListener('data', onData)
        stream.removeListener('end', onEnd)
        stream.removeListener('error', onEnd)
        stream.removeListener('close', cleanup)
      }
    }
    
    // to create serializable errors you must re-set message so
    // that it is enumerable and you must re configure the type
    // property so that is writable and enumerable
    function makeError(message, type) {
      var error = new Error()
      error.message = message
      Object.defineProperty(error, 'type', {
        value: type,
        enumerable: true,
        writable: true,
        configurable: true
      })
      return error
    }
    
    // https://github.com/Raynos/body/blob/2512ced39e31776e5a2f7492b907330badac3a40/index.js#L72
    // bug fix for missing `StringDecoder.end` in v0.8.x
    function endStringDecoder(decoder) {
        if (decoder.end) {
            return decoder.end()
        }
    
        var res = ""
    
        if (decoder.charReceived) {
            var cr = decoder.charReceived
            var buf = decoder.charBuffer
            var enc = decoder.encoding
            res += buf.slice(0, cr).toString(enc)
        }
    
        return res
    }
    
  provide("raw-body", module.exports);
}(global));

// pakmanager:multiparty
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  exports.Form = Form;
    
    var stream = require('readable-stream')
      , util = require('util')
      , fs = require('fs')
      , crypto = require('crypto')
      , path = require('path')
      , os = require('os')
      , StringDecoder = require('string_decoder').StringDecoder
      , StreamCounter = require('stream-counter')
    
    var START = 0
      , START_BOUNDARY = 1
      , HEADER_FIELD_START = 2
      , HEADER_FIELD = 3
      , HEADER_VALUE_START = 4
      , HEADER_VALUE = 5
      , HEADER_VALUE_ALMOST_DONE = 6
      , HEADERS_ALMOST_DONE = 7
      , PART_DATA_START = 8
      , PART_DATA = 9
      , PART_END = 10
      , CLOSE_BOUNDARY = 11
      , END = 12
    
      , LF = 10
      , CR = 13
      , SPACE = 32
      , HYPHEN = 45
      , COLON = 58
      , A = 97
      , Z = 122
    
    var CONTENT_TYPE_RE = /^multipart\/(?:form-data|related)(?:;|$)/i;
    var CONTENT_TYPE_PARAM_RE = /;\s*(\S+)=(?:"([^"]+)"|([^;]+))/gi;
    var FILE_EXT_RE = /(\.[_\-a-zA-Z0-9]{0,16}).*/;
    var LAST_BOUNDARY_SUFFIX_LEN = 4; // --\r\n
    
    util.inherits(Form, stream.Writable);
    function Form(options) {
      var self = this;
      stream.Writable.call(self);
    
      options = options || {};
    
      self.error = null;
      self.finished = false;
    
      self.autoFields = !!options.autoFields;
      self.autoFiles = !!options.autoFiles;
    
      self.maxFields = options.maxFields || 1000;
      self.maxFieldsSize = options.maxFieldsSize || 2 * 1024 * 1024;
      self.maxFilesSize = options.maxFilesSize || Infinity;
      self.uploadDir = options.uploadDir || os.tmpDir();
      self.encoding = options.encoding || 'utf8';
      self.hash = options.hash || false;
    
      self.bytesReceived = 0;
      self.bytesExpected = null;
    
      self.openedFiles = [];
      self.totalFieldSize = 0;
      self.totalFieldCount = 0;
      self.totalFileSize = 0;
      self.flushing = 0;
    
      self.backpressure = false;
      self.writeCbs = [];
    
      if (options.boundary) setUpParser(self, options.boundary);
    
      self.on('newListener', function(eventName) {
        if (eventName === 'file') {
          self.autoFiles = true;
        } else if (eventName === 'field') {
          self.autoFields = true;
        }
      });
    }
    
    Form.prototype.parse = function(req, cb) {
      var self = this;
    
      // if the user supplies a callback, this implies autoFields and autoFiles
      if (cb) {
        self.autoFields = true;
        self.autoFiles = true;
      }
    
      self.handleError = handleError;
      self.bytesExpected = getBytesExpected(req.headers);
    
      req.on('error', handleError);
      req.on('aborted', onReqAborted);
    
      var contentType = req.headers['content-type'];
      if (!contentType) {
        handleError(new Error('missing content-type header'));
        return;
      }
    
      var m = CONTENT_TYPE_RE.exec(contentType);
      if (!m) {
        handleError(new Error('unrecognized content-type: ' + contentType));
        return;
      }
    
      var boundary;
      CONTENT_TYPE_PARAM_RE.lastIndex = m.index + m[0].length - 1;
      while ((m = CONTENT_TYPE_PARAM_RE.exec(contentType))) {
        if (m[1].toLowerCase() !== 'boundary') continue;
        boundary = m[2] || m[3];
        break;
      }
    
      if (!boundary) {
        handleError(new Error('content-type missing boundary: ' + require('util').inspect(m)));
        return;
      }
    
      setUpParser(self, boundary);
      req.pipe(self);
    
      if (cb) {
        var fields = {};
        var files = {};
        self.on('error', function(err) {
          cb(err);
        });
        self.on('field', function(name, value) {
          var fieldsArray = fields[name] || (fields[name] = []);
          fieldsArray.push(value);
        });
        self.on('file', function(name, file) {
          var filesArray = files[name] || (files[name] = []);
          filesArray.push(file);
        });
        self.on('close', function() {
          cb(null, fields, files);
        });
      }
    
      function onReqAborted() {
        self.emit('aborted');
        handleError(new Error("Request aborted"));
      }
    
      function handleError(err) {
        var first = !self.error;
        if (first) {
          self.error = err;
          req.removeListener('aborted', onReqAborted);
    
          // welp. 0.8 doesn't support unpipe, too bad so sad.
          // let's drop support for 0.8 soon.
          if (req.unpipe) {
            req.unpipe(self);
          }
        }
    
        self.openedFiles.forEach(function(file) {
          destroyFile(self, file);
        });
        self.openedFiles = [];
    
        if (first) {
          self.emit('error', err);
        }
      }
    
    };
    
    Form.prototype._write = function(buffer, encoding, cb) {
      var self = this
        , i = 0
        , len = buffer.length
        , prevIndex = self.index
        , index = self.index
        , state = self.state
        , lookbehind = self.lookbehind
        , boundary = self.boundary
        , boundaryChars = self.boundaryChars
        , boundaryLength = self.boundary.length
        , boundaryEnd = boundaryLength - 1
        , bufferLength = buffer.length
        , c
        , cl
    
      for (i = 0; i < len; i++) {
        c = buffer[i];
        switch (state) {
          case START:
            index = 0;
            state = START_BOUNDARY;
            /* falls through */
          case START_BOUNDARY:
            if (index === boundaryLength - 2 && c === HYPHEN) {
              index = 1;
              state = CLOSE_BOUNDARY;
              break;
            } else if (index === boundaryLength - 2) {
              if (c !== CR) return self.handleError(new Error("Expected CR Received " + c));
              index++;
              break;
            } else if (index === boundaryLength - 1) {
              if (c !== LF) return self.handleError(new Error("Expected LF Received " + c));
              index = 0;
              self.onParsePartBegin();
              state = HEADER_FIELD_START;
              break;
            }
    
            if (c !== boundary[index+2]) index = -2;
            if (c === boundary[index+2]) index++;
            break;
          case HEADER_FIELD_START:
            state = HEADER_FIELD;
            self.headerFieldMark = i;
            index = 0;
            /* falls through */
          case HEADER_FIELD:
            if (c === CR) {
              self.headerFieldMark = null;
              state = HEADERS_ALMOST_DONE;
              break;
            }
    
            index++;
            if (c === HYPHEN) break;
    
            if (c === COLON) {
              if (index === 1) {
                // empty header field
                self.handleError(new Error("Empty header field"));
                return;
              }
              self.onParseHeaderField(buffer.slice(self.headerFieldMark, i));
              self.headerFieldMark = null;
              state = HEADER_VALUE_START;
              break;
            }
    
            cl = lower(c);
            if (cl < A || cl > Z) {
              self.handleError(new Error("Expected alphabetic character, received " + c));
              return;
            }
            break;
          case HEADER_VALUE_START:
            if (c === SPACE) break;
    
            self.headerValueMark = i;
            state = HEADER_VALUE;
            /* falls through */
          case HEADER_VALUE:
            if (c === CR) {
              self.onParseHeaderValue(buffer.slice(self.headerValueMark, i));
              self.headerValueMark = null;
              self.onParseHeaderEnd();
              state = HEADER_VALUE_ALMOST_DONE;
            }
            break;
          case HEADER_VALUE_ALMOST_DONE:
            if (c !== LF) return self.handleError(new Error("Expected LF Received " + c));
            state = HEADER_FIELD_START;
            break;
          case HEADERS_ALMOST_DONE:
            if (c !== LF) return self.handleError(new Error("Expected LF Received " + c));
            var err = self.onParseHeadersEnd(i + 1);
            if (err) return self.handleError(err);
            state = PART_DATA_START;
            break;
          case PART_DATA_START:
            state = PART_DATA;
            self.partDataMark = i;
            /* falls through */
          case PART_DATA:
            prevIndex = index;
    
            if (index === 0) {
              // boyer-moore derrived algorithm to safely skip non-boundary data
              i += boundaryEnd;
              while (i < bufferLength && !(buffer[i] in boundaryChars)) {
                i += boundaryLength;
              }
              i -= boundaryEnd;
              c = buffer[i];
            }
    
            if (index < boundaryLength) {
              if (boundary[index] === c) {
                if (index === 0) {
                  self.onParsePartData(buffer.slice(self.partDataMark, i));
                  self.partDataMark = null;
                }
                index++;
              } else {
                index = 0;
              }
            } else if (index === boundaryLength) {
              index++;
              if (c === CR) {
                // CR = part boundary
                self.partBoundaryFlag = true;
              } else if (c === HYPHEN) {
                index = 1;
                state = CLOSE_BOUNDARY;
                break;
              } else {
                index = 0;
              }
            } else if (index - 1 === boundaryLength)  {
              if (self.partBoundaryFlag) {
                index = 0;
                if (c === LF) {
                  self.partBoundaryFlag = false;
                  self.onParsePartEnd();
                  self.onParsePartBegin();
                  state = HEADER_FIELD_START;
                  break;
                }
              } else {
                index = 0;
              }
            }
    
            if (index > 0) {
              // when matching a possible boundary, keep a lookbehind reference
              // in case it turns out to be a false lead
              lookbehind[index-1] = c;
            } else if (prevIndex > 0) {
              // if our boundary turned out to be rubbish, the captured lookbehind
              // belongs to partData
              self.onParsePartData(lookbehind.slice(0, prevIndex));
              prevIndex = 0;
              self.partDataMark = i;
    
              // reconsider the current character even so it interrupted the sequence
              // it could be the beginning of a new sequence
              i--;
            }
    
            break;
          case CLOSE_BOUNDARY:
            if (c !== HYPHEN) return self.handleError(new Error("Expected HYPHEN Received " + c));
            if (index === 1) {
              self.onParsePartEnd();
              self.end();
              state = END;
            } else if (index > 1) {
              return self.handleError(new Error("Parser has invalid state."));
            }
            index++;
            break;
          case END:
            break;
          default:
            self.handleError(new Error("Parser has invalid state."));
            return;
        }
      }
    
      if (self.headerFieldMark != null) {
        self.onParseHeaderField(buffer.slice(self.headerFieldMark));
        self.headerFieldMark = 0;
      }
      if (self.headerValueMark != null) {
        self.onParseHeaderValue(buffer.slice(self.headerValueMark));
        self.headerValueMark = 0;
      }
      if (self.partDataMark != null) {
        self.onParsePartData(buffer.slice(self.partDataMark));
        self.partDataMark = 0;
      }
    
      self.index = index;
      self.state = state;
    
      self.bytesReceived += buffer.length;
      self.emit('progress', self.bytesReceived, self.bytesExpected);
    
      if (self.backpressure) {
        self.writeCbs.push(cb);
      } else {
        cb();
      }
    };
    
    Form.prototype.onParsePartBegin = function() {
      clearPartVars(this);
    }
    
    Form.prototype.onParseHeaderField = function(b) {
      this.headerField += this.headerFieldDecoder.write(b);
    }
    
    Form.prototype.onParseHeaderValue = function(b) {
      this.headerValue += this.headerValueDecoder.write(b);
    }
    
    Form.prototype.onParseHeaderEnd = function() {
      this.headerField = this.headerField.toLowerCase();
      this.partHeaders[this.headerField] = this.headerValue;
    
      var m;
      if (this.headerField === 'content-disposition') {
        if (m = this.headerValue.match(/\bname="([^"]+)"/i)) {
          this.partName = m[1];
        }
        this.partFilename = parseFilename(this.headerValue);
      } else if (this.headerField === 'content-transfer-encoding') {
        this.partTransferEncoding = this.headerValue.toLowerCase();
      }
    
      this.headerFieldDecoder = new StringDecoder(this.encoding);
      this.headerField = '';
      this.headerValueDecoder = new StringDecoder(this.encoding);
      this.headerValue = '';
    }
    
    Form.prototype.onParsePartData = function(b) {
      if (this.partTransferEncoding === 'base64') {
        this.backpressure = ! this.destStream.write(b.toString('ascii'), 'base64');
      } else {
        this.backpressure = ! this.destStream.write(b);
      }
    }
    
    Form.prototype.onParsePartEnd = function() {
      if (this.destStream) {
        flushWriteCbs(this);
        var s = this.destStream;
        process.nextTick(function() {
          s.end();
        });
      }
      clearPartVars(this);
    }
    
    Form.prototype.onParseHeadersEnd = function(offset) {
      var self = this;
      switch(self.partTransferEncoding){
        case 'binary':
        case '7bit':
        case '8bit':
        self.partTransferEncoding = 'binary';
        break;
    
        case 'base64': break;
        default:
        return new Error("unknown transfer-encoding: " + self.partTransferEncoding);
      }
    
      self.totalFieldCount += 1;
      if (self.totalFieldCount >= self.maxFields) {
        return new Error("maxFields " + self.maxFields + " exceeded.");
      }
    
      self.destStream = new stream.PassThrough();
      self.destStream.on('drain', function() {
        flushWriteCbs(self);
      });
      self.destStream.headers = self.partHeaders;
      self.destStream.name = self.partName;
      self.destStream.filename = self.partFilename;
      self.destStream.byteOffset = self.bytesReceived + offset;
      var partContentLength = self.destStream.headers['content-length'];
      self.destStream.byteCount = partContentLength ? parseInt(partContentLength, 10) :
        self.bytesExpected ? (self.bytesExpected - self.destStream.byteOffset -
          self.boundary.length - LAST_BOUNDARY_SUFFIX_LEN) :
        undefined;
    
      self.emit('part', self.destStream);
      if (self.destStream.filename == null && self.autoFields) {
        handleField(self, self.destStream);
      } else if (self.destStream.filename != null && self.autoFiles) {
        handleFile(self, self.destStream);
      }
    }
    
    function flushWriteCbs(self) {
      self.writeCbs.forEach(function(cb) {
        process.nextTick(cb);
      });
      self.writeCbs = [];
      self.backpressure = false;
    }
    
    function getBytesExpected(headers) {
      var contentLength = headers['content-length'];
      if (contentLength) {
        return parseInt(contentLength, 10);
      } else if (headers['transfer-encoding'] == null) {
        return 0;
      } else {
        return null;
      }
    }
    
    function beginFlush(self) {
      self.flushing += 1;
    }
    
    function endFlush(self) {
      self.flushing -= 1;
      maybeClose(self);
    }
    
    function maybeClose(self) {
      if (!self.flushing && self.finished && !self.error) {
        process.nextTick(function() {
          self.emit('close');
        });
      }
    }
    
    function destroyFile(self, file) {
      if (!file.ws) return;
      file.ws.removeAllListeners('close');
      file.ws.on('close', function() {
        fs.unlink(file.path, function(err) {
          if (err && !self.error) self.handleError(err);
        });
      });
      file.ws.destroy();
    }
    
    function handleFile(self, fileStream) {
      if (self.error) return;
      beginFlush(self);
      var file = {
        fieldName: fileStream.name,
        originalFilename: fileStream.filename,
        path: uploadPath(self.uploadDir, fileStream.filename),
        headers: fileStream.headers,
      };
      file.ws = fs.createWriteStream(file.path);
      self.openedFiles.push(file);
      fileStream.pipe(file.ws);
      var counter = new StreamCounter();
      var seenBytes = 0;
      fileStream.pipe(counter);
      var hashWorkaroundStream
        , hash = null;
      if (self.hash) {
        // workaround stream because https://github.com/joyent/node/issues/5216
        hashWorkaroundStream = stream.Writable();
        hash = crypto.createHash(self.hash);
        hashWorkaroundStream._write = function(buffer, encoding, callback) {
          hash.update(buffer);
          callback();
        };
        fileStream.pipe(hashWorkaroundStream);
      }
      counter.on('progress', function() {
        var deltaBytes = counter.bytes - seenBytes;
        seenBytes += deltaBytes;
        self.totalFileSize += deltaBytes;
        if (self.totalFileSize > self.maxFilesSize) {
          if (hashWorkaroundStream) fileStream.unpipe(hashWorkaroundStream);
          fileStream.unpipe(counter);
          fileStream.unpipe(file.ws);
          self.handleError(new Error("maxFilesSize " + self.maxFilesSize + " exceeded"));
        }
      });
      file.ws.on('error', function(err) {
        if (!self.error) self.handleError(err);
      });
      file.ws.on('close', function() {
        if (hash) file.hash = hash.digest('hex');
        file.size = counter.bytes;
        self.emit('file', fileStream.name, file);
        endFlush(self);
      });
    }
    
    function handleField(self, fieldStream) {
      var value = '';
      var decoder = new StringDecoder(self.encoding);
    
      beginFlush(self);
      fieldStream.on('readable', function() {
        var buffer = fieldStream.read();
        if (!buffer) return;
    
        self.totalFieldSize += buffer.length;
        if (self.totalFieldSize > self.maxFieldsSize) {
          self.handleError(new Error("maxFieldsSize " + self.maxFieldsSize + " exceeded"));
          return;
        }
        value += decoder.write(buffer);
      });
    
      fieldStream.on('end', function() {
        self.emit('field', fieldStream.name, value);
        endFlush(self);
      });
    }
    
    function clearPartVars(self) {
      self.partHeaders = {};
      self.partName = null;
      self.partFilename = null;
      self.partTransferEncoding = 'binary';
      self.destStream = null;
    
      self.headerFieldDecoder = new StringDecoder(self.encoding);
      self.headerField = "";
      self.headerValueDecoder = new StringDecoder(self.encoding);
      self.headerValue = "";
    }
    
    function setUpParser(self, boundary) {
      self.boundary = new Buffer(boundary.length + 4);
      self.boundary.write('\r\n--', 0, boundary.length + 4, 'ascii');
      self.boundary.write(boundary, 4, boundary.length, 'ascii');
      self.lookbehind = new Buffer(self.boundary.length + 8);
      self.state = START;
      self.boundaryChars = {};
      for (var i = 0; i < self.boundary.length; i++) {
        self.boundaryChars[self.boundary[i]] = true;
      }
    
      self.index = null;
      self.partBoundaryFlag = false;
    
      self.on('finish', function() {
        if ((self.state === HEADER_FIELD_START && self.index === 0) ||
            (self.state === PART_DATA && self.index === self.boundary.length))
        {
          self.onParsePartEnd();
        } else if (self.state !== END) {
          self.handleError(new Error('stream ended unexpectedly'));
        }
        self.finished = true;
        maybeClose(self);
      });
    }
    
    function uploadPath(baseDir, filename) {
      var ext = path.extname(filename).replace(FILE_EXT_RE, '$1');
      var name = process.pid + '-' +
        (Math.random() * 0x100000000 + 1).toString(36) + ext;
      return path.join(baseDir, name);
    }
    
    function parseFilename(headerValue) {
      var m = headerValue.match(/\bfilename="(.*?)"($|; )/i);
      if (!m) {
        m = headerValue.match(/\bfilename\*=utf-8\'\'(.*?)($|; )/i);
        if (m) {
          m[1] = decodeURI(m[1]);
        }
        else {
          return;
        }
      }
    
      var filename = m[1].substr(m[1].lastIndexOf('\\') + 1);
      filename = filename.replace(/%22/g, '"');
      filename = filename.replace(/&#([\d]{4});/g, function(m, code) {
        return String.fromCharCode(code);
      });
      return filename;
    }
    
    function lower(c) {
      return c | 0x20;
    }
    
    
  provide("multiparty", module.exports);
}(global));

// pakmanager:connect/lib/utils
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect - utils
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var http = require('http')
      , crypto = require('crypto')
      , parse = require('url').parse
      , sep = require('path').sep
      , signature = require('cookie-signature')
      , nodeVersion = process.versions.node.split('.');
    
    // pause is broken in node < 0.10
    exports.brokenPause = parseInt(nodeVersion[0], 10) === 0
      && parseInt(nodeVersion[1], 10) < 10;
    
    /**
     * Return `true` if the request has a body, otherwise return `false`.
     *
     * @param  {IncomingMessage} req
     * @return {Boolean}
     * @api private
     */
    
    exports.hasBody = function(req) {
      var encoding = 'transfer-encoding' in req.headers;
      var length = 'content-length' in req.headers && req.headers['content-length'] !== '0';
      return encoding || length;
    };
    
    /**
     * Extract the mime type from the given request's
     * _Content-Type_ header.
     *
     * @param  {IncomingMessage} req
     * @return {String}
     * @api private
     */
    
    exports.mime = function(req) {
      var str = req.headers['content-type'] || ''
        , i = str.indexOf(';');
      return ~i ? str.slice(0, i) : str;
    };
    
    /**
     * Generate an `Error` from the given status `code`
     * and optional `msg`.
     *
     * @param {Number} code
     * @param {String} msg
     * @return {Error}
     * @api private
     */
    
    exports.error = function(code, msg){
      var err = new Error(msg || http.STATUS_CODES[code]);
      err.status = code;
      return err;
    };
    
    /**
     * Return md5 hash of the given string and optional encoding,
     * defaulting to hex.
     *
     *     utils.md5('wahoo');
     *     // => "e493298061761236c96b02ea6aa8a2ad"
     *
     * @param {String} str
     * @param {String} encoding
     * @return {String}
     * @api private
     */
    
    exports.md5 = function(str, encoding){
      return crypto
        .createHash('md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex');
    };
    
    /**
     * Merge object b with object a.
     *
     *     var a = { foo: 'bar' }
     *       , b = { bar: 'baz' };
     *
     *     utils.merge(a, b);
     *     // => { foo: 'bar', bar: 'baz' }
     *
     * @param {Object} a
     * @param {Object} b
     * @return {Object}
     * @api private
     */
    
    exports.merge = function(a, b){
      if (a && b) {
        for (var key in b) {
          a[key] = b[key];
        }
      }
      return a;
    };
    
    /**
     * Escape the given string of `html`.
     *
     * @param {String} html
     * @return {String}
     * @api private
     */
    
    exports.escape = function(html){
      return String(html)
        .replace(/&(?!\w+;)/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
    };
    
    /**
     * Sign the given `val` with `secret`.
     *
     * @param {String} val
     * @param {String} secret
     * @return {String}
     * @api private
     */
    
    exports.sign = function(val, secret){
      console.warn('do not use utils.sign(), use https://github.com/visionmedia/node-cookie-signature')
      return val + '.' + crypto
        .createHmac('sha256', secret)
        .update(val)
        .digest('base64')
        .replace(/=+$/, '');
    };
    
    /**
     * Unsign and decode the given `val` with `secret`,
     * returning `false` if the signature is invalid.
     *
     * @param {String} val
     * @param {String} secret
     * @return {String|Boolean}
     * @api private
     */
    
    exports.unsign = function(val, secret){
      console.warn('do not use utils.unsign(), use https://github.com/visionmedia/node-cookie-signature')
      var str = val.slice(0, val.lastIndexOf('.'));
      return exports.sign(str, secret) == val
        ? str
        : false;
    };
    
    /**
     * Parse signed cookies, returning an object
     * containing the decoded key/value pairs,
     * while removing the signed key from `obj`.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */
    
    exports.parseSignedCookies = function(obj, secret){
      var ret = {};
      Object.keys(obj).forEach(function(key){
        var val = obj[key];
        if (0 == val.indexOf('s:')) {
          val = signature.unsign(val.slice(2), secret);
          if (val) {
            ret[key] = val;
            delete obj[key];
          }
        }
      });
      return ret;
    };
    
    /**
     * Parse a signed cookie string, return the decoded value
     *
     * @param {String} str signed cookie string
     * @param {String} secret
     * @return {String} decoded value
     * @api private
     */
    
    exports.parseSignedCookie = function(str, secret){
      return 0 == str.indexOf('s:')
        ? signature.unsign(str.slice(2), secret)
        : str;
    };
    
    /**
     * Parse JSON cookies.
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */
    
    exports.parseJSONCookies = function(obj){
      Object.keys(obj).forEach(function(key){
        var val = obj[key];
        var res = exports.parseJSONCookie(val);
        if (res) obj[key] = res;
      });
      return obj;
    };
    
    /**
     * Parse JSON cookie string
     *
     * @param {String} str
     * @return {Object} Parsed object or null if not json cookie
     * @api private
     */
    
    exports.parseJSONCookie = function(str) {
      if (0 == str.indexOf('j:')) {
        try {
          return JSON.parse(str.slice(2));
        } catch (err) {
          // no op
        }
      }
    };
    
    /**
     * Pause `data` and `end` events on the given `obj`.
     * Middleware performing async tasks _should_ utilize
     * this utility (or similar), to re-emit data once
     * the async operation has completed, otherwise these
     * events may be lost. Pause is only required for
     * node versions less than 10, and is replaced with
     * noop's otherwise.
     *
     *      var pause = utils.pause(req);
     *      fs.readFile(path, function(){
     *         next();
     *         pause.resume();
     *      });
     *
     * @param {Object} obj
     * @return {Object}
     * @api private
     */
    
    exports.pause = exports.brokenPause
      ? require('pause')
      : function () {
        return {
          end: noop,
          resume: noop
        }
      }
    
    /**
     * Strip `Content-*` headers from `res`.
     *
     * @param {ServerResponse} res
     * @api private
     */
    
    exports.removeContentHeaders = function(res){
      if (!res._headers) return;
      Object.keys(res._headers).forEach(function(field){
        if (0 == field.indexOf('content')) {
          res.removeHeader(field);
        }
      });
    };
    
    /**
     * Check if `req` is a conditional GET request.
     *
     * @param {IncomingMessage} req
     * @return {Boolean}
     * @api private
     */
    
    exports.conditionalGET = function(req) {
      return req.headers['if-modified-since']
        || req.headers['if-none-match'];
    };
    
    /**
     * Respond with 401 "Unauthorized".
     *
     * @param {ServerResponse} res
     * @param {String} realm
     * @api private
     */
    
    exports.unauthorized = function(res, realm) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="' + realm + '"');
      res.end('Unauthorized');
    };
    
    /**
     * Respond with 304 "Not Modified".
     *
     * @param {ServerResponse} res
     * @param {Object} headers
     * @api private
     */
    
    exports.notModified = function(res) {
      exports.removeContentHeaders(res);
      res.statusCode = 304;
      res.end();
    };
    
    /**
     * Return an ETag in the form of `"<size>-<mtime>"`
     * from the given `stat`.
     *
     * @param {Object} stat
     * @return {String}
     * @api private
     */
    
    exports.etag = function(stat) {
      return '"' + stat.size + '-' + Number(stat.mtime) + '"';
    };
    
    /**
     * Parse the given Cache-Control `str`.
     *
     * @param {String} str
     * @return {Object}
     * @api private
     */
    
    exports.parseCacheControl = function(str){
      var directives = str.split(',')
        , obj = {};
    
      for(var i = 0, len = directives.length; i < len; i++) {
        var parts = directives[i].split('=')
          , key = parts.shift().trim()
          , val = parseInt(parts.shift(), 10);
    
        obj[key] = isNaN(val) ? true : val;
      }
    
      return obj;
    };
    
    /**
     * Parse the `req` url with memoization.
     *
     * @param {ServerRequest} req
     * @return {Object}
     * @api private
     */
    
    exports.parseUrl = function(req){
      var parsed = req._parsedUrl;
      if (parsed && parsed.href == req.url) {
        return parsed;
      } else {
        parsed = parse(req.url);
    
        if (parsed.auth && !parsed.protocol && ~parsed.href.indexOf('//')) {
          // This parses pathnames, and a strange pathname like //r@e should work
          parsed = parse(req.url.replace(/@/g, '%40'));
        }
    
        return req._parsedUrl = parsed;
      }
    };
    
    /**
     * Parse byte `size` string.
     *
     * @param {String} size
     * @return {Number}
     * @api private
     */
    
    exports.parseBytes = require('bytes');
    
    /**
     * Normalizes the path separator from system separator
     * to URL separator, aka `/`.
     *
     * @param {String} path
     * @return {String}
     * @api private
     */
    
    exports.normalizeSlashes = function normalizeSlashes(path) {
      return path.split(sep).join('/');
    };
    
    function noop() {}
    
  provide("connect/lib/utils", module.exports);
}(global));

// pakmanager:connect/lib/proto
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - HTTPServer
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var http = require('http')
      , utils =  require('connect/lib/utils')
      , debug = require('debug')('connect:dispatcher');
    
    // prototype
    
    var app = module.exports = {};
    
    // environment
    
    var env = process.env.NODE_ENV || 'development';
    
    /**
     * Utilize the given middleware `handle` to the given `route`,
     * defaulting to _/_. This "route" is the mount-point for the
     * middleware, when given a value other than _/_ the middleware
     * is only effective when that segment is present in the request's
     * pathname.
     *
     * For example if we were to mount a function at _/admin_, it would
     * be invoked on _/admin_, and _/admin/settings_, however it would
     * not be invoked for _/_, or _/posts_.
     *
     * Examples:
     *
     *      var app = connect();
     *      app.use(connect.favicon());
     *      app.use(connect.logger());
     *      app.use(connect.static(__dirname + '/public'));
     *
     * If we wanted to prefix static files with _/public_, we could
     * "mount" the `static()` middleware:
     *
     *      app.use('/public', connect.static(__dirname + '/public'));
     *
     * This api is chainable, so the following is valid:
     *
     *      connect()
     *        .use(connect.favicon())
     *        .use(connect.logger())
     *        .use(connect.static(__dirname + '/public'))
     *        .listen(3000);
     *
     * @param {String|Function|Server} route, callback or server
     * @param {Function|Server} callback or server
     * @return {Server} for chaining
     * @api public
     */
    
    app.use = function(route, fn){
      // default route to '/'
      if ('string' != typeof route) {
        fn = route;
        route = '/';
      }
    
      // wrap sub-apps
      if ('function' == typeof fn.handle) {
        var server = fn;
        fn.route = route;
        fn = function(req, res, next){
          server.handle(req, res, next);
        };
      }
    
      // wrap vanilla http.Servers
      if (fn instanceof http.Server) {
        fn = fn.listeners('request')[0];
      }
    
      // strip trailing slash
      if ('/' == route[route.length - 1]) {
        route = route.slice(0, -1);
      }
    
      // add the middleware
      debug('use %s %s', route || '/', fn.name || 'anonymous');
      this.stack.push({ route: route, handle: fn });
    
      return this;
    };
    
    /**
     * Handle server requests, punting them down
     * the middleware stack.
     *
     * @api private
     */
    
    app.handle = function(req, res, out) {
      var stack = this.stack
        , search = 1 + req.url.indexOf('?')
        , pathlength = search ? search - 1 : req.url.length
        , fqdn = 1 + req.url.substr(0, pathlength).indexOf('://')
        , protohost = fqdn ? req.url.substr(0, req.url.indexOf('/', 2 + fqdn)) : ''
        , removed = ''
        , slashAdded = false
        , index = 0;
    
      function next(err) {
        var layer, path, c;
    
        if (slashAdded) {
          req.url = req.url.substr(1);
          slashAdded = false;
        }
    
        req.url = protohost + removed + req.url.substr(protohost.length);
        req.originalUrl = req.originalUrl || req.url;
        removed = '';
    
        // next callback
        layer = stack[index++];
    
        // all done
        if (!layer || res.headerSent) {
          // delegate to parent
          if (out) return out(err);
    
          // unhandled error
          if (err) {
            // default to 500
            if (res.statusCode < 400) res.statusCode = 500;
            debug('default %s', res.statusCode);
    
            // respect err.status
            if (err.status) res.statusCode = err.status;
    
            // production gets a basic error message
            var msg = 'production' == env
              ? http.STATUS_CODES[res.statusCode]
              : err.stack || err.toString();
            msg = utils.escape(msg);
    
            // log to stderr in a non-test env
            if ('test' != env) console.error(err.stack || err.toString());
            if (res.headerSent) return req.socket.destroy();
            res.setHeader('Content-Type', 'text/html');
            res.setHeader('Content-Length', Buffer.byteLength(msg));
            if ('HEAD' == req.method) return res.end();
            res.end(msg);
          } else {
            debug('default 404');
            res.statusCode = 404;
            res.setHeader('Content-Type', 'text/html');
            if ('HEAD' == req.method) return res.end();
            res.end('Cannot ' + utils.escape(req.method) + ' ' + utils.escape(req.originalUrl) + '\n');
          }
          return;
        }
    
        try {
          path = utils.parseUrl(req).pathname;
          if (undefined == path) path = '/';
    
          // skip this layer if the route doesn't match.
          if (0 != path.toLowerCase().indexOf(layer.route.toLowerCase())) return next(err);
    
          c = path[layer.route.length];
          if (c && '/' != c && '.' != c) return next(err);
    
          // Call the layer handler
          // Trim off the part of the url that matches the route
          removed = layer.route;
          req.url = protohost + req.url.substr(protohost.length + removed.length);
    
          // Ensure leading slash
          if (!fqdn && '/' != req.url[0]) {
            req.url = '/' + req.url;
            slashAdded = true;
          }
    
          debug('%s %s : %s', layer.handle.name || 'anonymous', layer.route, req.originalUrl);
          var arity = layer.handle.length;
          if (err) {
            if (arity === 4) {
              layer.handle(err, req, res, next);
            } else {
              next(err);
            }
          } else if (arity < 4) {
            layer.handle(req, res, next);
          } else {
            next();
          }
        } catch (e) {
          next(e);
        }
      }
      next();
    };
    
    /**
     * Listen for connections.
     *
     * This method takes the same arguments
     * as node's `http.Server#listen()`.
     *
     * HTTP and HTTPS:
     *
     * If you run your application both as HTTP
     * and HTTPS you may wrap them individually,
     * since your Connect "server" is really just
     * a JavaScript `Function`.
     *
     *      var connect =  require('connect')
     *        , http = require('http')
     *        , https = require('https');
     *
     *      var app = connect();
     *
     *      http.createServer(app).listen(80);
     *      https.createServer(options, app).listen(443);
     *
     * @return {http.Server}
     * @api public
     */
    
    app.listen = function(){
      var server = http.createServer(this);
      return server.listen.apply(server, arguments);
    };
    
  provide("connect/lib/proto", module.exports);
}(global));

// pakmanager:connect/lib/patch
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /*!
     * Connect
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var http = require('http')
      , res = http.ServerResponse.prototype
      , setHeader = res.setHeader
      , _renderHeaders = res._renderHeaders
      , writeHead = res.writeHead;
    
    // apply only once
    
    if (!res._hasConnectPatch) {
    
      /**
       * Provide a public "header sent" flag
       * until node does.
       *
       * @return {Boolean}
       * @api public
       */
    
      res.__defineGetter__('headerSent', function(){
        return this._header;
      });
    
      /**
       * Set header `field` to `val`, special-casing
       * the `Set-Cookie` field for multiple support.
       *
       * @param {String} field
       * @param {String} val
       * @api public
       */
    
      res.setHeader = function(field, val){
        var key = field.toLowerCase()
          , prev;
    
        // special-case Set-Cookie
        if (this._headers && 'set-cookie' == key) {
          if (prev = this.getHeader(field)) {
              if (Array.isArray(prev)) {
                  val = prev.concat(val);
              } else if (Array.isArray(val)) {
                  val = val.concat(prev);
              } else {
                  val = [prev, val];
              }
          }
        // charset
        } else if ('content-type' == key && this.charset) {
          val += '; charset=' + this.charset;
        }
    
        return setHeader.call(this, field, val);
      };
    
      /**
       * Proxy to emit "header" event.
       */
    
      res._renderHeaders = function(){
        if (!this._emittedHeader) this.emit('header');
        this._emittedHeader = true;
        return _renderHeaders.call(this);
      };
    
      res.writeHead = function(statusCode, reasonPhrase, headers){
        if (typeof reasonPhrase === 'object') headers = reasonPhrase;
        if (typeof headers === 'object') {
          Object.keys(headers).forEach(function(key){
            this.setHeader(key, headers[key]);
          }, this);
        }
        if (!this._emittedHeader) this.emit('header');
        this._emittedHeader = true;
        return writeHead.call(this, statusCode, reasonPhrase);
      };
    
      res._hasConnectPatch = true;
    }
    
  provide("connect/lib/patch", module.exports);
}(global));

// pakmanager:connect/lib/middleware/static
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect - static
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Static:
     *
     * See [serve-static](https://github.com/expressjs/serve-static)
     *
     * @param {String} root
     * @param {Object} options
     * @return {Function}
     * @api public
     */
    
    module.exports = require('serve-static');
    
  provide("connect/lib/middleware/static", module.exports);
}(global));

// pakmanager:connect/lib/connect
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*!
     * Connect
     * Copyright(c) 2010 Sencha Inc.
     * Copyright(c) 2011 TJ Holowaychuk
     * MIT Licensed
     */
    
    /**
     * Module dependencies.
     */
    
    var EventEmitter = require('events').EventEmitter
      , proto =  require('connect/lib/proto')
      , utils =  require('connect/lib/utils')
      , path = require('path')
      , basename = path.basename
      , fs = require('fs');
    
    // node patches
    
     require('connect/lib/patch');
    
    // expose createServer() as the module
    
    exports = module.exports = createServer;
    
    /**
     * Framework version.
     */
    
    exports.version = '2.7.11';
    
    /**
     * Expose mime module.
     */
    
    exports.mime =  require('connect/lib/middleware/static').mime;
    
    /**
     * Expose the prototype.
     */
    
    exports.proto = proto;
    
    /**
     * Auto-load middleware getters.
     */
    
    exports.middleware = {};
    
    /**
     * Expose utilities.
     */
    
    exports.utils = utils;
    
    /**
     * Create a new connect server.
     *
     * @return {Function}
     * @api public
     */
    
    function createServer() {
      function app(req, res, next){ app.handle(req, res, next); }
      utils.merge(app, proto);
      utils.merge(app, EventEmitter.prototype);
      app.route = '/';
      app.stack = [];
      for (var i = 0; i < arguments.length; ++i) {
        app.use(arguments[i]);
      }
      return app;
    };
    
    /**
     * Support old `.createServer()` method.
     */
    
    createServer.createServer = createServer;
    
    /**
     * Auto-load bundled middleware with getters.
     */
    
    fs.readdirSync(__dirname + '/middleware').forEach(function(filename){
      if (!/\.js$/.test(filename)) return;
      var name = basename(filename, '.js');
      function load(){ return require('./middleware/' + name); }
      exports.middleware.__defineGetter__(name, load);
      exports.__defineGetter__(name, load);
    });
    
  provide("connect/lib/connect", module.exports);
}(global));

// pakmanager:connect
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    module.exports = process.env.CONNECT_COV
      ? require('./lib-cov/connect')
      :  require('connect/lib/connect');
  provide("connect", module.exports);
}(global));

// pakmanager:commander
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var EventEmitter = require('events').EventEmitter;
    var spawn = require('child_process').spawn;
    var fs = require('fs');
    var exists = fs.existsSync;
    var path = require('path');
    var dirname = path.dirname;
    var basename = path.basename;
    
    /**
     * Expose the root command.
     */
    
    exports = module.exports = new Command;
    
    /**
     * Expose `Command`.
     */
    
    exports.Command = Command;
    
    /**
     * Expose `Option`.
     */
    
    exports.Option = Option;
    
    /**
     * Initialize a new `Option` with the given `flags` and `description`.
     *
     * @param {String} flags
     * @param {String} description
     * @api public
     */
    
    function Option(flags, description) {
      this.flags = flags;
      this.required = ~flags.indexOf('<');
      this.optional = ~flags.indexOf('[');
      this.bool = !~flags.indexOf('-no-');
      flags = flags.split(/[ ,|]+/);
      if (flags.length > 1 && !/^[[<]/.test(flags[1])) this.short = flags.shift();
      this.long = flags.shift();
      this.description = description || '';
    }
    
    /**
     * Return option name.
     *
     * @return {String}
     * @api private
     */
    
    Option.prototype.name = function(){
      return this.long
        .replace('--', '')
        .replace('no-', '');
    };
    
    /**
     * Check if `arg` matches the short or long flag.
     *
     * @param {String} arg
     * @return {Boolean}
     * @api private
     */
    
    Option.prototype.is = function(arg){
      return arg == this.short
        || arg == this.long;
    };
    
    /**
     * Initialize a new `Command`.
     *
     * @param {String} name
     * @api public
     */
    
    function Command(name) {
      this.commands = [];
      this.options = [];
      this._execs = [];
      this._args = [];
      this._name = name;
    }
    
    /**
     * Inherit from `EventEmitter.prototype`.
     */
    
    Command.prototype.__proto__ = EventEmitter.prototype;
    
    /**
     * Add command `name`.
     *
     * The `.action()` callback is invoked when the
     * command `name` is specified via __ARGV__,
     * and the remaining arguments are applied to the
     * function for access.
     *
     * When the `name` is "*" an un-matched command
     * will be passed as the first arg, followed by
     * the rest of __ARGV__ remaining.
     *
     * Examples:
     *
     *      program
     *        .version('0.0.1')
     *        .option('-C, --chdir <path>', 'change the working directory')
     *        .option('-c, --config <path>', 'set config path. defaults to ./deploy.conf')
     *        .option('-T, --no-tests', 'ignore test hook')
     *
     *      program
     *        .command('setup')
     *        .description('run remote setup commands')
     *        .action(function(){
     *          console.log('setup');
     *        });
     *
     *      program
     *        .command('exec <cmd>')
     *        .description('run the given remote command')
     *        .action(function(cmd){
     *          console.log('exec "%s"', cmd);
     *        });
     *
     *      program
     *        .command('*')
     *        .description('deploy the given env')
     *        .action(function(env){
     *          console.log('deploying "%s"', env);
     *        });
     *
     *      program.parse(process.argv);
      *
     * @param {String} name
     * @param {String} [desc]
     * @return {Command} the new command
     * @api public
     */
    
    Command.prototype.command = function(name, desc){
      var args = name.split(/ +/);
      var cmd = new Command(args.shift());
      if (desc) cmd.description(desc);
      if (desc) this.executables = true;
      if (desc) this._execs[cmd._name] = true;
      this.commands.push(cmd);
      cmd.parseExpectedArgs(args);
      cmd.parent = this;
      if (desc) return this;
      return cmd;
    };
    
    /**
     * Add an implicit `help [cmd]` subcommand
     * which invokes `--help` for the given command.
     *
     * @api private
     */
    
    Command.prototype.addImplicitHelpCommand = function() {
      this.command('help [cmd]', 'display help for [cmd]');
    };
    
    /**
     * Parse expected `args`.
     *
     * For example `["[type]"]` becomes `[{ required: false, name: 'type' }]`.
     *
     * @param {Array} args
     * @return {Command} for chaining
     * @api public
     */
    
    Command.prototype.parseExpectedArgs = function(args){
      if (!args.length) return;
      var self = this;
      args.forEach(function(arg){
        switch (arg[0]) {
          case '<':
            self._args.push({ required: true, name: arg.slice(1, -1) });
            break;
          case '[':
            self._args.push({ required: false, name: arg.slice(1, -1) });
            break;
        }
      });
      return this;
    };
    
    /**
     * Register callback `fn` for the command.
     *
     * Examples:
     *
     *      program
     *        .command('help')
     *        .description('display verbose help')
     *        .action(function(){
     *           // output help here
     *        });
     *
     * @param {Function} fn
     * @return {Command} for chaining
     * @api public
     */
    
    Command.prototype.action = function(fn){
      var self = this;
      this.parent.on(this._name, function(args, unknown){
        // Parse any so-far unknown options
        unknown = unknown || [];
        var parsed = self.parseOptions(unknown);
    
        // Output help if necessary
        outputHelpIfNecessary(self, parsed.unknown);
    
        // If there are still any unknown options, then we simply
        // die, unless someone asked for help, in which case we give it
        // to them, and then we die.
        if (parsed.unknown.length > 0) {
          self.unknownOption(parsed.unknown[0]);
        }
    
        // Leftover arguments need to be pushed back. Fixes issue #56
        if (parsed.args.length) args = parsed.args.concat(args);
    
        self._args.forEach(function(arg, i){
          if (arg.required && null == args[i]) {
            self.missingArgument(arg.name);
          }
        });
    
        // Always append ourselves to the end of the arguments,
        // to make sure we match the number of arguments the user
        // expects
        if (self._args.length) {
          args[self._args.length] = self;
        } else {
          args.push(self);
        }
    
        fn.apply(this, args);
      });
      return this;
    };
    
    /**
     * Define option with `flags`, `description` and optional
     * coercion `fn`.
     *
     * The `flags` string should contain both the short and long flags,
     * separated by comma, a pipe or space. The following are all valid
     * all will output this way when `--help` is used.
     *
     *    "-p, --pepper"
     *    "-p|--pepper"
     *    "-p --pepper"
     *
     * Examples:
     *
     *     // simple boolean defaulting to false
     *     program.option('-p, --pepper', 'add pepper');
     *
     *     --pepper
     *     program.pepper
     *     // => Boolean
     *
     *     // simple boolean defaulting to false
     *     program.option('-C, --no-cheese', 'remove cheese');
     *
     *     program.cheese
     *     // => true
     *
     *     --no-cheese
     *     program.cheese
     *     // => true
     *
     *     // required argument
     *     program.option('-C, --chdir <path>', 'change the working directory');
     *
     *     --chdir /tmp
     *     program.chdir
     *     // => "/tmp"
     *
     *     // optional argument
     *     program.option('-c, --cheese [type]', 'add cheese [marble]');
     *
     * @param {String} flags
     * @param {String} description
     * @param {Function|Mixed} fn or default
     * @param {Mixed} defaultValue
     * @return {Command} for chaining
     * @api public
     */
    
    Command.prototype.option = function(flags, description, fn, defaultValue){
      var self = this
        , option = new Option(flags, description)
        , oname = option.name()
        , name = camelcase(oname);
    
      // default as 3rd arg
      if ('function' != typeof fn) defaultValue = fn, fn = null;
    
      // preassign default value only for --no-*, [optional], or <required>
      if (false == option.bool || option.optional || option.required) {
        // when --no-* we make sure default is true
        if (false == option.bool) defaultValue = true;
        // preassign only if we have a default
        if (undefined !== defaultValue) self[name] = defaultValue;
      }
    
      // register the option
      this.options.push(option);
    
      // when it's passed assign the value
      // and conditionally invoke the callback
      this.on(oname, function(val){
        // coercion
        if (null !== val && fn) val = fn(val, undefined === self[name] ? defaultValue : self[name]);
    
        // unassigned or bool
        if ('boolean' == typeof self[name] || 'undefined' == typeof self[name]) {
          // if no value, bool true, and we have a default, then use it!
          if (null == val) {
            self[name] = option.bool
              ? defaultValue || true
              : false;
          } else {
            self[name] = val;
          }
        } else if (null !== val) {
          // reassign
          self[name] = val;
        }
      });
    
      return this;
    };
    
    /**
     * Parse `argv`, settings options and invoking commands when defined.
     *
     * @param {Array} argv
     * @return {Command} for chaining
     * @api public
     */
    
    Command.prototype.parse = function(argv){
      // implicit help
      if (this.executables) this.addImplicitHelpCommand();
    
      // store raw args
      this.rawArgs = argv;
    
      // guess name
      this._name = this._name || basename(argv[1], '.js');
    
      // process argv
      var parsed = this.parseOptions(this.normalize(argv.slice(2)));
      var args = this.args = parsed.args;
    
      var result = this.parseArgs(this.args, parsed.unknown);
    
      // executable sub-commands
      var name = result.args[0];
      if (this._execs[name]) return this.executeSubCommand(argv, args, parsed.unknown);
    
      return result;
    };
    
    /**
     * Execute a sub-command executable.
     *
     * @param {Array} argv
     * @param {Array} args
     * @param {Array} unknown
     * @api private
     */
    
    Command.prototype.executeSubCommand = function(argv, args, unknown) {
      args = args.concat(unknown);
    
      if (!args.length) this.help();
      if ('help' == args[0] && 1 == args.length) this.help();
    
      // <cmd> --help
      if ('help' == args[0]) {
        args[0] = args[1];
        args[1] = '--help';
      }
    
      // executable
      var dir = dirname(argv[1]);
      var bin = basename(argv[1], '.js') + '-' + args[0];
    
      // check for ./<bin> first
      var local = path.join(dir, bin);
    
      // run it
      args = args.slice(1);
      args.unshift(local);
      var proc = spawn('node', args, { stdio: 'inherit', customFds: [0, 1, 2] });
      proc.on('error', function(err){
        if (err.code == "ENOENT") {
          console.error('\n  %s(1) does not exist, try --help\n', bin);
        } else if (err.code == "EACCES") {
          console.error('\n  %s(1) not executable. try chmod or run with root\n', bin);
        }
      });
    
      this.runningCommand = proc;
    };
    
    /**
     * Normalize `args`, splitting joined short flags. For example
     * the arg "-abc" is equivalent to "-a -b -c".
     * This also normalizes equal sign and splits "--abc=def" into "--abc def".
     *
     * @param {Array} args
     * @return {Array}
     * @api private
     */
    
    Command.prototype.normalize = function(args){
      var ret = []
        , arg
        , lastOpt
        , index;
    
      for (var i = 0, len = args.length; i < len; ++i) {
        arg = args[i];
        i > 0 && (lastOpt = this.optionFor(args[i-1]));
    
        if (lastOpt && lastOpt.required) {
         	ret.push(arg);
        } else if (arg.length > 1 && '-' == arg[0] && '-' != arg[1]) {
          arg.slice(1).split('').forEach(function(c){
            ret.push('-' + c);
          });
        } else if (/^--/.test(arg) && ~(index = arg.indexOf('='))) {
          ret.push(arg.slice(0, index), arg.slice(index + 1));
        } else {
          ret.push(arg);
        }
      }
    
      return ret;
    };
    
    /**
     * Parse command `args`.
     *
     * When listener(s) are available those
     * callbacks are invoked, otherwise the "*"
     * event is emitted and those actions are invoked.
     *
     * @param {Array} args
     * @return {Command} for chaining
     * @api private
     */
    
    Command.prototype.parseArgs = function(args, unknown){
      var cmds = this.commands
        , len = cmds.length
        , name;
    
      if (args.length) {
        name = args[0];
        if (this.listeners(name).length) {
          this.emit(args.shift(), args, unknown);
        } else {
          this.emit('*', args);
        }
      } else {
        outputHelpIfNecessary(this, unknown);
    
        // If there were no args and we have unknown options,
        // then they are extraneous and we need to error.
        if (unknown.length > 0) {
          this.unknownOption(unknown[0]);
        }
      }
    
      return this;
    };
    
    /**
     * Return an option matching `arg` if any.
     *
     * @param {String} arg
     * @return {Option}
     * @api private
     */
    
    Command.prototype.optionFor = function(arg){
      for (var i = 0, len = this.options.length; i < len; ++i) {
        if (this.options[i].is(arg)) {
          return this.options[i];
        }
      }
    };
    
    /**
     * Parse options from `argv` returning `argv`
     * void of these options.
     *
     * @param {Array} argv
     * @return {Array}
     * @api public
     */
    
    Command.prototype.parseOptions = function(argv){
      var args = []
        , len = argv.length
        , literal
        , option
        , arg;
    
      var unknownOptions = [];
    
      // parse options
      for (var i = 0; i < len; ++i) {
        arg = argv[i];
    
        // literal args after --
        if ('--' == arg) {
          literal = true;
          continue;
        }
    
        if (literal) {
          args.push(arg);
          continue;
        }
    
        // find matching Option
        option = this.optionFor(arg);
    
        // option is defined
        if (option) {
          // requires arg
          if (option.required) {
            arg = argv[++i];
            if (null == arg) return this.optionMissingArgument(option);
            this.emit(option.name(), arg);
          // optional arg
          } else if (option.optional) {
            arg = argv[i+1];
            if (null == arg || ('-' == arg[0] && '-' != arg)) {
              arg = null;
            } else {
              ++i;
            }
            this.emit(option.name(), arg);
          // bool
          } else {
            this.emit(option.name());
          }
          continue;
        }
    
        // looks like an option
        if (arg.length > 1 && '-' == arg[0]) {
          unknownOptions.push(arg);
    
          // If the next argument looks like it might be
          // an argument for this option, we pass it on.
          // If it isn't, then it'll simply be ignored
          if (argv[i+1] && '-' != argv[i+1][0]) {
            unknownOptions.push(argv[++i]);
          }
          continue;
        }
    
        // arg
        args.push(arg);
      }
    
      return { args: args, unknown: unknownOptions };
    };
    
    /**
     * Argument `name` is missing.
     *
     * @param {String} name
     * @api private
     */
    
    Command.prototype.missingArgument = function(name){
      console.error();
      console.error("  error: missing required argument `%s'", name);
      console.error();
      process.exit(1);
    };
    
    /**
     * `Option` is missing an argument, but received `flag` or nothing.
     *
     * @param {String} option
     * @param {String} flag
     * @api private
     */
    
    Command.prototype.optionMissingArgument = function(option, flag){
      console.error();
      if (flag) {
        console.error("  error: option `%s' argument missing, got `%s'", option.flags, flag);
      } else {
        console.error("  error: option `%s' argument missing", option.flags);
      }
      console.error();
      process.exit(1);
    };
    
    /**
     * Unknown option `flag`.
     *
     * @param {String} flag
     * @api private
     */
    
    Command.prototype.unknownOption = function(flag){
      console.error();
      console.error("  error: unknown option `%s'", flag);
      console.error();
      process.exit(1);
    };
    
    
    /**
     * Set the program version to `str`.
     *
     * This method auto-registers the "-V, --version" flag
     * which will print the version number when passed.
     *
     * @param {String} str
     * @param {String} flags
     * @return {Command} for chaining
     * @api public
     */
    
    Command.prototype.version = function(str, flags){
      if (0 == arguments.length) return this._version;
      this._version = str;
      flags = flags || '-V, --version';
      this.option(flags, 'output the version number');
      this.on('version', function(){
        console.log(str);
        process.exit(0);
      });
      return this;
    };
    
    /**
     * Set the description `str`.
     *
     * @param {String} str
     * @return {String|Command}
     * @api public
     */
    
    Command.prototype.description = function(str){
      if (0 == arguments.length) return this._description;
      this._description = str;
      return this;
    };
    
    /**
     * Set / get the command usage `str`.
     *
     * @param {String} str
     * @return {String|Command}
     * @api public
     */
    
    Command.prototype.usage = function(str){
      var args = this._args.map(function(arg){
        return arg.required
          ? '<' + arg.name + '>'
          : '[' + arg.name + ']';
      });
    
      var usage = '[options'
        + (this.commands.length ? '] [command' : '')
        + ']'
        + (this._args.length ? ' ' + args : '');
    
      if (0 == arguments.length) return this._usage || usage;
      this._usage = str;
    
      return this;
    };
    
    /**
     * Return the largest option length.
     *
     * @return {Number}
     * @api private
     */
    
    Command.prototype.largestOptionLength = function(){
      return this.options.reduce(function(max, option){
        return Math.max(max, option.flags.length);
      }, 0);
    };
    
    /**
     * Return help for options.
     *
     * @return {String}
     * @api private
     */
    
    Command.prototype.optionHelp = function(){
      var width = this.largestOptionLength();
    
      // Prepend the help information
      return [pad('-h, --help', width) + '  ' + 'output usage information']
        .concat(this.options.map(function(option){
          return pad(option.flags, width)
            + '  ' + option.description;
          }))
        .join('\n');
    };
    
    /**
     * Return command help documentation.
     *
     * @return {String}
     * @api private
     */
    
    Command.prototype.commandHelp = function(){
      if (!this.commands.length) return '';
      return [
          ''
        , '  Commands:'
        , ''
        , this.commands.map(function(cmd){
          var args = cmd._args.map(function(arg){
            return arg.required
              ? '<' + arg.name + '>'
              : '[' + arg.name + ']';
          }).join(' ');
    
          return pad(cmd._name
            + (cmd.options.length
              ? ' [options]'
              : '') + ' ' + args, 22)
            + (cmd.description()
              ? ' ' + cmd.description()
              : '');
        }).join('\n').replace(/^/gm, '    ')
        , ''
      ].join('\n');
    };
    
    /**
     * Return program help documentation.
     *
     * @return {String}
     * @api private
     */
    
    Command.prototype.helpInformation = function(){
      return [
          ''
        , '  Usage: ' + this._name + ' ' + this.usage()
        , '' + this.commandHelp()
        , '  Options:'
        , ''
        , '' + this.optionHelp().replace(/^/gm, '    ')
        , ''
        , ''
      ].join('\n');
    };
    
    /**
     * Output help information for this command
     *
     * @api public
     */
    
    Command.prototype.outputHelp = function(){
      process.stdout.write(this.helpInformation());
      this.emit('--help');
    };
    
    /**
     * Output help information and exit.
     *
     * @api public
     */
    
    Command.prototype.help = function(){
      this.outputHelp();
      process.exit();
    };
    
    /**
     * Camel-case the given `flag`
     *
     * @param {String} flag
     * @return {String}
     * @api private
     */
    
    function camelcase(flag) {
      return flag.split('-').reduce(function(str, word){
        return str + word[0].toUpperCase() + word.slice(1);
      });
    }
    
    /**
     * Pad `str` to `width`.
     *
     * @param {String} str
     * @param {Number} width
     * @return {String}
     * @api private
     */
    
    function pad(str, width) {
      var len = Math.max(0, width - str.length);
      return str + Array(len + 1).join(' ');
    }
    
    /**
     * Output help information if necessary
     *
     * @param {Command} command to output help for
     * @param {Array} array of options to search for -h or --help
     * @api private
     */
    
    function outputHelpIfNecessary(cmd, options) {
      options = options || [];
      for (var i = 0; i < options.length; i++) {
        if (options[i] == '--help' || options[i] == '-h') {
          cmd.outputHelp();
          process.exit(0);
        }
      }
    }
    
  provide("commander", module.exports);
}(global));

// pakmanager:mkdirp
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var path = require('path');
    var fs = require('fs');
    
    module.exports = mkdirP.mkdirp = mkdirP.mkdirP = mkdirP;
    
    function mkdirP (p, mode, f, made) {
        if (typeof mode === 'function' || mode === undefined) {
            f = mode;
            mode = 0777 & (~process.umask());
        }
        if (!made) made = null;
    
        var cb = f || function () {};
        if (typeof mode === 'string') mode = parseInt(mode, 8);
        p = path.resolve(p);
    
        fs.mkdir(p, mode, function (er) {
            if (!er) {
                made = made || p;
                return cb(null, made);
            }
            switch (er.code) {
                case 'ENOENT':
                    mkdirP(path.dirname(p), mode, function (er, made) {
                        if (er) cb(er, made);
                        else mkdirP(p, mode, cb, made);
                    });
                    break;
    
                // In the case of any other error, just see if there's a dir
                // there already.  If so, then hooray!  If not, then something
                // is borked.
                default:
                    fs.stat(p, function (er2, stat) {
                        // if the stat fails, then that's super weird.
                        // let the original error be the failure reason.
                        if (er2 || !stat.isDirectory()) cb(er, made)
                        else cb(null, made);
                    });
                    break;
            }
        });
    }
    
    mkdirP.sync = function sync (p, mode, made) {
        if (mode === undefined) {
            mode = 0777 & (~process.umask());
        }
        if (!made) made = null;
    
        if (typeof mode === 'string') mode = parseInt(mode, 8);
        p = path.resolve(p);
    
        try {
            fs.mkdirSync(p, mode);
            made = made || p;
        }
        catch (err0) {
            switch (err0.code) {
                case 'ENOENT' :
                    made = sync(path.dirname(p), mode, made);
                    sync(p, mode, made);
                    break;
    
                // In the case of any other error, just see if there's a dir
                // there already.  If so, then hooray!  If not, then something
                // is borked.
                default:
                    var stat;
                    try {
                        stat = fs.statSync(p);
                    }
                    catch (err1) {
                        throw err0;
                    }
                    if (!stat.isDirectory()) throw err0;
                    break;
            }
        }
    
        return made;
    };
    
  provide("mkdirp", module.exports);
}(global));

// pakmanager:merge-descriptors
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports = function (dest, src) {
      Object.getOwnPropertyNames(src).forEach(function (name) {
        var descriptor = Object.getOwnPropertyDescriptor(src, name)
        Object.defineProperty(dest, name, descriptor)
      })
    
      return dest
    }
  provide("merge-descriptors", module.exports);
}(global));

// pakmanager:express/lib/utils
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var mime = require('connect').mime
      , crc32 = require('buffer-crc32');
    
    /**
     * toString ref.
     */
    
    var toString = {}.toString;
    
    /**
     * Return ETag for `body`.
     *
     * @param {String|Buffer} body
     * @return {String}
     * @api private
     */
    
    exports.etag = function(body){
      return '"' + crc32.signed(body) + '"';
    };
    
    /**
     * Make `locals()` bound to the given `obj`.
     *
     * This is used for `app.locals` and `res.locals`.
     *
     * @param {Object} obj
     * @return {Function}
     * @api private
     */
    
    exports.locals = function(){
      function locals(obj){
        for (var key in obj) locals[key] = obj[key];
        return obj;
      };
    
      return locals;
    };
    
    /**
     * Check if `path` looks absolute.
     *
     * @param {String} path
     * @return {Boolean}
     * @api private
     */
    
    exports.isAbsolute = function(path){
      if ('/' == path[0]) return true;
      if (':' == path[1] && '\\' == path[2]) return true;
      if ('\\\\' == path.substring(0, 2)) return true; // Microsoft Azure absolute path
    };
    
    /**
     * Flatten the given `arr`.
     *
     * @param {Array} arr
     * @return {Array}
     * @api private
     */
    
    exports.flatten = function(arr, ret){
      var ret = ret || []
        , len = arr.length;
      for (var i = 0; i < len; ++i) {
        if (Array.isArray(arr[i])) {
          exports.flatten(arr[i], ret);
        } else {
          ret.push(arr[i]);
        }
      }
      return ret;
    };
    
    /**
     * Normalize the given `type`, for example "html" becomes "text/html".
     *
     * @param {String} type
     * @return {Object}
     * @api private
     */
    
    exports.normalizeType = function(type){
      return ~type.indexOf('/')
        ? acceptParams(type)
        : { value: mime.lookup(type), params: {} };
    };
    
    /**
     * Normalize `types`, for example "html" becomes "text/html".
     *
     * @param {Array} types
     * @return {Array}
     * @api private
     */
    
    exports.normalizeTypes = function(types){
      var ret = [];
    
      for (var i = 0; i < types.length; ++i) {
        ret.push(exports.normalizeType(types[i]));
      }
    
      return ret;
    };
    
    /**
     * Return the acceptable type in `types`, if any.
     *
     * @param {Array} types
     * @param {String} str
     * @return {String}
     * @api private
     */
    
    exports.acceptsArray = function(types, str){
      // accept anything when Accept is not present
      if (!str) return types[0];
    
      // parse
      var accepted = exports.parseAccept(str)
        , normalized = exports.normalizeTypes(types)
        , len = accepted.length;
    
      for (var i = 0; i < len; ++i) {
        for (var j = 0, jlen = types.length; j < jlen; ++j) {
          if (exports.accept(normalized[j], accepted[i])) {
            return types[j];
          }
        }
      }
    };
    
    /**
     * Check if `type(s)` are acceptable based on
     * the given `str`.
     *
     * @param {String|Array} type(s)
     * @param {String} str
     * @return {Boolean|String}
     * @api private
     */
    
    exports.accepts = function(type, str){
      if ('string' == typeof type) type = type.split(/ *, */);
      return exports.acceptsArray(type, str);
    };
    
    /**
     * Check if `type` array is acceptable for `other`.
     *
     * @param {Object} type
     * @param {Object} other
     * @return {Boolean}
     * @api private
     */
    
    exports.accept = function(type, other){
      var t = type.value.split('/');
      return (t[0] == other.type || '*' == other.type)
        && (t[1] == other.subtype || '*' == other.subtype)
        && paramsEqual(type.params, other.params);
    };
    
    /**
     * Check if accept params are equal.
     *
     * @param {Object} a
     * @param {Object} b
     * @return {Boolean}
     * @api private
     */
    
    function paramsEqual(a, b){
      return !Object.keys(a).some(function(k) {
        return a[k] != b[k];
      });
    }
    
    /**
     * Parse accept `str`, returning
     * an array objects containing
     * `.type` and `.subtype` along
     * with the values provided by
     * `parseQuality()`.
     *
     * @param {Type} name
     * @return {Type}
     * @api private
     */
    
    exports.parseAccept = function(str){
      return exports
        .parseParams(str)
        .map(function(obj){
          var parts = obj.value.split('/');
          obj.type = parts[0];
          obj.subtype = parts[1];
          return obj;
        });
    };
    
    /**
     * Parse quality `str`, returning an
     * array of objects with `.value`,
     * `.quality` and optional `.params`
     *
     * @param {String} str
     * @return {Array}
     * @api private
     */
    
    exports.parseParams = function(str){
      return str
        .split(/ *, */)
        .map(acceptParams)
        .filter(function(obj){
          return obj.quality;
        })
        .sort(function(a, b){
          if (a.quality === b.quality) {
            return a.originalIndex - b.originalIndex;
          } else {
            return b.quality - a.quality;
          }
        });
    };
    
    /**
     * Parse accept params `str` returning an
     * object with `.value`, `.quality` and `.params`.
     * also includes `.originalIndex` for stable sorting
     *
     * @param {String} str
     * @return {Object}
     * @api private
     */
    
    function acceptParams(str, index) {
      var parts = str.split(/ *; */);
      var ret = { value: parts[0], quality: 1, params: {}, originalIndex: index };
    
      for (var i = 1; i < parts.length; ++i) {
        var pms = parts[i].split(/ *= */);
        if ('q' == pms[0]) {
          ret.quality = parseFloat(pms[1]);
        } else {
          ret.params[pms[0]] = pms[1];
        }
      }
    
      return ret;
    }
    
    /**
     * Escape special characters in the given string of html.
     *
     * @param  {String} html
     * @return {String}
     * @api private
     */
    
    exports.escape = function(html) {
      return String(html)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
    };
    
    /**
     * Normalize the given path string,
     * returning a regular expression.
     *
     * An empty array should be passed,
     * which will contain the placeholder
     * key names. For example "/user/:id" will
     * then contain ["id"].
     *
     * @param  {String|RegExp|Array} path
     * @param  {Array} keys
     * @param  {Boolean} sensitive
     * @param  {Boolean} strict
     * @return {RegExp}
     * @api private
     */
    
    exports.pathRegexp = function(path, keys, sensitive, strict) {
      if (toString.call(path) == '[object RegExp]') return path;
      if (Array.isArray(path)) path = '(' + path.join('|') + ')';
      path = path
        .concat(strict ? '' : '/?')
        .replace(/\/\(/g, '(?:/')
        .replace(/(\/)?(\.)?:(\w+)(?:(\(.*?\)))?(\?)?(\*)?/g, function(_, slash, format, key, capture, optional, star){
          keys.push({ name: key, optional: !! optional });
          slash = slash || '';
          return ''
            + (optional ? '' : slash)
            + '(?:'
            + (optional ? slash : '')
            + (format || '') + (capture || (format && '([^/.]+?)' || '([^/]+?)')) + ')'
            + (optional || '')
            + (star ? '(/*)?' : '');
        })
        .replace(/([\/.])/g, '\\$1')
        .replace(/\*/g, '(.*)');
      return new RegExp('^' + path + '$', sensitive ? '' : 'i');
    }
    
  provide("express/lib/utils", module.exports);
}(global));

// pakmanager:express/lib/router/route
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var utils =  require('express/lib/utils');
    
    /**
     * Expose `Route`.
     */
    
    module.exports = Route;
    
    /**
     * Initialize `Route` with the given HTTP `method`, `path`,
     * and an array of `callbacks` and `options`.
     *
     * Options:
     *
     *   - `sensitive`    enable case-sensitive routes
     *   - `strict`       enable strict matching for trailing slashes
     *
     * @param {String} method
     * @param {String} path
     * @param {Array} callbacks
     * @param {Object} options.
     * @api private
     */
    
    function Route(method, path, callbacks, options) {
      options = options || {};
      this.path = path;
      this.method = method;
      this.callbacks = callbacks;
      this.regexp = utils.pathRegexp(path
        , this.keys = []
        , options.sensitive
        , options.strict);
    }
    
    /**
     * Check if this route matches `path`, if so
     * populate `.params`.
     *
     * @param {String} path
     * @return {Boolean}
     * @api private
     */
    
    Route.prototype.match = function(path){
      var keys = this.keys
        , params = this.params = []
        , m = this.regexp.exec(path);
    
      if (!m) return false;
    
      for (var i = 1, len = m.length; i < len; ++i) {
        var key = keys[i - 1];
    
        try {
          var val = 'string' == typeof m[i]
            ? decodeURIComponent(m[i])
            : m[i];
        } catch(e) {
          var err = new Error("Failed to decode param '" + m[i] + "'");
          err.status = 400;
          throw err;
        }
    
        if (key) {
          params[key.name] = val;
        } else {
          params.push(val);
        }
      }
    
      return true;
    };
    
  provide("express/lib/router/route", module.exports);
}(global));

// pakmanager:express/lib/router
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var Route =  require('express/lib/router/route')
      , utils =  require('express/lib/utils')
      , methods = require('methods')
      , debug = require('debug')('express:router')
      , parse = require('connect').utils.parseUrl;
    
    /**
     * Expose `Router` constructor.
     */
    
    exports = module.exports = Router;
    
    /**
     * Initialize a new `Router` with the given `options`.
     *
     * @param {Object} options
     * @api private
     */
    
    function Router(options) {
      options = options || {};
      var self = this;
      this.map = {};
      this.params = {};
      this._params = [];
      this.caseSensitive = options.caseSensitive;
      this.strict = options.strict;
      this.middleware = function router(req, res, next){
        self._dispatch(req, res, next);
      };
    }
    
    /**
     * Register a param callback `fn` for the given `name`.
     *
     * @param {String|Function} name
     * @param {Function} fn
     * @return {Router} for chaining
     * @api public
     */
    
    Router.prototype.param = function(name, fn){
      // param logic
      if ('function' == typeof name) {
        this._params.push(name);
        return;
      }
    
      // apply param functions
      var params = this._params
        , len = params.length
        , ret;
    
      for (var i = 0; i < len; ++i) {
        if (ret = params[i](name, fn)) {
          fn = ret;
        }
      }
    
      // ensure we end up with a
      // middleware function
      if ('function' != typeof fn) {
        throw new Error('invalid param() call for ' + name + ', got ' + fn);
      }
    
      (this.params[name] = this.params[name] || []).push(fn);
      return this;
    };
    
    /**
     * Route dispatcher aka the route "middleware".
     *
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @param {Function} next
     * @api private
     */
    
    Router.prototype._dispatch = function(req, res, next){
      var params = this.params
        , self = this;
    
      debug('dispatching %s %s (%s)', req.method, req.url, req.originalUrl);
    
      // route dispatch
      (function pass(i, err){
        var paramCallbacks
          , paramIndex = 0
          , paramVal
          , route
          , keys
          , key;
    
        // match next route
        function nextRoute(err) {
          pass(req._route_index + 1, err);
        }
    
        // match route
        req.route = route = self.matchRequest(req, i);
    
        // implied OPTIONS
        if (!route && 'OPTIONS' == req.method) return self._options(req, res, next);
    
        // no route
        if (!route) return next(err);
        debug('matched %s %s', route.method, route.path);
    
        // we have a route
        // start at param 0
        req.params = route.params;
        keys = route.keys;
        i = 0;
    
        // param callbacks
        function param(err) {
          paramIndex = 0;
          key = keys[i++];
          paramVal = key && req.params[key.name];
          paramCallbacks = key && params[key.name];
    
          try {
            if ('route' == err) {
              nextRoute();
            } else if (err) {
              i = 0;
              callbacks(err);
            } else if (paramCallbacks && undefined !== paramVal) {
              paramCallback();
            } else if (key) {
              param();
            } else {
              i = 0;
              callbacks();
            }
          } catch (err) {
            param(err);
          }
        };
    
        param(err);
    
        // single param callbacks
        function paramCallback(err) {
          var fn = paramCallbacks[paramIndex++];
          if (err || !fn) return param(err);
          fn(req, res, paramCallback, paramVal, key.name);
        }
    
        // invoke route callbacks
        function callbacks(err) {
          var fn = route.callbacks[i++];
          try {
            if ('route' == err) {
              nextRoute();
            } else if (err && fn) {
              if (fn.length < 4) return callbacks(err);
              fn(err, req, res, callbacks);
            } else if (fn) {
              if (fn.length < 4) return fn(req, res, callbacks);
              callbacks();
            } else {
              nextRoute(err);
            }
          } catch (err) {
            callbacks(err);
          }
        }
      })(0);
    };
    
    /**
     * Respond to __OPTIONS__ method.
     *
     * @param {IncomingMessage} req
     * @param {ServerResponse} res
     * @api private
     */
    
    Router.prototype._options = function(req, res, next){
      var path = parse(req).pathname
        , body = this._optionsFor(path).join(',');
      if (!body) return next();
      res.set('Allow', body).send(body);
    };
    
    /**
     * Return an array of HTTP verbs or "options" for `path`.
     *
     * @param {String} path
     * @return {Array}
     * @api private
     */
    
    Router.prototype._optionsFor = function(path){
      var self = this;
      return methods.filter(function(method){
        var routes = self.map[method];
        if (!routes || 'options' == method) return;
        for (var i = 0, len = routes.length; i < len; ++i) {
          if (routes[i].match(path)) return true;
        }
      }).map(function(method){
        return method.toUpperCase();
      });
    };
    
    /**
     * Attempt to match a route for `req`
     * with optional starting index of `i`
     * defaulting to 0.
     *
     * @param {IncomingMessage} req
     * @param {Number} i
     * @return {Route}
     * @api private
     */
    
    Router.prototype.matchRequest = function(req, i, head){
      var method = req.method.toLowerCase()
        , url = parse(req)
        , path = url.pathname
        , routes = this.map
        , i = i || 0
        , route;
    
      // HEAD support
      if (!head && 'head' == method) {
        route = this.matchRequest(req, i, true);
        if (route) return route;
         method = 'get';
      }
    
      // routes for this method
      if (routes = routes[method]) {
    
        // matching routes
        for (var len = routes.length; i < len; ++i) {
          route = routes[i];
          if (route.match(path)) {
            req._route_index = i;
            return route;
          }
        }
      }
    };
    
    /**
     * Attempt to match a route for `method`
     * and `url` with optional starting
     * index of `i` defaulting to 0.
     *
     * @param {String} method
     * @param {String} url
     * @param {Number} i
     * @return {Route}
     * @api private
     */
    
    Router.prototype.match = function(method, url, i, head){
      var req = { method: method, url: url };
      return  this.matchRequest(req, i, head);
    };
    
    /**
     * Route `method`, `path`, and one or more callbacks.
     *
     * @param {String} method
     * @param {String} path
     * @param {Function} callback...
     * @return {Router} for chaining
     * @api private
     */
    
    Router.prototype.route = function(method, path, callbacks){
      var method = method.toLowerCase()
        , callbacks = utils.flatten([].slice.call(arguments, 2));
    
      // ensure path was given
      if (!path) throw new Error('Router#' + method + '() requires a path');
    
      // ensure all callbacks are functions
      callbacks.forEach(function(fn){
        if ('function' == typeof fn) return;
        var type = {}.toString.call(fn);
        var msg = '.' + method + '() requires callback functions but got a ' + type;
        throw new Error(msg);
      });
    
      // create the route
      debug('defined %s %s', method, path);
      var route = new Route(method, path, callbacks, {
        sensitive: this.caseSensitive,
        strict: this.strict
      });
    
      // add it
      (this.map[method] = this.map[method] || []).push(route);
      return this;
    };
    
    Router.prototype.all = function(path) {
      var self = this;
      var args = [].slice.call(arguments);
      methods.forEach(function(method){
          self.route.apply(self, [method].concat(args));
      });
      return this;
    };
    
    methods.forEach(function(method){
      Router.prototype[method] = function(path){
        var args = [method].concat([].slice.call(arguments));
        this.route.apply(this, args);
        return this;
      };
    });
    
  provide("express/lib/router", module.exports);
}(global));

// pakmanager:express/lib/middleware
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var utils = require('./utils');
    
    /**
     * Initialization middleware, exposing the
     * request and response to eachother, as well
     * as defaulting the X-Powered-By header field.
     *
     * @param {Function} app
     * @return {Function}
     * @api private
     */
    
    exports.init = function(app){
      return function expressInit(req, res, next){
        if (app.enabled('x-powered-by')) res.setHeader('X-Powered-By', 'Express');
        req.res = res;
        res.req = req;
        req.next = next;
    
        req.__proto__ = app.request;
        res.__proto__ = app.response;
    
        res.locals = res.locals || utils.locals(res);
    
        next();
      }
    };
    
  provide("express/lib/middleware", module.exports);
}(global));

// pakmanager:express/lib/view
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var path = require('path')
      , fs = require('fs')
      , utils = require('./utils')
      , dirname = path.dirname
      , basename = path.basename
      , extname = path.extname
      , exists = fs.existsSync || path.existsSync
      , join = path.join;
    
    /**
     * Expose `View`.
     */
    
    module.exports = View;
    
    /**
     * Initialize a new `View` with the given `name`.
     *
     * Options:
     *
     *   - `defaultEngine` the default template engine name
     *   - `engines` template engine require() cache
     *   - `root` root path for view lookup
     *
     * @param {String} name
     * @param {Object} options
     * @api private
     */
    
    function View(name, options) {
      options = options || {};
      this.name = name;
      this.root = options.root;
      var engines = options.engines;
      this.defaultEngine = options.defaultEngine;
      var ext = this.ext = extname(name);
      if (!ext && !this.defaultEngine) throw new Error('No default engine was specified and no extension was provided.');
      if (!ext) name += (ext = this.ext = ('.' != this.defaultEngine[0] ? '.' : '') + this.defaultEngine);
      this.engine = engines[ext] || (engines[ext] = require(ext.slice(1)).__express);
      this.path = this.lookup(name);
    }
    
    /**
     * Lookup view by the given `path`
     *
     * @param {String} path
     * @return {String}
     * @api private
     */
    
    View.prototype.lookup = function(path){
      var ext = this.ext;
    
      // <path>.<engine>
      if (!utils.isAbsolute(path)) path = join(this.root, path);
      if (exists(path)) return path;
    
      // <path>/index.<engine>
      path = join(dirname(path), basename(path, ext), 'index' + ext);
      if (exists(path)) return path;
    };
    
    /**
     * Render with the given `options` and callback `fn(err, str)`.
     *
     * @param {Object} options
     * @param {Function} fn
     * @api private
     */
    
    View.prototype.render = function(options, fn){
      this.engine(this.path, options, fn);
    };
    
  provide("express/lib/view", module.exports);
}(global));

// pakmanager:express/lib/application
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var connect = require('connect')
      , Router =  require('express/lib/router')
      , methods = require('methods')
      , middleware =  require('express/lib/middleware')
      , debug = require('debug')('express:application')
      , locals = require('./utils').locals
      , View =  require('express/lib/view')
      , utils = connect.utils
      , http = require('http');
    
    /**
     * Application prototype.
     */
    
    var app = exports = module.exports = {};
    
    /**
     * Initialize the server.
     *
     *   - setup default configuration
     *   - setup default middleware
     *   - setup route reflection methods
     *
     * @api private
     */
    
    app.init = function(){
      this.cache = {};
      this.settings = {};
      this.engines = {};
      this.defaultConfiguration();
    };
    
    /**
     * Initialize application configuration.
     *
     * @api private
     */
    
    app.defaultConfiguration = function(){
      // default settings
      this.enable('x-powered-by');
      this.enable('etag');
      this.set('env', process.env.NODE_ENV || 'development');
      this.set('subdomain offset', 2);
      debug('booting in %s mode', this.get('env'));
    
      // implicit middleware
      this.use(connect.query());
      this.use(middleware.init(this));
    
      // inherit protos
      this.on('mount', function(parent){
        this.request.__proto__ = parent.request;
        this.response.__proto__ = parent.response;
        this.engines.__proto__ = parent.engines;
        this.settings.__proto__ = parent.settings;
      });
    
      // router
      this._router = new Router(this);
      this.routes = this._router.map;
      this.__defineGetter__('router', function(){
        this._usedRouter = true;
        this._router.caseSensitive = this.enabled('case sensitive routing');
        this._router.strict = this.enabled('strict routing');
        return this._router.middleware;
      });
    
      // setup locals
      this.locals = locals(this);
    
      // default locals
      this.locals.settings = this.settings;
    
      // default configuration
      this.set('view', View);
      this.set('views', process.cwd() + '/views');
      this.set('jsonp callback name', 'callback');
    
      this.configure('development', function(){
        this.set('json spaces', 2);
      });
    
      this.configure('production', function(){
        this.enable('view cache');
      });
    };
    
    /**
     * Proxy `connect#use()` to apply settings to
     * mounted applications.
     *
     * @param {String|Function|Server} route
     * @param {Function|Server} fn
     * @return {app} for chaining
     * @api public
     */
    
    app.use = function(route, fn){
      var app;
    
      // default route to '/'
      if ('string' != typeof route) fn = route, route = '/';
    
      // express app
      if (fn.handle && fn.set) app = fn;
    
      // restore .app property on req and res
      if (app) {
        app.route = route;
        fn = function(req, res, next) {
          var orig = req.app;
          app.handle(req, res, function(err){
            req.__proto__ = orig.request;
            res.__proto__ = orig.response;
            next(err);
          });
        };
      }
    
      connect.proto.use.call(this, route, fn);
    
      // mounted an app
      if (app) {
        app.parent = this;
        app.emit('mount', this);
      }
    
      return this;
    };
    
    /**
     * Register the given template engine callback `fn`
     * as `ext`.
     *
     * By default will `require()` the engine based on the
     * file extension. For example if you try to render
     * a "foo.jade" file Express will invoke the following internally:
     *
     *     app.engine('jade', require('jade').__express);
     *
     * For engines that do not provide `.__express` out of the box,
     * or if you wish to "map" a different extension to the template engine
     * you may use this method. For example mapping the EJS template engine to
     * ".html" files:
     *
     *     app.engine('html', require('ejs').renderFile);
     *
     * In this case EJS provides a `.renderFile()` method with
     * the same signature that Express expects: `(path, options, callback)`,
     * though note that it aliases this method as `ejs.__express` internally
     * so if you're using ".ejs" extensions you dont need to do anything.
     *
     * Some template engines do not follow this convention, the
     * [Consolidate.js](https://github.com/visionmedia/consolidate.js)
     * library was created to map all of node's popular template
     * engines to follow this convention, thus allowing them to
     * work seamlessly within Express.
     *
     * @param {String} ext
     * @param {Function} fn
     * @return {app} for chaining
     * @api public
     */
    
    app.engine = function(ext, fn){
      if ('function' != typeof fn) throw new Error('callback function required');
      if ('.' != ext[0]) ext = '.' + ext;
      this.engines[ext] = fn;
      return this;
    };
    
    /**
     * Map the given param placeholder `name`(s) to the given callback(s).
     *
     * Parameter mapping is used to provide pre-conditions to routes
     * which use normalized placeholders. For example a _:user_id_ parameter
     * could automatically load a user's information from the database without
     * any additional code,
     *
     * The callback uses the same signature as middleware, the only difference
     * being that the value of the placeholder is passed, in this case the _id_
     * of the user. Once the `next()` function is invoked, just like middleware
     * it will continue on to execute the route, or subsequent parameter functions.
     *
     *      app.param('user_id', function(req, res, next, id){
     *        User.find(id, function(err, user){
     *          if (err) {
     *            next(err);
     *          } else if (user) {
     *            req.user = user;
     *            next();
     *          } else {
     *            next(new Error('failed to load user'));
     *          }
     *        });
     *      });
     *
     * @param {String|Array} name
     * @param {Function} fn
     * @return {app} for chaining
     * @api public
     */
    
    app.param = function(name, fn){
      var self = this
        , fns = [].slice.call(arguments, 1);
    
      // array
      if (Array.isArray(name)) {
        name.forEach(function(name){
          fns.forEach(function(fn){
            self.param(name, fn);
          });
        });
      // param logic
      } else if ('function' == typeof name) {
        this._router.param(name);
      // single
      } else {
        if (':' == name[0]) name = name.substr(1);
        fns.forEach(function(fn){
          self._router.param(name, fn);
        });
      }
    
      return this;
    };
    
    /**
     * Assign `setting` to `val`, or return `setting`'s value.
     *
     *    app.set('foo', 'bar');
     *    app.get('foo');
     *    // => "bar"
     *
     * Mounted servers inherit their parent server's settings.
     *
     * @param {String} setting
     * @param {String} val
     * @return {Server} for chaining
     * @api public
     */
    
    app.set = function(setting, val){
      if (1 == arguments.length) {
        return this.settings[setting];
      } else {
        this.settings[setting] = val;
        return this;
      }
    };
    
    /**
     * Return the app's absolute pathname
     * based on the parent(s) that have
     * mounted it.
     *
     * For example if the application was
     * mounted as "/admin", which itself
     * was mounted as "/blog" then the
     * return value would be "/blog/admin".
     *
     * @return {String}
     * @api private
     */
    
    app.path = function(){
      return this.parent
        ? this.parent.path() + this.route
        : '';
    };
    
    /**
     * Check if `setting` is enabled (truthy).
     *
     *    app.enabled('foo')
     *    // => false
     *
     *    app.enable('foo')
     *    app.enabled('foo')
     *    // => true
     *
     * @param {String} setting
     * @return {Boolean}
     * @api public
     */
    
    app.enabled = function(setting){
      return !!this.set(setting);
    };
    
    /**
     * Check if `setting` is disabled.
     *
     *    app.disabled('foo')
     *    // => true
     *
     *    app.enable('foo')
     *    app.disabled('foo')
     *    // => false
     *
     * @param {String} setting
     * @return {Boolean}
     * @api public
     */
    
    app.disabled = function(setting){
      return !this.set(setting);
    };
    
    /**
     * Enable `setting`.
     *
     * @param {String} setting
     * @return {app} for chaining
     * @api public
     */
    
    app.enable = function(setting){
      return this.set(setting, true);
    };
    
    /**
     * Disable `setting`.
     *
     * @param {String} setting
     * @return {app} for chaining
     * @api public
     */
    
    app.disable = function(setting){
      return this.set(setting, false);
    };
    
    /**
     * Configure callback for zero or more envs,
     * when no `env` is specified that callback will
     * be invoked for all environments. Any combination
     * can be used multiple times, in any order desired.
     *
     * Examples:
     *
     *    app.configure(function(){
     *      // executed for all envs
     *    });
     *
     *    app.configure('stage', function(){
     *      // executed staging env
     *    });
     *
     *    app.configure('stage', 'production', function(){
     *      // executed for stage and production
     *    });
     *
     * Note:
     *
     *  These callbacks are invoked immediately, and
     *  are effectively sugar for the following:
     *
     *     var env = process.env.NODE_ENV || 'development';
     *
     *      switch (env) {
     *        case 'development':
     *          ...
     *          break;
     *        case 'stage':
     *          ...
     *          break;
     *        case 'production':
     *          ...
     *          break;
     *      }
     *
     * @param {String} env...
     * @param {Function} fn
     * @return {app} for chaining
     * @api public
     */
    
    app.configure = function(env, fn){
      var envs = 'all'
        , args = [].slice.call(arguments);
      fn = args.pop();
      if (args.length) envs = args;
      if ('all' == envs || ~envs.indexOf(this.settings.env)) fn.call(this);
      return this;
    };
    
    /**
     * Delegate `.VERB(...)` calls to `router.VERB(...)`.
     */
    
    methods.forEach(function(method){
      app[method] = function(path){
        if ('get' == method && 1 == arguments.length) return this.set(path);
    
        // deprecated
        if (Array.isArray(path)) {
          console.trace('passing an array to app.VERB() is deprecated and will be removed in 4.0');
        }
    
        // if no router attached yet, attach the router
        if (!this._usedRouter) this.use(this.router);
    
        // setup route
        this._router[method].apply(this._router, arguments);
        return this;
      };
    });
    
    /**
     * Special-cased "all" method, applying the given route `path`,
     * middleware, and callback to _every_ HTTP method.
     *
     * @param {String} path
     * @param {Function} ...
     * @return {app} for chaining
     * @api public
     */
    
    app.all = function(path){
      var args = arguments;
      methods.forEach(function(method){
        app[method].apply(this, args);
      }, this);
      return this;
    };
    
    // del -> delete alias
    
    app.del = app.delete;
    
    /**
     * Render the given view `name` name with `options`
     * and a callback accepting an error and the
     * rendered template string.
     *
     * Example:
     *
     *    app.render('email', { name: 'Tobi' }, function(err, html){
     *      // ...
     *    })
     *
     * @param {String} name
     * @param {String|Function} options or fn
     * @param {Function} fn
     * @api public
     */
    
    app.render = function(name, options, fn){
      var opts = {}
        , cache = this.cache
        , engines = this.engines
        , view;
    
      // support callback function as second arg
      if ('function' == typeof options) {
        fn = options, options = {};
      }
    
      // merge app.locals
      utils.merge(opts, this.locals);
    
      // merge options._locals
      if (options._locals) utils.merge(opts, options._locals);
    
      // merge options
      utils.merge(opts, options);
    
      // set .cache unless explicitly provided
      opts.cache = null == opts.cache
        ? this.enabled('view cache')
        : opts.cache;
    
      // primed cache
      if (opts.cache) view = cache[name];
    
      // view
      if (!view) {
        view = new (this.get('view'))(name, {
          defaultEngine: this.get('view engine'),
          root: this.get('views'),
          engines: engines
        });
    
        if (!view.path) {
          var err = new Error('Failed to lookup view "' + name + '" in views directory "' + view.root + '"');
          err.view = view;
          return fn(err);
        }
    
        // prime the cache
        if (opts.cache) cache[name] = view;
      }
    
      // render
      try {
        view.render(opts, fn);
      } catch (err) {
        fn(err);
      }
    };
    
    /**
     * Listen for connections.
     *
     * A node `http.Server` is returned, with this
     * application (which is a `Function`) as its
     * callback. If you wish to create both an HTTP
     * and HTTPS server you may do so with the "http"
     * and "https" modules as shown here:
     *
     *    var http = require('http')
     *      , https = require('https')
     *      , express =  require('express')
     *      , app = express();
     *
     *    http.createServer(app).listen(80);
     *    https.createServer({ ... }, app).listen(443);
     *
     * @return {http.Server}
     * @api public
     */
    
    app.listen = function(){
      var server = http.createServer(this);
      return server.listen.apply(server, arguments);
    };
    
  provide("express/lib/application", module.exports);
}(global));

// pakmanager:express/lib/request
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    /**
     * Module dependencies.
     */
    
    var http = require('http')
      , utils = require('./utils')
      , connect = require('connect')
      , fresh = require('fresh')
      , parseRange = require('range-parser')
      , parse = connect.utils.parseUrl
      , mime = connect.mime;
    
    /**
     * Request prototype.
     */
    
    var req = exports = module.exports = {
      __proto__: http.IncomingMessage.prototype
    };
    
    /**
     * Return request header.
     *
     * The `Referrer` header field is special-cased,
     * both `Referrer` and `Referer` are interchangeable.
     *
     * Examples:
     *
     *     req.get('Content-Type');
     *     // => "text/plain"
     *
     *     req.get('content-type');
     *     // => "text/plain"
     *
     *     req.get('Something');
     *     // => undefined
     *
     * Aliased as `req.header()`.
     *
     * @param {String} name
     * @return {String}
     * @api public
     */
    
    req.get =
    req.header = function(name){
      switch (name = name.toLowerCase()) {
        case 'referer':
        case 'referrer':
          return this.headers.referrer
            || this.headers.referer;
        default:
          return this.headers[name];
      }
    };
    
    /**
     * Check if the given `type(s)` is acceptable, returning
     * the best match when true, otherwise `undefined`, in which
     * case you should respond with 406 "Not Acceptable".
     *
     * The `type` value may be a single mime type string
     * such as "application/json", the extension name
     * such as "json", a comma-delimted list such as "json, html, text/plain",
     * an argument list such as `"json", "html", "text/plain"`,
     * or an array `["json", "html", "text/plain"]`. When a list
     * or array is given the _best_ match, if any is returned.
     *
     * Examples:
     *
     *     // Accept: text/html
     *     req.accepts('html');
     *     // => "html"
     *
     *     // Accept: text/*, application/json
     *     req.accepts('html');
     *     // => "html"
     *     req.accepts('text/html');
     *     // => "text/html"
     *     req.accepts('json, text');
     *     // => "json"
     *     req.accepts('application/json');
     *     // => "application/json"
     *
     *     // Accept: text/*, application/json
     *     req.accepts('image/png');
     *     req.accepts('png');
     *     // => undefined
     *
     *     // Accept: text/*;q=.5, application/json
     *     req.accepts(['html', 'json']);
     *     req.accepts('html', 'json');
     *     req.accepts('html, json');
     *     // => "json"
     *
     * @param {String|Array} type(s)
     * @return {String}
     * @api public
     */
    
    req.accepts = function(type){
      var args = arguments.length > 1 ? [].slice.apply(arguments) : type;
      return utils.accepts(args, this.get('Accept'));
    };
    
    /**
     * Check if the given `encoding` is accepted.
     *
     * @param {String} encoding
     * @return {Boolean}
     * @api public
     */
    
    req.acceptsEncoding = function(encoding){
      return !! ~this.acceptedEncodings.indexOf(encoding);
    };
    
    /**
     * Check if the given `charset` is acceptable,
     * otherwise you should respond with 406 "Not Acceptable".
     *
     * @param {String} charset
     * @return {Boolean}
     * @api public
     */
    
    req.acceptsCharset = function(charset){
      var accepted = this.acceptedCharsets;
      return accepted.length
        ? !! ~accepted.indexOf(charset)
        : true;
    };
    
    /**
     * Check if the given `lang` is acceptable,
     * otherwise you should respond with 406 "Not Acceptable".
     *
     * @param {String} lang
     * @return {Boolean}
     * @api public
     */
    
    req.acceptsLanguage = function(lang){
      var accepted = this.acceptedLanguages;
      return accepted.length
        ? !! ~accepted.indexOf(lang)
        : true;
    };
    
    /**
     * Parse Range header field,
     * capping to the given `size`.
     *
     * Unspecified ranges such as "0-" require
     * knowledge of your resource length. In
     * the case of a byte range this is of course
     * the total number of bytes. If the Range
     * header field is not given `null` is returned,
     * `-1` when unsatisfiable, `-2` when syntactically invalid.
     *
     * NOTE: remember that ranges are inclusive, so
     * for example "Range: users=0-3" should respond
     * with 4 users when available, not 3.
     *
     * @param {Number} size
     * @return {Array}
     * @api public
     */
    
    req.range = function(size){
      var range = this.get('Range');
      if (!range) return;
      return parseRange(size, range);
    };
    
    /**
     * Return an array of encodings.
     *
     * Examples:
     *
     *     ['gzip', 'deflate']
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('acceptedEncodings', function(){
      var accept = this.get('Accept-Encoding');
      return accept
        ? accept.trim().split(/ *, */)
        : [];
    });
    
    /**
     * Return an array of Accepted media types
     * ordered from highest quality to lowest.
     *
     * Examples:
     *
     *     [ { value: 'application/json',
     *         quality: 1,
     *         type: 'application',
     *         subtype: 'json' },
     *       { value: 'text/html',
     *         quality: 0.5,
     *         type: 'text',
     *         subtype: 'html' } ]
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('accepted', function(){
      var accept = this.get('Accept');
      return accept
        ? utils.parseAccept(accept)
        : [];
    });
    
    /**
     * Return an array of Accepted languages
     * ordered from highest quality to lowest.
     *
     * Examples:
     *
     *     Accept-Language: en;q=.5, en-us
     *     ['en-us', 'en']
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('acceptedLanguages', function(){
      var accept = this.get('Accept-Language');
      return accept
        ? utils
          .parseParams(accept)
          .map(function(obj){
            return obj.value;
          })
        : [];
    });
    
    /**
     * Return an array of Accepted charsets
     * ordered from highest quality to lowest.
     *
     * Examples:
     *
     *     Accept-Charset: iso-8859-5;q=.2, unicode-1-1;q=0.8
     *     ['unicode-1-1', 'iso-8859-5']
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('acceptedCharsets', function(){
      var accept = this.get('Accept-Charset');
      return accept
        ? utils
          .parseParams(accept)
          .map(function(obj){
            return obj.value;
          })
        : [];
    });
    
    /**
     * Return the value of param `name` when present or `defaultValue`.
     *
     *  - Checks route placeholders, ex: _/user/:id_
     *  - Checks body params, ex: id=12, {"id":12}
     *  - Checks query string params, ex: ?id=12
     *
     * To utilize request bodies, `req.body`
     * should be an object. This can be done by using
     * the `connect.bodyParser()` middleware.
     *
     * @param {String} name
     * @param {Mixed} [defaultValue]
     * @return {String}
     * @api public
     */
    
    req.param = function(name, defaultValue){
      var params = this.params || {};
      var body = this.body || {};
      var query = this.query || {};
      if (null != params[name] && params.hasOwnProperty(name)) return params[name];
      if (null != body[name]) return body[name];
      if (null != query[name]) return query[name];
      return defaultValue;
    };
    
    /**
     * Check if the incoming request contains the "Content-Type"
     * header field, and it contains the give mime `type`.
     *
     * Examples:
     *
     *      // With Content-Type: text/html; charset=utf-8
     *      req.is('html');
     *      req.is('text/html');
     *      req.is('text/*');
     *      // => true
     *
     *      // When Content-Type is application/json
     *      req.is('json');
     *      req.is('application/json');
     *      req.is('application/*');
     *      // => true
     *
     *      req.is('html');
     *      // => false
     *
     * @param {String} type
     * @return {Boolean}
     * @api public
     */
    
    req.is = function(type){
      var ct = this.get('Content-Type');
      if (!ct) return false;
      ct = ct.split(';')[0];
      if (!~type.indexOf('/')) type = mime.lookup(type);
      if (~type.indexOf('*')) {
        type = type.split('/');
        ct = ct.split('/');
        if ('*' == type[0] && type[1] == ct[1]) return true;
        if ('*' == type[1] && type[0] == ct[0]) return true;
        return false;
      }
      return !! ~ct.indexOf(type);
    };
    
    /**
     * Return the protocol string "http" or "https"
     * when requested with TLS. When the "trust proxy"
     * setting is enabled the "X-Forwarded-Proto" header
     * field will be trusted. If you're running behind
     * a reverse proxy that supplies https for you this
     * may be enabled.
     *
     * @return {String}
     * @api public
     */
    
    req.__defineGetter__('protocol', function(){
      var trustProxy = this.app.get('trust proxy');
      if (this.connection.encrypted) return 'https';
      if (!trustProxy) return 'http';
      var proto = this.get('X-Forwarded-Proto') || 'http';
      return proto.split(/\s*,\s*/)[0];
    });
    
    /**
     * Short-hand for:
     *
     *    req.protocol == 'https'
     *
     * @return {Boolean}
     * @api public
     */
    
    req.__defineGetter__('secure', function(){
      return 'https' == this.protocol;
    });
    
    /**
     * Return the remote address, or when
     * "trust proxy" is `true` return
     * the upstream addr.
     *
     * @return {String}
     * @api public
     */
    
    req.__defineGetter__('ip', function(){
      return this.ips[0] || this.connection.remoteAddress;
    });
    
    /**
     * When "trust proxy" is `true`, parse
     * the "X-Forwarded-For" ip address list.
     *
     * For example if the value were "client, proxy1, proxy2"
     * you would receive the array `["client", "proxy1", "proxy2"]`
     * where "proxy2" is the furthest down-stream.
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('ips', function(){
      var trustProxy = this.app.get('trust proxy');
      var val = this.get('X-Forwarded-For');
      return trustProxy && val
        ? val.split(/ *, */)
        : [];
    });
    
    /**
     * Return basic auth credentials.
     *
     * Examples:
     *
     *    // http://tobi:hello@example.com
     *    req.auth
     *    // => { username: 'tobi', password: 'hello' }
     *
     * @return {Object} or undefined
     * @api public
     */
    
    req.__defineGetter__('auth', function(){
      // missing
      var auth = this.get('Authorization');
      if (!auth) return;
    
      // malformed
      var parts = auth.split(' ');
      if ('basic' != parts[0].toLowerCase()) return;
      if (!parts[1]) return;
      auth = parts[1];
    
      // credentials
      auth = new Buffer(auth, 'base64').toString().match(/^([^:]*):(.*)$/);
      if (!auth) return;
      return { username: auth[1], password: auth[2] };
    });
    
    /**
     * Return subdomains as an array.
     *
     * Subdomains are the dot-separated parts of the host before the main domain of
     * the app. By default, the domain of the app is assumed to be the last two
     * parts of the host. This can be changed by setting "subdomain offset".
     *
     * For example, if the domain is "tobi.ferrets.example.com":
     * If "subdomain offset" is not set, req.subdomains is `["ferrets", "tobi"]`.
     * If "subdomain offset" is 3, req.subdomains is `["tobi"]`.
     *
     * @return {Array}
     * @api public
     */
    
    req.__defineGetter__('subdomains', function(){
      var offset = this.app.get('subdomain offset');
      return (this.host || '')
        .split('.')
        .reverse()
        .slice(offset);
    });
    
    /**
     * Short-hand for `url.parse(req.url).pathname`.
     *
     * @return {String}
     * @api public
     */
    
    req.__defineGetter__('path', function(){
      return parse(this).pathname;
    });
    
    /**
     * Parse the "Host" header field hostname.
     *
     * @return {String}
     * @api public
     */
    
    req.__defineGetter__('host', function(){
      var trustProxy = this.app.get('trust proxy');
      var host = trustProxy && this.get('X-Forwarded-Host');
      host = host || this.get('Host');
      if (!host) return;
      return host.split(':')[0];
    });
    
    /**
     * Check if the request is fresh, aka
     * Last-Modified and/or the ETag
     * still match.
     *
     * @return {Boolean}
     * @api public
     */
    
    req.__defineGetter__('fresh', function(){
      var method = this.method;
      var s = this.res.statusCode;
    
      // GET or HEAD for weak freshness validation only
      if ('GET' != method && 'HEAD' != method) return false;
    
      // 2xx or 304 as per rfc2616 14.26
      if ((s >= 200 && s < 300) || 304 == s) {
        return fresh(this.headers, this.res._headers);
      }
    
      return false;
    });
    
    /**
     * Check if the request is stale, aka
     * "Last-Modified" and / or the "ETag" for the
     * resource has changed.
     *
     * @return {Boolean}
     * @api public
     */
    
    req.__defineGetter__('stale', function(){
      return !this.fresh;
    });
    
    /**
     * Check if the request was an _XMLHttpRequest_.
     *
     * @return {Boolean}
     * @api public
     */
    
    req.__defineGetter__('xhr', function(){
      var val = this.get('X-Requested-With') || '';
      return 'xmlhttprequest' == val.toLowerCase();
    });
    
  provide("express/lib/request", module.exports);
}(global));

// pakmanager:express/lib/response
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var http = require('http')
      , path = require('path')
      , connect = require('connect')
      , utils = connect.utils
      , sign = require('cookie-signature').sign
      , normalizeType = require('./utils').normalizeType
      , normalizeTypes = require('./utils').normalizeTypes
      , etag = require('./utils').etag
      , statusCodes = http.STATUS_CODES
      , cookie = require('cookie')
      , send = require('send')
      , mime = connect.mime
      , resolve = require('url').resolve
      , basename = path.basename
      , extname = path.extname;
    
    /**
     * Response prototype.
     */
    
    var res = module.exports = {
      __proto__: http.ServerResponse.prototype
    };
    
    /**
     * Set status `code`.
     *
     * @param {Number} code
     * @return {ServerResponse}
     * @api public
     */
    
    res.status = function(code){
      this.statusCode = code;
      return this;
    };
    
    /**
     * Set Link header field with the given `links`.
     *
     * Examples:
     *
     *    res.links({
     *      next: 'http://api.example.com/users?page=2',
     *      last: 'http://api.example.com/users?page=5'
     *    });
     *
     * @param {Object} links
     * @return {ServerResponse}
     * @api public
     */
    
    res.links = function(links){
      var link = this.get('Link') || '';
      if (link) link += ', ';
      return this.set('Link', link + Object.keys(links).map(function(rel){
        return '<' + links[rel] + '>; rel="' + rel + '"';
      }).join(', '));
    };
    
    /**
     * Send a response.
     *
     * Examples:
     *
     *     res.send(new Buffer('wahoo'));
     *     res.send({ some: 'json' });
     *     res.send('<p>some html</p>');
     *     res.send(404, 'Sorry, cant find that');
     *     res.send(404);
     *
     * @param {Mixed} body or status
     * @param {Mixed} body
     * @return {ServerResponse}
     * @api public
     */
    
    res.send = function(body){
      var req = this.req;
      var head = 'HEAD' == req.method;
      var len;
    
      // settings
      var app = this.app;
    
      // allow status / body
      if (2 == arguments.length) {
        // res.send(body, status) backwards compat
        if ('number' != typeof body && 'number' == typeof arguments[1]) {
          this.statusCode = arguments[1];
        } else {
          this.statusCode = body;
          body = arguments[1];
        }
      }
    
      switch (typeof body) {
        // response status
        case 'number':
          this.get('Content-Type') || this.type('txt');
          this.statusCode = body;
          body = http.STATUS_CODES[body];
          break;
        // string defaulting to html
        case 'string':
          if (!this.get('Content-Type')) {
            this.charset = this.charset || 'utf-8';
            this.type('html');
          }
          break;
        case 'boolean':
        case 'object':
          if (null == body) {
            body = '';
          } else if (Buffer.isBuffer(body)) {
            this.get('Content-Type') || this.type('bin');
          } else {
            return this.json(body);
          }
          break;
      }
    
      // populate Content-Length
      if (undefined !== body && !this.get('Content-Length')) {
        this.set('Content-Length', len = Buffer.isBuffer(body)
          ? body.length
          : Buffer.byteLength(body));
      }
    
      // ETag support
      // TODO: W/ support
      if (app.settings.etag && len && 'GET' == req.method) {
        if (!this.get('ETag')) {
          this.set('ETag', etag(body));
        }
      }
    
      // freshness
      if (req.fresh) this.statusCode = 304;
    
      // strip irrelevant headers
      if (204 == this.statusCode || 304 == this.statusCode) {
        this.removeHeader('Content-Type');
        this.removeHeader('Content-Length');
        this.removeHeader('Transfer-Encoding');
        body = '';
      }
    
      // respond
      this.end(head ? null : body);
      return this;
    };
    
    /**
     * Send JSON response.
     *
     * Examples:
     *
     *     res.json(null);
     *     res.json({ user: 'tj' });
     *     res.json(500, 'oh noes!');
     *     res.json(404, 'I dont have that');
     *
     * @param {Mixed} obj or status
     * @param {Mixed} obj
     * @return {ServerResponse}
     * @api public
     */
    
    res.json = function(obj){
      // allow status / body
      if (2 == arguments.length) {
        // res.json(body, status) backwards compat
        if ('number' == typeof arguments[1]) {
          this.statusCode = arguments[1];
        } else {
          this.statusCode = obj;
          obj = arguments[1];
        }
      }
    
      // settings
      var app = this.app;
      var replacer = app.get('json replacer');
      var spaces = app.get('json spaces');
      var body = JSON.stringify(obj, replacer, spaces);
    
      // content-type
      this.charset = this.charset || 'utf-8';
      this.get('Content-Type') || this.set('Content-Type', 'application/json');
    
      return this.send(body);
    };
    
    /**
     * Send JSON response with JSONP callback support.
     *
     * Examples:
     *
     *     res.jsonp(null);
     *     res.jsonp({ user: 'tj' });
     *     res.jsonp(500, 'oh noes!');
     *     res.jsonp(404, 'I dont have that');
     *
     * @param {Mixed} obj or status
     * @param {Mixed} obj
     * @return {ServerResponse}
     * @api public
     */
    
    res.jsonp = function(obj){
      // allow status / body
      if (2 == arguments.length) {
        // res.json(body, status) backwards compat
        if ('number' == typeof arguments[1]) {
          this.statusCode = arguments[1];
        } else {
          this.statusCode = obj;
          obj = arguments[1];
        }
      }
    
      // settings
      var app = this.app;
      var replacer = app.get('json replacer');
      var spaces = app.get('json spaces');
      var body = JSON.stringify(obj, replacer, spaces)
        .replace(/\u2028/g, '\\u2028')
        .replace(/\u2029/g, '\\u2029');
      var callback = this.req.query[app.get('jsonp callback name')];
    
      // content-type
      this.charset = this.charset || 'utf-8';
      this.set('Content-Type', 'application/json');
    
      // jsonp
      if (callback) {
        if (Array.isArray(callback)) callback = callback[0];
        this.set('Content-Type', 'text/javascript');
        var cb = callback.replace(/[^\[\]\w$.]/g, '');
        body = 'typeof ' + cb + ' === \'function\' && ' + cb + '(' + body + ');';
      }
    
      return this.send(body);
    };
    
    /**
     * Transfer the file at the given `path`.
     *
     * Automatically sets the _Content-Type_ response header field.
     * The callback `fn(err)` is invoked when the transfer is complete
     * or when an error occurs. Be sure to check `res.sentHeader`
     * if you wish to attempt responding, as the header and some data
     * may have already been transferred.
     *
     * Options:
     *
     *   - `maxAge` defaulting to 0
     *   - `root`   root directory for relative filenames
     *
     * Examples:
     *
     *  The following example illustrates how `res.sendfile()` may
     *  be used as an alternative for the `static()` middleware for
     *  dynamic situations. The code backing `res.sendfile()` is actually
     *  the same code, so HTTP cache support etc is identical.
     *
     *     app.get('/user/:uid/photos/:file', function(req, res){
     *       var uid = req.params.uid
     *         , file = req.params.file;
     *
     *       req.user.mayViewFilesFrom(uid, function(yes){
     *         if (yes) {
     *           res.sendfile('/uploads/' + uid + '/' + file);
     *         } else {
     *           res.send(403, 'Sorry! you cant see that.');
     *         }
     *       });
     *     });
     *
     * @param {String} path
     * @param {Object|Function} options or fn
     * @param {Function} fn
     * @api public
     */
    
    res.sendfile = function(path, options, fn){
      var self = this
        , req = self.req
        , next = this.req.next
        , options = options || {}
        , done;
    
      // support function as second arg
      if ('function' == typeof options) {
        fn = options;
        options = {};
      }
    
      // socket errors
      req.socket.on('error', error);
    
      // errors
      function error(err) {
        if (done) return;
        done = true;
    
        // clean up
        cleanup();
        if (!self.headerSent) self.removeHeader('Content-Disposition');
    
        // callback available
        if (fn) return fn(err);
    
        // list in limbo if there's no callback
        if (self.headerSent) return;
    
        // delegate
        next(err);
      }
    
      // streaming
      function stream(stream) {
        if (done) return;
        cleanup();
        if (fn) stream.on('end', fn);
      }
    
      // cleanup
      function cleanup() {
        req.socket.removeListener('error', error);
      }
    
      // transfer
      var file = send(req, path);
      if (options.root) file.root(options.root);
      file.maxage(options.maxAge || 0);
      file.on('error', error);
      file.on('directory', next);
      file.on('stream', stream);
      file.pipe(this);
      this.on('finish', cleanup);
    };
    
    /**
     * Transfer the file at the given `path` as an attachment.
     *
     * Optionally providing an alternate attachment `filename`,
     * and optional callback `fn(err)`. The callback is invoked
     * when the data transfer is complete, or when an error has
     * ocurred. Be sure to check `res.headerSent` if you plan to respond.
     *
     * This method uses `res.sendfile()`.
     *
     * @param {String} path
     * @param {String|Function} filename or fn
     * @param {Function} fn
     * @api public
     */
    
    res.download = function(path, filename, fn){
      // support function as second arg
      if ('function' == typeof filename) {
        fn = filename;
        filename = null;
      }
    
      filename = filename || path;
      this.set('Content-Disposition', 'attachment; filename="' + basename(filename) + '"');
      return this.sendfile(path, fn);
    };
    
    /**
     * Set _Content-Type_ response header with `type` through `mime.lookup()`
     * when it does not contain "/", or set the Content-Type to `type` otherwise.
     *
     * Examples:
     *
     *     res.type('.html');
     *     res.type('html');
     *     res.type('json');
     *     res.type('application/json');
     *     res.type('png');
     *
     * @param {String} type
     * @return {ServerResponse} for chaining
     * @api public
     */
    
    res.contentType =
    res.type = function(type){
      return this.set('Content-Type', ~type.indexOf('/')
        ? type
        : mime.lookup(type));
    };
    
    /**
     * Respond to the Acceptable formats using an `obj`
     * of mime-type callbacks.
     *
     * This method uses `req.accepted`, an array of
     * acceptable types ordered by their quality values.
     * When "Accept" is not present the _first_ callback
     * is invoked, otherwise the first match is used. When
     * no match is performed the server responds with
     * 406 "Not Acceptable".
     *
     * Content-Type is set for you, however if you choose
     * you may alter this within the callback using `res.type()`
     * or `res.set('Content-Type', ...)`.
     *
     *    res.format({
     *      'text/plain': function(){
     *        res.send('hey');
     *      },
     *
     *      'text/html': function(){
     *        res.send('<p>hey</p>');
     *      },
     *
     *      'appliation/json': function(){
     *        res.send({ message: 'hey' });
     *      }
     *    });
     *
     * In addition to canonicalized MIME types you may
     * also use extnames mapped to these types:
     *
     *    res.format({
     *      text: function(){
     *        res.send('hey');
     *      },
     *
     *      html: function(){
     *        res.send('<p>hey</p>');
     *      },
     *
     *      json: function(){
     *        res.send({ message: 'hey' });
     *      }
     *    });
     *
     * By default Express passes an `Error`
     * with a `.status` of 406 to `next(err)`
     * if a match is not made. If you provide
     * a `.default` callback it will be invoked
     * instead.
     *
     * @param {Object} obj
     * @return {ServerResponse} for chaining
     * @api public
     */
    
    res.format = function(obj){
      var req = this.req
        , next = req.next;
    
      var fn = obj.default;
      if (fn) delete obj.default;
      var keys = Object.keys(obj);
    
      var key = req.accepts(keys);
    
      this.vary("Accept");
    
      if (key) {
        var type = normalizeType(key).value;
        var charset = mime.charsets.lookup(type);
        if (charset) type += '; charset=' + charset;
        this.set('Content-Type', type);
        obj[key](req, this, next);
      } else if (fn) {
        fn();
      } else {
        var err = new Error('Not Acceptable');
        err.status = 406;
        err.types = normalizeTypes(keys).map(function(o){ return o.value });
        next(err);
      }
    
      return this;
    };
    
    /**
     * Set _Content-Disposition_ header to _attachment_ with optional `filename`.
     *
     * @param {String} filename
     * @return {ServerResponse}
     * @api public
     */
    
    res.attachment = function(filename){
      if (filename) this.type(extname(filename));
      this.set('Content-Disposition', filename
        ? 'attachment; filename="' + basename(filename) + '"'
        : 'attachment');
      return this;
    };
    
    /**
     * Set header `field` to `val`, or pass
     * an object of header fields.
     *
     * Examples:
     *
     *    res.set('Foo', ['bar', 'baz']);
     *    res.set('Accept', 'application/json');
     *    res.set({ Accept: 'text/plain', 'X-API-Key': 'tobi' });
     *
     * Aliased as `res.header()`.
     *
     * @param {String|Object|Array} field
     * @param {String} val
     * @return {ServerResponse} for chaining
     * @api public
     */
    
    res.set =
    res.header = function(field, val){
      if (2 == arguments.length) {
        if (Array.isArray(val)) val = val.map(String);
        else val = String(val);
        this.setHeader(field, val);
      } else {
        for (var key in field) {
          this.set(key, field[key]);
        }
      }
      return this;
    };
    
    /**
     * Get value for header `field`.
     *
     * @param {String} field
     * @return {String}
     * @api public
     */
    
    res.get = function(field){
      return this.getHeader(field);
    };
    
    /**
     * Clear cookie `name`.
     *
     * @param {String} name
     * @param {Object} options
     * @param {ServerResponse} for chaining
     * @api public
     */
    
    res.clearCookie = function(name, options){
      var opts = { expires: new Date(1), path: '/' };
      return this.cookie(name, '', options
        ? utils.merge(opts, options)
        : opts);
    };
    
    /**
     * Set cookie `name` to `val`, with the given `options`.
     *
     * Options:
     *
     *    - `maxAge`   max-age in milliseconds, converted to `expires`
     *    - `signed`   sign the cookie
     *    - `path`     defaults to "/"
     *
     * Examples:
     *
     *    // "Remember Me" for 15 minutes
     *    res.cookie('rememberme', '1', { expires: new Date(Date.now() + 900000), httpOnly: true });
     *
     *    // save as above
     *    res.cookie('rememberme', '1', { maxAge: 900000, httpOnly: true })
     *
     * @param {String} name
     * @param {String|Object} val
     * @param {Options} options
     * @api public
     */
    
    res.cookie = function(name, val, options){
      options = utils.merge({}, options);
      var secret = this.req.secret;
      var signed = options.signed;
      if (signed && !secret) throw new Error('connect.cookieParser("secret") required for signed cookies');
      if ('number' == typeof val) val = val.toString();
      if ('object' == typeof val) val = 'j:' + JSON.stringify(val);
      if (signed) val = 's:' + sign(val, secret);
      if ('maxAge' in options) {
        options.expires = new Date(Date.now() + options.maxAge);
        options.maxAge /= 1000;
      }
      if (null == options.path) options.path = '/';
      this.set('Set-Cookie', cookie.serialize(name, String(val), options));
      return this;
    };
    
    
    /**
     * Set the location header to `url`.
     *
     * The given `url` can also be "back", which redirects
     * to the _Referrer_ or _Referer_ headers or "/".
     *
     * Examples:
     *
     *    res.location('/foo/bar').;
     *    res.location('http://example.com');
     *    res.location('../login'); // /blog/post/1 -> /blog/login
     *
     * Mounting:
     *
     *   When an application is mounted and `res.location()`
     *   is given a path that does _not_ lead with "/" it becomes
     *   relative to the mount-point. For example if the application
     *   is mounted at "/blog", the following would become "/blog/login".
     *
     *      res.location('login');
     *
     *   While the leading slash would result in a location of "/login":
     *
     *      res.location('/login');
     *
     * @param {String} url
     * @api public
     */
    
    res.location = function(url){
      var app = this.app
        , req = this.req
        , path;
    
      // "back" is an alias for the referrer
      if ('back' == url) url = req.get('Referrer') || '/';
    
      // relative
      if (!~url.indexOf('://') && 0 != url.indexOf('//')) {
        // relative to path
        if ('.' == url[0]) {
          path = req.originalUrl.split('?')[0];
          path = path + ('/' == path[path.length - 1] ? '' : '/');
          url = resolve(path, url);
          // relative to mount-point
        } else if ('/' != url[0]) {
          path = app.path();
          url = path + '/' + url;
        }
      }
    
      // Respond
      this.set('Location', url);
      return this;
    };
    
    /**
     * Redirect to the given `url` with optional response `status`
     * defaulting to 302.
     *
     * The resulting `url` is determined by `res.location()`, so
     * it will play nicely with mounted apps, relative paths,
     * `"back"` etc.
     *
     * Examples:
     *
     *    res.redirect('/foo/bar');
     *    res.redirect('http://example.com');
     *    res.redirect(301, 'http://example.com');
     *    res.redirect('http://example.com', 301);
     *    res.redirect('../login'); // /blog/post/1 -> /blog/login
     *
     * @param {String} url
     * @param {Number} code
     * @api public
     */
    
    res.redirect = function(url){
      var head = 'HEAD' == this.req.method
        , status = 302
        , body;
    
      // allow status / url
      if (2 == arguments.length) {
        if ('number' == typeof url) {
          status = url;
          url = arguments[1];
        } else {
          status = arguments[1];
        }
      }
    
      // Set location header
      this.location(url);
      url = this.get('Location');
    
      // Support text/{plain,html} by default
      this.format({
        text: function(){
          body = statusCodes[status] + '. Redirecting to ' + encodeURI(url);
        },
    
        html: function(){
          var u = utils.escape(url);
          body = '<p>' + statusCodes[status] + '. Redirecting to <a href="' + u + '">' + u + '</a></p>';
        },
    
        default: function(){
          body = '';
        }
      });
    
      // Respond
      this.statusCode = status;
      this.set('Content-Length', Buffer.byteLength(body));
      this.end(head ? null : body);
    };
    
    /**
     * Add `field` to Vary. If already present in the Vary set, then
     * this call is simply ignored.
     *
     * @param {Array|String} field
     * @param {ServerResponse} for chaining
     * @api public
     */
    
    res.vary = function(field){
      var self = this;
    
      // nothing
      if (!field) return this;
    
      // array
      if (Array.isArray(field)) {
        field.forEach(function(field){
          self.vary(field);
        });
        return;
      }
    
      var vary = this.get('Vary');
    
      // append
      if (vary) {
        vary = vary.split(/ *, */);
        if (!~vary.indexOf(field)) vary.push(field);
        this.set('Vary', vary.join(', '));
        return this;
      }
    
      // set
      this.set('Vary', field);
      return this;
    };
    
    /**
     * Render `view` with the given `options` and optional callback `fn`.
     * When a callback function is given a response will _not_ be made
     * automatically, otherwise a response of _200_ and _text/html_ is given.
     *
     * Options:
     *
     *  - `cache`     boolean hinting to the engine it should cache
     *  - `filename`  filename of the view being rendered
     *
     * @param  {String} view
     * @param  {Object|Function} options or callback function
     * @param  {Function} fn
     * @api public
     */
    
    res.render = function(view, options, fn){
      var self = this
        , options = options || {}
        , req = this.req
        , app = req.app;
    
      // support callback function as second arg
      if ('function' == typeof options) {
        fn = options, options = {};
      }
    
      // merge res.locals
      options._locals = self.locals;
    
      // default callback to respond
      fn = fn || function(err, str){
        if (err) return req.next(err);
        self.send(str);
      };
    
      // render
      app.render(view, options, fn);
    };
    
  provide("express/lib/response", module.exports);
}(global));

// pakmanager:express/lib/express
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Module dependencies.
     */
    
    var merge = require('merge-descriptors');
    var connect = require('connect')
      , proto =  require('express/lib/application')
      , Route =  require('express/lib/router/route')
      , Router =  require('express/lib/router')
      , req =  require('express/lib/request')
      , res =  require('express/lib/response')
      , utils = connect.utils;
    
    /**
     * Expose `createApplication()`.
     */
    
    exports = module.exports = createApplication;
    
    /**
     * Expose mime.
     */
    
    exports.mime = connect.mime;
    
    /**
     * Create an express application.
     *
     * @return {Function}
     * @api public
     */
    
    function createApplication() {
      var app = connect();
      utils.merge(app, proto);
      app.request = { __proto__: req, app: app };
      app.response = { __proto__: res, app: app };
      app.init();
      return app;
    }
    
    /**
     * Expose connect.middleware as express.*
     * for example `express.logger` etc.
     */
    
    merge(exports, connect.middleware);
    
    /**
     * Error on createServer().
     */
    
    exports.createServer = function(){
      console.warn('Warning: express.createServer() is deprecated, express');
      console.warn('applications no longer inherit from http.Server,');
      console.warn('please use:');
      console.warn('');
      console.warn('  var express =  require('express');');
      console.warn('  var app = express();');
      console.warn('');
      return createApplication();
    };
    
    /**
     * Expose the prototypes.
     */
    
    exports.application = proto;
    exports.request = req;
    exports.response = res;
    
    /**
     * Expose constructors.
     */
    
    exports.Route = Route;
    exports.Router = Router;
    
    // Error handler title
    
    exports.errorHandler.title = 'Express';
    
    
  provide("express/lib/express", module.exports);
}(global));

// pakmanager:express
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  
    module.exports = process.env.EXPRESS_COV
      ? require('./lib-cov/express')
      :  require('express/lib/express');
  provide("express", module.exports);
}(global));

// pakmanager:csv/lib/utils
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    module.exports = {
      merge: function(obj1, obj2) {
        var key, r;
        r = obj1 || {};
        for (key in obj2) {
          r[key] = obj2[key];
        }
        return r;
      }
    };
    
  provide("csv/lib/utils", module.exports);
}(global));

// pakmanager:csv/lib/state
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    module.exports = function() {
      return {
        count: 0,
        line: [],
        countWriten: 0,
        transforming: 0
      };
    };
    
  provide("csv/lib/state", module.exports);
}(global));

// pakmanager:csv/lib/options
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    
    /*
    Input and output options
    ========================
    
    The `options` property provide access to the `from` and `to` object used to store options. This
    property is for internal usage and could be considered private. It is recommanded to use
    the `from.options()` and `to.options()` to access those objects.
     */
    module.exports = function() {
      return {
        from: {
          rowDelimiter: null,
          delimiter: ',',
          quote: '"',
          escape: '"',
          columns: null,
          comment: '',
          flags: 'r',
          encoding: 'utf8',
          trim: false,
          ltrim: false,
          relax: false,
          rtrim: false
        },
        to: {
          delimiter: null,
          quote: null,
          quoted: false,
          escape: null,
          columns: null,
          header: false,
          lineBreaks: null,
          flags: 'w',
          encoding: 'utf8',
          newColumns: false,
          end: true,
          eof: false
        }
      };
    };
    
  provide("csv/lib/options", module.exports);
}(global));

// pakmanager:csv/lib/from
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    var Stream, fs, nextTick, path, timers, utils;
    
    fs = require('fs');
    
    path = require('path');
    
    if (fs.exists == null) {
      fs.exists = path.exists;
    }
    
    utils =  require('csv/lib/utils');
    
    Stream = require('stream');
    
    timers = require('timers');
    
    nextTick = timers.setImmediate ? timers.setImmediate : process.nextTick;
    
    
    /*
    
    Reading data from a source
    ==========================
    
    The `csv().from` property provides functions to read from an external 
    source and write to a CSV instance. The source may be a string, a file, 
    a buffer or a readable stream.   
    
    You may call the `from` function or one of its sub function. For example, 
    here are two identical ways to read from a file:
    
        csv().from('/tmp/data.csv').on('data', console.log);
        csv().from.path('/tmp/data.csv').on('data', console.log);
     */
    
    module.exports = function(csv) {
    
      /*
      
      `from(mixed)`
      -------------
      
      Read from any sort of source. It should be considered as a convenient function which 
      will discover the nature of the data source to parse.   
      
      If the parameter is a string, check if a file of that path exists. If so, read the file contents,
      otherwise, treat the string as CSV data. If the parameter is an instance of `stream`, consider the
      object to be an input stream. If it is an array, then treat each line as a record.   
      
      Here are some examples on how to use this function:
      
          csv()
          .from('"1","2","3","4"\n"a","b","c","d"')
          .on('end', function(){ console.log('done') })
      
          csv()
          .from('./path/to/file.csv')
          .on('end', function(){ console.log('done') })
      
          csv()
          .from(fs.createReadStream('./path/to/file.csv'))
          .on('end', function(){ console.log('done') })
      
          csv()
          .from(['"1","2","3","4","5"',['1','2','3','4','5']])
          .on('end', function(){ console.log('done') })
       */
      var from;
      from = function(mixed, options) {
        var error;
        error = false;
        switch (typeof mixed) {
          case 'string':
            fs.exists(mixed, function(exists) {
              if (exists) {
                return from.path(mixed, options);
              } else {
                return from.string(mixed, options);
              }
            });
            break;
          case 'object':
            if (Array.isArray(mixed)) {
              from.array(mixed, options);
            } else {
              if (mixed instanceof Stream) {
                from.stream(mixed, options);
              } else {
                error = true;
              }
            }
            break;
          default:
            error = true;
        }
        if (error) {
          csv.error(new Error("Invalid mixed argument in from"));
        }
        return csv;
      };
    
      /*
      
      `from.options([options])`
      -------------------------
      
      Update and retrieve options relative to the input source. Return 
      the options as an object if no argument is provided.
      
      *   `delimiter`     Set the field delimiter. One character only, defaults to comma.
      *   `rowDelimiter`  String used to delimit record rows or a special value; special values are 'auto', 'unix', 'mac', 'windows', 'unicode'; defaults to 'auto' (discovered in source or 'unix' if no source is specified).
      *   `quote`         Optionnal character surrounding a field, one character only, defaults to double quotes.
      *   `escape`        Set the escape character, one character only, defaults to double quotes.
      *   `columns`       List of fields or true if autodiscovered in the first CSV line, default to null. Impact the `transform` argument and the `data` event by providing an object instead of an array, order matters, see the transform and the columns sections for more details.
      *   `comment`       Treat all the characteres after this one as a comment, default to '#'
      *   `flags`         Used to read a file stream, default to the r charactere.
      *   `encoding`      Encoding of the read stream, defaults to 'utf8', applied when a readable stream is created.
      *   `trim`          If true, ignore whitespace immediately around the delimiter, defaults to false.
      *   `ltrim`         If true, ignore whitespace immediately following the delimiter (i.e. left-trim all fields), defaults to false.
      *   `rtrim`         If true, ignore whitespace immediately preceding the delimiter (i.e. right-trim all fields), defaults to false.
      
      Additionnally, in case you are working with stream, you can pass all 
      the options accepted by the `stream.pipe` function.
       */
      from.options = function(options) {
        if (options != null) {
          utils.merge(csv.options.from, options);
          return csv;
        } else {
          return csv.options.from;
        }
      };
    
      /*
      
      `from.array(data, [options])`
      ------------------------------
      
      Read from an array. Take an array as first argument and optionally 
      an object of options as a second argument. Each element of the array 
      represents a CSV record. Those elements may be a string, a buffer, an
      array or an object.
       */
      from.array = function(data, options) {
        this.options(options);
        nextTick(function() {
          var record, _i, _len;
          for (_i = 0, _len = data.length; _i < _len; _i++) {
            record = data[_i];
            csv.write(record);
          }
          return csv.end();
        });
        return csv;
      };
    
      /*
      
      `from.string(data, [options])`
      -------------------------------
      
      Read from a string or a buffer. Take a string as first argument and 
      optionally an object of options as a second argument. The string 
      must be the complete csv data, look at the streaming alternative if your 
      CSV is large.
      
          csv()
          .from( '"1","2","3","4"\n"a","b","c","d"' )
          .to( function(data){} )
       */
      from.string = function(data, options) {
        this.options(options);
        nextTick(function() {
          csv.write(data);
          return csv.end();
        });
        return csv;
      };
    
      /*
      
      `from.stream(stream, [options])`
      --------------------------------
      
      Read from a stream. Take a readable stream as first argument and optionally 
      an object of options as a second argument.
      
      Additionnal options may be defined. See the [`readable.pipe` 
      documentation][srpdo] for additionnal information.
      
      [srpdo]: http://www.nodejs.org/api/stream.html#stream_readable_pipe_destination_options
       */
      from.stream = function(stream, options) {
        if (options) {
          this.options(options);
        }
        if (stream.setEncoding) {
          stream.setEncoding(csv.from.options().encoding);
        }
        stream.pipe(csv, csv.from.options());
        return csv;
      };
    
      /*
      
      `from.path(path, [options])`
      ----------------------------
      
      Read from a file path. Take a file path as first argument and optionally an object 
      of options as a second argument.
      
      Additionnal options may be defined with the following default:
      
          { flags: 'r',
            encoding: null,
            fd: null,
            mode: 0666,
            bufferSize: 64 * 1024,
            autoClose: true }
      
      See the [`fs.createReadStream` documentation][fscpo] for additionnal information.
      
      [fscpo]: http://www.nodejs.org/api/fs.html#fs_fs_createreadstream_path_options
       */
      from.path = function(path, options) {
        var stream;
        this.options(options);
        stream = fs.createReadStream(path, csv.from.options());
        stream.on('error', function(err) {
          return csv.error(err);
        });
        return csv.from.stream(stream);
      };
      return from;
    };
    
  provide("csv/lib/from", module.exports);
}(global));

// pakmanager:csv/lib/to
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    var Stream, fs, utils;
    
    fs = require('fs');
    
    Stream = require('stream');
    
    utils =  require('csv/lib/utils');
    
    
    /*
    
    Writing data to a destination
    =============================
    
    The `csv().to` property provides functions to read from a CSV instance and
    to write to an external destination. The destination may be a stream, a file
    or a callback. 
    
    You may call the `to` function or one of its sub function. For example, 
    here are two identical ways to write to a file:
    
        csv.from(data).to('/tmp/data.csv');
        csv.from(data).to.path('/tmp/data.csv');
     */
    
    module.exports = function(csv) {
    
      /*
      
      `to(mixed)`
      -----------
      
      Write from any sort of destination. It should be considered as a convenient function 
      which will discover the nature of the destination where to write the CSV data.   
      
      If the parameter is a function, then the csv will be provided as the first argument 
      of the callback. If it is a string, then it is expected to be a 
      file path. If it is an instance of `stream`, it consider the object to be an  
      output stream. 
      
      Here's some examples on how to use this function:
      
          csv()
          .from('"1","2","3","4","5"')
          .to(function(data){ console.log(data) })
      
          csv()
          .from('"1","2","3","4","5"')
          .to('./path/to/file.csv')
      
          csv()
          .from('"1","2","3","4","5"')
          .to(fs.createWriteStream('./path/to/file.csv'))
       */
      var to;
      to = function(mixed, options) {
        var error;
        error = false;
        switch (typeof mixed) {
          case 'string':
            to.path(mixed, options);
            break;
          case 'object':
            if (mixed instanceof Stream) {
              to.stream(mixed, options);
            } else {
              error = true;
            }
            break;
          case 'function':
            to.string(mixed, options);
            break;
          default:
            error = true;
        }
        if (error) {
          csv.error(new Error("Invalid mixed argument in from"));
        }
        return csv;
      };
    
      /*
      
      `to.options([options])`
      -----------------------
      
      Update and retrieve options relative to the output. Return the options 
      as an object if no argument is provided.
      
      *   `delimiter`   Set the field delimiter, one character only, defaults to `options.from.delimiter` which is a comma.
      *   `quote`       Defaults to the quote read option.
      *   `quoted`      Boolean, default to false, quote all the fields even if not required.
      *   `escape`      Defaults to the escape read option.
      *   `columns`     List of fields, applied when `transform` returns an object, order matters, read the transformer documentation for additionnal information.
      *   `header`      Display the column names on the first line if the columns option is provided.
      *   `lineBreaks`  String used to delimit record rows or a special value; special values are 'auto', 'unix', 'mac', 'windows', 'unicode'; defaults to 'auto' (discovered in source or 'unix' if no source is specified).
      *   `flags`       Defaults to 'w', 'w' to create or overwrite an file, 'a' to append to a file. Applied when using the `toPath` method.
      *   `newColumns`  If the `columns` option is not specified (which means columns will be taken from the reader options, will automatically append new columns if they are added during `transform()`.
      *   `end`         Prevent calling `end` on the destination, so that destination is no longer writable.
      *   `eof`         Add a linebreak on the last line, default to false, expect a charactere or use '\n' if value is set to "true"
      
      The end options is similar to passing `{end: false}` option in `stream.pipe()`. According to the Node.js documentation:
      > By default end() is called on the destination when the source stream emits end, so that destination is no longer writable. Pass { end: false } as options to keep the destination stream open.
       */
      to.options = function(options) {
        if (options != null) {
          utils.merge(csv.options.to, options);
          if (csv.options.to.lineBreaks) {
            console.log('To options linebreaks is replaced by rowDelimiter');
            if (!csv.options.to.rowDelimiter) {
              csv.options.to.rowDelimiter = csv.options.to.lineBreaks;
            }
          }
          switch (csv.options.to.rowDelimiter) {
            case 'auto':
              csv.options.to.rowDelimiter = null;
              break;
            case 'unix':
              csv.options.to.rowDelimiter = "\n";
              break;
            case 'mac':
              csv.options.to.rowDelimiter = "\r";
              break;
            case 'windows':
              csv.options.to.rowDelimiter = "\r\n";
              break;
            case 'unicode':
              csv.options.to.rowDelimiter = "\u2028";
          }
          return csv;
        } else {
          return csv.options.to;
        }
      };
    
      /*
      
      `to.string(callback, [options])`
      ------------------------------
      
      Provide the output string to a callback.
      
          csv()
          .from( '"1","2","3"\n"a","b","c"' )
          .to.string( function(data, count){} )
      
      Callback is called with 2 arguments:
      *   data      Entire CSV as a string
      *   count     Number of stringified records
       */
      to.string = function(callback, options) {
        var data, stream;
        this.options(options);
        data = [];
        stream = new Stream;
        stream.writable = true;
        stream.write = function(d) {
          data.push(d);
          return true;
        };
        stream.end = function() {
          return callback(data.join(''), csv.state.countWriten);
        };
        csv.pipe(stream);
        return csv;
      };
    
      /*
      
      `to.stream(stream, [options])`
      ------------------------------
      
      Write to a stream. Take a writable stream as first argument and  
      optionally an object of options as a second argument.
      
      Additionnal options may be defined. See the [`readable.pipe` 
      documentation][srpdo] for additionnal information.
      
      [srpdo]: http://www.nodejs.org/api/stream.html#stream_readable_pipe_destination_options
       */
      to.stream = function(stream, options) {
        this.options(options);
        csv.pipe(stream, csv.options.to);
        stream.on('error', function(e) {
          return csv.error(e);
        });
        stream.on('close', function() {
          return csv.emit('close', csv.state.count);
        });
        stream.on('finish', function() {
          return csv.emit('finish', csv.state.count);
        });
        return csv;
      };
    
      /*
      
      `to.path(path, [options])`
      --------------------------
      
      Write to a path. Take a file path as first argument and optionally an object of 
      options as a second argument. The `close` event is sent after the file is written. 
      Relying on the `end` event is incorrect because it is sent when parsing is done 
      but before the file is written.
      
      Additionnal options may be defined with the following default:
      
          { flags: 'w',
            encoding: null,
            mode: 0666 }
      
      See the [`fs.createReadStream` documentation][fscpo] for additionnal information.
      
      [fscpo]: http://www.nodejs.org/api/fs.html#fs_fs_createwritestream_path_options
      
      Example to modify a file rather than replacing it:
      
          csv()
          .to.file('my.csv', {flags:'r+'})
          .write(['hello', 'node'])
          .end()
       */
      to.path = function(path, options) {
        var stream;
        this.options(options);
        options = utils.merge({}, csv.options.to);
        delete options.end;
        stream = fs.createWriteStream(path, options);
        csv.to.stream(stream, null);
        return csv;
      };
    
      /*
      
      `to.array(path, [options])`
      --------------------------
      
      Provide the output string to a callback.
      
          csv()
          .from( '"1","2","3"\n"a","b","c"' )
          .to.array( function(data, count){} )
      
      Callback is called with 2 arguments:
      *   data      Entire CSV as an array of records
      *   count     Number of stringified records
       */
      to.array = function(callback, options) {
        var records;
        this.options(options);
        records = [];
        csv.on('record', function(record) {
          var column, i, _i, _j, _len, _len1, _record, _ref, _ref1;
          if (this.options.to.columns) {
            if (Array.isArray(record)) {
              _record = record;
              record = {};
              _ref = this.options.to.columns;
              for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
                column = _ref[i];
                record[column] = _record[i];
              }
            } else {
              _record = record;
              record = {};
              _ref1 = this.options.to.columns;
              for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
                column = _ref1[_j];
                record[column] = _record[column];
              }
            }
          }
          return records.push(record);
        });
        csv.on('end', function() {
          return callback(records, csv.state.countWriten);
        });
        return csv;
      };
      return to;
    };
    
  provide("csv/lib/to", module.exports);
}(global));

// pakmanager:csv/lib/stringifier
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    
    /*
    
    Stringifier
    ===========
    
    Convert an array or an object into a CSV line.
     */
    var Stringifier,
      __hasProp = {}.hasOwnProperty;
    
    Stringifier = function(csv) {
      this.csv = csv;
      return this;
    };
    
    
    /*
    
    `write(line, [preserve])`
    -------------------------
    
    Write a line to the written stream. Line may be an object, an array or a string
    The `preserve` argument is for the lines which are not considered as CSV data.
     */
    
    Stringifier.prototype.write = function(line) {
      var e, preserve;
      if (line == null) {
        return;
      }
      preserve = typeof line !== 'object';
      if (!preserve) {
        try {
          this.csv.emit('record', line, this.csv.state.count - 1);
        } catch (_error) {
          e = _error;
          return this.csv.error(e);
        }
        line = this.csv.stringifier.stringify(line);
      }
      if (typeof line === 'number') {
        line = "" + line;
      }
      this.csv.emit('data', line);
      if (!preserve) {
        this.csv.state.countWriten++;
      }
      return true;
    };
    
    
    /*
    
    `Stringifier(line)`
    -------------------
    
    Convert a line to a string. Line may be an object, an array or a string.
     */
    
    Stringifier.prototype.stringify = function(line) {
      var column, columns, containsLinebreak, containsQuote, containsdelimiter, delimiter, e, escape, field, i, newLine, quote, regexp, _i, _j, _line, _ref, _ref1;
      if (typeof line !== 'object') {
        return line;
      }
      columns = this.csv.options.to.columns || this.csv.options.from.columns;
      if (typeof columns === 'object' && columns !== null && !Array.isArray(columns)) {
        columns = Object.keys(columns);
      }
      delimiter = this.csv.options.to.delimiter || this.csv.options.from.delimiter;
      quote = this.csv.options.to.quote || this.csv.options.from.quote;
      escape = this.csv.options.to.escape || this.csv.options.from.escape;
      if (!Array.isArray(line)) {
        _line = [];
        if (columns) {
          for (i = _i = 0, _ref = columns.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
            column = columns[i];
            _line[i] = typeof line[column] === 'undefined' || line[column] === null ? '' : line[column];
          }
        } else {
          for (column in line) {
            if (!__hasProp.call(line, column)) continue;
            _line.push(line[column]);
          }
        }
        line = _line;
        _line = null;
      } else if (columns) {
        line.splice(columns.length);
      }
      if (Array.isArray(line)) {
        newLine = this.csv.state.countWriten ? this.csv.options.to.rowDelimiter || "\n" : '';
        for (i = _j = 0, _ref1 = line.length; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; i = 0 <= _ref1 ? ++_j : --_j) {
          field = line[i];
          if (typeof field === 'string') {
    
          } else if (typeof field === 'number') {
            field = '' + field;
          } else if (typeof field === 'boolean') {
            field = field ? '1' : '';
          } else if (field instanceof Date) {
            field = '' + field.getTime();
          } else if (field instanceof Number) {
            field = '' + field.valueOf();
          } else {
            try {
              field = field.toString();
            } catch (_error) {
              e = _error;
            }
          }
          if (field) {
            containsdelimiter = field.indexOf(delimiter) >= 0;
            containsQuote = field.indexOf(quote) >= 0;
            containsLinebreak = field.indexOf("\r") >= 0 || field.indexOf("\n") >= 0;
            if (containsQuote) {
              regexp = new RegExp(quote, 'g');
              field = field.replace(regexp, escape + quote);
            }
            if (containsQuote || containsdelimiter || containsLinebreak || this.csv.options.to.quoted) {
              field = quote + field + quote;
            }
            newLine += field;
          }
          if (i !== line.length - 1) {
            newLine += delimiter;
          }
        }
        line = newLine;
      }
      return line;
    };
    
    module.exports = function(csv) {
      return new Stringifier(csv);
    };
    
    module.exports.Stringifier = Stringifier;
    
  provide("csv/lib/stringifier", module.exports);
}(global));

// pakmanager:csv/lib/parser
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    var EventEmitter, Parser;
    
    EventEmitter = require('events').EventEmitter;
    
    
    /*
    
    Parsing
    =======
    
    The library extend the [EventEmitter][event] and emit the following events:
    
    *   *row*   
      Emitted by the parser on each line with the line content as an array of fields.
    *   *end*   
      Emitted when no more data will be parsed.
    *   *error*   
      Emitted when an error occured.
     */
    
    Parser = function(csv) {
      this.csv = csv;
      this.options = csv.options.from;
      this.lines = 0;
      this.buf = '';
      this.quoting = false;
      this.commenting = false;
      this.field = '';
      this.lastC = '';
      this.nextChar = null;
      this.closingQuote = 0;
      this.line = [];
      return this;
    };
    
    Parser.prototype.__proto__ = EventEmitter.prototype;
    
    
    /*
    
    `write(chars)`
    --------------
    
    Parse a string which may hold multiple lines.
    Private state object is enriched on each character until 
    transform is called on a new line.
     */
    
    Parser.prototype.write = function(chars, end) {
      var areNextCharsRowDelimiters, char, delimLength, escapeIsQuote, i, isDelimiter, isEscape, isQuote, isRowDelimiter, l, ltrim, nextNextCharPas, nextNextCharPos, rowDelimiter, rtrim, _results;
      ltrim = this.options.trim || this.options.ltrim;
      rtrim = this.options.trim || this.options.rtrim;
      chars = this.buf + chars;
      l = chars.length;
      delimLength = this.options.rowDelimiter ? this.options.rowDelimiter.length : 0;
      i = 0;
      if (this.lines === 0 && 0xFEFF === chars.charCodeAt(0)) {
        i++;
      }
      while (i < l) {
        if ((i + delimLength >= l && chars.substr(i, this.options.rowDelimiter.length) !== this.options.rowDelimiter) && !end) {
          break;
        }
        if ((i + this.options.escape.length >= l && chars.substr(i, this.options.escape.length) === this.options.escape) && !end) {
          break;
        }
        char = this.nextChar ? this.nextChar : chars.charAt(i);
        this.lastC = char;
        this.nextChar = chars.charAt(i + 1);
        if (this.options.rowDelimiter == null) {
          if ((this.field === '') && (char === '\n' || char === '\r')) {
            rowDelimiter = char;
            nextNextCharPos = i + 1;
          } else if (this.nextChar === '\n' || this.nextChar === '\r') {
            rowDelimiter = this.nextChar;
            nextNextCharPas = i + 2;
          }
          if (rowDelimiter) {
            this.options.rowDelimiter = rowDelimiter;
            if (rowDelimiter === '\r' && chars.charAt(nextNextCharPas) === '\n') {
              this.options.rowDelimiter += '\n';
            }
            delimLength = this.options.rowDelimiter.length;
          }
        }
        if (char === this.options.escape) {
          escapeIsQuote = this.options.escape === this.options.quote;
          isEscape = this.nextChar === this.options.escape;
          isQuote = this.nextChar === this.options.quote;
          if (!(escapeIsQuote && !this.field && !this.quoting) && (isEscape || isQuote)) {
            i++;
            char = this.nextChar;
            this.nextChar = chars.charAt(i + 1);
            this.field += char;
            i++;
            continue;
          }
        }
        if (char === this.options.quote) {
          if (this.quoting) {
            areNextCharsRowDelimiters = this.options.rowDelimiter && chars.substr(i + 1, this.options.rowDelimiter.length) === this.options.rowDelimiter;
            if (!this.options.relax && this.nextChar && !areNextCharsRowDelimiters && this.nextChar !== this.options.delimiter && this.nextChar !== this.options.comment) {
              return this.error(new Error("Invalid closing quote at line " + (this.lines + 1) + "; found " + (JSON.stringify(this.nextChar)) + " instead of delimiter " + (JSON.stringify(this.options.delimiter))));
            }
            this.quoting = false;
            this.closingQuote = i;
            i++;
            continue;
          } else if (!this.field) {
            this.quoting = true;
            i++;
            continue;
          }
        }
        isDelimiter = char === this.options.delimiter;
        isRowDelimiter = this.options.rowDelimiter && chars.substr(i, this.options.rowDelimiter.length) === this.options.rowDelimiter;
        if (!this.commenting && !this.quoting && char === this.options.comment) {
          this.commenting = true;
        } else if (this.commenting && isRowDelimiter) {
          this.commenting = false;
        }
        if (!this.commenting && !this.quoting && (isDelimiter || isRowDelimiter)) {
          if (isRowDelimiter && this.line.length === 0 && this.field === '') {
            i += this.options.rowDelimiter.length;
            this.nextChar = chars.charAt(i);
            continue;
          }
          if (rtrim) {
            if (this.closingQuote) {
              this.field = this.field.substr(0, this.closingQuote);
            } else {
              this.field = this.field.trimRight();
            }
          }
          this.line.push(this.field);
          this.closingQuote = 0;
          this.field = '';
          if (isRowDelimiter) {
            this.emit('row', this.line);
            this.line = [];
            i += this.options.rowDelimiter.length;
            this.nextChar = chars.charAt(i);
            continue;
          }
        } else if (!this.commenting && !this.quoting && (char === ' ' || char === '\t')) {
          if (!(ltrim && !this.field)) {
            this.field += char;
          }
        } else if (!this.commenting) {
          this.field += char;
        }
        i++;
      }
      this.buf = '';
      _results = [];
      while (i < l) {
        this.buf += chars.charAt(i);
        _results.push(i++);
      }
      return _results;
    };
    
    Parser.prototype.end = function() {
      this.write('', true);
      if (this.quoting) {
        return this.error(new Error("Quoted field not terminated at line " + (this.lines + 1)));
      }
      if (this.field || this.lastC === this.options.delimiter || this.lastC === this.options.quote) {
        if (this.options.trim || this.options.rtrim) {
          this.field = this.field.trimRight();
        }
        this.line.push(this.field);
        this.field = '';
      }
      if (this.line.length > 0) {
        this.emit('row', this.line);
      }
      return this.emit('end', null);
    };
    
    Parser.prototype.error = function(e) {
      return this.emit('error', e);
    };
    
    module.exports = function(csv) {
      return new Parser(csv);
    };
    
    module.exports.Parser = Parser;
    
    
    /*
    [event]: http://nodejs.org/api/events.html
     */
    
  provide("csv/lib/parser", module.exports);
}(global));

// pakmanager:csv/lib/transformer
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    var Transformer, nextTick, stream, timers;
    
    stream = require('stream');
    
    timers = require('timers');
    
    nextTick = timers.setImmediate ? timers.setImmediate : process.nextTick;
    
    
    /*
    Transforming data
    =================
    
    Transformations may occur synchronously or asynchronously depending
    on the provided transform callback and its declared arguments length.
    
    Callbacks are called for each line, with these arguments:    
    
    *   *row*   
      CSV record
    *   *index*   
      Incremented counter
    *   *callback*   
      Callback function to be called in asynchronous mode
    
    If you specify the `columns` read option, the `row` argument will be 
    provided as an object with keys matching columns names. Otherwise it
    will be provided as an array.
    
    In synchronous mode, the contract is quite simple, you will receive an array 
    of fields for each record and the transformed array should be returned.
    
    In asynchronous mode, it is your responsibility to call the callback 
    provided as the third argument. It must be called with two arguments,
    an error (if any), and the transformed record.
    
    Transformed records may be an array, an associative array, a 
    string or `null`. If `null`, the record will simply be skipped. When the
    returned value is an array, the fields are merged in order. 
    When the returned value is an object, the module will search for
    the `columns` property in the write or in the read options and 
    intelligently order the values. If no `columns` options are found, 
    it will merge the values in their order of appearance. When the 
    returned value is a string, it is directly sent to the destination 
    and it is your responsibility to delimit, quote, escape 
    or define line breaks.
    
    Transform callback run synchronously:
    
        csv()
        .from('82,Preisner,Zbigniew\n94,Gainsbourg,Serge')
        .to(console.log)
        .transform(function(row, index){
            return row.reverse()
        });
        // Executing `node samples/transform.js`, print:
        // Zbigniew,Preisner,82\nSerge,Gainsbourg,94
    
    Transform callback run asynchronously:
    
        csv()
        .from('82,Preisner,Zbigniew\n94,Gainsbourg,Serge')
        .to(console.log)
        .transform(function(row, index, callback){
            process.nextTick(function(){
                callback(null, row.reverse());
            });
        });
        // Executing `node samples/transform.js`, print:
        // Zbigniew,Preisner,82\nSerge,Gainsbourg,94
    
    Transform callback returning a string:
    
        csv()
        .from('82,Preisner,Zbigniew\n94,Gainsbourg,Serge')
        .to(console.log)
        .transform(function(row, index){
            return (index>0 ? ',' : '') + row[0] + ":" + row[2] + ' ' + row[1];
        });
        // Executing `node samples/transform.js`, print:
        // 82:Zbigniew Preisner,94:Serge Gainsbourg
     */
    
    Transformer = function(csv) {
      this.csv = csv;
      this.running = 0;
      this.options = {
        parallel: 100
      };
      this.todo = [];
      return this;
    };
    
    Transformer.prototype.__proto__ = stream.prototype;
    
    
    /* no doc
    
    `headers()`
    ----------------------------
    
    Print headers.
     */
    
    Transformer.prototype.headers = function() {
      var k, label, labels;
      labels = this.csv.options.to.columns || this.csv.options.from.columns;
      if (typeof labels === 'object') {
        labels = (function() {
          var _results;
          _results = [];
          for (k in labels) {
            label = labels[k];
            _results.push(label);
          }
          return _results;
        })();
      }
      return this.csv.stringifier.write(labels);
    };
    
    
    /* no doc
    
    `write(line)`
    ----------------------------------
    
    Call a callback to transform a line. Called for each line after being parsed. 
    It is responsible for transforming the data and finally calling `write`.
     */
    
    Transformer.prototype.write = function(line) {
      var column, columns, csv, done, finish, i, lineAsObject, run, self, sync, _i, _j, _len, _len1;
      self = this;
      csv = this.csv;
      if (this.columns == null) {
        columns = csv.options.from.columns;
        if (typeof columns === 'object' && columns !== null && !Array.isArray(columns)) {
          columns = Object.keys(columns);
        }
        if (csv.state.count === 0 && columns === true) {
          columns = csv.options.from.columns = line;
          return;
        }
        this.columns = columns != null ? columns : false;
      } else {
        columns = this.columns;
      }
      if (columns) {
        if (Array.isArray(line)) {
          lineAsObject = {};
          for (i = _i = 0, _len = columns.length; _i < _len; i = ++_i) {
            column = columns[i];
            lineAsObject[column] = line[i] != null ? line[i] : null;
          }
          line = lineAsObject;
        } else {
          lineAsObject = {};
          for (i = _j = 0, _len1 = columns.length; _j < _len1; i = ++_j) {
            column = columns[i];
            lineAsObject[column] = line[column] != null ? line[column] : null;
          }
          line = lineAsObject;
        }
      }
      finish = function(line) {
        if (csv.options.to.header === true && (csv.state.count - csv.state.transforming) === 1) {
          self.headers();
        }
        csv.stringifier.write(line);
        line = self.todo.shift();
        if (line) {
          return run(line);
        }
        if (csv.state.transforming === 0 && self.closed === true) {
          return self.emit('end', csv.state.count);
        }
      };
      csv.state.count++;
      if (!this.callback) {
        return finish(line);
      }
      sync = this.callback.length !== 3;
      csv.state.transforming++;
      self = this;
      done = function(err, line) {
        var isObject;
        self.running--;
        if (err) {
          return csv.error(err);
        }
        isObject = typeof line === 'object' && !Array.isArray(line);
        if (isObject && csv.options.to.newColumns && !csv.options.to.columns) {
          Object.keys(line).filter(function(column) {
            return self.columns.indexOf(column) === -1;
          }).forEach(function(column) {
            return self.columns.push(column);
          });
        }
        csv.state.transforming--;
        return finish(line);
      };
      run = function(line) {
        var err;
        self.running++;
        try {
          if (sync) {
            return done(null, self.callback(line, csv.state.count - self.todo.length - 1));
          } else {
            return self.callback(line, csv.state.count - self.todo.length - 1, done);
          }
        } catch (_error) {
          err = _error;
          return done(err);
        }
      };
      if (this.running === this.options.parallel) {
        this.todo.push(line);
        return false;
      }
      run(line);
      return true;
    };
    
    
    /* no doc
    `end()`
    ------------------------
    
    A transformer instance extends the EventEmitter and 
    emit the 'end' event when the last callback is called.
     */
    
    Transformer.prototype.end = function() {
      if (this.closed) {
        return this.csv.error(new Error('Transformer already closed'));
      }
      this.closed = true;
      if (this.csv.state.transforming === 0) {
        return this.emit('end');
      }
    };
    
    module.exports = function(csv) {
      return new Transformer(csv);
    };
    
    module.exports.Transformer = Transformer;
    
  provide("csv/lib/transformer", module.exports);
}(global));

// pakmanager:csv/lib
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    
    /*
    
    Node CSV
    ========
    
    This project provides CSV parsing and has been tested and used 
    on large input files.
    
    *   Follow the Node.js streaming API
    *   Async and event based
    *   Support delimiters, quotes, escape characters and comments
    *   Line breaks discovery: detected in source and reported to destination
    *   Data transformation
    *   Support for large datasets
    *   Complete test coverage as sample and inspiration
    *   no external dependencies
    
    Important: this documentation covers the current version (0.2.x) of 
    `node-csv-parser`. The documentation for the previous version (0.1.0) is 
    available [here](https://github.com/wdavidw/node-csv-parser/tree/v0.1).
    
    Installation
    ------------
    
    ```bash
    npm install csv
    ```
    
    Quick example
    -------------
    
    This take a string with a comment and convert it to an array:
    
        // node samples/string.js
        csv()
        .from.string(
          '#Welcome\n"1","2","3","4"\n"a","b","c","d"',
          {comment: '#'} )
        .to.array( function(data){
          console.log(data)
        } );
        // [ [ '1', '2', '3', '4' ], [ 'a', 'b', 'c', 'd' ] ]
    
    
    Advanced example
    ----------------
    
    The following example illustrates 4 usages of the library:
    1.  Plug a readable stream from a file
    2.  Direct output to a file path
    3.  Transform each row
    4.  Listen to events
        
        // node samples/sample.js
        var csv =  require('csv');
        var fs = require('fs');
        csv()
        .from.stream(fs.createReadStream(__dirname+'/sample.in'))
        .to.path(__dirname+'/sample.out')
        .transform( function(row){
          row.unshift(row.pop());
          return row;
        })
        .on('record', function(row,index){
          console.log('#'+index+' '+JSON.stringify(row));
        })
        .on('end', function(count){
          console.log('Number of lines: '+count);
        })
        .on('error', function(error){
          console.log(error.message);
        });
        // #0 ["2000-01-01","20322051544","1979.0","8.8017226E7","ABC","45"]
        // #1 ["2050-11-27","28392898392","1974.0","8.8392926E7","DEF","23"]
        // Number of lines: 2
    
    Pipe example
    ------------
    
    The module follow a Stream architecture. At its core, the parser and 
    the stringifier utilities provide a [Stream Writer][writable_stream] 
    and a [Stream Reader][readable_stream] implementation available in the CSV API.
    
        +--------+      +----------+----------+       +--------+
        |        |      |          |          |       |        |
        |        |      |         CSV         |       |        |
        |        |      |          |          |       |        |
        | Stream |      |  Writer  |  Reader  |       | Stream |
        | Reader |.pipe(|   API    |   API    |).pipe(| Writer |)
        |        |      |          |          |       |        |
        |        |      |          |          |       |        |
        +--------+      +----------+----------+       +--------+
    
    Here's a quick example:
    
        in = fs.createReadStream('./in')
        out = fs.createWriteStream('./out')
        in.pipe(csv()).pipe(out)
    
    Installing
    ----------
    
    Via [npm](http://github.com/isaacs/npm):
    ```bash
    npm install csv
    ```
    
    Via git (or downloaded tarball):
    ```bash
    git clone http://github.com/wdavidw/node-csv-parser.git
    ```
    
    Events
    ------
    
    The library extends Node [EventEmitter][event] class and emit all
    the events of the Writable and Readable [Stream API][stream]. Additionally, the useful "records" event 
    is emitted.
    
    *   *record*   
      Emitted by the stringifier when a new row is parsed and transformed. The data is 
      the value returned by the user `transform` callback if any. Note however that the event won't 
      be called if `transform` returns `null` since the record is skipped.
      The callback provides two arguments. `row` is the CSV line being processed (an array or an object)
      and `index` is the index number of the line starting at zero
    *   *data*   
      Emitted by the stringifier on each line once the data has been transformed and stringified.
    *   *drain*   
    *   *end*   
      Emitted when the CSV content has been parsed.
    *   *finish*   
      Emitted when all data has been flushed to the underlying system. For example, when writting to a file with `csv().to.path()`, the event will be called once the writing process is complete and the file closed.
    *   *error*   
      Thrown whenever an error occured.
    
    Architecture
    ------------
    
    The code is organized mainly around 5 main components. 
    The "from" properties provide convenient functions 
    to write CSV text or to plug a Stream Reader. The "parser" 
    takes this CSV content and transform it into an array or 
    an object for each line. The "transformer" provides the ability 
    to work on each line in a synchronous or asynchronous mode. 
    The "stringifier" takes an array or an object and serializes it into 
    CSV text. Finally, the "to" properties provide convenient 
    functions to retrieve the text or to write to plug a Stream Writer.
    
        +-------+--------+--------------+--------------+-----+
        |       |        |              |              |     |
        | from -> parser -> transformer -> stringifier -> to |
        |       |        |              |              |     |
        +-------+--------+--------------+--------------+-----+
    
    Note, even though the "parser", "transformer" and "singifier" are available
    as properties, you won't have to interact with those.
     */
    var CSV, from, options, parser, state, stream, stringifier, to, transformer, utils;
    
    stream = require('stream');
    
    state =  require('csv/lib/state');
    
    options =  require('csv/lib/options');
    
    from =  require('csv/lib/from');
    
    to =  require('csv/lib/to');
    
    stringifier =  require('csv/lib/stringifier');
    
    parser =  require('csv/lib/parser');
    
    transformer =  require('csv/lib/transformer');
    
    utils =  require('csv/lib/utils');
    
    CSV = function() {
      var self;
      self = this;
      this.paused = false;
      this.readable = true;
      this.writable = true;
      this.state = state();
      this.options = options();
      this.from = from(this);
      this.to = to(this);
      this.parser = parser(this);
      this.parser.on('row', function(row) {
        return self.transformer.write(row);
      });
      this.parser.on('end', function() {
        if (self.state.count === 0) {
          self.transformer.headers();
        }
        return self.transformer.end();
      });
      this.parser.on('error', function(e) {
        return self.error(e);
      });
      this.stringifier = stringifier(this);
      this.transformer = transformer(this);
      this.transformer.on('end', function() {
        var eof;
        eof = self.options.to.eof;
        if (eof) {
          if (eof === true) {
            eof = '\n';
          }
          self.stringifier.write(eof);
        }
        return self.emit('end', self.state.count);
      });
      return this;
    };
    
    CSV.prototype.__proto__ = stream.prototype;
    
    
    /*
    
    `pause()`
    ---------
    
    Implementation of the Readable Stream API, requesting that no further data 
    be sent until resume() is called.
     */
    
    CSV.prototype.pause = function() {
      this.paused = true;
      return this;
    };
    
    
    /*
    
    `resume()`
    ----------
    
    Implementation of the Readable Stream API, resuming the incoming 'data' 
    events after a pause().
     */
    
    CSV.prototype.resume = function() {
      this.paused = false;
      this.emit('drain');
      return this;
    };
    
    
    /*
    
    `write(data, [preserve])`
    -------------------------
    
    Implementation of the Writable Stream API with a larger signature. Data
    may be a string, a buffer, an array or an object.
    
    If data is a string or a buffer, it could span multiple lines. If data 
    is an object or an array, it must represent a single line.
    Preserve is for line which are not considered as CSV data.
     */
    
    CSV.prototype.write = function(chunk, preserve) {
      var csv;
      if (!this.writable) {
        return this.emit('error', new Error('CSV no longer writable'));
      }
      if (chunk instanceof Buffer) {
        chunk = chunk.toString();
      }
      if (typeof chunk === 'string' && !preserve) {
        this.parser.write(chunk);
      } else if (Array.isArray(chunk) && !this.state.transforming) {
        csv = this;
        this.transformer.write(chunk);
      } else {
        if (preserve || this.state.transforming) {
          this.stringifier.write(chunk);
        } else {
          this.transformer.write(chunk);
        }
      }
      return !this.paused;
    };
    
    
    /*
    
    `end()`
    -------
    
    Terminate the parsing. Call this method when no more csv data is 
    to be parsed. It implement the StreamWriter API by setting the `writable` 
    property to "false" and emitting the `end` event.
     */
    
    CSV.prototype.end = function() {
      if (!this.writable) {
        return;
      }
      this.readable = false;
      this.writable = false;
      this.parser.end();
      return this;
    };
    
    
    /*
    
    `transform(callback, [options])`
    ---------------------
    
    Register the transformer callback. The callback is a user provided 
    function call on each line to filter, enrich or modify the 
    dataset. More information in the "transforming data" section.
     */
    
    CSV.prototype.transform = function(callback, options) {
      this.transformer.callback = callback;
      if (options) {
        utils.merge(this.transformer.options, options);
      }
      return this;
    };
    
    
    /*
    
    `error(error)`
    --------------
    
    Unified mechanism to handle error, emit the error and mark the 
    stream as non readable and non writable.
     */
    
    CSV.prototype.error = function(e) {
      this.readable = false;
      this.writable = false;
      this.emit('error', e);
      if (this.readStream) {
        this.readStream.destroy();
      }
      return this;
    };
    
    module.exports = function() {
      return new CSV;
    };
    
    
    /*
    [event]: http://nodejs.org/api/events.html
    [stream]: http://nodejs.org/api/stream.html
    [writable_stream]: http://nodejs.org/api/stream.html#stream_writable_stream
    [readable_stream]: http://nodejs.org/api/stream.html#stream_readable_stream
     */
    
  provide("csv/lib", module.exports);
}(global));

// pakmanager:csv/lib/generator
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  // Generated by CoffeeScript 1.7.0
    var Generator, Stream, nextTick, timers, util;
    
    Stream = require('stream');
    
    util = require('util');
    
    timers = require('timers');
    
    nextTick = timers.setImmediate ? timers.setImmediate : process.nextTick;
    
    
    /*
    
    `generator([options])`: Generate random CSV data
    ================================================
    
    This function is provided for conveniency in case you need to generate random CSV data.
    
    Note, it is quite simple at the moment, more functionnalities could come later. The code 
    originates from "./samples/perf.coffee" and was later extracted in case other persons need 
    its functionnalities.
    
    Options may include
    
    *   duration          Period to run in milliseconds, default to 4 minutes.
    *   nb_columns        Number of fields per record
    *   max_word_length   Maximum number of characters per word
    *   start             Start the generation on next tick, otherwise you must call resume
    
    Starting a generation
    
      csv = require 'csv'
      generator = csv.generator
      generator(start: true).pipe csv().to.path "#{__dirname}/perf.out"
     */
    
    Generator = function(options) {
      var _base, _base1;
      this.options = options != null ? options : {};
      if ((_base = this.options).duration == null) {
        _base.duration = 4 * 60 * 1000;
      }
      this.options.nb_columns = 8;
      if ((_base1 = this.options).max_word_length == null) {
        _base1.max_word_length = 16;
      }
      this.start = Date.now();
      this.end = this.start + this.options.duration;
      this.readable = true;
      if (this.options.start) {
        nextTick(this.resume.bind(this));
      }
      return this;
    };
    
    Generator.prototype.__proto__ = Stream.prototype;
    
    Generator.prototype.resume = function() {
      var char, column, line, nb_chars, nb_words, _i, _j, _ref, _ref1;
      this.paused = false;
      while (!this.paused && this.readable) {
        if (Date.now() > this.end) {
          return this.destroy();
        }
        line = [];
        for (nb_words = _i = 0, _ref = this.options.nb_columns; 0 <= _ref ? _i < _ref : _i > _ref; nb_words = 0 <= _ref ? ++_i : --_i) {
          column = [];
          for (nb_chars = _j = 0, _ref1 = Math.ceil(Math.random() * this.options.max_word_length); 0 <= _ref1 ? _j < _ref1 : _j > _ref1; nb_chars = 0 <= _ref1 ? ++_j : --_j) {
            char = Math.floor(Math.random() * 32);
            column.push(String.fromCharCode(char + (char < 16 ? 65 : 97 - 16)));
          }
          line.push(column.join(''));
        }
        this.emit('data', new Buffer("" + (line.join(',')) + "\n", this.options.encoding));
      }
    };
    
    Generator.prototype.pause = function() {
      return this.paused = true;
    };
    
    Generator.prototype.destroy = function() {
      this.readable = false;
      this.emit('end');
      return this.emit('close');
    };
    
    
    /*
    `setEncoding([encoding])`
    
    Makes the 'data' event emit a string instead of a Buffer. 
    encoding can be 'utf8', 'utf16le' ('ucs2'), 'ascii', or 
    'hex'. Defaults to 'utf8'.
     */
    
    Generator.prototype.setEncoding = function(encoding) {
      return this.options.encoding = encoding;
    };
    
    module.exports = function(options) {
      return new Generator(options);
    };
    
    module.exports.Generator = Generator;
    
  provide("csv/lib/generator", module.exports);
}(global));

// pakmanager:csv
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var csv =  require('csv/lib');
    csv.generator =  require('csv/lib/generator');
    
    module.exports = csv;
    
  provide("csv", module.exports);
}(global));

// pakmanager:csvtojson/libs/core/parser.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports=Parser;
    
    function Parser(name,regExp,parser){
        this.name=typeof name == "undefined"?"Default":name;
        this.regExp=null;
        if (typeof regExp !="undefined"){
            if (typeof regExp =="string"){
                this.regExp=new RegExp(regExp);
            }else{
                this.regExp=regExp;    
            }
        }
        if (typeof parser!="undefined"){
            this.parse=parser;
        }
    };
    
    Parser.prototype.test=function(str){
        if (this.regExp==null){
             return true;
        }else{
            return this.regExp.test(str);
        }
    }
    // Parser.prototype.newProcess=function(mixedColumnTitle){
    //     var title=this.getTitle(mixedColumnTitle);
    //     return {
    //         "title"
    //     }
    // }
    // Parser.prototype.getTitle=function(mixedTitle){
    //     return mixedTitle.replace(this.regExp,"");
    // }
    Parser.prototype.parse=function(params){
        params.resultRow[params.head]=params.item;
    }
  provide("csvtojson/libs/core/parser.js", module.exports);
}(global));

// pakmanager:csvtojson/libs/core/parserMgr.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  //module interfaces
    module.exports.addParser=addParser;
    module.exports.initParsers=initParsers;
    module.exports.getParser=getParser;
    //implementation
    var registeredParsers=[];
    var Parser= require('csvtojson/libs/core/parser.js');
    var defaultParsers=[
    {"name":"array", "regExp":/^\*array\*/,"parserFunc":_arrayParser},
    {"name":"json", "regExp":/^\*json\*/,"parserFunc":_jsonParser},
    {"name":"omit", "regExp":/^\*omit\*/,"parserFunc":function(){}},
    {"name":"jsonarray","regExp":/^\*jsonarray\*/,"parserFunc":_jsonArrParser}
    ];
    
    function registerParser(parser){
        if (parser instanceof Parser){
            if (registeredParsers.indexOf(parser)==-1){
                registeredParsers.push(parser);
            }
        }
    }
    
    function addParser(name,regExp,parseFunc){
        var parser=new Parser(name,regExp,parseFunc);
        registerParser(parser);
    }
    
    function initDefaultParsers(){
        for (var i=0;i<defaultParsers.length;i++){
            var parserCfg=defaultParsers[i];
            addParser(parserCfg.name,parserCfg.regExp,parserCfg.parserFunc);
        }
    }
    function initParsers(row){
        var parsers=[];
        for (var i=0;i<row.length;i++){
            var columnTitle=row[i];
            parsers.push(getParser(columnTitle));
        }
        return parsers;
    }
    
    function getParser(columnTitle){
        for (var i=0;i<registeredParsers.length;i++){
            var parser=registeredParsers[i];
            if (parser.test(columnTitle)){
                return parser;
            }
        }
        return new Parser();
    }
    
    //default parsers
    function _arrayParser(params){
        var fieldName=params.head.replace(this.regExp,"");
        if (params.resultRow[fieldName]==undefined){
            params.resultRow[fieldName]=[];
        }
        params.resultRow[fieldName].push(params.item);
    }
    
    function _jsonParser(params){
        var fieldStr=params.head.replace(this.regExp,"");
        var headArr=fieldStr.split(".");
        var pointer=params.resultRow;
        while (headArr.length>1){
            var headStr=headArr.shift();
            if (pointer[headStr]==undefined){
                pointer[headStr]={};
            }
            pointer=pointer[headStr];
        }
        pointer[headArr.shift()]=params.item;
    }
    function _jsonArrParser(params){
        var fieldStr=params.head.replace(this.regExp,"");
        var headArr=fieldStr.split(".");
        var pointer=params.resultRow;
        while (headArr.length>1){
            var headStr=headArr.shift();
            if (pointer[headStr]==undefined){
                pointer[headStr]={};
            }
            pointer=pointer[headStr];
        }
        var arrFieldName=headArr.shift();
        if (pointer[arrFieldName]==undefined){
            pointer[arrFieldName]=[];
        }
        pointer[arrFieldName].push(params.item);
    }
    
    initDefaultParsers();
  provide("csvtojson/libs/core/parserMgr.js", module.exports);
}(global));

// pakmanager:csvtojson/libs/core/csvConverter.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports=csvAdv;
    
    //implementation
    var csv=require("csv");//see http://www.adaltas.com/projects/node-csv/from.html
    var parserMgr= require('csvtojson/libs/core/parserMgr.js');
    var utils=require("util");
    
    //it is a bridge from csv component to our parsers
    function csvAdv(constructResult){
        if (constructResult !== false){
            constructResult=true;
        }
        var instance= csv.apply(this);
    
        this.parseRules=[];
        this.resultObject={csvRows:[]};
        this.headRow=[];
        var that=this;
        instance.on("record",function(row,index){
            if (index ==0){
                that._headRowProcess(row);
            }else{
                var resultRow={};
                that._rowProcess(row,index,resultRow);
                if (constructResult){
                    that.resultObject.csvRows.push(resultRow);
                }
                instance.emit("record_parsed",resultRow,row,index);
            }
        });
    
        instance.on("end",function(){
            instance.emit("end_parsed",that.resultObject,that.resultObject.csvRows);
        });
    
        return instance;
    };
    csvAdv.prototype._headRowProcess=function(headRow){
        this.headRow=headRow;
        this.parseRules=parserMgr.initParsers(headRow);
    }
    csvAdv.prototype._rowProcess=function(row,index,resultRow){
        for (var i=0;i<this.parseRules.length;i++){
            var item=row[i];
            var parser=this.parseRules[i];
            var head=this.headRow[i];
            parser.parse({
                head:head,
                item:item,
                itemIndex:i,
                rawRow:row,
                resultRow:resultRow,
                rowIndex:index,
                resultObject:this.resultObject
            });
        }
    }
    
    
    
  provide("csvtojson/libs/core/csvConverter.js", module.exports);
}(global));

// pakmanager:csvtojson/libs/core
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports.Converter= require('csvtojson/libs/core/csvConverter.js');
    module.exports.Parser= require('csvtojson/libs/core/parser.js');
    module.exports.parserMgr= require('csvtojson/libs/core/parserMgr.js');
  provide("csvtojson/libs/core", module.exports);
}(global));

// pakmanager:csvtojson/libs/interfaces/web/webServer.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  //module interfaces
    module.exports.startWebServer=startWebServer;
    module.exports.applyWebServer=applyWebServer;
    //implementation
    var express=require("express");
    var expressApp=express();
    var CSVConverter= require('csvtojson/libs/core').Converter;
    var defaultArgs={
        "port":"8801",
        "urlPath":"/parseCSV"
    }
    
    function applyWebServer(app,url){
        app.post(url,_POSTData);
    }
    function startWebServer(args){
        if (typeof args=="undefined"){
            args={};
        }
        var serverArgs={};
        for (var key in defaultArgs){
            if (args[key]){
                serverArgs[key]=args[key];
            }else{
                serverArgs[key]=defaultArgs[key];
            }
        }
       //expressApp.use(express.bodyParser());
        expressApp.post(serverArgs.urlPath,_POSTData);
        expressApp.get("/",function(req,res){
            res.end("POST to "+serverArgs.urlPath+" with CSV data to get parsed.");
        });
        expressApp.listen(serverArgs.port);
        console.log("CSV Web Server Listen On:"+serverArgs.port);
        console.log("POST to "+serverArgs.urlPath+" with CSV data to get parsed.");
        return expressApp;
    }
    
    function _POSTData(req,res){
        var csvString="";
        req.setEncoding('utf8');
        req.on("data",function(chunk){
            csvString+=chunk;
        });
        req.on("end",function(){
            _ParseString(csvString,function(err,JSONData){
                if (err){
                    console.error(err)    
                }else{
                   res.json(JSONData);
                }
            });
        });
    }
    
    function _ParseString(csvString,cb){
        var converter=new CSVConverter();
        converter.on("end_parsed",function(JSONData){
            cb(null,JSONData);
        });
        converter.on("error",function(err){
            cb(err);
        });
        converter.from(csvString);
    }
    
    function _ParseFile(filePath,cb){
    
    }
  provide("csvtojson/libs/interfaces/web/webServer.js", module.exports);
}(global));

// pakmanager:csvtojson/libs/interfaces/cli/main.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /**
     * Convert input to process stdout
     */
    
    //module interfaces
    module.exports.convertFile=convertFile;
    module.exports.convertString=convertString;
    //implementation
    var Converter= require('csvtojson/libs/core').Converter;
    function convertFile(fileName){
        var csvConverter=_initConverter();
        csvConverter.from(fileName);
    }
    
    function convertString(csvString){
        var csvConverter=_initConverter();
        csvConverter.from(csvString);
    }
    
    function _initConverter(){
        var csvConverter=new Converter();
        var started=false;
        var writeStream=process.stdout;
       csvConverter.on("record_parsed",function(rowJSON){
            if (started){
                writeStream.write(",\n");
            }
            writeStream.write(JSON.stringify(rowJSON));  //write parsed JSON object one by one.
            if (started==false){
                started=true;
            }
        });
        writeStream.write("[\n"); //write array symbol
    
        csvConverter.on("end_parsed",function(){
            writeStream.write("\n]"); //end array symbol
        });
        csvConverter.on("error",function(err){
            console.error(err);
            process.exit(-1);
        });
        return csvConverter;
    }
  provide("csvtojson/libs/interfaces/cli/main.js", module.exports);
}(global));

// pakmanager:csvtojson/libs/interfaces/web
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports= require('csvtojson/libs/interfaces/web/webServer.js');
  provide("csvtojson/libs/interfaces/web", module.exports);
}(global));

// pakmanager:csvtojson/libs/interfaces/cli
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports= require('csvtojson/libs/interfaces/cli/main.js');
  provide("csvtojson/libs/interfaces/cli", module.exports);
}(global));

// pakmanager:csvtojson/libs/interfaces
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports.web= require('csvtojson/libs/interfaces/web');
    module.exports.cli= require('csvtojson/libs/interfaces/cli');
  provide("csvtojson/libs/interfaces", module.exports);
}(global));

// pakmanager:csvtojson/libs/csv2json.js
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports.core= require('csvtojson/libs/core');
    module.exports.interfaces= require('csvtojson/libs/interfaces');
  provide("csvtojson/libs/csv2json.js", module.exports);
}(global));

// pakmanager:csvtojson
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  module.exports= require('csvtojson/libs/csv2json.js');
  provide("csvtojson", module.exports);
}(global));

// pakmanager:taffydb
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  /*
    
     Software License Agreement (BSD License)
     http://taffydb.com
     Copyright (c)
     All rights reserved.
    
    
     Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following condition is met:
    
     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    
     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
     LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
     */
    
    /*jslint        browser : true, continue : true,
     devel  : true, indent  : 2,    maxerr   : 500,
     newcap : true, nomen   : true, plusplus : true,
     regexp : true, sloppy  : true, vars     : false,
     white  : true
    */
    
    // BUILD 193d48d, modified by mmikowski to pass jslint
    
    // Setup TAFFY name space to return an object with methods
    var TAFFY, exports, T;
    (function () {
      'use strict';
      var
        typeList,     makeTest,     idx,    typeKey,
        version,      TC,           idpad,  cmax,
        API,          protectJSON,  each,   eachin,
        isIndexable,  returnFilter, runFilters,
        numcharsplit, orderByCol,   run,    intersection,
        filter,       makeCid,      safeForJson,
        isRegexp
        ;
    
    
      if ( ! TAFFY ){
        // TC = Counter for Taffy DBs on page, used for unique IDs
        // cmax = size of charnumarray conversion cache
        // idpad = zeros to pad record IDs with
        version = '2.7';
        TC      = 1;
        idpad   = '000000';
        cmax    = 1000;
        API     = {};
    
        protectJSON = function ( t ) {
          // ****************************************
          // *
          // * Takes: a variable
          // * Returns: the variable if object/array or the parsed variable if JSON
          // *
          // ****************************************  
          if ( TAFFY.isArray( t ) || TAFFY.isObject( t ) ){
            return t;
          }
          else {
            return JSON.parse( t );
          }
        };
        
        // gracefully stolen from underscore.js
        intersection = function(array1, array2) {
            return filter(array1, function(item) {
              return array2.indexOf(item) >= 0;
            });
        };
    
        // gracefully stolen from underscore.js
        filter = function(obj, iterator, context) {
            var results = [];
            if (obj == null) return results;
            if (Array.prototype.filter && obj.filter === Array.prototype.filter) return obj.filter(iterator, context);
            each(obj, function(value, index, list) {
              if (iterator.call(context, value, index, list)) results[results.length] = value;
            });
            return results;
        };
        
        isRegexp = function(aObj) {
            return Object.prototype.toString.call(aObj)==='[object RegExp]';
        }
        
        safeForJson = function(aObj) {
            var myResult = T.isArray(aObj) ? [] : T.isObject(aObj) ? {} : null;
            if(aObj===null) return aObj;
            for(var i in aObj) {
                myResult[i]  = isRegexp(aObj[i]) ? aObj[i].toString() : T.isArray(aObj[i]) || T.isObject(aObj[i]) ? safeForJson(aObj[i]) : aObj[i];
            }
            return myResult;
        }
        
        makeCid = function(aContext) {
            var myCid = JSON.stringify(aContext);
            if(myCid.match(/regex/)===null) return myCid;
            return JSON.stringify(safeForJson(aContext));
        }
        
        each = function ( a, fun, u ) {
          var r, i, x, y;
          // ****************************************
          // *
          // * Takes:
          // * a = an object/value or an array of objects/values
          // * f = a function
          // * u = optional flag to describe how to handle undefined values
          //   in array of values. True: pass them to the functions,
          //   False: skip. Default False;
          // * Purpose: Used to loop over arrays
          // *
          // ****************************************  
          if ( a && ((T.isArray( a ) && a.length === 1) || (!T.isArray( a ))) ){
            fun( (T.isArray( a )) ? a[0] : a, 0 );
          }
          else {
            for ( r, i, x = 0, a = (T.isArray( a )) ? a : [a], y = a.length;
                  x < y; x++ )
            {
              i = a[x];
              if ( !T.isUndefined( i ) || (u || false) ){
                r = fun( i, x );
                if ( r === T.EXIT ){
                  break;
                }
    
              }
            }
          }
        };
    
        eachin = function ( o, fun ) {
          // ****************************************
          // *
          // * Takes:
          // * o = an object
          // * f = a function
          // * Purpose: Used to loop over objects
          // *
          // ****************************************  
          var x = 0, r, i;
    
          for ( i in o ){
            if ( o.hasOwnProperty( i ) ){
              r = fun( o[i], i, x++ );
              if ( r === T.EXIT ){
                break;
              }
            }
          }
    
        };
    
        API.extend = function ( m, f ) {
          // ****************************************
          // *
          // * Takes: method name, function
          // * Purpose: Add a custom method to the API
          // *
          // ****************************************  
          API[m] = function () {
            return f.apply( this, arguments );
          };
        };
    
        isIndexable = function ( f ) {
          var i;
          // Check to see if record ID
          if ( T.isString( f ) && /[t][0-9]*[r][0-9]*/i.test( f ) ){
            return true;
          }
          // Check to see if record
          if ( T.isObject( f ) && f.___id && f.___s ){
            return true;
          }
    
          // Check to see if array of indexes
          if ( T.isArray( f ) ){
            i = true;
            each( f, function ( r ) {
              if ( !isIndexable( r ) ){
                i = false;
    
                return TAFFY.EXIT;
              }
            });
            return i;
          }
    
          return false;
        };
    
        runFilters = function ( r, filter ) {
          // ****************************************
          // *
          // * Takes: takes a record and a collection of filters
          // * Returns: true if the record matches, false otherwise
          // ****************************************
          var match = true;
    
    
          each( filter, function ( mf ) {
            switch ( T.typeOf( mf ) ){
              case 'function':
                // run function
                if ( !mf.apply( r ) ){
                  match = false;
                  return TAFFY.EXIT;
                }
                break;
              case 'array':
                // loop array and treat like a SQL or
                match = (mf.length === 1) ? (runFilters( r, mf[0] )) :
                  (mf.length === 2) ? (runFilters( r, mf[0] ) ||
                    runFilters( r, mf[1] )) :
                    (mf.length === 3) ? (runFilters( r, mf[0] ) ||
                      runFilters( r, mf[1] ) || runFilters( r, mf[2] )) :
                      (mf.length === 4) ? (runFilters( r, mf[0] ) ||
                        runFilters( r, mf[1] ) || runFilters( r, mf[2] ) ||
                        runFilters( r, mf[3] )) : false;
                if ( mf.length > 4 ){
                  each( mf, function ( f ) {
                    if ( runFilters( r, f ) ){
                      match = true;
                    }
                  });
                }
                break;
            }
          });
    
          return match;
        };
    
        returnFilter = function ( f ) {
          // ****************************************
          // *
          // * Takes: filter object
          // * Returns: a filter function
          // * Purpose: Take a filter object and return a function that can be used to compare
          // * a TaffyDB record to see if the record matches a query
          // ****************************************  
          var nf = [];
          if ( T.isString( f ) && /[t][0-9]*[r][0-9]*/i.test( f ) ){
            f = { ___id : f };
          }
          if ( T.isArray( f ) ){
            // if we are working with an array
    
            each( f, function ( r ) {
              // loop the array and return a filter func for each value
              nf.push( returnFilter( r ) );
            });
            // now build a func to loop over the filters and return true if ANY of the filters match
            // This handles logical OR expressions
            f = function () {
              var that = this, match = false;
              each( nf, function ( f ) {
                if ( runFilters( that, f ) ){
                  match = true;
                }
              });
              return match;
            };
            return f;
    
          }
          // if we are dealing with an Object
          if ( T.isObject( f ) ){
            if ( T.isObject( f ) && f.___id && f.___s ){
              f = { ___id : f.___id };
            }
    
            // Loop over each value on the object to prep match type and match value
            eachin( f, function ( v, i ) {
    
              // default match type to IS/Equals
              if ( !T.isObject( v ) ){
                v = {
                  'is' : v
                };
              }
              // loop over each value on the value object  - if any
              eachin( v, function ( mtest, s ) {
                // s = match type, e.g. is, hasAll, like, etc
                var c = [], looper;
    
                // function to loop and apply filter
                looper = (s === 'hasAll') ?
                  function ( mtest, func ) {
                    func( mtest );
                  } : each;
    
                // loop over each test
                looper( mtest, function ( mtest ) {
    
                  // su = match success
                  // f = match false
                  var su = true, f = false, matchFunc;
    
    
                  // push a function onto the filter collection to do the matching
                  matchFunc = function () {
    
                    // get the value from the record
                    var
                      mvalue   = this[i],
                      eqeq     = '==',
                      bangeq   = '!=',
                      eqeqeq   = '===',
                      lt   = '<',
                      gt   = '>',
                      lteq   = '<=',
                      gteq   = '>=',
                      bangeqeq = '!==',
                      r
                      ;
    
                    if (typeof mvalue === 'undefined') {
                      return false;
                    }
                    
                    if ( (s.indexOf( '!' ) === 0) && s !== bangeq &&
                      s !== bangeqeq )
                    {
                      // if the filter name starts with ! as in '!is' then reverse the match logic and remove the !
                      su = false;
                      s = s.substring( 1, s.length );
                    }
                    // get the match results based on the s/match type
                    /*jslint eqeq : true */
                    r = (
                      (s === 'regex') ? (mtest.test( mvalue )) : (s === 'lt' || s === lt)
                      ? (mvalue < mtest)  : (s === 'gt' || s === gt)
                      ? (mvalue > mtest)  : (s === 'lte' || s === lteq)
                      ? (mvalue <= mtest) : (s === 'gte' || s === gteq)
                      ? (mvalue >= mtest) : (s === 'left')
                      ? (mvalue.indexOf( mtest ) === 0) : (s === 'leftnocase')
                      ? (mvalue.toLowerCase().indexOf( mtest.toLowerCase() )
                        === 0) : (s === 'right')
                      ? (mvalue.substring( (mvalue.length - mtest.length) )
                        === mtest) : (s === 'rightnocase')
                      ? (mvalue.toLowerCase().substring(
                        (mvalue.length - mtest.length) ) === mtest.toLowerCase())
                        : (s === 'like')
                      ? (mvalue.indexOf( mtest ) >= 0) : (s === 'likenocase')
                      ? (mvalue.toLowerCase().indexOf(mtest.toLowerCase()) >= 0)
                        : (s === eqeqeq || s === 'is')
                      ? (mvalue ===  mtest) : (s === eqeq)
                      ? (mvalue == mtest) : (s === bangeqeq)
                      ? (mvalue !==  mtest) : (s === bangeq)
                      ? (mvalue != mtest) : (s === 'isnocase')
                      ? (mvalue.toLowerCase
                        ? mvalue.toLowerCase() === mtest.toLowerCase()
                          : mvalue === mtest) : (s === 'has')
                      ? (T.has( mvalue, mtest )) : (s === 'hasall')
                      ? (T.hasAll( mvalue, mtest )) : (s === 'contains')
                      ? (TAFFY.isArray(mvalue) && mvalue.indexOf(mtest) > -1) : (
                        s.indexOf( 'is' ) === -1
                          && !TAFFY.isNull( mvalue )
                          && !TAFFY.isUndefined( mvalue )
                          && !TAFFY.isObject( mtest )
                          && !TAFFY.isArray( mtest )
                        )
                      ? (mtest === mvalue[s])
                        : (T[s] && T.isFunction( T[s] )
                        && s.indexOf( 'is' ) === 0)
                      ? T[s]( mvalue ) === mtest
                        : (T[s] && T.isFunction( T[s] ))
                      ? T[s]( mvalue, mtest ) : (false)
                    );
                    /*jslint eqeq : false */
                    r = (r && !su) ? false : (!r && !su) ? true : r;
    
                    return r;
                  };
                  c.push( matchFunc );
    
                });
                // if only one filter in the collection push it onto the filter list without the array
                if ( c.length === 1 ){
    
                  nf.push( c[0] );
                }
                else {
                  // else build a function to loop over all the filters and return true only if ALL match
                  // this is a logical AND
                  nf.push( function () {
                    var that = this, match = false;
                    each( c, function ( f ) {
                      if ( f.apply( that ) ){
                        match = true;
                      }
                    });
                    return match;
                  });
                }
              });
            });
            // finally return a single function that wraps all the other functions and will run a query
            // where all functions have to return true for a record to appear in a query result
            f = function () {
              var that = this, match = true;
              // faster if less than  4 functions
              match = (nf.length === 1 && !nf[0].apply( that )) ? false :
                (nf.length === 2 &&
                  (!nf[0].apply( that ) || !nf[1].apply( that ))) ? false :
                  (nf.length === 3 &&
                    (!nf[0].apply( that ) || !nf[1].apply( that ) ||
                      !nf[2].apply( that ))) ? false :
                    (nf.length === 4 &&
                      (!nf[0].apply( that ) || !nf[1].apply( that ) ||
                        !nf[2].apply( that ) || !nf[3].apply( that ))) ? false
                      : true;
              if ( nf.length > 4 ){
                each( nf, function ( f ) {
                  if ( !runFilters( that, f ) ){
                    match = false;
                  }
                });
              }
              return match;
            };
            return f;
          }
    
          // if function
          if ( T.isFunction( f ) ){
            return f;
          }
        };
    
        orderByCol = function ( ar, o ) {
          // ****************************************
          // *
          // * Takes: takes an array and a sort object
          // * Returns: the array sorted
          // * Purpose: Accept filters such as "[col], [col2]" or "[col] desc" and sort on those columns
          // *
          // ****************************************
    
          var sortFunc = function ( a, b ) {
            // function to pass to the native array.sort to sort an array
            var r = 0;
    
            T.each( o, function ( sd ) {
              // loop over the sort instructions
              // get the column name
              var o, col, dir, c, d;
              o = sd.split( ' ' );
              col = o[0];
    
              // get the direction
              dir = (o.length === 1) ? "logical" : o[1];
    
    
              if ( dir === 'logical' ){
                // if dir is logical than grab the charnum arrays for the two values we are looking at
                c = numcharsplit( a[col] );
                d = numcharsplit( b[col] );
                // loop over the charnumarrays until one value is higher than the other
                T.each( (c.length <= d.length) ? c : d, function ( x, i ) {
                  if ( c[i] < d[i] ){
                    r = -1;
                    return TAFFY.EXIT;
                  }
                  else if ( c[i] > d[i] ){
                    r = 1;
                    return TAFFY.EXIT;
                  }
                } );
              }
              else if ( dir === 'logicaldesc' ){
                // if logicaldesc than grab the charnum arrays for the two values we are looking at
                c = numcharsplit( a[col] );
                d = numcharsplit( b[col] );
                // loop over the charnumarrays until one value is lower than the other
                T.each( (c.length <= d.length) ? c : d, function ( x, i ) {
                  if ( c[i] > d[i] ){
                    r = -1;
                    return TAFFY.EXIT;
                  }
                  else if ( c[i] < d[i] ){
                    r = 1;
                    return TAFFY.EXIT;
                  }
                } );
              }
              else if ( dir === 'asec' && a[col] < b[col] ){
                // if asec - default - check to see which is higher
                r = -1;
                return T.EXIT;
              }
              else if ( dir === 'asec' && a[col] > b[col] ){
                // if asec - default - check to see which is higher
                r = 1;
                return T.EXIT;
              }
              else if ( dir === 'desc' && a[col] > b[col] ){
                // if desc check to see which is lower
                r = -1;
                return T.EXIT;
    
              }
              else if ( dir === 'desc' && a[col] < b[col] ){
                // if desc check to see which is lower
                r = 1;
                return T.EXIT;
    
              }
              // if r is still 0 and we are doing a logical sort than look to see if one array is longer than the other
              if ( r === 0 && dir === 'logical' && c.length < d.length ){
                r = -1;
              }
              else if ( r === 0 && dir === 'logical' && c.length > d.length ){
                r = 1;
              }
              else if ( r === 0 && dir === 'logicaldesc' && c.length > d.length ){
                r = -1;
              }
              else if ( r === 0 && dir === 'logicaldesc' && c.length < d.length ){
                r = 1;
              }
    
              if ( r !== 0 ){
                return T.EXIT;
              }
    
    
            } );
            return r;
          };
          // call the sort function and return the newly sorted array
          return (ar && ar.push) ? ar.sort( sortFunc ) : ar;
    
    
        };
    
        // ****************************************
        // *
        // * Takes: a string containing numbers and letters and turn it into an array
        // * Returns: return an array of numbers and letters
        // * Purpose: Used for logical sorting. String Example: 12ABC results: [12,'ABC']
        // **************************************** 
        (function () {
          // creates a cache for numchar conversions
          var cache = {}, cachcounter = 0;
          // creates the numcharsplit function
          numcharsplit = function ( thing ) {
            // if over 1000 items exist in the cache, clear it and start over
            if ( cachcounter > cmax ){
              cache = {};
              cachcounter = 0;
            }
    
            // if a cache can be found for a numchar then return its array value
            return cache['_' + thing] || (function () {
              // otherwise do the conversion
              // make sure it is a string and setup so other variables
              var nthing = String( thing ),
                na = [],
                rv = '_',
                rt = '',
                x, xx, c;
    
              // loop over the string char by char
              for ( x = 0, xx = nthing.length; x < xx; x++ ){
                // take the char at each location
                c = nthing.charCodeAt( x );
                // check to see if it is a valid number char and append it to the array.
                // if last char was a string push the string to the charnum array
                if ( ( c >= 48 && c <= 57 ) || c === 46 ){
                  if ( rt !== 'n' ){
                    rt = 'n';
                    na.push( rv.toLowerCase() );
                    rv = '';
                  }
                  rv = rv + nthing.charAt( x );
                }
                else {
                  // check to see if it is a valid string char and append to string
                  // if last char was a number push the whole number to the charnum array
                  if ( rt !== 's' ){
                    rt = 's';
                    na.push( parseFloat( rv ) );
                    rv = '';
                  }
                  rv = rv + nthing.charAt( x );
                }
              }
              // once done, push the last value to the charnum array and remove the first uneeded item
              na.push( (rt === 'n') ? parseFloat( rv ) : rv.toLowerCase() );
              na.shift();
              // add to cache
              cache['_' + thing] = na;
              cachcounter++;
              // return charnum array
              return na;
            }());
          };
        }());
    
        // ****************************************
        // *
        // * Runs a query
        // **************************************** 
    
    
        run = function () {
          this.context( {
            results : this.getDBI().query( this.context() )
          });
    
        };
    
        API.extend( 'filter', function () {
          // ****************************************
          // *
          // * Takes: takes unlimited filter objects as arguments
          // * Returns: method collection
          // * Purpose: Take filters as objects and cache functions for later lookup when a query is run
          // **************************************** 
          var
            nc = TAFFY.mergeObj( this.context(), { run : null } ),
            nq = []
          ;
          each( nc.q, function ( v ) {
            nq.push( v );
          });
          nc.q = nq;
          // Hadnle passing of ___ID or a record on lookup.
          each( arguments, function ( f ) {
            nc.q.push( returnFilter( f ) );
            nc.filterRaw.push( f );
          });
    
          return this.getroot( nc );
        });
    
        API.extend( 'order', function ( o ) {
          // ****************************************
          // *
          // * Purpose: takes a string and creates an array of order instructions to be used with a query
          // ****************************************
    
          o = o.split( ',' );
          var x = [], nc;
    
          each( o, function ( r ) {
            x.push( r.replace( /^\s*/, '' ).replace( /\s*$/, '' ) );
          });
    
          nc = TAFFY.mergeObj( this.context(), {sort : null} );
          nc.order = x;
    
          return this.getroot( nc );
        });
    
        API.extend( 'limit', function ( n ) {
          // ****************************************
          // *
          // * Purpose: takes a limit number to limit the number of rows returned by a query. Will update the results
          // * of a query
          // **************************************** 
          var nc = TAFFY.mergeObj( this.context(), {}),
            limitedresults
            ;
    
          nc.limit = n;
    
          if ( nc.run && nc.sort ){
            limitedresults = [];
            each( nc.results, function ( i, x ) {
              if ( (x + 1) > n ){
                return TAFFY.EXIT;
              }
              limitedresults.push( i );
            });
            nc.results = limitedresults;
          }
    
          return this.getroot( nc );
        });
    
        API.extend( 'start', function ( n ) {
          // ****************************************
          // *
          // * Purpose: takes a limit number to limit the number of rows returned by a query. Will update the results
          // * of a query
          // **************************************** 
          var nc = TAFFY.mergeObj( this.context(), {} ),
            limitedresults
            ;
    
          nc.start = n;
    
          if ( nc.run && nc.sort && !nc.limit ){
            limitedresults = [];
            each( nc.results, function ( i, x ) {
              if ( (x + 1) > n ){
                limitedresults.push( i );
              }
            });
            nc.results = limitedresults;
          }
          else {
            nc = TAFFY.mergeObj( this.context(), {run : null, start : n} );
          }
    
          return this.getroot( nc );
        });
    
        API.extend( 'update', function ( arg0, arg1, arg2 ) {
          // ****************************************
          // *
          // * Takes: a object and passes it off DBI update method for all matched records
          // **************************************** 
          var runEvent = true, o = {}, args = arguments, that;
          if ( TAFFY.isString( arg0 ) &&
            (arguments.length === 2 || arguments.length === 3) )
          {
            o[arg0] = arg1;
            if ( arguments.length === 3 ){
              runEvent = arg2;
            }
          }
          else {
            o = arg0;
            if ( args.length === 2 ){
              runEvent = arg1;
            }
          }
    
          that = this;
          run.call( this );
          each( this.context().results, function ( r ) {
            var c = o;
            if ( TAFFY.isFunction( c ) ){
              c = c.apply( TAFFY.mergeObj( r, {} ) );
            }
            else {
              if ( T.isFunction( c ) ){
                c = c( TAFFY.mergeObj( r, {} ) );
              }
            }
            if ( TAFFY.isObject( c ) ){
              that.getDBI().update( r.___id, c, runEvent );
            }
          });
          if ( this.context().results.length ){
            this.context( { run : null });
          }
          return this;
        });
        API.extend( 'remove', function ( runEvent ) {
          // ****************************************
          // *
          // * Purpose: removes records from the DB via the remove and removeCommit DBI methods
          // **************************************** 
          var that = this, c = 0;
          run.call( this );
          each( this.context().results, function ( r ) {
            that.getDBI().remove( r.___id );
            c++;
          });
          if ( this.context().results.length ){
            this.context( {
              run : null
            });
            that.getDBI().removeCommit( runEvent );
          }
    
          return c;
        });
    
    
        API.extend( 'count', function () {
          // ****************************************
          // *
          // * Returns: The length of a query result
          // **************************************** 
          run.call( this );
          return this.context().results.length;
        });
    
        API.extend( 'callback', function ( f, delay ) {
          // ****************************************
          // *
          // * Returns null;
          // * Runs a function on return of run.call
          // **************************************** 
          if ( f ){
            var that = this;
            setTimeout( function () {
              run.call( that );
              f.call( that.getroot( that.context() ) );
            }, delay || 0 );
          }
    
    
          return null;
        });
    
        API.extend( 'get', function () {
          // ****************************************
          // *
          // * Returns: An array of all matching records
          // **************************************** 
          run.call( this );
          return this.context().results;
        });
    
        API.extend( 'stringify', function () {
          // ****************************************
          // *
          // * Returns: An JSON string of all matching records
          // **************************************** 
          return JSON.stringify( this.get() );
        });
        API.extend( 'first', function () {
          // ****************************************
          // *
          // * Returns: The first matching record
          // **************************************** 
          run.call( this );
          return this.context().results[0] || false;
        });
        API.extend( 'last', function () {
          // ****************************************
          // *
          // * Returns: The last matching record
          // **************************************** 
          run.call( this );
          return this.context().results[this.context().results.length - 1] ||
            false;
        });
    
    
        API.extend( 'sum', function () {
          // ****************************************
          // *
          // * Takes: column to sum up
          // * Returns: Sums the values of a column
          // **************************************** 
          var total = 0, that = this;
          run.call( that );
          each( arguments, function ( c ) {
            each( that.context().results, function ( r ) {
              total = total + (r[c] || 0);
            });
          });
          return total;
        });
    
        API.extend( 'min', function ( c ) {
          // ****************************************
          // *
          // * Takes: column to find min
          // * Returns: the lowest value
          // **************************************** 
          var lowest = null;
          run.call( this );
          each( this.context().results, function ( r ) {
            if ( lowest === null || r[c] < lowest ){
              lowest = r[c];
            }
          });
          return lowest;
        });
    
        //  Taffy innerJoin Extension (OCD edition)
        //  =======================================
        //
        //  How to Use
        //  **********
        //
        //  left_table.innerJoin( right_table, condition1 <,... conditionN> )
        //
        //  A condition can take one of 2 forms:
        //
        //    1. An ARRAY with 2 or 3 values:
        //    A column name from the left table, an optional comparison string,
        //    and column name from the right table.  The condition passes if the test
        //    indicated is true.   If the condition string is omitted, '===' is assumed.
        //    EXAMPLES: [ 'last_used_time', '>=', 'current_use_time' ], [ 'user_id','id' ]
        //
        //    2. A FUNCTION:
        //    The function receives a left table row and right table row during the
        //    cartesian join.  If the function returns true for the rows considered,
        //    the merged row is included in the result set.
        //    EXAMPLE: function (l,r){ return l.name === r.label; }
        //
        //  Conditions are considered in the order they are presented.  Therefore the best
        //  performance is realized when the least expensive and highest prune-rate
        //  conditions are placed first, since if they return false Taffy skips any
        //  further condition tests.
        //
        //  Other notes
        //  ***********
        //
        //  This code passes jslint with the exception of 2 warnings about
        //  the '==' and '!=' lines.  We can't do anything about that short of
        //  deleting the lines.
        //
        //  Credits
        //  *******
        //
        //  Heavily based upon the work of Ian Toltz.
        //  Revisions to API by Michael Mikowski.
        //  Code convention per standards in http://manning.com/mikowski
        (function () {
          var innerJoinFunction = (function () {
            var fnCompareList, fnCombineRow, fnMain;
    
            fnCompareList = function ( left_row, right_row, arg_list ) {
              var data_lt, data_rt, op_code, error;
    
              if ( arg_list.length === 2 ){
                data_lt = left_row[arg_list[0]];
                op_code = '===';
                data_rt = right_row[arg_list[1]];
              }
              else {
                data_lt = left_row[arg_list[0]];
                op_code = arg_list[1];
                data_rt = right_row[arg_list[2]];
              }
    
              /*jslint eqeq : true */
              switch ( op_code ){
                case '===' :
                  return data_lt === data_rt;
                case '!==' :
                  return data_lt !== data_rt;
                case '<'   :
                  return data_lt < data_rt;
                case '>'   :
                  return data_lt > data_rt;
                case '<='  :
                  return data_lt <= data_rt;
                case '>='  :
                  return data_lt >= data_rt;
                case '=='  :
                  return data_lt == data_rt;
                case '!='  :
                  return data_lt != data_rt;
                default :
                  throw String( op_code ) + ' is not supported';
              }
              // 'jslint eqeq : false'  here results in
              // "Unreachable '/*jslint' after 'return'".
              // We don't need it though, as the rule exception
              // is discarded at the end of this functional scope
            };
    
            fnCombineRow = function ( left_row, right_row ) {
              var out_map = {}, i, prefix;
    
              for ( i in left_row ){
                if ( left_row.hasOwnProperty( i ) ){
                  out_map[i] = left_row[i];
                }
              }
              for ( i in right_row ){
                if ( right_row.hasOwnProperty( i ) && i !== '___id' &&
                  i !== '___s' )
                {
                  prefix = !TAFFY.isUndefined( out_map[i] ) ? 'right_' : '';
                  out_map[prefix + String( i ) ] = right_row[i];
                }
              }
              return out_map;
            };
    
            fnMain = function ( table ) {
              var
                right_table, i,
                arg_list = arguments,
                arg_length = arg_list.length,
                result_list = []
                ;
    
              if ( typeof table.filter !== 'function' ){
                if ( table.TAFFY ){ right_table = table(); }
                else {
                  throw 'TAFFY DB or result not supplied';
                }
              }
              else { right_table = table; }
    
              this.context( {
                results : this.getDBI().query( this.context() )
              } );
    
              TAFFY.each( this.context().results, function ( left_row ) {
                right_table.each( function ( right_row ) {
                  var arg_data, is_ok = true;
                  CONDITION:
                    for ( i = 1; i < arg_length; i++ ){
                      arg_data = arg_list[i];
                      if ( typeof arg_data === 'function' ){
                        is_ok = arg_data( left_row, right_row );
                      }
                      else if ( typeof arg_data === 'object' && arg_data.length ){
                        is_ok = fnCompareList( left_row, right_row, arg_data );
                      }
                      else {
                        is_ok = false;
                      }
    
                      if ( !is_ok ){ break CONDITION; } // short circuit
                    }
    
                  if ( is_ok ){
                    result_list.push( fnCombineRow( left_row, right_row ) );
                  }
                } );
              } );
              return TAFFY( result_list )();
            };
    
            return fnMain;
          }());
    
          API.extend( 'join', innerJoinFunction );
        }());
    
        API.extend( 'max', function ( c ) {
          // ****************************************
          // *
          // * Takes: column to find max
          // * Returns: the highest value
          // ****************************************
          var highest = null;
          run.call( this );
          each( this.context().results, function ( r ) {
            if ( highest === null || r[c] > highest ){
              highest = r[c];
            }
          });
          return highest;
        });
    
        API.extend( 'select', function () {
          // ****************************************
          // *
          // * Takes: columns to select values into an array
          // * Returns: array of values
          // * Note if more than one column is given an array of arrays is returned
          // **************************************** 
    
          var ra = [], args = arguments;
          run.call( this );
          if ( arguments.length === 1 ){
    
            each( this.context().results, function ( r ) {
    
              ra.push( r[args[0]] );
            });
          }
          else {
            each( this.context().results, function ( r ) {
              var row = [];
              each( args, function ( c ) {
                row.push( r[c] );
              });
              ra.push( row );
            });
          }
          return ra;
        });
        API.extend( 'distinct', function () {
          // ****************************************
          // *
          // * Takes: columns to select unique alues into an array
          // * Returns: array of values
          // * Note if more than one column is given an array of arrays is returned
          // **************************************** 
          var ra = [], args = arguments;
          run.call( this );
          if ( arguments.length === 1 ){
    
            each( this.context().results, function ( r ) {
              var v = r[args[0]], dup = false;
              each( ra, function ( d ) {
                if ( v === d ){
                  dup = true;
                  return TAFFY.EXIT;
                }
              });
              if ( !dup ){
                ra.push( v );
              }
            });
          }
          else {
            each( this.context().results, function ( r ) {
              var row = [], dup = false;
              each( args, function ( c ) {
                row.push( r[c] );
              });
              each( ra, function ( d ) {
                var ldup = true;
                each( args, function ( c, i ) {
                  if ( row[i] !== d[i] ){
                    ldup = false;
                    return TAFFY.EXIT;
                  }
                });
                if ( ldup ){
                  dup = true;
                  return TAFFY.EXIT;
                }
              });
              if ( !dup ){
                ra.push( row );
              }
            });
          }
          return ra;
        });
        API.extend( 'supplant', function ( template, returnarray ) {
          // ****************************************
          // *
          // * Takes: a string template formated with key to be replaced with values from the rows, flag to determine if we want array of strings
          // * Returns: array of values or a string
          // **************************************** 
          var ra = [];
          run.call( this );
          each( this.context().results, function ( r ) {
            // TODO: The curly braces used to be unescaped
            ra.push( template.replace( /\{([^\{\}]*)\}/g, function ( a, b ) {
              var v = r[b];
              return typeof v === 'string' || typeof v === 'number' ? v : a;
            } ) );
          });
          return (!returnarray) ? ra.join( "" ) : ra;
        });
    
    
        API.extend( 'each', function ( m ) {
          // ****************************************
          // *
          // * Takes: a function
          // * Purpose: loops over every matching record and applies the function
          // **************************************** 
          run.call( this );
          each( this.context().results, m );
          return this;
        });
        API.extend( 'map', function ( m ) {
          // ****************************************
          // *
          // * Takes: a function
          // * Purpose: loops over every matching record and applies the function, returing the results in an array
          // **************************************** 
          var ra = [];
          run.call( this );
          each( this.context().results, function ( r ) {
            ra.push( m( r ) );
          });
          return ra;
        });
    
    
    
        T = function ( d ) {
          // ****************************************
          // *
          // * T is the main TAFFY object
          // * Takes: an array of objects or JSON
          // * Returns a new TAFFYDB
          // **************************************** 
          var TOb = [],
            ID = {},
            RC = 1,
            settings = {
              template          : false,
              onInsert          : false,
              onUpdate          : false,
              onRemove          : false,
              onDBChange        : false,
              storageName       : false,
              forcePropertyCase : null,
              cacheSize         : 100,
              name              : ''
            },
            dm = new Date(),
            CacheCount = 0,
            CacheClear = 0,
            Cache = {},
            DBI, runIndexes, root
            ;
          // ****************************************
          // *
          // * TOb = this database
          // * ID = collection of the record IDs and locations within the DB, used for fast lookups
          // * RC = record counter, used for creating IDs
          // * settings.template = the template to merge all new records with
          // * settings.onInsert = event given a copy of the newly inserted record
          // * settings.onUpdate = event given the original record, the changes, and the new record
          // * settings.onRemove = event given the removed record
          // * settings.forcePropertyCase = on insert force the proprty case to be lower or upper. default lower, null/undefined will leave case as is
          // * dm = the modify date of the database, used for query caching
          // **************************************** 
    
    
          runIndexes = function ( indexes ) {
            // ****************************************
            // *
            // * Takes: a collection of indexes
            // * Returns: collection with records matching indexed filters
            // **************************************** 
    
            var records = [], UniqueEnforce = false;
    
            if ( indexes.length === 0 ){
              return TOb;
            }
    
            each( indexes, function ( f ) {
              // Check to see if record ID
              if ( T.isString( f ) && /[t][0-9]*[r][0-9]*/i.test( f ) &&
                TOb[ID[f]] )
              {
                records.push( TOb[ID[f]] );
                UniqueEnforce = true;
              }
              // Check to see if record
              if ( T.isObject( f ) && f.___id && f.___s &&
                TOb[ID[f.___id]] )
              {
                records.push( TOb[ID[f.___id]] );
                UniqueEnforce = true;
              }
              // Check to see if array of indexes
              if ( T.isArray( f ) ){
                each( f, function ( r ) {
                  each( runIndexes( r ), function ( rr ) {
                    records.push( rr );
                  });
    
                });
              }
            });
            if ( UniqueEnforce && records.length > 1 ){
              records = [];
            }
    
            return records;
          };
    
          DBI = {
            // ****************************************
            // *
            // * The DBI is the internal DataBase Interface that interacts with the data
            // **************************************** 
            dm           : function ( nd ) {
              // ****************************************
              // *
              // * Takes: an optional new modify date
              // * Purpose: used to get and set the DB modify date
              // **************************************** 
              if ( nd ){
                dm = nd;
                Cache = {};
                CacheCount = 0;
                CacheClear = 0;
              }
              if ( settings.onDBChange ){
                setTimeout( function () {
                  settings.onDBChange.call( TOb );
                }, 0 );
              }
              if ( settings.storageName ){
                setTimeout( function () {
                  localStorage.setItem( 'taffy_' + settings.storageName,
                    JSON.stringify( TOb ) );
                });
              }
              return dm;
            },
            insert       : function ( i, runEvent ) {
              // ****************************************
              // *
              // * Takes: a new record to insert
              // * Purpose: merge the object with the template, add an ID, insert into DB, call insert event
              // **************************************** 
              var columns = [],
                records   = [],
                input     = protectJSON( i )
                ;
              each( input, function ( v, i ) {
                var nv, o;
                if ( T.isArray( v ) && i === 0 ){
                  each( v, function ( av ) {
    
                    columns.push( (settings.forcePropertyCase === 'lower')
                      ? av.toLowerCase()
                        : (settings.forcePropertyCase === 'upper')
                      ? av.toUpperCase() : av );
                  });
                  return true;
                }
                else if ( T.isArray( v ) ){
                  nv = {};
                  each( v, function ( av, ai ) {
                    nv[columns[ai]] = av;
                  });
                  v = nv;
    
                }
                else if ( T.isObject( v ) && settings.forcePropertyCase ){
                  o = {};
    
                  eachin( v, function ( av, ai ) {
                    o[(settings.forcePropertyCase === 'lower') ? ai.toLowerCase()
                      : (settings.forcePropertyCase === 'upper')
                      ? ai.toUpperCase() : ai] = v[ai];
                  });
                  v = o;
                }
    
                RC++;
                v.___id = 'T' + String( idpad + TC ).slice( -6 ) + 'R' +
                  String( idpad + RC ).slice( -6 );
                v.___s = true;
                records.push( v.___id );
                if ( settings.template ){
                  v = T.mergeObj( settings.template, v );
                }
                TOb.push( v );
    
                ID[v.___id] = TOb.length - 1;
                if ( settings.onInsert &&
                  (runEvent || TAFFY.isUndefined( runEvent )) )
                {
                  settings.onInsert.call( v );
                }
                DBI.dm( new Date() );
              });
              return root( records );
            },
            sort         : function ( o ) {
              // ****************************************
              // *
              // * Purpose: Change the sort order of the DB itself and reset the ID bucket
              // **************************************** 
              TOb = orderByCol( TOb, o.split( ',' ) );
              ID = {};
              each( TOb, function ( r, i ) {
                ID[r.___id] = i;
              });
              DBI.dm( new Date() );
              return true;
            },
            update       : function ( id, changes, runEvent ) {
              // ****************************************
              // *
              // * Takes: the ID of record being changed and the changes
              // * Purpose: Update a record and change some or all values, call the on update method
              // ****************************************
    
              var nc = {}, or, nr, tc, hasChange;
              if ( settings.forcePropertyCase ){
                eachin( changes, function ( v, p ) {
                  nc[(settings.forcePropertyCase === 'lower') ? p.toLowerCase()
                    : (settings.forcePropertyCase === 'upper') ? p.toUpperCase()
                    : p] = v;
                });
                changes = nc;
              }
    
              or = TOb[ID[id]];
              nr = T.mergeObj( or, changes );
    
              tc = {};
              hasChange = false;
              eachin( nr, function ( v, i ) {
                if ( TAFFY.isUndefined( or[i] ) || or[i] !== v ){
                  tc[i] = v;
                  hasChange = true;
                }
              });
              if ( hasChange ){
                if ( settings.onUpdate &&
                  (runEvent || TAFFY.isUndefined( runEvent )) )
                {
                  settings.onUpdate.call( nr, TOb[ID[id]], tc );
                }
                TOb[ID[id]] = nr;
                DBI.dm( new Date() );
              }
            },
            remove       : function ( id ) {
              // ****************************************
              // *
              // * Takes: the ID of record to be removed
              // * Purpose: remove a record, changes its ___s value to false
              // **************************************** 
              TOb[ID[id]].___s = false;
            },
            removeCommit : function ( runEvent ) {
              var x;
              // ****************************************
              // *
              // * 
              // * Purpose: loop over all records and remove records with ___s = false, call onRemove event, clear ID
              // ****************************************
              for ( x = TOb.length - 1; x > -1; x-- ){
    
                if ( !TOb[x].___s ){
                  if ( settings.onRemove &&
                    (runEvent || TAFFY.isUndefined( runEvent )) )
                  {
                    settings.onRemove.call( TOb[x] );
                  }
                  ID[TOb[x].___id] = undefined;
                  TOb.splice( x, 1 );
                }
              }
              ID = {};
              each( TOb, function ( r, i ) {
                ID[r.___id] = i;
              });
              DBI.dm( new Date() );
            },
            query : function ( context ) {
              // ****************************************
              // *
              // * Takes: the context object for a query and either returns a cache result or a new query result
              // **************************************** 
              var returnq, cid, results, indexed, limitq, ni;
    
              if ( settings.cacheSize ) {
                cid = '';
                each( context.filterRaw, function ( r ) {
                  if ( T.isFunction( r ) ){
                    cid = 'nocache';
                    return TAFFY.EXIT;
                  }
                });
                if ( cid === '' ){
                  cid = makeCid( T.mergeObj( context,
                    {q : false, run : false, sort : false} ) );
                }
              }
              // Run a new query if there are no results or the run date has been cleared
              if ( !context.results || !context.run ||
                (context.run && DBI.dm() > context.run) )
              {
                results = [];
    
                // check Cache
    
                if ( settings.cacheSize && Cache[cid] ){
    
                  Cache[cid].i = CacheCount++;
                  return Cache[cid].results;
                }
                else {
                  // if no filter, return DB
                  if ( context.q.length === 0 && context.index.length === 0 ){
                    each( TOb, function ( r ) {
                      results.push( r );
                    });
                    returnq = results;
                  }
                  else {
                    // use indexes
    
                    indexed = runIndexes( context.index );
    
                    // run filters
                    each( indexed, function ( r ) {
                      // Run filter to see if record matches query
                      if ( context.q.length === 0 || runFilters( r, context.q ) ){
                        results.push( r );
                      }
                    });
    
                    returnq = results;
                  }
                }
    
    
              }
              else {
                // If query exists and run has not been cleared return the cache results
                returnq = context.results;
              }
              // If a custom order array exists and the run has been clear or the sort has been cleared
              if ( context.order.length > 0 && (!context.run || !context.sort) ){
                // order the results
                returnq = orderByCol( returnq, context.order );
              }
    
              // If a limit on the number of results exists and it is less than the returned results, limit results
              if ( returnq.length &&
                ((context.limit && context.limit < returnq.length) ||
                  context.start)
              ) {
                limitq = [];
                each( returnq, function ( r, i ) {
                  if ( !context.start ||
                    (context.start && (i + 1) >= context.start) )
                  {
                    if ( context.limit ){
                      ni = (context.start) ? (i + 1) - context.start : i;
                      if ( ni < context.limit ){
                        limitq.push( r );
                      }
                      else if ( ni > context.limit ){
                        return TAFFY.EXIT;
                      }
                    }
                    else {
                      limitq.push( r );
                    }
                  }
                });
                returnq = limitq;
              }
    
              // update cache
              if ( settings.cacheSize && cid !== 'nocache' ){
                CacheClear++;
    
                setTimeout( function () {
                  var bCounter, nc;
                  if ( CacheClear >= settings.cacheSize * 2 ){
                    CacheClear = 0;
                    bCounter = CacheCount - settings.cacheSize;
                    nc = {};
                    eachin( function ( r, k ) {
                      if ( r.i >= bCounter ){
                        nc[k] = r;
                      }
                    });
                    Cache = nc;
                  }
                }, 0 );
    
                Cache[cid] = { i : CacheCount++, results : returnq };
              }
              return returnq;
            }
          };
    
    
          root = function () {
            var iAPI, context;
            // ****************************************
            // *
            // * The root function that gets returned when a new DB is created
            // * Takes: unlimited filter arguments and creates filters to be run when a query is called
            // **************************************** 
            // ****************************************
            // *
            // * iAPI is the the method collection valiable when a query has been started by calling dbname
            // * Certain methods are or are not avaliable once you have started a query such as insert -- you can only insert into root
            // ****************************************
            iAPI = TAFFY.mergeObj( TAFFY.mergeObj( API, { insert : undefined } ),
              { getDBI  : function () { return DBI; },
                getroot : function ( c ) { return root.call( c ); },
              context : function ( n ) {
                // ****************************************
                // *
                // * The context contains all the information to manage a query including filters, limits, and sorts
                // **************************************** 
                if ( n ){
                  context = TAFFY.mergeObj( context,
                    n.hasOwnProperty('results')
                      ? TAFFY.mergeObj( n, { run : new Date(), sort: new Date() })
                      : n
                  );
                }
                return context;
              },
              extend  : undefined
            });
    
            context = (this && this.q) ? this : {
              limit     : false,
              start     : false,
              q         : [],
              filterRaw : [],
              index     : [],
              order     : [],
              results   : false,
              run       : null,
              sort      : null,
              settings  : settings
            };
            // ****************************************
            // *
            // * Call the query method to setup a new query
            // **************************************** 
            each( arguments, function ( f ) {
    
              if ( isIndexable( f ) ){
                context.index.push( f );
              }
              else {
                context.q.push( returnFilter( f ) );
              }
              context.filterRaw.push( f );
            });
    
    
            return iAPI;
          };
    
          // ****************************************
          // *
          // * If new records have been passed on creation of the DB either as JSON or as an array/object, insert them
          // **************************************** 
          TC++;
          if ( d ){
            DBI.insert( d );
          }
    
    
          root.insert = DBI.insert;
    
          root.merge = function ( i, key, runEvent ) {
            var
              search      = {},
              finalSearch = [],
              obj         = {}
              ;
    
            runEvent    = runEvent || false;
            key         = key      || 'id';
    
            each( i, function ( o ) {
              var existingObject;
              search[key] = o[key];
              finalSearch.push( o[key] );
              existingObject = root( search ).first();
              if ( existingObject ){
                DBI.update( existingObject.___id, o, runEvent );
              }
              else {
                DBI.insert( o, runEvent );
              }
            });
    
            obj[key] = finalSearch;
            return root( obj );
          };
    
          root.TAFFY = true;
          root.sort = DBI.sort;
          // ****************************************
          // *
          // * These are the methods that can be accessed on off the root DB function. Example dbname.insert;
          // **************************************** 
          root.settings = function ( n ) {
            // ****************************************
            // *
            // * Getting and setting for this DB's settings/events
            // **************************************** 
            if ( n ){
              settings = TAFFY.mergeObj( settings, n );
              if ( n.template ){
    
                root().update( n.template );
              }
            }
            return settings;
          };
    
          // ****************************************
          // *
          // * These are the methods that can be accessed on off the root DB function. Example dbname.insert;
          // **************************************** 
          root.store = function ( n ) {
            // ****************************************
            // *
            // * Setup localstorage for this DB on a given name
            // * Pull data into the DB as needed
            // **************************************** 
            var r = false, i;
            if ( localStorage ){
              if ( n ){
                i = localStorage.getItem( 'taffy_' + n );
                if ( i && i.length > 0 ){
                  root.insert( i );
                  r = true;
                }
                if ( TOb.length > 0 ){
                  setTimeout( function () {
                    localStorage.setItem( 'taffy_' + settings.storageName,
                      JSON.stringify( TOb ) );
                  });
                }
              }
              root.settings( {storageName : n} );
            }
            return root;
          };
    
          // ****************************************
          // *
          // * Return root on DB creation and start having fun
          // **************************************** 
          return root;
        };
        // ****************************************
        // *
        // * Sets the global TAFFY object
        // **************************************** 
        TAFFY = T;
    
    
        // ****************************************
        // *
        // * Create public each method
        // *
        // ****************************************   
        T.each = each;
    
        // ****************************************
        // *
        // * Create public eachin method
        // *
        // ****************************************   
        T.eachin = eachin;
        // ****************************************
        // *
        // * Create public extend method
        // * Add a custom method to the API
        // *
        // ****************************************   
        T.extend = API.extend;
    
    
        // ****************************************
        // *
        // * Creates TAFFY.EXIT value that can be returned to stop an each loop
        // *
        // ****************************************  
        TAFFY.EXIT = 'TAFFYEXIT';
    
        // ****************************************
        // *
        // * Create public utility mergeObj method
        // * Return a new object where items from obj2
        // * have replaced or been added to the items in
        // * obj1
        // * Purpose: Used to combine objs
        // *
        // ****************************************   
        TAFFY.mergeObj = function ( ob1, ob2 ) {
          var c = {};
          eachin( ob1, function ( v, n ) { c[n] = ob1[n]; });
          eachin( ob2, function ( v, n ) { c[n] = ob2[n]; });
          return c;
        };
    
    
        // ****************************************
        // *
        // * Create public utility has method
        // * Returns true if a complex object, array
        // * or taffy collection contains the material
        // * provided in the second argument
        // * Purpose: Used to comare objects
        // *
        // ****************************************
        TAFFY.has = function ( var1, var2 ) {
    
          var re = false, n;
    
          if ( (var1.TAFFY) ){
            re = var1( var2 );
            if ( re.length > 0 ){
              return true;
            }
            else {
              return false;
            }
          }
          else {
    
            switch ( T.typeOf( var1 ) ){
              case 'object':
                if ( T.isObject( var2 ) ){
                  eachin( var2, function ( v, n ) {
                    if ( re === true && !T.isUndefined( var1[n] ) &&
                      var1.hasOwnProperty( n ) )
                    {
                      re = T.has( var1[n], var2[n] );
                    }
                    else {
                      re = false;
                      return TAFFY.EXIT;
                    }
                  });
                }
                else if ( T.isArray( var2 ) ){
                  each( var2, function ( v, n ) {
                    re = T.has( var1, var2[n] );
                    if ( re ){
                      return TAFFY.EXIT;
                    }
                  });
                }
                else if ( T.isString( var2 ) ){
                  if ( !TAFFY.isUndefined( var1[var2] ) ){
                    return true;
                  }
                  else {
                    return false;
                  }
                }
                return re;
              case 'array':
                if ( T.isObject( var2 ) ){
                  each( var1, function ( v, i ) {
                    re = T.has( var1[i], var2 );
                    if ( re === true ){
                      return TAFFY.EXIT;
                    }
                  });
                }
                else if ( T.isArray( var2 ) ){
                  each( var2, function ( v2, i2 ) {
                    each( var1, function ( v1, i1 ) {
                      re = T.has( var1[i1], var2[i2] );
                      if ( re === true ){
                        return TAFFY.EXIT;
                      }
                    });
                    if ( re === true ){
                      return TAFFY.EXIT;
                    }
                  });
                }
                else if ( T.isString( var2 ) || T.isNumber( var2 ) ){
                 re = false;
                  for ( n = 0; n < var1.length; n++ ){
                    re = T.has( var1[n], var2 );
                    if ( re ){
                      return true;
                    }
                  }
                }
                return re;
              case 'string':
                if ( T.isString( var2 ) && var2 === var1 ){
                  return true;
                }
                break;
              default:
                if ( T.typeOf( var1 ) === T.typeOf( var2 ) && var1 === var2 ){
                  return true;
                }
                break;
            }
          }
          return false;
        };
    
        // ****************************************
        // *
        // * Create public utility hasAll method
        // * Returns true if a complex object, array
        // * or taffy collection contains the material
        // * provided in the call - for arrays it must
        // * contain all the material in each array item
        // * Purpose: Used to comare objects
        // *
        // ****************************************
        TAFFY.hasAll = function ( var1, var2 ) {
    
          var T = TAFFY, ar;
          if ( T.isArray( var2 ) ){
            ar = true;
            each( var2, function ( v ) {
              ar = T.has( var1, v );
              if ( ar === false ){
                return TAFFY.EXIT;
              }
            });
            return ar;
          }
          else {
            return T.has( var1, var2 );
          }
        };
    
    
        // ****************************************
        // *
        // * typeOf Fixed in JavaScript as public utility
        // *
        // ****************************************
        TAFFY.typeOf = function ( v ) {
          var s = typeof v;
          if ( s === 'object' ){
            if ( v ){
              if ( typeof v.length === 'number' &&
                !(v.propertyIsEnumerable( 'length' )) )
              {
                s = 'array';
              }
            }
            else {
              s = 'null';
            }
          }
          return s;
        };
    
        // ****************************************
        // *
        // * Create public utility getObjectKeys method
        // * Returns an array of an objects keys
        // * Purpose: Used to get the keys for an object
        // *
        // ****************************************   
        TAFFY.getObjectKeys = function ( ob ) {
          var kA = [];
          eachin( ob, function ( n, h ) {
            kA.push( h );
          });
          kA.sort();
          return kA;
        };
    
        // ****************************************
        // *
        // * Create public utility isSameArray
        // * Returns an array of an objects keys
        // * Purpose: Used to get the keys for an object
        // *
        // ****************************************   
        TAFFY.isSameArray = function ( ar1, ar2 ) {
          return (TAFFY.isArray( ar1 ) && TAFFY.isArray( ar2 ) &&
            ar1.join( ',' ) === ar2.join( ',' )) ? true : false;
        };
    
        // ****************************************
        // *
        // * Create public utility isSameObject method
        // * Returns true if objects contain the same
        // * material or false if they do not
        // * Purpose: Used to comare objects
        // *
        // ****************************************   
        TAFFY.isSameObject = function ( ob1, ob2 ) {
          var T = TAFFY, rv = true;
    
          if ( T.isObject( ob1 ) && T.isObject( ob2 ) ){
            if ( T.isSameArray( T.getObjectKeys( ob1 ),
              T.getObjectKeys( ob2 ) ) )
            {
              eachin( ob1, function ( v, n ) {
                if ( ! ( (T.isObject( ob1[n] ) && T.isObject( ob2[n] ) &&
                  T.isSameObject( ob1[n], ob2[n] )) ||
                  (T.isArray( ob1[n] ) && T.isArray( ob2[n] ) &&
                    T.isSameArray( ob1[n], ob2[n] )) || (ob1[n] === ob2[n]) )
                ) {
                  rv = false;
                  return TAFFY.EXIT;
                }
              });
            }
            else {
              rv = false;
            }
          }
          else {
            rv = false;
          }
          return rv;
        };
    
        // ****************************************
        // *
        // * Create public utility is[DataType] methods
        // * Return true if obj is datatype, false otherwise
        // * Purpose: Used to determine if arguments are of certain data type
        // *
        // * mmikowski 2012-08-06 refactored to make much less "magical":
        // *   fewer closures and passes jslint
        // *
        // ****************************************
    
        typeList = [
          'String',  'Number', 'Object',   'Array',
          'Boolean', 'Null',   'Function', 'Undefined'
        ];
      
        makeTest = function ( thisKey ) {
          return function ( data ) {
            return TAFFY.typeOf( data ) === thisKey.toLowerCase() ? true : false;
          };
        };
      
        for ( idx = 0; idx < typeList.length; idx++ ){
          typeKey = typeList[idx];
          TAFFY['is' + typeKey] = makeTest( typeKey );
        }
      }
    }());
    
    if ( typeof(exports) === 'object' ){
      exports.taffy = TAFFY;
    }
    
    
  provide("taffydb", module.exports);
}(global));

// pakmanager:csvServer
(function (context) {
  
  var module = { exports: {} }, exports = module.exports
    , $ = require("ender")
    ;
  
  var express = require('express');
    var app = express();
    var TAFFY = require("taffydb").taffy;
    var fs = require("fs");
    
    app.get('/streams/:stream', function(req, res) {
    
      var Converter = require("csvtojson").core.Converter;
      var csvConverter = new Converter();
    
      var path = "data/" + req.params.stream + ".csv";
      if (!fs.existsSync(path)) {
        res.send(404, "The dataset you are looking for cannot be found.");
      }
      csvConverter.from(path);
    
      csvConverter.on("end_parsed", function(jsonObj) {
        try {
    
          var result = jsonObj.csvRows;
          if( req.query.q){
            var db = TAFFY(result);
             result = db(JSON.parse(req.query.q.toString())).get();
          }
    
          res.json(result);
        } catch (ex) {
          res.send(500, "The query could not be executed.");
        }
      });
    });
    
    
    var server = app.listen(3000, function() {
      console.log('Listening on port %d', server.address().port);
    });
    
  provide("csvServer", module.exports);
}(global));