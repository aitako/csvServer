#! /usr/bin/env node

var express = require('express');
var app = express();
var TAFFY = require("taffydb").taffy;
var fs = require("fs");


if (process.argv.length < 4) {
  console.error('\n\nUsage: csvserver [datapath] [port]');
  console.error('The data path should point to where your csv files are stored.\n\n');
  return;
}

var settings = {
  dataPath: process.argv[2],
  port: process.argv[3]
};

// Enable CORS so that we can actually use the server for something useful.
app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
 });


app.get('/streams/:stream', function(req, res) {

  var Converter = require("csvtojson").core.Converter;
  var csvConverter = new Converter();

  var path = settings.dataPath + "/" + req.params.stream + ".csv";
  if (!fs.existsSync(path)) {
    res.send(404, "The dataset you are looking for cannot be found.");
  }
  csvConverter.from(path);

  csvConverter.on("end_parsed", function(jsonObj) {
    try {

      var result = jsonObj.csvRows;
      if (req.query.q) {
        var db = TAFFY(result);
        result = db(JSON.parse(req.query.q.toString())).get();
      }

      res.json(result);
    } catch (ex) {
      res.send(500, "The query could not be executed.");
    }
  });
});


var server = app.listen(settings.port, function() {
  console.log('Listening on port %d', server.address().port);
});
